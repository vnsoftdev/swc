<?php
/**
 * @package        MijoVideos
 * @copyright      2009-2014 Miwisoft LLC, miwisoft.com
 * @license        GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined('_JEXEC') or die ('Restricted access');

jimport('joomla.plugin.plugin');
require_once(JPATH_ADMINISTRATOR.'/components/com_mijovideos/library/mijovideos.php');

class plgMijovideosJwPlayer extends JPlugin {

	public    $width;
	public    $height;
	public    $pluginParams;
	public    $output;
	protected $item = null;

	public function __construct(&$subject, $config) {
		parent::__construct($subject, $config);
		$this->config = MijoVideos::getConfig();
	}

	public function getPlayer(&$output, $pluginParams, $item) {
		if (strpos($output, '{mijovideos ') === false) {
			return false;
		}

		$document = JFactory::getDocument();

		if (MijoVideos::getInput()->getCmd('view') == 'video' and MijoVideos::getInput()->getInt('playlist_id', 0) > 0) {
			$document->addStyleSheet(JUri::root() . 'components/com_mijovideos/assets/css/playlist_jwplayer.css');
		}

		// Autoplay
		switch ($autoplay = $pluginParams->get('autoplay', 'global')) {
			case "global":
				if ($this->config->get('autoplay') == 1) {
					$autoplay = true;
				}
				else {
					$autoplay = false;
				}
				break;
			case "1":
				$autoplay = true;
				break;
			case "0":
				$autoplay = false;
		}

		if ($pluginParams->get('id')) {
			$image = ($pluginParams->get('jpg') ? $pluginParams->get('jpg') : 'http://i1.ytimg.com/vi/'.$pluginParams->get('id').'/maxresdefault.jpg');
			$output .= ' align=[center] provider=[youtube] file=[http://www.youtube.com/v/'.$pluginParams->get('id').'] image=['.$image.'] width=[%100] height=[%100]}';
		}
		else {
			$image = ($pluginParams->get('jpg') ? $pluginParams->get('jpg') : '');
			$output .= ' align=[center] provider=[video] width=[%100] height=[%100] autoplay=['.$autoplay.'] image=['.$image.']}';
		}

		$this->output       = $output;
		$this->pluginParams = $pluginParams;
		$this->item         = $item;

		$output  = preg_replace_callback('#{mijovideos\s*(.*?)}#s', array(&$this, '_processMatches'), $output);
		$content = '';

		$document->addStyleDeclaration('#mijovideos-container .media-respond { max-width:'.$this->config->get('mediaitem_size', '500').'px!important;}');

		ob_start();
		echo '<div class="media-respond">';
		echo '<div class="media-aspect" data-aspect="'.$this->config->get('video_aspect', '0.75').'"></div>';
		echo $content;
		echo '</div>';
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}

	protected function _processMatches(&$matches) {
		$result = null;
		static $id = 1;
		$videoParams     = $matches[1];
		$videoParamsList = $this->getParams($videoParams);
		$html            = $this->getHtmlOutput($id, $videoParamsList);

		if (isset($id)) {
			$id++;
		}

		$pattern = str_replace('[', '\[', $matches[0]);
		$pattern = str_replace(']', '\]', $pattern);
		$pattern = str_replace('/', '\/', $pattern);
		$pattern = str_replace('|', '\|', $pattern);

		$output = preg_replace('/'.$pattern.'/', $html, $this->output, 1);

		return $output;
	}

	protected function getParams($videoParams) {
		$pluginParams                         = $this->pluginParams;
		$videoParamsList['width']             = $pluginParams->get('width');
		$videoParamsList['height']            = $pluginParams->get('height');
		$videoParamsList['controls']          = $pluginParams->get('controls');
		$videoParamsList['autoplay']          = $pluginParams->get('autoplay');
		$videoParamsList['preload']           = $pluginParams->get('preload');
		$videoParamsList['loop']              = $pluginParams->get('loop');
		$videoParamsList['poster_visibility'] = $pluginParams->get('poster_visibility');
		$videoParamsList['video_mp4']         = '';
		$videoParamsList['video_webm']        = '';
		$videoParamsList['video_ogg']         = '';
		$videoParamsList['poster']            = '';
		$videoParamsList['text_track']        = '';

		$items = explode(' ', $videoParams);

		foreach ($items as $item) {
			if ($item != '') {
				$item  = explode('=', $item);
				$name  = $item[0];
				$value = strtr($item[1], array('[' => '', ']' => ''));
				if ($name == "text_track") {
					$videoParamsList[ $name ][] = $value;
				}
				else {
					$videoParamsList[ $name ] = $value;
				}
			}
		}

		return $videoParamsList;
	}

	public function getHtmlOutput($id, &$videoParamsList) {

		$align = (($videoParamsList['align'] == 'center') ? 'margin: 0 auto;' : 'float:'.$videoParamsList['align'].';');

		// Check if the width is an integer value of something else - i.e. percentage
		$width = (is_int($this->config->get('width', 640)) ? $this->config->get('width', 640).'px' : $this->config->get('width', 640));

		$output = '';
		$output .= (($this->pluginParams->get('wrap', '0') == '0') ? '<div style="clear:both;"></div>' : '');
		$output .= '<div class="media-content" style="width:'.$width.';'.$align.'">';
		$output .= $this->_load($id, $videoParamsList);
		$output .= '</div>';
		$output .= (($this->pluginParams->get('wrap', '0') == '0') ? '<div style="clear:both;"></div>' : '');

		return $output;
	}

	protected function _load($id, $videoParamsList) {
		$utility = MijoVideos::get('utility');
		if (!$videoParamsList['provider']) {
			return '<!-- Failed media plugin: '.$this->pluginParams.'-->';
		}

		$document = JFactory::getDocument();
		$default_size = $utility->getVideoSize(MIJOVIDEOS_UPLOAD_DIR.'/videos/'.$this->item->id.'/orig/'.$this->item->source);
		$files = MijoVideos::get('files')->getVideoFiles($this->item->id);
		$video_mp4 = $video_webm = $video_ogg = $video_flv = $default_res = '';

		if ($this->config->get('video_quality') == $default_size) {
			$default_res = 'true';
		}

		foreach ($files as $file) {

			if (!$this->item->duration) {
				$orig = "{ 'file': '" . JUri::root(true) . "/media/com_mijovideos/videos/" . $this->item->id . "/orig/" . $file->source . "', type: '".$this->item->ext."'}";
			}

			if ($file->process_type == '200' or $file->process_type < 7) continue;
			$size = $utility->getSize($file->process_type);

			if ($this->config->get('video_quality') == $size) {
				$default_res = 'true';
			}

			$src = $utility->getVideoFilePath($file->video_id, $size, $file->source, 'url');

			if ($file->ext == 'mp4' and $file->process_type == '100') {
				$src = $utility->getVideoFilePath($file->video_id, $default_size, $videoParamsList['video_mp4'], 'url');
				$video_mp4 .= "{ 'file': '" . $src . "', type: 'video/mp4', label: '" . $default_size . "p', \"default\": '".$default_res."'},\n";
			} else if ($file->ext == 'mp4') {
				$video_mp4 .= "{ 'file': '" . $src . "', type: 'video/mp4', label: '" . $size . "p', \"default\": '".$default_res."'},\n";

			}

			if ($file->ext == 'webm' and $file->process_type == '100') {
				$src = $utility->getVideoFilePath($file->video_id, $default_size, $videoParamsList['video_webm'], 'url');
				$video_webm .= "{ 'file': '" . $src . "', type: 'video/webm', label: '" . $default_size . "p', \"default\": '".$default_res."'},\n";
			} else if ($file->ext == 'webm') {
				$video_webm .= "{ 'file': '" . $src . "', type: 'video/webm', label: '" . $size . "p', \"default\": '".$default_res."'},\n";

			}

			if (($file->ext == 'ogg' or $file->ext == 'ogv') and $file->process_type == '100') {
				$src = $utility->getVideoFilePath($file->video_id, $default_size, $videoParamsList['video_ogg'], 'url');
				$video_ogg .= "{ 'file': '" . $src . "', type: 'video/ogg', label: '" . $default_size . "p', \"default\": '".$default_res."'},\n";
			} else if ($file->ext == 'ogg' or $file->ext == 'ogv') {
				$video_ogg .= "{ 'file': '" . $src . "', type: 'video/ogg', label: '" . $size . "p', \"default\": '".$default_res."'},\n";
			}

			if ($file->ext == 'flv') {
				$video_flv .= "{ 'file': '" . $src . "', type: 'video/flash', label: '" . $size . "p', \"default\": '".$default_res."'},\n";
			}
			$default_res = '';
		}

		// Version 6
		if ($this->pluginParams->get('version', 'version6') == 'version6') {
			$document->addScript(JURI::root(true).'/plugins/mijovideos/jwplayer/assets/6/jwplayer.js');
			if ($this->pluginParams->get('licensekey')) {
				$document->addScriptDeclaration('jwplayer.key="'.$this->pluginParams->get('licensekey').'";');
			}
			ob_start();
			?>
			<div id='mediaspace<?php echo $id; ?>'>This text will be replaced</div>
			<script type='text/javascript'>
				jwplayer('mediaspace<?php echo $id; ?>').setup({
					'width': '<?php echo $this->pluginParams->get('width', '640'); ?>',
					'height': '<?php echo $this->pluginParams->get('height', '480'); ?>',
					'controls': '<?php echo $this->pluginParams->get('controls', 'true'); ?>',
					<?php if ($this->pluginParams->get('skin6') == 'custom'):
					echo ($this->pluginParams->get('customskin') ? "'skin': '".$this->pluginParams->get('customskin')."'," : '');
					elseif ($this->pluginParams->get('skin6')):
					echo ($this->pluginParams->get('skin6') ? "'skin': '".$this->pluginParams->get('skin6')."'," : '');
					endif; ?>
					'aspectratio': '<?php echo $this->pluginParams->get('aspectratio', '16:9'); ?>',
					<?php echo ($videoParamsList['autoplay'] ? "'autostart': '".$videoParamsList['autoplay']."'," : ''); ?>
					'fallback': 'true',
					'mute': '<?php echo ($this->pluginParams->get('mute') ? 'true' : 'false'); ?>',
					'primary': '<?php echo (($this->config->get('fallback')) ? 'flash' : 'html5'); ?>',
					'repeat': '<?php echo ($this->pluginParams->get('repeat') == 'none' ? 'false' : 'true'); ?>',
					'stretching': '<?php echo $this->pluginParams->get('stretching', 'uniform'); ?>',
					'flashplayer': '<?php echo JURI::root( true ).'/plugins/mijovideos/jwplayer/assets/6/jwplayer.flash.swf'; ?>',
					'html5player': '<?php echo JURI::root( true ).'/plugins/mijovideos/jwplayer/assets/6/jwplayer.html5.js'; ?>',
					'logo': {
						'file': '<?php echo $this->pluginParams->get('logofile'); ?>',
						'link': '<?php echo $this->pluginParams->get('logolink'); ?>',
						'hide': '<?php echo $this->pluginParams->get('logohide'); ?>',
						'margin': '<?php echo $this->pluginParams->get('logomargin'); ?>',
						'position': '<?php echo $this->pluginParams->get('logoposition'); ?>'
					},
					'abouttext': '<?php echo $this->pluginParams->get('abouttext', 'MijoVideos'); ?>',
					'aboutlink': '<?php echo $this->pluginParams->get('aboutlink', 'http://miwisoft.com/'); ?>',
					<?php if ($this->pluginParams->get('plugfacebook') == 1 || $this->pluginParams->get('plugtwitter') == 1 || $this->pluginParams->get('plugviral') == 1) : ?>
					'sharing': {},
					<?php endif; ?>
					<?php if ($this->pluginParams->get('videoadsclient') == 1) : ?>
					advertising: {
						client: '<?php echo $this->pluginParams->get('videoadsclient', 'vast'); ?>',
						tag: '<?php echo $this->pluginParams->get('videoadstag', ''); ?>'
					},
					<?php endif; ?>
					<?php if ($videoParamsList['provider'] == "playlist") { ?>
					'provider': 'http',
					'playlist': '<?php echo $videoParamsList['file']; ?>',
					'primary': 'flash'
					<?php } elseif ($videoParamsList['provider'] == "youtube") { ?>
					'provider': 'youtube',
					'file': '<?php echo $videoParamsList['file']; ?>',
					'image': '<?php echo $videoParamsList['image']; ?>',
					<?php } else { ?>
					'image': '<?php echo $this->pluginParams->get('image'); ?>',
					'sources': [
						<?php echo (!$this->item->duration) ? $orig : ''; ?>
						<?php echo !empty($video_mp4) ? $video_mp4 : ''; ?>
						<?php echo !empty($video_webm) ? $video_webm : ''; ?>
						<?php echo !empty($video_ogg) ? $video_ogg : ''; ?>
						<?php echo !empty($video_flv) ? $video_flv : ''; ?>
					],
					<?php } ?>
				});
			</script>
			<?php
			$retval = ob_get_contents();
			ob_end_clean();
		}
		// Version 5
		else {
			$document->addScript(JURI::root(true).'/plugins/mijovideos/jwplayer/assets/jwplayer.js');
			$id     = rand();
			$player = ($this->pluginParams->get('plugviral', '0') == 1 ? 'player-viral.swf' : 'player.swf');
			ob_start();
			?>
			<div id='mediaspace<?php echo $id; ?>'>This text will be replaced</div>
			<script type='text/javascript'>
				jwplayer('mediaspace<?php echo $id; ?>').setup({
					'flashplayer': '<?php echo JURI::root( true ); ?>/plugins/mijovideos/jwplayer/assets/<?php echo $player; ?>',
					'width': '<?php echo $this->pluginParams->get('width', '640'); ?>',
					'height': '<?php echo $this->pluginParams->get('height', '480'); ?>',
					'controlbar.position': '<?php echo $this->pluginParams->get('controlbarposition', 'over'); ?>',
					'controlbar.idlehide': <?php echo $this->pluginParams->get('controlbaridlehide', 'false'); ?>,
					'display.showmute': <?php echo $this->pluginParams->get('displayshowmute', 'false'); ?>,
					'dock': <?php echo ($this->pluginParams->get('dock', 'true') == 1 ? 'true' : 'false'); ?>,
					'icons': <?php echo ($this->pluginParams->get('icons', 'true') == 1 ? 'true' : 'false'); ?>,
					<?php if ($this->pluginParams->get('skin') == 'custom') {
					echo ($this->pluginParams->get('customskin') ? "'skin': '".$this->pluginParams->get('customskin')."'," : '');
					} elseif ($this->pluginParams->get('skin')) {
					echo ($this->pluginParams->get('skin') ? "'skin': '".JURI::root( true )."/plugins/mijovideos/jwplayer/assets/skins/".$this->pluginParams->get('skin')."/".$this->pluginParams->get('skin').".zip'," : '');
					} ?>
					<?php echo ($videoParamsList['autoplay'] ? "'autostart': '".$videoParamsList['autoplay']."'," : ''); ?>
					'bufferlength': <?php echo $this->pluginParams->get('bufferlength', 1); ?>,
					'mute': <?php echo $this->pluginParams->get('mute', 'false'); ?>,
					<?php echo ($this->pluginParams->get('skin') ? "'playerready': '".$this->pluginParams->get('playerready')."'," : ''); ?>
					'repeat': '<?php echo $this->pluginParams->get('repeat', 'none'); ?>',
					'shuffle': <?php echo $this->pluginParams->get('shuffle', 'false'); ?>,
					'smoothing': <?php echo $this->pluginParams->get('smoothing', 'true'); ?>,
					'stretching': '<?php echo $this->pluginParams->get('stretching', 'uniform'); ?>',
					'volume': <?php echo $this->pluginParams->get('volume', '90'); ?>,
					'logo.file': '<?php echo $this->pluginParams->get('logofile'); ?>',
					'logo.link': '<?php echo $this->pluginParams->get('logolink'); ?>',
					'logo.linktarget': '<?php echo $this->pluginParams->get('logolinktarget'); ?>',
					'logo.hide': '<?php echo $this->pluginParams->get('logohide'); ?>',
					'logo.margin': '<?php echo $this->pluginParams->get('logomargin'); ?>',
					'logo.position': '<?php echo $this->pluginParams->get('logoposition'); ?>',
					'logo.timeout': '<?php echo $this->pluginParams->get('logotimeout'); ?>',
					'logo.over': '<?php echo $this->pluginParams->get('logoover'); ?>',
					'logo.out': '<?php echo $this->pluginParams->get('logoout'); ?>',
					<?php if ($this->pluginParams->get('provider') == "playlist") { ?>
					'provider': 'http',
					'playlistfile': '<?php echo $this->pluginParams->get('file'); ?>',
					'playlist.position': '<?php echo $this->pluginParams->get('playlistposition', 'none'); ?>',
					'playlist.size': '<?php echo $this->pluginParams->get('playlistsize', 180); ?>',
					<?php } elseif ($videoParamsList['provider'] == "youtube") { ?>
					'provider': 'youtube',
					'image': '<?php echo $videoParamsList['image']; ?>',
					'file': '<?php echo $videoParamsList['file']; ?>',
					<?php } else { ?>
					'image': '<?php echo $this->pluginParams->get('image'); ?>',
					'provider': 'http',
					'levels': [
						<?php echo (!$this->item->duration) ? $orig : ''; ?>
						<?php echo !empty($video_mp4) ? $video_mp4 : ''; ?>
						<?php echo !empty($video_webm) ? $video_webm : ''; ?>
						<?php echo !empty($video_ogg) ? $video_ogg : ''; ?>
						<?php echo !empty($video_flv) ? $video_flv : ''; ?>
					],
					<?php } ?>
					'modes': [
						<?php if ($this->config->get('fallback')) { ?>
						{
							'type': 'flash',
							src: '<?php echo JURI::root( true ); ?>/plugins/mijovideos/jwplayer/assets/<?php echo $player; ?>'
						},
						{'type': 'html5'}
						<?php } else { ?>
						{'type': 'html5'},
						{
							'type': 'flash',
							src: '<?php echo JURI::root( true ); ?>/plugins/mijovideos/jwplayer/assets/<?php echo $player; ?>'
						}
						<?php } ?>
					],
					'plugins': {
						'': '' // This is a dummy plugin to allow us more easily to insert correct syntax
						<?php if ($this->pluginParams->get('plugfacebook') == 1 && (!is_int($this->pluginParams->get('width', 640)) || $this->pluginParams->get('width', 640) > 200) && (!is_int($this->pluginParams->get('height', 300)) || $this->pluginParams->get('height', 300) > 200)) { ?>,
						'fbit-1': {}
						<?php } ?>
						<?php if ($this->pluginParams->get('plugtwitter') == 1 && (!is_int($this->pluginParams->get('width', 640)) || $this->pluginParams->get('width', 640) > 200) && (!is_int($this->pluginParams->get('height', 300)) || $this->pluginParams->get('height', 300) > 200)) { ?>,
						'tweetit-1': {}
						<?php } ?>
						<?php if ($this->pluginParams->get('plughd') == 1 && (!is_int($this->pluginParams->get('width', 640)) || $this->pluginParams->get('width', 640) > 200)) { ?>,
						'hd-2': {
							<?php echo ($this->pluginParams->get('hdfile')       ? "'file': '".$this->pluginParams->get('hdfile')."',\n" : ""); ?>
							<?php echo ($this->pluginParams->get('hdstate')      ? "'state': true,\n" : "'state': false,\n"); ?>
							<?php echo ($this->pluginParams->get('hdfullscreen') ? "'fullscreen': true,\n" : "'fullscreen': false,\n"); ?>
						}
						<?php } ?>
						<?php if ($this->pluginParams->get('plugltas') == 1) { ?>,
						'ltas': {
							<?php echo ($this->pluginParams->get('ltas_channel')? "'cc': '".$this->pluginParams->get('ltas_channel')."'\n" : '' ); ?>
						}
						<?php } ?>
						<?php if ($this->pluginParams->get('pluginvideous') == 1) { ?>,
						'http://plugin.invideous.com/v5/invideous.swf': {
							<?php echo ($this->pluginParams->get('invideouspid')? "'pid': '".$this->pluginParams->get('invideouspid')."'\n" : '' ); ?>
						}
						<?php } ?>
					}
				});
			</script>
			<?php
			$retval = ob_get_contents();
			ob_end_clean();
		}

		return $retval;
	}
}
