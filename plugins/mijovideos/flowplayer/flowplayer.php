<?php
/**
 * @package        MijoVideos
 * @copyright      2009-2014 Miwisoft LLC, miwisoft.com
 * @license        GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined('_JEXEC') or die ('Restricted access');

jimport('joomla.plugin.plugin');
require_once(JPATH_ADMINISTRATOR.'/components/com_mijovideos/library/mijovideos.php');

class plgMijovideosFlowPlayer extends JPlugin {

	public    $width;
	public    $height;
	public    $pluginParams;
	public    $output;
	protected $item = null;

	public function __construct(&$subject, $config) {
		parent::__construct($subject, $config);
		$this->config = MijoVideos::getConfig();
	}

	public function getPlayer(&$output, $pluginParams, $item) {
		if (strpos($output, '{mijovideos ') === false) {
			return false;
		}

		$document = JFactory::getDocument();

		if (MijoVideos::getInput()->getCmd('view') == 'video' and MijoVideos::getInput()->getInt('playlist_id', 0) > 0) {
			$document->addStyleSheet(JUri::root().'components/com_mijovideos/assets/css/playlist_flowplayer.css');
		}

		// Autoplay
		switch ($autoplay = $pluginParams->get('autoplay', 'global')) {
			case "global":
				if ($this->config->get('autoplay') == 1) {
					$autoplay = true;
				}
				else {
					$autoplay = false;
				}
				break;
			case "1":
				$autoplay = true;
				break;
			case "0":
				$autoplay = false;
		}

		if ($pluginParams->get('id')) {
			$image = ($pluginParams->get('jpg') ? $pluginParams->get('jpg') : 'http://i1.ytimg.com/vi/'.$pluginParams->get('id').'/maxresdefault.jpg');
			$output .= ' align=[center] provider=[youtube] file=[http://www.youtube.com/v/'.$pluginParams->get('id').'] image=['.$image.'] width=[%100] height=[%100]}';
		}
		else {
			$image = MijoVideos::get('utility')->getThumbPath($item->id, 'videos', $item->thumb);
			$output .= ' align=[center] provider=[video] width=[%100] height=[%100] autoplay=['.$autoplay.'] image=['.$image.']}';
		}

		$this->output       = $output;
		$this->pluginParams = $pluginParams;
		$this->item         = $item;

		$output = preg_replace_callback('#{mijovideos\s*(.*?)}#s', array(&$this, '_processMatches'), $output);

		ob_start();
		$html = ob_get_contents();
		ob_end_clean();
		return $html;
	}

	protected function _processMatches(&$matches) {
		$result = null;
		static $id = 1;
		$videoParams     = $matches[1];
		$videoParamsList = $this->getParams($videoParams);
		$html            = $this->getHtmlOutput($id, $videoParamsList);

		if (isset($id)) {
			$id++;
		}

		$pattern = str_replace('[', '\[', $matches[0]);
		$pattern = str_replace(']', '\]', $pattern);
		$pattern = str_replace('/', '\/', $pattern);
		$pattern = str_replace('|', '\|', $pattern);

		$output = preg_replace('/'.$pattern.'/', $html, $this->output, 1);

		return $output;
	}

	protected function getParams($videoParams) {
		$pluginParams                         = $this->pluginParams;
		$videoParamsList['width']             = $pluginParams->get('width');
		$videoParamsList['height']            = $pluginParams->get('height');
		$videoParamsList['controls']          = $pluginParams->get('controls');
		$videoParamsList['autoplay']          = $pluginParams->get('autoplay');
		$videoParamsList['preload']           = $pluginParams->get('preload');
		$videoParamsList['loop']              = $pluginParams->get('loop');
		$videoParamsList['poster_visibility'] = $pluginParams->get('poster_visibility');
		$videoParamsList['video_mp4']         = '';
		$videoParamsList['video_webm']        = '';
		$videoParamsList['video_ogg']         = '';
		$videoParamsList['poster']            = '';
		$videoParamsList['text_track']        = '';

		$items = explode(' ', $videoParams);

		foreach ($items as $item) {
			if ($item != '') {
				$item  = explode('=', $item);
				$name  = $item[0];
				$value = strtr($item[1], array('[' => '', ']' => ''));
				if ($name == "text_track") {
					$videoParamsList[ $name ][] = $value;
				}
				else {
					$videoParamsList[ $name ] = $value;
				}
			}
		}

		return $videoParamsList;
	}

	public function getHtmlOutput($id, &$videoParamsList) {

		$align = (($videoParamsList['align'] == 'center') ? 'margin: 0 auto;' : 'float:'.$videoParamsList['align'].';');

		// Check if the width is an integer value of something else - i.e. percentage
		$width = (is_int($this->config->get('width', 640)) ? $this->config->get('width', 640).'px' : $this->config->get('width', 640));

		$output = '';
		$output .= (($this->pluginParams->get('wrap', '0') == '0') ? '<div style="clear:both;"></div>' : '');
		$output .= '<div class="media-content" style="width:'.$width.';'.$align.'">';
		$output .= $this->_load($id, $videoParamsList);
		$output .= '</div>';
		$output .= (($this->pluginParams->get('wrap', '0') == '0') ? '<div style="clear:both;"></div>' : '');

		return $output;
	}

	protected function _load($id, $videoParamsList) {
		if (!$videoParamsList['provider']) {
			return '<!-- Failed media plugin: '.$this->config.'-->';
		}

		$utility  = MijoVideos::get('utility');
		$document = JFactory::getDocument();

		ob_start();
		if ($videoParamsList['provider'] == 'video') {
			$document->addScript(JUri::root().'plugins/mijovideos/flowplayer/assets/flowplayer.js');
			$document->addStyleDeclaration('.flowplayer .fp-logo {display: block;opacity: 1;}');
			if ($this->pluginParams->get('skin') == 'custom') {
				$document->addStyleSheet($this->pluginParams->get('customskin'));
			}
			else {
				$document->addStyleSheet(JUri::root().'plugins/mijovideos/flowplayer/assets/skin/'.$this->pluginParams->get('skin', 'minimalist').'.css');
			}

			$files        = MijoVideos::get('files')->getVideoFiles($this->item->id);
			$default_size = $utility->getVideoSize(MIJOVIDEOS_UPLOAD_DIR.'/videos/'.$this->item->id.'/orig/'.$this->item->source);
			$video_mp4    = $video_webm = $video_ogg = $video_flv = $default_res = '';

			foreach ($files as $file) {

				if (!$this->item->duration) {
					$orig = '<source src="'.JUri::root(true).'/media/com_mijovideos/videos/'.$this->item->id.'/orig/'.$file->source.'" type="video/'.$file->ext.'"/>';;
				}

				if ($file->process_type == '200' or $file->process_type < 7)
					continue;
				$size = $utility->getSize($file->process_type);
				if ($this->config->get('video_quality') == $size) {
					$default_res = 'true';
				}

				$src = $utility->getVideoFilePath($file->video_id, $size, $file->source, 'url');

				if ($file->ext == 'mp4' and $file->process_type == '100') {
					$src = $utility->getVideoFilePath($file->video_id, $default_size, $videoParamsList['video_mp4'], 'url');
					$video_mp4 .= '<source src="'.$src.'" type="video/mp4" data-res="'.$default_size.'p" data-default="'.$default_res.'" />';
				}
				else if ($file->ext == 'mp4') {
					$video_mp4 .= '<source src="'.$src.'" type="video/mp4" data-res="'.$size.'p" data-default="'.$default_res.'" />';
				}

				if ($file->ext == 'webm' and $file->process_type == '100') {
					$src = $utility->getVideoFilePath($file->video_id, $default_size, $videoParamsList['video_webm'], 'url');
					$video_webm .= '<source src="'.$src.'" type="video/webm" data-res="'.$default_size.'p" data-default="'.$default_res.'" />';
				}
				else if ($file->ext == 'webm') {
					$video_webm .= '<source src="'.$src.'" type="video/webm" data-res="'.$size.'p" data-default="'.$default_res.'" />';
				}

				if (($file->ext == 'ogg' or $file->ext == 'ogv') and $file->process_type == '100') {
					$src = $utility->getVideoFilePath($file->video_id, $default_size, $videoParamsList['video_ogg'], 'url');
					$video_ogg .= '<source src="'.$src.'" type="video/ogg" data-res="'.$default_size.'p" data-default="'.$default_res.'" />';
				}
				else if ($file->ext == 'ogg' or $file->ext == 'ogv') {
					$video_ogg .= '<source src="'.$src.'" type="video/ogg" data-res="'.$size.'p" data-default="'.$default_res.'" />';
				}

				if ($file->ext == 'flv') {
					$video_flv .= '<source src="'.$src.'" type="video/flash" data-res="'.$size.'p" data-default="'.$default_res.'" />';
				}
				$default_res = '';
			}

			$tech = $this->config->get('fallback');
			if ($tech) {
				$tech = 'data-engine="flash"';
			}
			else {
				$tech = '';
			}

			?>
			<div id="mijovideos_flowplayer" <?php echo $tech; ?> class="is-splash" style="background-image:url('<?php echo($videoParamsList['image'] ? $videoParamsList['image'] : ''); ?>');width:<?php echo intval($this->pluginParams->get('width')); ?>px;">
				<video<?php echo $videoParamsList['autoplay']; ?><?php echo($videoParamsList['loop'] == '1' ? ' loop' : ''); ?>>
					<?php echo (!$this->item->duration) ? $orig : ''; ?>
					<?php echo !empty($video_mp4) ? $video_mp4 : ''; ?>
					<?php echo !empty($video_webm) ? $video_webm : ''; ?>
					<?php echo !empty($video_ogg) ? $video_ogg : ''; ?>
					<?php echo !empty($video_flv) ? $video_flv : ''; ?>
				</video>
			</div>
			<script>
				jQuery.noConflict();
				jQuery(document).ready(function ($) {
					// Install flowplayer to an element with CSS class "mijovideos-flow"
					$("#mijovideos_flowplayer").flowplayer({
						swf: '<?php echo JUri::root(); ?>/plugins/mijovideos/flowplayer/assets/flowplayer.swf',
						ratio: <?php echo $this->config->get('video_aspect', 0.56); ?>,
						engine: '<?php echo ($this->config->get('fallback', '0') == '3' ? 'flash' : 'html5'); ?>',
						debug: <?php echo ($this->config->get('debug', '0') == '1' ? 'true' : 'false'); ?>,
						key: 'flowlicensecontainer',
						volume: '<?php echo $this->pluginParams->get('volume'); ?>',
						muted: <?php echo ($this->pluginParams->get('muted', '1') == '1' ? 'true' : 'false'); ?>,
						logo: '<?php echo $this->pluginParams->get('logofile'); ?>'
					});
				});
			</script>
		<?php
		}
		$retval = ob_get_contents();
		ob_end_clean();

		return $retval;
	}
}
