EasySocial.module( 'site/users/item' , function($)
{
	var module	= this;

	EasySocial.template('info/item', '<li data-es-user-filter><a class="ml-20" href="[%= url %]" title="[%= title %]" data-info-item data-info-index="[%= index %]"><i class="ies-info ies-small mr-5"></i> [%= title %]</a></li>');

	EasySocial.require()
	.done(function($) {
		EasySocial.Controller('Users.Item.Screenplay', {
				defaultOptions: {
					"{delete}" 			: "[data-apps-screenplay-delete]",
				}
			}, function(self) {
				return {

					init : function()
					{
						self.options.id 	= self.element.data( 'play-id' );
						self.options.userId = self.element.data( 'user-id' );
					},

					"{delete} click" : function( el , event )
					{
						EasySocial.dialog(
						{
							content 	: EasySocial.ajax( 'apps/user/screenplay/controllers/screenplay/confirmDelete' , { "id" : self.options.id , "userId" : self.options.userId })
						});
					}
				}
			}
		);

		module.resolve();
	});
});
