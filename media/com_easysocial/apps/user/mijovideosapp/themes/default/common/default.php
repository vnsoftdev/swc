<?php
/**
 * @package        MijoVideos
 * @copyright      2009-2014 Miwisoft LLC, miwisoft.com
 * @license        GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined('_JEXEC') or die;
/*
if (!file_exists(JPATH_ROOT.'/plugins/mijovideos/easysocialstream/easysocialstream.php')) {
	echo 'Warning: Please install plugin <a href="https://miwisoft.com/downloads/mijovideos/plugins" target="_blank"> here</a> for this app to work with mijovideos';
	return;
}*/

JHtml::_('behavior.modal');
JHtml::_('behavior.framework', true);

$thumb_size = MijoVideos::get('utility')->getThumbSize($config->get('thumb_size'));
?>
	<div class="es-filterbar">
		<div class="h5 pull-left filterbar-title"><?php echo JText::_( 'APP_MIJOVIDEOS_DASHBOARD_TITLE' ); ?></div>

		<a class="btn btn-es-primary btn-sm pull-right" href="<?php echo JRoute::_("index.php?option=com_mijovideos&amp;view=upload&amp;dashboard=1&amp;Itemid=167"); ?>"><?php echo JText::_( 'APP_MIJOVIDEOS_NEW_BUTTON' ); ?></a>
	</div>

<link rel="stylesheet" type="text/css" href="<?php echo JUri::root().'components/com_mijovideos/assets/css/mijovideos.css'; ?>">
<?php // Load first jQuery lib
if (MijoVideos::is30()) {
	JHtml::_('jquery.framework');
	JHtml::_('behavior.framework');
} ?>
<script type="text/javascript" src="<?php echo JUri::root().'components/com_mijovideos/assets/js/thumbnail.js'; ?>"></script>
<script type="text/javascript" src="<?php echo JUri::root().'components/com_mijovideos/assets/js/watchlater.js'; ?>"></script>

<form action="<?php echo MijoVideos::get('utility')->getActiveUrl(); ?>" method="post" name="adminForm" id="adminForm">
	<div id="mijovideos-container" class="ml-20">
		<?php if ($params->get('show_playlists', 0) && isset($playlists)) { ?>
			<?php echo JHtml::_('sliders.start', 'fd'); ?>
			<?php echo JHtml::_('sliders.panel', JText::_('COM_MIJOVIDEOS_VIDEOS'), 'videos'); ?>
			<?php echo $this->output('themes:/apps/user/mijovideosapp/common/default_uploads'); ?>
			<div class="clear"></div>
			<p class="readmore">
				<a href="<?php echo JRoute::_('index.php?option=com_mijovideos&view=channel&channel_id='.$channel->id.'&tmpl=component'); ?>" class="modal" rel="{handler: 'iframe', size: {x: 840, y: 500}}">
					<?php echo JText::_('COM_MIJOVIDEOS_VIEW_ALL'); ?>
				</a>
			</p>
			<?php echo JHtml::_('sliders.panel', JText::_('COM_MIJOVIDEOS_PLAYLISTS'), 'playlists'); ?>
			<?php echo $this->output('themes:/apps/user/mijovideosapp/common/default_playlists'); ?>
			<div class="clear"></div>
			<p class="readmore">
				<a href="<?php echo JRoute::_('index.php?option=com_mijovideos&view=channel&channel_id='.$channel->id.'&tmpl=component'); ?>" class="modal" rel="{handler: 'iframe', size: {x: 840, y: 500}}">
					<?php echo JText::_('COM_MIJOVIDEOS_VIEW_ALL'); ?>
				</a>
			</p>
			<?php echo JHtml::_('sliders.end'); ?>
		<?php }
		else { ?>
			<?php echo $this->output('themes:/apps/user/mijovideosapp/common/default_uploads'); ?>
			<div class="clear"></div>
			<p class="readmore">
				<a href="<?php echo JRoute::_('index.php?option=com_mijovideos&view=channel&channel_id='.$channel->id.'&tmpl=component'); ?>" class="modal" rel="{handler: 'iframe', size: {x: 840, y: 500}}">
					<?php echo JText::_('COM_MIJOVIDEOS_VIEW_ALL'); ?>
				</a>
			</p>
		<?php } ?>
	</div>
</form>
<?php if (JRequest::getString('format') == 'ajax') { ?>
	<script type="text/javascript">
		jQuery(window).trigger('load');
		var allPanels = jQuery('.pane-slider').hide();
		jQuery('.pane-toggler').toggle(function() {
			allPanels.slideUp('300');
			jQuery(this).next().slideDown('300');
		}, function() {
			allPanels.slideUp('300');
		});
	</script>
<?php } ?>
