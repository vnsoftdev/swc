<?php
/**
 * @package        MijoVideos
 * @copyright      2009-2014 Miwisoft LLC, miwisoft.com
 * @license        GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined('_JEXEC') or die;
if (count($videos)) {
	$utility    = MijoVideos::get('utility');
	foreach ($videos as $item) {
		$Itemid = MijoVideos::get('router')->getItemid(array('view' => 'video', 'video_id' => $item->id), null, true);
		$url    = JRoute::_('index.php?option=com_mijovideos&view=video&video_id='.$item->id.$Itemid); ?>
		<div class="mijovideos_column<?php echo $config->get('items_per_column'); ?>">
			<div class="videos-grid-item">
				<div class="videos-aspect<?php echo $config->get('thumb_aspect'); ?>"></div>
				<a href="<?php echo $url; ?>">
					<img class="videos-items-grid-thumb" src="<?php echo $utility->getThumbPath($item->id, 'videos', $item->thumb); ?>" title="<?php echo $item->title; ?>" alt="<?php echo $item->title; ?>"/>
				</a>
			</div>
			<div class="videos-items-grid-box-content">
				<div class="mijovideos_box_h3">
					<a href="<?php echo $url; ?>" title="<?php echo $item->title; ?>">
						<?php echo htmlspecialchars(JHtmlString::truncate($item->title, $config->get('title_truncation'), false, false), ENT_COMPAT); ?>
					</a>
				</div>

				<div class="videos-meta">
					<div class="mijovideos-meta-info">
						<div class="videos-view">
							<span class="value"><?php echo number_format($item->hits); ?></span>
							<span class="key"><?php echo JText::_('COM_MIJOVIDEOS_VIEWS'); ?></span>
						</div>
						<div class="date-created">
							<span class="value"><?php echo JHTML::_('date', htmlspecialchars( $item->created ),JText::_('DATE_FORMAT_LC4'));//echo MijoVideos::agoDateFormat($item->created); ?></span>
												
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
<?php } ?>