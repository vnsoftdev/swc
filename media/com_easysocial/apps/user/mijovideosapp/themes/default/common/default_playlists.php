<?php
/**
 * @package		MijoVideos
 * @copyright	2009-2014 Miwisoft LLC, miwisoft.com
 * @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined( '_JEXEC' ) or die ;
if (count($playlists)) {
    $utility = MijoVideos::get('utility');
    foreach ($playlists as $item) {

        if (empty($item->videos)) {
            continue;
        } else {
            $video = $item->videos[0];
        }

        if(!empty($item->thumb)) {
            $thumb = $utility->getThumbPath($item->id, 'playlists', $item->thumb);
        } else {
            $thumb = $utility->getThumbPath($video->video_id, 'videos', $video->thumb);
        }
        $Itemid = MijoVideos::get('router')->getItemid(array('view' => 'playlist', 'playlist_id' => $item->id), null, true);
        $url = JRoute::_('index.php?option=com_mijovideos&view=video&video_id='.$video->video_id.'&playlist_id='.$item->id.$Itemid); ?>
        <div class="mijovideos_column<?php echo $config->get('items_per_column'); ?>">
            <div class="videos-grid-item">
                <div class="videos-aspect<?php echo $config->get('thumb_aspect'); ?>"></div>
                <a href="<?php echo $url; ?>">
                    <img class="videos-items-grid-thumb" src="<?php echo $thumb; ?>" title="<?php echo $item->title; ?>" alt="<?php echo $item->title; ?>"/>
                </a>
            </div>
            <div class="videos-items-grid-box-content">
				<h3 class="mijovideos_box_h3">
					<a href="<?php echo $url; ?>" title="<?php echo $item->title; ?>">
                        <?php echo htmlspecialchars(JHtmlString::truncate($item->title, $config->get('title_truncation'), false, false), ENT_COMPAT); ?>
					</a>
				</h3>
				<div class="videos-meta">
					<div class="mijovideos-meta-info">
						<div class="videos-view">
                            <span class="value"><?php echo number_format($item->hits); ?></span>
                            <span class="key"><?php echo JText::_('COM_MIJOVIDEOS_VIEWS'); ?></span>
						</div>
						<div class="date-created">
                            <span class="value"><?php echo MijoVideos::agoDateFormat($item->created); ?></span>
						</div>
					</div>
				</div>
            </div>
        </div>
    <?php } ?>
<?php } ?>