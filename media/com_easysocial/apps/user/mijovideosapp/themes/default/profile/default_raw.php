<?php
/*
* @package		MijoVideos
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );
require_once(JPATH_ADMINISTRATOR . '/components/com_mijovideos/library/mijovideos.php');

if (!MijoVideos::get('utility')->is30()) { ?>
<script language="javascript" type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<?php
} 
?>
<script>
jQuery('#refresh_videos').live( "click", function() {
	var total_start = jQuery('#refresh_videos_start').val();
	var total_show  = jQuery('#refresh_videos_show').val();

		jQuery.ajax({
			url: 'index.php?option=com_easysocial&view=profile&type=ajax&id=<?php echo $my->id.':'.$my->username;?>&appId=<?php echo $appId;?>',
			type: 'post',
			data: {total_start: total_start, total_show: total_show},
			dataType: 'html',
			success: function(html) {
				jQuery('.pagination').remove();
				jQuery('#change_videos').before(html);
			}
		});
	});		
</script>
<?php if( $items ){ ?>
<?php 	
if(count($items)==1){
	$item = $items;
}
foreach( $items as $item ){ ?>
<script>
EasySocial.require().script('site/comments/frame').done(function($) {
	var selector = '[data-comments-user-<?php echo $item['type'];?>-<?php echo $item['id'];?>]';

	$(selector).addController('EasySocial.Controller.Comments');
});
	
</script>
<li  style="padding-top: 10px;" class="type-<?php echo $item['type'];?> streamItem es-stream-full stream-context-<?php echo $item['type'];?> stream-actor-<?php echo $my->id;?> DS06179644821677357" data-id="<?php echo $item['id'];?>" data-ishidden="0" data-context="<?php echo $item['type'];?>" data-actor="<?php echo $my->id;?>" data-streamitem="">
    <div class="es-stream" data-stream-item="">

        <div class="es-stream-meta">
            <div class="media">

                <div class="media-object pull-left">
                    <div class="es-avatar es-avatar-small es-stream-avatar" data-comments-item-avatar="">
                        <a href="<?php echo JRoute::_(JURI::root().'index.php?option=com_easysocial&view=profile&id='.$my->id.':'.$my->username);?>">
                            <img src="<?php echo $my->getAvatar( SOCIAL_AVATAR_SMALL ); ?>" alt="<?php echo $my->username;?>">
                        </a>
                    </div>
                </div>

                <div class="media-body">
                    <div class="es-stream-title">
                        <a href="<?php echo JRoute::_(JURI::root().'index.php?option=com_easysocial&view=profile&id='.$my->id.':'.$my->username); ?>" alt="<?php echo $my->username;?>">
                        <?php echo $my->username;?>
                        </a>
                    </div>

                    <div class="es-stream-meta-footer">
                        <span class="stream-icon" style="border: 1px solid #294523;background:#294523" data-original-title="" data-es-provide="tooltip" data-placement="left">
                            <span><i class="ies-lamp"></i></span>
                        </span>
                        <time>
                            <a href="<?php echo JRoute::_(JURI::root().'index.php?option=com_easysocial&view=stream&id='.$item['id'].'&layout=item'); ?>"><?php echo $item['date'];?></a>
                        </time>
                    </div>
                </div>
            </div>
        </div>

        <div class="es-stream-content">
			<img src="<?php echo JURI::root().'media/com_easysocial/apps/user/mijovideosapp/assets/icons/comments.png' ; ?>" width="32" alt="Idea Comment">
			<?php echo $item['desc'] ;?>
		</div>

        <div class="es-action-wrap">
            <ul class="list-unstyled es-action-feedback">
                <li class="action-title-likes streamAction" data-key="likes" data-streamitem-actions="">
                    <?php if( empty($item['like']) ){?>
					<a data-stream-action-likes="" href="javascript:void(0);" class="fd-small">
                        <span data-likes-action="" data-id="<?php echo $item['id'];?>" data-type="<?php echo $item['type'];?>" data-group="user" data-streamid="<?php echo $item['id'];?>">
                            Like
                        </span>
                    </a>
					<?php } else { ?>
					<a data-stream-action-unlikes="" href="javascript:void(0);" class="fd-small">
                        <span data-unlikes-action="" data-id="<?php echo $item['id'];?>" data-type="<?php echo $item['type'];?>" data-group="user" data-streamid="<?php echo $item['id'];?>">
                            Unlike
                        </span>
                    </a>
					<?php } ?>
                </li>

                <li class="action-title-comments streamAction" data-key="comments" data-streamitem-actions="">
                    <a data-stream-action-comments="" href="javascript:void(0);" class="fd-small">Comment</a>
                </li>

                <li class="action-title-repost streamAction" data-key="repost" data-streamitem-actions="">
                    <a data-stream-action-repost="" href="javascript:void(0);" class="fd-small"><span data-repost-action="" data-id="<?php echo $item['id'];?>" data-element="stream" data-group="user" class="fd-small">Repost</span>
                    </a>
                </li>

                <li class="action-title-social streamAction" data-key="social" data-streamitem-actions="">
                    <a href="javascript:void(0);" class="fd-small" data-es-share-button="" data-url="<?php echo JRoute::_(JURI::root().'index.php?option=com_easysocial&view=stream&id='.$item['id'].'&layout=item&xhtml=1'); ?>" data-title="" data-summary="">
                        Bookmark
                    </a>
                </li>

                <li>
                    <div class="es-privacy" data-es-provide="tooltip" data-placement="top" data-original-title="Shared with: Everyone" data-privacy-mode="ajax" data-es-privacy-container="">

                        <a class="es-privacy-toggle btn btn-es btn-notext" href="javascript:void(0);" data-privacy-toggle="">
                            <i class="ies-earth" data-privacy-icon=""></i>
                            <span class="caret"></span>
                        </a>

                        <ul class="es-privacy-menu dropdown-menu" data-privacy-menu="">
                            <li data-privacy-item="" data-value="public" data-utype="mijovideosapp" data-uid="<?php echo $item['id'];?>" data-pid="2" data-pitemid="0" class="privacyItem active" data-icon="ies-earth">
                                <a href="javascript:void(0);">
                                    <i class="ies-earth"></i>
                                    Everyone
                                </a>
                            </li>

                            <li data-privacy-item="" data-value="member" data-utype="mijovideosapp" data-uid="<?php echo $item['id'];?>" data-pid="2" data-pitemid="0" class="privacyItem " data-icon="ies-user-2">
                                <a href="javascript:void(0);">
                                    <i class="ies-user-2"></i>
                                    Registered Users
                                </a>
                            </li>

                            <li data-privacy-item="" data-value="friend" data-utype="mijovideosapp" data-uid="<?php echo $item['id'];?>" data-pid="2" data-pitemid="0" class="privacyItem " data-icon="ies-users">
                                <a href="javascript:void(0);">
                                    <i class="ies-users"></i>
                                    My Friends
                                </a>
                            </li>

                            <li data-privacy-item="" data-value="only_me" data-utype="mijovideosapp" data-uid="<?php echo $item['id'];?>" data-pid="2" data-pitemid="0" class="privacyItem " data-icon="ies-locked">
                                <a href="javascript:void(0);">
                                    <i class="ies-locked"></i>
                                    Only Me
                                </a>
                            </li>

                        </ul>

                        <div class="es-privacy-menu es-privacy-custom-form dropdown-menu dropdown-arrow-topright" data-privacy-custom-form="">
                            <div class="pb-5">Start typing your friend's name in the textbox below:</div>
                            <div>
                                <div class="textboxlist" data-textfield="">
                                    <input type="text" class="textboxlist-textField" data-textboxlist-textfield="" placeholder="Type a name..." autocomplete="off">
                                </div>
                            </div>
                            <div class="pt-10 pb-5">
                                <button data-cancel-button="" type="button" class="btn btn-es">Cancel</button>
                                <button data-save-button="" type="button" class="btn btn-es-primary">Save</button>
                            </div>
                        </div>


                        <input type="hidden" name="privacy" value="public" data-privacy-hidden="">
                        <input type="hidden" name="privacyCustom" value="" data-privacy-custom-hidden="">
                    </div>
                </li>

            </ul>
		<?php if( !empty($item['like']) ){?>
			<div data-stream-counter="" class="es-stream-counter">
				<div class="es-stream-actions action-contents-likes" data-streamitem-contents="" data-streamitem-contents-likes="" data-action-contents-likes="">
					<div class="es-likes-wrap" data-likes-content="" data-id="<?php echo $item['id'];?>" data-type="<?php echo $item['type'];?>" data-group="user" data-count="1" data-likes-<?php echo $item['type'];?>-user-<?php echo $item['id'];?>="">
						<i class="icon-es-heart"></i>
							<a href="<?php echo JRoute::_(JURI::root().'index.php?option=com_easysocial&view=profile&id='.$my->id.':'.$my->username); ?>">You</a> like this.	
					</div>
				</div>
		
				<div class="es-stream-actions action-contents-repost pull-right hide" data-streamitem-contents="" data-streamitem-contents-repost="" data-action-contents-repost="">
					<div class="es-repost-wrap hide" data-repost-content="" data-id="<?php echo $item['id'];?>" data-element="stream.user" data-count="0" data-repost-stream-user-<?php echo $item['id'];?>="">
						<a href="javascript:void(0);" data-popbox="module://easysocial/repost/authors" data-popbox-toggle="click">
							<i class="icon-es-shared"></i> <span class="repost-counter">0 repost</span>
						</a>
					</div>
				</div>
			</div>
		<?php } ?>	
        <div data-stream-counter="" class="es-stream-counter hide">

            <div class="es-stream-actions action-contents-likes" data-streamitem-contents="" data-streamitem-contents-likes="" data-action-contents-likes="">
                <div class="es-likes-wrap hide" data-likes-content="" data-id="<?php echo $item['id'];?>" data-type="<?php echo $item['type'];?>" data-group="user" data-count="0" data-likes-<?php echo $item['type'];?>-user-<?php echo $item['id'];?>="">
                </div>
            </div>

            <div class="es-stream-actions action-contents-repost pull-right hide" data-streamitem-contents="" data-streamitem-contents-repost="" data-action-contents-repost="">
                <div class="es-repost-wrap hide" data-repost-content="" data-id="<?php echo $item['id'];?>" data-element="stream.user" data-count="0" data-repost-stream-user-<?php echo $item['id'];?>="">

                    <a href="javascript:void(0);" data-popbox="module://easysocial/repost/authors" data-popbox-toggle="click">
                        <i class="icon-es-shared"></i> <span class="repost-counter">0 repost</span>
                    </a>
                </div>
			</div>
        </div>

        <div class="es-stream-actions action-contents-comments" data-streamitem-contents="" data-streamitem-contents-comments="" data-action-contents-comments="">
            <div class="es-comments-wrap DS006081875832751393" data-comments="" data-comments-user-<?php echo $item['type'];?>-<?php echo $item['id'];?>="" data-group="user" data-element="<?php echo $item['type'];?>" data-uid="<?php echo $item['id'];?>" data-count="2" data-total="2" data-url="<?php echo JRoute::_(JURI::root().'index.php?option=com_easysocial&view=stream&id='.$item['id'].'&layout=item'); ?>" data-streamid="<?php echo $item['id'];?>">
        <?php if(!empty($item['comment'])){
                foreach($item['comment'] as $comment){
        ?>
                <ul class="list-unstyled es-comments" data-comments-list="">
                    <li class="fd-small es-comment DS035352420108392835" data-comments-item="" data-id="<?php echo $comment['comment_id'];?>" data-child="0">
                        <div class="media es-flyout">
                            <div class="media-object pull-left">
                                <div class="es-avatar es-avatar-small" data-comments-item-avatar="">
                                    <a href="<?php echo JRoute::_(JURI::root().'index.php?option=com_easysocial&view=profile&id='.$my->id.':'.$my->username); ?>" title="<?php echo $my->username;?>">
                                        <img src="<?php echo $my->getAvatar( SOCIAL_AVATAR_SMALL ); ?>">
                                    </a>
                                </div>
                            </div>
                            <div class="media-body">
                                <div data-comments-item-commentframe="" data-comments-item-frame="">
                                    <div data-comments-item-author="">
                                        <a href="<?php echo JRoute::_(JURI::root().'index.php?option=com_easysocial&view=profile&id='.$my->id.':'.$my->username); ?>">
                                            <?php echo $my->username;?>
                                        </a>
                                    </div>
                                    <div class="es-comment-actions es-flyout-content" data-comments-item-actions="">
                                        <div class="es-comment-actions-flyout">
                                            <a class="es-comment-actions-toggle" href="javascript:void(0);">
                                                <i class="icon-es-comment-action"></i>
                                            </a>
                                            <ul class="fd-nav fd-nav-stacked pull-right es-comment-actions-nav">
                                                <li>
                                                    <a href="javascript:void(0);" data-reports-link="" data-url="<?php echo JRoute::_(JURI::root().'index.php?option=com_easysocial'); ?>" data-uid="4" data-type="comments" data-extension="com_easysocial" data-object="Comment item by admin" data-title="Report Comment" data-description="Do you want to report this comment?">
                                                        Report Comment
                                                    </a>
                                                </li>

                                                <li class="btn-comment-edit" data-comments-item-actions-edit="">
                                                    <a href="javascript:void(0);">Edit</a>
                                                </li>

                                                <li class="btn-comment-delete" data-comments-item-actions-delete="">
                                                    <a href="javascript:void(0);">Delete</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="mt-5" data-comments-item-comment=""><?php echo $comment['comment'] ; ?> </div>

                                    <div class="es-comment-item-meta" data-comments-item-meta="">
                                        <div class="es-comment-item-date" data-comments-item-date="">
                                            <i class="icon-es-clock"></i>
                                            <a href="<?php echo JRoute::_(JURI::root().'index.php?option=com_easysocial&view=stream&layout=item&id='.$item['id'].'#commentid='.$comment['comment_id']); ?>" title="<?php echo $comment['full_date'];?>">
                                                <a href="<?php echo JRoute::_(JURI::root().'index.php?option=com_easysocial&view=stream&layout=item&id='.$item['id'].'#commentid='.$comment['comment_id']); ?>" title=""><?php echo $comment['date'];?>
                                                </a>
                                            </a>
                                        </div>


                                        <div class="es-comment-item-like" data-comments-item-like="">
                                            <i class="icon-es-heart"></i>
                                            <a href="javascript:void(0);">
                                                Like
                                            </a>
                                        </div>
                                        <div data-comments-item-likecount="" class="es-comment-item-likecount" data-original-title="" data-placement="top" data-es-provide="tooltip">
                                            0
                                        </div>
                                    </div>
                                </div>

                                <div data-comments-item-loadreplies="" class="es-comment-item-loadreply" style="display: none;">
                                    <a href="javascript:void(0);">
                                        View replies
                                    </a>
                                </div>

                                <div data-comments-item-frame="" data-comments-item-editframe="" style="display: none;">
                                    <textarea class="full-width" row="1" data-comments-item-edit-input=""></textarea>
                                    <a class="btn btn-es-primary btn-sm pull-right ml-10" href="javascript:void(0);" data-comments-item-edit-submit="">
                                        Submit
                                    </a>
                                    <div data-comments-item-edit-status="" class="pull-right">
                                        Press esc to cancel
                                    </div>
                                </div>

                                <div data-comments-item-statusframe="" data-comments-item-frame="" style="display: none;">
                                    <div class="alert alert-comment-error"></div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
        <?php } ?>

<?php } ?>
       <?php if( $this->access->allowed( 'comments.add' ) && $this->my->id ) { ?>
            <div class="es-comments-form" data-comments-form>
                <div class="es-avatar es-avatar-small" data-comments-item-avatar>
                    <img src="<?php echo $my->getAvatar( SOCIAL_AVATAR_SMALL ); ?>" />
                </div>
                <div class="es-form" data-comment-form-header>
                    <div data-comment-form-editor-area>
                        <div class="mentions">
                            <div data-mentions-overlay data-default=""></div>
                            <textarea class="form-control input-sm" row="1" name="message" autocomplete="off"
                                      data-mentions-textarea
                                      data-default=""
                                      data-initial="0"
                                      data-comments-form-input
                                      placeholder="<?php echo JText::_( 'COM_EASYSOCIAL_COMMENTS_FORM_PLACEHOLDER' , true );?>"></textarea>
                        </div>
                    </div>
                </div>
                <div class="es-form-footer mt-5">
                    <a class="btn btn-es btn-sm pull-right" href="javascript:void(0);" data-comments-form-submit><?php echo JText::_( 'COM_EASYSOCIAL_COMMENTS_ACTION_SUBMIT' ); ?></a>
                    <span class="label" style="display: none;" data-comments-form-status></span>
                </div>

            </div>
       <?php } ?>
            </div>

        </div>


        </div>
        </div>
        </li>
    <?php   } ?>
    <!-- Pagination -->
    <li class="pagination">
        <?php if( !empty( $paganation ) ){ ?>
            <a class="btn btn-es-primary btn-stream-updates"  id="refresh_videos" href="javascript:void(0);">
                <i class="ies-refresh"></i>
                Load previous stream items
				
				<input type="hidden" id="refresh_videos_start" value="<?php echo $total_start; ?>"/>
				<input type="hidden" id="refresh_videos_show"  value="<?php echo $total_show;  ?>"/>
            </a>
        <?php } ?>
    </li>

<?php } ?>

