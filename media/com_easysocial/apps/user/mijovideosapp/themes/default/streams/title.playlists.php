<?php
/*
* @package		MijoVideos
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Unauthorized Access');

$Itemid     = MijoVideos::get('router')->getItemid(array('view' => 'playlist', 'playlist_id' => $item->id), null, true);
$url        = JRoute::_('index.php?option=com_mijovideos&view=playlist&playlist_id='.$item->id.$Itemid);

if (!MijoVideos::get('utility')->is30()) { ?>
	<script type="text/javascript" src="<?php echo JUri::root().'administrator/components/com_mijovideos/assets/js/jquery.min.js'; ?>"></script>;
<?php } ?>
<?php if (!$style_added) { ?>
	<link rel="stylesheet" type="text/css" href="<?php echo JUri::root().'media/com_easysocial/apps/user/mijovideosapp/assets/css/mijovideosapp.css'; ?>">
<?php } ?>
<?php if (!$actor->isBlock()) { ?>
	<a href="<?php echo $actor->getPermalink(); ?>" alt="<?php echo $this->html('string.escape', $actor->getName()); ?>"><?php echo $actor->getName(); ?></a> <?php echo JText::sprintf('APP_USER_MIJOVIDEOSAPP_PLAYLIST', $action, $url); ?>
<?php }
else { ?>
	<?php echo $actor->getName(); ?>
<?php } ?>

<?php if ($target && $actor->id != $target->id) { ?>
	<i class="ies-arrow-right"></i>
	<?php if (!$target->isBlock()) { ?>
		<a href="<?php echo $target->getPermalink(); ?>"><?php echo $target->getName(); ?></a> <?php echo JText::sprintf('APP_USER_MIJOVIDEOSAPP_PLAYLIST', $action, $url); ?>
	<?php }
	else { ?>
		<?php echo $target->getName(); ?>
	<?php } ?>
<?php } ?>
