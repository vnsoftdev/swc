<?php
/*
* @package		MijoVideos
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Unauthorized Access');

$utility = MijoVideos::get('utility');
$config  = MijoVideos::getConfig();
$Itemid     = MijoVideos::get('router')->getItemid(array('view' => 'video', 'video_id' => $item->id), null, true);
if (isset($playlist_id)) {
	$Itemid     = MijoVideos::get('router')->getItemid(array('view' => 'playlist', 'playlist_id' => $playlist_id), null, true);
	$url = JRoute::_('index.php?option=com_mijovideos&view=video&video_id='.$item->id.'&playlist_id='.$playlist_id.$Itemid);
}
else {
	$url        = JRoute::_('index.php?option=com_mijovideos&view=video&video_id='.$item->id.$Itemid);
}
$thumb_url  = $utility->getThumbPath($item->id, 'videos', $item->thumb);
$thumb_size = $utility->getThumbSize($config->get('thumb_size')); ?>

<div class="es-stream-preview mijovideosapp">
	<div class="stream-links">
		<div class="content-title">
			<a href="<?php echo $url; ?>" title="<?php echo $item->title; ?>">
				<?php echo htmlspecialchars(JHtmlString::truncate($item->title, $config->get('title_truncation'), false, false), ENT_COMPAT); ?>
			</a>
			<div class="videos-view fd-small ml-10">
				<span class="value"><?php echo number_format($item->hits); ?></span>
				<span class="key"><?php echo JText::_('COM_MIJOVIDEOS_VIEWS'); ?></span>
			</div>
			<div class="views_likes_dislikes fd-small">
				<div class="like_image_div">
					<i class="ies-thumbs-up"></i>
					<span class="likes_count mr-10"> <span id="mijovideos_like1"><?php echo number_format($item->likes); ?></span></span>
				</div>
				<div class="dislike_image_div">
					<i class="ies-thumbs-down"></i>
					<span class="dislikes_count"><span id="mijovideos_like2"><?php echo number_format($item->dislikes); ?></span></span>
				</div>
			</div>
		</div>
		<div class="links-content">
			<p class="mt-5 fd-small">
				<?php if (!isset($playlist_id)) { ?>
					<a href="javascript:void(0);" class="stream-preview-image" data-es-links-embed-item data-es-stream-embed-player="<?php echo htmlspecialchars("<iframe width=\"480\" height=\"270\" src=\""/*.rtrim(JUri::root(), '/')*/.JRoute::_("index.php?option=com_mijovideos&view=video&video_id=".$item->id.$Itemid."&layout=player&tmpl=component")."\" frameborder=\"0\" allowfullscreen></iframe>"); ?>">
						<img src="<?php echo $thumb_url; ?>" title="<?php echo $item->title; ?>" alt="<?php echo $item->title; ?>"/>
						<i class="icon-es-video-play"></i>
					</a>
				<?php } else { ?>
					<a href="<?php echo $url; ?>" target="_blank" class="stream-preview-image">
						<img src="<?php echo $thumb_url; ?>" title="<?php echo $item->title; ?>" alt="<?php echo $item->title; ?>"/>
						<i class="icon-es-video-play"></i>
					</a>
				<?php } ?>
				<?php if (!empty($item->introtext)) { ?>
					<?php echo JHtmlString::truncate(html_entity_decode($item->introtext, ENT_QUOTES), $config->get('desc_truncation'), false, false); ?>
				<?php } ?>
			</p>
		</div>
	</div>
</div>