<?php
/*
* @package		MijoVideos
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

class MijoVideosWidgetsProfile extends SocialAppsWidgets {

	public function sidebarBottom( $userId ){	}

	public function sidebarTop( $userId ){	}

	public function aboveHeader( $userId ){	}

}
