<?php
/*
* @package		MijoVideos
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

class MijovideosWidgetsDashboard extends SocialAppsWidgets {

	public function sidebarBottom( $user ) {
		$params 	= $this->getParams();

		if( $params->get( 'widget_profile' , true ) ) {
			echo $this->getVideos( $user );
		}
	}

	public function getVideos( $user )	{
	
		$model 		= $this->getModel( 'MijovideosApp' );
		$groups 	= $model->getItems( array( 'uid' => $user->id ) );

		// Get the total groups the user owns
		$total 		= $user->getTotalGroups();

		$theme 		= Foundry::themes();
		$theme->set( 'groups'	, $groups );
		//$theme->set( 'total' 	, $total );

		return $theme->output( 'themes:/apps/user/mijovideosapp/widgets/profile/mijovideosapp' );
	}
}
