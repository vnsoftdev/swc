<?php
/*
* @package		MijoVideos
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Unauthorized Access');

class MijovideosAppViewProfile extends SocialAppsView {
	public function display($userId = null, $docType = null) {
		// Requires the viewer to be logged in to access this app
		Foundry::requireLogin();

		$params = $this->getUserParams($userId);
		$channel = MijoVideos::get('channels')->getDefaultChannel();
		JRequest::setVar('channel_id', $channel->id);

		/*JRequest::setVar('option', 'com_mijovideos');
		$channel_model   = MijoVideos::get('controller', $options = array('basePath' => JPATH_MIJOVIDEOS))->getModel('Channel', 'MijoVideosModel');
		$playlists_model = MijoVideos::get('controller')->getModel('Playlists', 'MijoVideosModel');
		JRequest::setVar('option', 'com_community');*/
		$version = new JVersion();
		($version->RELEASE >= 3.0 ? JModelLegacy::addIncludePath(JPATH_MIJOVIDEOS.'/models') : JModel::addIncludePath(JPATH_MIJOVIDEOS.'/models'));
		$channel_model = ($version->RELEASE >= 3.0 ? JModelLegacy::getInstance('Channel', 'MijovideosModel', array('ignore_request' => true)) : JModel::getInstance('Channel', 'MijovideosModel', array('ignore_request' => true)));
		$channel_model->setState('limit', $params->get('total_show', 10));
		$this->set('channel', $channel_model->getItem());
		$this->set('videos', $channel_model->getChannelVideos());

		// Check for errors.
		if (count($errors = $channel_model->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
		if ($params->get('show_playlists', 0)) {
			$playlists_model = ($version->RELEASE >= 3.0 ? JModelLegacy::getInstance('Playlists', 'MijovideosModel', array('ignore_request' => true)) : JModel::getInstance('Playlists', 'MijovideosModel', array('ignore_request' => true)));
			$playlists_model->setState('limit', $params->get('total_show', 10));
			$this->set('playlists', $playlists_model->getChannelPlaylists());
			if (count($errors = $playlists_model->get('Errors'))) {
				JError::raiseError(500, implode('<br />', $errors));
				return false;
			}
		}

		$this->set('params', $params);
		$this->set('config', MijoVideos::getConfig());

		$namespace = 'common/default';

		echo parent::display($namespace);
	}
}