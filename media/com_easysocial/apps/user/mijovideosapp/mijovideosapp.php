<?php
/*
* @package		MijoVideos
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Unauthorized Access');

// We want to import our app library
Foundry::import('admin:/includes/apps/apps');

class SocialUserAppMijovideosApp extends SocialAppItem {
	public function __construct() {
		parent::__construct();
	}

	public function onPrepareStream(SocialStreamItem &$stream, $includePrivacy = true) {
		if (($stream->context != 'mijovideosapp')) {
			return false;
		}

		$easysocialstream = MijoVideos::get('utility')->getPlugin('easysocialstream', 'mijovideos');
		$my  = Foundry::user();

		if (!empty($easysocialstream) and ($my->id == 0 and $easysocialstream->params->get('ispublic', 0) == 0)) {
			return false;
		}

		static $style_added = false;
		if (!$style_added) {
			$this->set('style_added', false);
			$style_added = true;
		}
		else {
			$this->set('style_added', true);
		}

		$app = Foundry::table('App');
		$app->load($this->getAppId());
		$map = Foundry::table('AppsMap');
		$map->load(array('app_id' => $app->id, 'uid' => $my->id));

		$params = Foundry::registry($map->params);

		$data = json_decode($stream->content);

		if (!$params->get($data->action, 1)) {
			return false;
		}

		$uid     = $stream->uid;
		$privacy = Foundry::privacy($my->id);
		if ($includePrivacy) {
			if (!$privacy->validate('mijovideosapp.view', $uid, 'mijovideosapp', $stream->actor->id)) {
				return false;
			}
		}

		$row = MijoVideos::getTable('Mijovideos'.ucfirst($stream->verb));
		$row->load($data->id);

		/*$channel = MijoVideos::get('channels')->getChannel($row->channel_id);
		$properties       = $row->getProperties(1);
		$item             = JArrayHelper::toObject($properties, 'JObject');
		$item->set('channel_title', $channel->title);
		$item->set('channel_thumb', $channel->thumb);*/

		$stream->display  = SOCIAL_STREAM_DISPLAY_FULL;
		$stream->color    = '#A00000';
		$stream->fonticon = 'ies-film';
		$stream->label    = JText::sprintf('APP_USER_MIJOVIDEOSAPP_UPDATES_STREAM_TOOLTIP', ucfirst(rtrim($stream->verb, 's')));

		$likes = Foundry::likes();
		$likes->get($stream->uid, $stream->context);
		$stream->likes    = $likes;
		$stream->comments = Foundry::comments($stream->uid, $stream->context, SOCIAL_APPS_GROUP_USER, array('url' => FRoute::stream(array('layout' => 'item', 'id' => $stream->uid))));
		$stream->repost   = Foundry::get('Repost', $stream->uid, SOCIAL_TYPE_STREAM);
		//$stream->friendlyDate = $this->getDate($stream->created);//MijoVideos::agoDateFormat($row->created);
		//$stream->lapsed       = $this->getDate($stream->created);//MijoVideos::agoDateFormat($row->created);

		$this->set('item', $row);
		if (isset($data->playlist_id)) {
			$playlist = MijoVideos::get('playlists')->getPlaylist($data->playlist_id);
			$this->set('playlist_id', $data->playlist_id);
			$this->set('playlist_title', $playlist->title);
		}
		$this->set('action', $data->action);
		$this->set('actor', $stream->actor);
		$this->set('target', count($stream->targets) > 0 ? $stream->targets[0] : '');

		$stream->title = parent::display('streams/title.'.$stream->verb);
		switch ($stream->verb) {
			case 'videos':
				$stream->content = parent::display('streams/preview.'.$stream->verb);
				break;
			case 'playlists':
				$stream->content = '';
				break;
		}

		if ($includePrivacy) {
			$stream->privacy = $privacy->form($uid, 'mijovideosapp', $stream->actor->id, 'mijovideosapp.view');
		}

		return true;
	}

	public function getAppId() {
		$db = JFactory::getDBO();
		$db->setQuery("SELECT id FROM `#__social_apps` WHERE  `element` = 'mijovideosapp'");
		$result = $db->loadResult();
		return $result;
	}

	public function onPrepareActivityLog(SocialStreamItem &$item, $includePrivacy = true) {
	}

	public function onAfterLikeSave($likes) {
		if (JRequest::getString('type') != 'mijovideosapp') {
			return;
		}

		$db = JFactory::getDBO();
		$db->setQuery("SELECT context_type,content FROM #__social_stream WHERE id=".$likes->uid);
		$item = $db->loadObject();

		$model_lib = MijoVideos::get('model');
		$model_lib->likeDislikeItem($likes->created_by, $item->content, 'videos', 1);
		MijoDB::query("UPDATE #__mijovideos_videos SET `likes` = `likes` + 1 WHERE id IN (".$item->content.")");
	}

	public function onAfterLikeDelete($likes) {
		if (JRequest::getString('type') != 'mijovideosapp') {
			return;
		}

		$db = JFactory::getDBO();
		$db->setQuery("SELECT context_type,content FROM #__social_stream WHERE id=".$likes->uid);
		$item = $db->loadObject();

		$model_lib = MijoVideos::get('model');
		$model_lib->unlikeItem($item->content, 'videos', 1);
		MijoDB::query("UPDATE #__mijovideos_videos SET `likes` = `likes` - 1 WHERE id IN (".$item->content.")");
	}

	public function getIp() {
		if (getenv("HTTP_CLIENT_IP")) {
			$ip = getenv("HTTP_CLIENT_IP");
		}
		elseif (getenv("HTTP_X_FORWARDED_FOR")) {
			$ip = getenv("HTTP_X_FORWARDED_FOR");
			if (strstr($ip, ',')) {
				$tmp = explode(',', $ip);
				$ip  = trim($tmp[0]);
			}
		}
		else {
			$ip = getenv("REMOTE_ADDR");
		}
		return $ip;
	}

	public function onAfterCommentSave($comment) {
	}

	public function onNotificationLoad($item) {
	}

}
