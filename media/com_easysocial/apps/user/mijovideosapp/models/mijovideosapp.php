<?php
/*
* @package		MijoVideos
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Unauthorized Access');

// Import the model file from the core
Foundry::import('admin:/includes/model');

class MijovideosAppModel extends EasySocialModel {

	public function getVideos($userId, $total_start = 0, $total_show = 10) {
		$row           = MijoVideos::getTable('MijovideosVideos');
		$items         = $this->getItems($userId, $total_start, $total_show);
		$likes         = $this->getLikes($userId);
		$comments      = $this->getComments($userId);
		$comment_likes = $this->getCommentLikes($userId);
		$result        = array();

		if (!empty($items)) {
			foreach ($items as $item) {
				$row->load($item->content);
				$result[ $item->id ]['video_id']  = $row->id;
				$result[ $item->id ]['id']        = $item->id;
				$result[ $item->id ]['title']     = $row->title;
				$result[ $item->id ]['introtext'] = $row->introtext;
				$result[ $item->id ]['likes']     = $row->likes;
				$result[ $item->id ]['dislikes']  = $row->dislikes;
				$result[ $item->id ]['hits']      = $row->hits;
				$result[ $item->id ]['type']      = $item->context_type;
				$result[ $item->id ]['created']   = $row->created;
				$result[ $item->id ]['thumb']     = $row->thumb;

				$comment_count = 0;
				foreach ($comments as $comment) {
					if ($item->id == $comment->comment_video_id) {
						$result[ $item->id ]['comment'][ $comment_count ]['comment']    = $comment->comment;
						$result[ $item->id ]['comment'][ $comment_count ]['comment_id'] = $comment->comment_id;
						$result[ $item->id ]['comment'][ $comment_count ]['full_date']  = $comment->created;
						$result[ $item->id ]['comment'][ $comment_count ]['date']       = $this->getDate($comment->created);
						$comment_count++;
					}

					$comment_like_count = 0;
					foreach ($comment_likes as $comment_like) {
						if ($comment->comment_id == $comment_like->commentlike_video_id) {
							$result[ $item->id ]['comment_like'][ $comment_like_count ]['comment_like'] = $comment_like->commentlike_video_id;
							$comment_like_count++;
						}
					}
				}
				foreach ($likes as $like) {
					if ($item->id == $like->like_video_id) {
						$result[ $item->id ]['like'] = $like->like_video_id;
					}
				}
			}
		}

		if (!$result) {
			return false;
		}

		return $result;
	}

	public function getItems($userId, $total_start, $total_show, $fullItem = false) {
		$db = JFactory::getDBO();
		if (!$fullItem) {
			$limit = ' LIMIT '.$total_start.','.$total_show;
		}
		else {
			$limit = '';
		}

		$db->setQuery("SELECT * FROM #__social_stream WHERE (context_type='mijovideosapp') AND actor_id=".$userId.$limit);

		if (!$result = $db->loadObjectList()) {
			return false;
		}

		return $result;
	}

	public function getLikes($userId) {
		$db = JFactory::getDBO();
		$db->setQuery("SELECT uid as like_video_id FROM `#__social_likes` WHERE (type='mijovideosapp.user') AND created_by=".$userId);
		$result = $db->loadObjectList();
		return $result;
	}

	public function getComments($userId) {
		$db = JFactory::getDBO();
		$db->setQuery("SELECT uid as comment_video_id, comment, id as comment_id, created  FROM `#__social_comments` WHERE (element='mijovideosapp.user') AND created_by=".$userId);
		$result = $db->loadObjectList();
		return $result;
	}

	public function getCommentLikes($userId) {
		$db = JFactory::getDBO();
		$db->setQuery("SELECT uid as commentlike_video_id  FROM `#__social_likes` WHERE type='comments.user' AND created_by=".$userId);
		$result = $db->loadObjectList();
		return $result;
	}

	public function getDate($date_change, $format = '') {
		$config  = Foundry::config();
		$date    = Foundry::date($date_change);
		$elapsed = $config->get('comments_elapsed_time', true);
		// If format is passed in as true or false, this means disregard the elapsed time settings and obey the decision of format
		if ($format === true || $format === false) {
			$elapsed = $format;

			$format = '';
		}
		if ($elapsed && empty($format)) {
			return $date->toLapsed();
		}
		if (empty($format)) {
			return $date_change;
		}
		return $date->format($format);
	}

	public function getAppUserSetting($userId) {
		$db = JFactory::getDBO();
		$db->setQuery("SELECT id FROM `#__social_apps` WHERE element='mijovideosapp'");
		$aplication_id = $db->loadResult();
		$db->setQuery("SELECT params FROM `#__social_apps_map` WHERE app_id=".$aplication_id." AND uid=".$userId);
		$__result = $db->loadResult();
		$result   = json_decode($__result);
		return $result;
	}

	public function getAppId() {
		$db = JFactory::getDBO();
		$db->setQuery("SELECT id, title  FROM `#__social_apps` WHERE  `element` = 'mijovideosapp'");
		$__result = $db->loadObjectList();
		$_result  = $__result[0];
		$result   = $_result->id.':'.$_result->title;
		return $result;
	}
}