<?php
/**
* @package		%PACKAGE%
* @subpackge	%SUBPACKAGE%
* @copyright	Copyright (C) 2010 - 2012 %COMPANY_NAME%. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*
* %PACKAGE% is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

// We want to import our app library
Foundry::import( 'admin:/includes/apps/apps' );

/**
 * Some application for EasySocial. Take note that all classes must be derived from the `SocialAppItem` class
 *
 * Remember to rename the Textbook to your own element.
 * @since	1.0
 * @author	Author Name <author@email.com>
 */
class SocialUserAppScreenplay extends SocialAppItem
{
	/**
	 * Class constructor.
	 *
	 * @since	1.0
	 * @access	public
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Triggers the preparation of stream.
	 *
	 * If you need to manipulate the stream object, you may do so in this trigger.
	 *
	 * @since	1.0
	 * @access	public
	 * @param	SocialStreamItem	The stream object.
	 * @param	bool				Determines if we should respect the privacy
	 */
	public function onPrepareStream( SocialStreamItem &$item, $includePrivacy = true )
	{
		// You should be testing for app context
		if( $item->context != 'screenplay' ) {
			return;
		}
		
		// Decorate the stream item
		$item->display 	= SOCIAL_STREAM_DISPLAY_MINI;
		$item->color 	= '#FCCD1B';
		$item->fonticon	= 'ies-list-2';
		$item->label 	= FD::_( 'APP_SCREENPLAY_STREAM_TOOLTIP', true );

		// Get application params
		$params 	= $this->getParams();

		if ($item->verb == 'create' && $params->get('stream_create', true)) {
			$this->prepareCreateScreenplayStream( $item );
		}

		if ($item->verb == 'update' && $params->get('stream_update', true)) {
			$this->prepareUpdateScreenplayStream( $item );
		}

		if ($item->verb == 'read' && $params->get('stream_read', true)) {
			$this->prepareReadScreenplayStream( $item );
		}
	}

	/**
	 * Triggers the preparation of activity logs which appears in the user's activity log.
	 *
	 * @since	1.0
	 * @access	public
	 * @param	SocialStreamItem	The stream object.
	 * @param	bool				Determines if we should respect the privacy
	 */
	public function onPrepareActivityLog( SocialStreamItem &$item, $includePrivacy = true )
	{
		if( $item->context != 'screenplay' ) {
			return;
		}
	}

	/**
	 * Prepares the stream item for new screenplay creation
	 *
	 * @since	1.0
	 * @access	public
	 * @param	SocialStreamItem	The stream item.
	 * @return
	 */
	private function prepareCreateScreenplayStream( &$item )
	{
		// Retrieve initial data
		$play = $this->getTable('Screenplay');
		$play->load($item->contextId);

		if (!$play->state) {
			return;
		}

		// Get the actor
		$actor 		= $item->actor;

		// Get the creation date
		$date 		= FD::date( $play->created );

		if (!$this->canViewScreenplay($play)) {
			return;
		}

		// Get the content
		$content = "";
		
		$permalink = FRoute::apps( array( 'layout' => 'canvas' , 'id' => '159:screenplay' , 'cid' => $play->id , 'uid' => $actor->getAlias() , 'type' => SOCIAL_TYPE_USER ) );

		$this->set( 'content' , $content );
		$this->set( 'date' , $date );
		$this->set( 'permalink'	, $permalink );
		$this->set( 'play' , $play );
		$this->set( 'actor'	, $actor );

		// Load up the contents now.
		$item->title 	= parent::display( 'streams/create.title' );
		$item->content 	= parent::display( 'streams/create.content' );
		$item->opengraph->addDescription( $content );
	}

	/**
	 * Prepares the stream item for screenplay updating
	 *
	 * @since	1.0
	 * @access	public
	 * @param	SocialStreamItem	The stream item.
	 * @return
	 */
	private function prepareUpdateScreenplayStream( &$item )
	{
		// Retrieve initial data
		$play = $this->getTable('Screenplay');
		$play->load($item->contextId);

		if (!$play->state) {
			return;
		}

		if (!$this->canViewScreenplay($play)) {
			return;
		}

		// Get the creation date
		$date = FD::date( $play->created );

		// Get the actor
		$actor = $item->actor;

		$permalink = FRoute::apps( array( 'layout' => 'canvas' , 'id' => '159:screenplay' , 'cid' => $play->id , 'uid' => $actor->getAlias() , 'type' => SOCIAL_TYPE_USER ) );

		// Get the content
		$content 	= "";

		$this->set( 'content' , $content );
		$this->set( 'date' , $date );
		$this->set( 'permalink'	, $permalink );
		$this->set( 'play'	, $play );
		$this->set( 'actor'	, $actor );

		// Load up the contents now.
		$item->title 	= parent::display( 'streams/update.title' );
		$item->content 	= parent::display( 'streams/update.content' );
		$item->opengraph->addDescription( $content );
	}

	/**
	 * Prepares the stream item when an article is being read
	 *
	 * @since	1.0
	 * @access	public
	 * @param	SocialStreamItem	The stream item.
	 * @return
	 */
	private function prepareReadScreenplayStream( &$item )
	{
		// Retrieve initial data
		$play = $this->getTable('Screenplay');
		$play->load($item->contextId);

		if (!$play->state) {
			return;
		}

		if (!$this->canViewScreenplay($play)) {
			return;
		}
		
		// Get the actor
		$actor 	= $item->actor;

		// Get the creation date
		$date = FD::date( $play->created );

		// Get the content
		$content 	= "";

		$permalink = FRoute::apps( array( 'layout' => 'canvas' , 'id' => '159:screenplay' , 'cid' => $play->id , 'uid' => $actor->getAlias() , 'type' => SOCIAL_TYPE_USER ) );		
		
		$this->set( 'content'	, $content );
		$this->set( 'date'		, $date );
		$this->set( 'permalink'	, $permalink );
		$this->set( 'play'	, $play );
		$this->set( 'actor'		, $actor );

		// Load up the contents now.
		$item->title 	= parent::display( 'streams/read.title' );
		$item->content 	= parent::display( 'streams/read.content' );
		$item->opengraph->addDescription( $content );
	}

	/**
	 * Triggers after a like is saved.
	 *
	 * This trigger is useful when you want to manipulate the likes process.
	 *
	 * @since	1.0
	 * @access	public
	 * @param	SocialTableLikes	The likes object.
	 *
	 * @return	none
	 */
	public function onAfterLikeSave( &$likes )
	{
	}

	/**
	 * Triggered when a comment save occurs.
	 *
	 * This trigger is useful when you want to manipulate comments.
	 *
	 * @since	1.0
	 * @access	public
	 * @param	SocialTableComments	The comment object
	 * @return
	 */
	public function onAfterCommentSave( &$comment )
	{
		/*if ($comment->element == 'screenplay.user.create') {
			$model 	= FD::model( 'Screenplay' ); //phải tạo file model trong thư mục administrator
			$model->updateCommentCount($comment->uid);
		}*/
		return;
	}

	/**
	 * Renders the notification item that is loaded for the user.
	 *
	 * This trigger is useful when you want to manipulate the notification item that appears
	 * in the notification drop down.
	 *
	 * @since	1.0
	 * @access	public
	 * @param	string
	 * @return
	 */
	public function onNotificationLoad( $item )
	{
	}

	/**
	 * Determines if a screenplay is viewable by the user
	 *
	 * @since	1.0
	 * @access	public
	 * @param	string
	 * @return	
	 */
	private function canViewScreenplay($play)
	{
		// If no access filter is set, the layout takes some responsibility for display of limited information.
		/*$my = Foundry::user();

		// Get viewing levels
		$groups = $my->getAuthorisedViewLevels();

		if (in_array($play->access, $groups)) {
			return true;
		}

		return false;*/
		return true;
	}	
}
