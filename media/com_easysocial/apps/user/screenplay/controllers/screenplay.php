<?php
/**
* @package		%PACKAGE%
* @subpackge	%SUBPACKAGE%
* @copyright	Copyright (C) 2010 - 2012 %COMPANY_NAME%. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*
* %PACKAGE% is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

/**
 * If you need to perform any logics, you can do so in your controller. Be it ajax calls, or normal requests.
 *
 * @since	1.0
 * @author	Author Name <author@email.com>
 */
class ScreenplayControllerScreenplay extends SocialAppsController
{
	/**
	 * Displays confirmation dialog to delete a screenplay
	 *
	 * @since	1.0
	 * @access	public
	 */
	public function confirmDelete()
	{
		// Check for request forgeriess
		FD::checkToken();

		// Ensure that the user is logged in.
		FD::requireLogin();

		$ajax 	= FD::ajax();

		$id 		= JRequest::getInt( 'id' );
		$userId	= JRequest::getInt( 'userId' );
		$user		= FD::user( $userId );

		/*if( !$group->isAdmin() ) {
			return $ajax->reject();
		}*/

		$theme 	= FD::themes();
		$theme->set( 'user' , $user );
		$theme->set( 'appId' , $this->getApp()->id );
		$theme->set( 'id' , $id );
		$output	= $theme->output( 'apps/user/screenplay/canvas/dialog.delete' );

		return $ajax->resolve( $output );
	}

	/**
	 * Processes scrrenplay deletion
	 *
	 * @since	1.0
	 * @access	public
	 */
	public function delete()
	{
		// Check for request forgeriess
		FD::checkToken();

		// Ensure that the user is logged in.
		FD::requireLogin();

		$ajax = FD::ajax();

		$id = JRequest::getInt( 'id' );
		$userId	= JRequest::getInt( 'userId' );
		$user = FD::user( $userId );

		/*if( !$group->isAdmin() )
		{
			return $this->redirect( $group->getPermalink( false ) );
		}*/

		// Load the screenplay
		$screenplay = $this->getTable( 'Screenplay' );
		$screenplay->load( $id );

		/*if( !$group->isAdmin() )
		{
			return $this->redirect( $group->getPermalink( false ) );
		}*/

		$state 	= $screenplay->delete();

		// @points: delete.screenplay
		// Deduct points from the scrrenplay creator when the screenplay is deleted.
		$points = FD::points();
		$points->assign( 'delete.screenplay' , 'com_easysocial' , $screenplay->created_by );

		$message = $state ? JText::_( 'APP_SCREENPLAY_DELETED_SUCCESS' ) : JText::_( 'APP_SCREENPLAY_DELETED_FAILED' );
		FD::info()->set( $message , SOCIAL_MSG_SUCCESS );

		$this->redirect( JRoute::_('index.php?option=com_easysocial&view=dashboard&appId=159:screenplay') );
	}
	
	/**
	 * Store screenplay into database
	 *
	 * @since	1.0
	 * @access	public
	 * @return
	 */
	public function save()
	{
		// Check for request forgeriess
		FD::checkToken();

		// Ensure that the user is logged in.
		FD::requireLogin();

		// Get the posted data
		$post = JRequest::get( 'post' );

		// Determines if this is an edited screenplay
		$id = JRequest::getInt('playId');

		// Load up the screenplay object
		$play = $this->getTable( 'Screenplay' );
		$play->load( $id );

		$message 	= !$play->id ? JText::_( 'APP_SCREENPLAY_CREATED_SUCCESSFULLY' ) : JText::_( 'APP_SCREENPLAY_UPDATED_SUCCESSFULLY' );

		$my 		= FD::user();

		// Get the app id
		$app 		= $this->getApp();
		$appid = $app->id . ':' . $app->getAlias();

		$options					= array();
		$options[ 'title' ]			= JRequest::getVar( 'title' );
		$options[ 'content' ]		= JRequest::getVar( 'screenplay_content' , '', 'post', 'string', JREQUEST_ALLOWRAW );
		$options[ 'state' ]			= SOCIAL_STATE_PUBLISHED;

		// Only bind this if it's a new item
		if( !$play->id ) {
			$options[ 'created_by' ]	= $my->id;
			$options[ 'hits' ]			= 0;
			$options[ 'comments' ]		= 0;
			$options[ 'scenes' ]		= 0;
		}

		// Bind the data
		$play->bind( $options );

		// Check if there are any errors
		if( !$play->check() ) {
			FD::info()->set( $play->getError() , SOCIAL_MSG_ERROR );

			//$url 	= FRoute::apps( array( 'layout' => 'canvas' , 'sublayout' => 'create' , 'uid' => $my->id , 'type' => SOCIAL_TYPE_USER , 'id' => $app->getAlias() ) , false );
			$url = FRoute::apps(array('layout' => 'canvas' , 'id' => $appid , 'sublayout' => 'create' , 'uid' => $my->getAlias()));

			return $this->redirect( $url );
		}
		
		
		// If everything is okay, bind the data.
		$play->store();

		// Create a stream record
		$verb = $id ? 'update' : 'create';
		$stream =  $play->createStream( $verb );

		//$play->load($play->id);
		$play->uid = $stream->uid;
		$play->store();
		//$play->bind($play)
		
		//print_r($stream);exit();
		// Redirect to the appropriate page now
		//$url 		= FRoute::apps( array( 'layout' => 'dashboard' , 'uid' => $my->id , 'type' => SOCIAL_TYPE_USER , 'id' => $app->getAlias() , 'playId' => $play->id ) , false );
		$url = JRoute::_('index.php?option=com_easysocial&view=dashboard&appId=' . $appid);

		// If it is a new item, we want to run some other stuffs here.
		if (!$id) {
			// @points: create.screenplay
			// Add points to the user that created the screenplay
			$points = FD::points();
			$points->assign( 'create.screenplay' , 'com_easysocial' , $my->id );

			//$app 		= $this->getApp();
			//$permalink 	= $app->getPermalink('canvas', array('playId' => $play->id));
			// Notify users about the news.
			//$options 	= array( 'userId' => $my->id , 'permalink' => $permalink, 'playId' => $play->id, 'playTitle' => $play->title , 'playContent' => strip_tags( $play->content ) );

			//$group->notifyMembers('scrrenplay.create', $options);
		}

		FD::info()->set( $message , SOCIAL_MSG_SUCCESS );
		//@Vi: Add privacy items for share 
		$privacy = $this->input->get('privacy', '', 'default');
		$customPrivacy = $this->input->get('privacyCustom', '', 'string');
		$privacyLib = FD::privacy();
		$privacyLib->add("screenplay.view", $stream->uid, "screenplay", $privacy, null, $customPrivacy);
		// Perform a redirection
		$this->redirect($url);
	}
}
