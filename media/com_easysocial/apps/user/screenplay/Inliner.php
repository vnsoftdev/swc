<?php
/*
 * Copyright (c) 2013, Christoph Mewes, http://www.xrstf.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

class Inliner {
	private static $patterns = array(
		'note'                  => '<!-- $1 -->',

		'line_break'            => '<br />',

		'bold_italic_underline' => '<span class="bold italic underline">$2</span>',
		'bold_underline'        => '<span class="bold underline">$2</span>',
		'italic_underline'      => '<span class="italic underline">$2</span>',
		'bold_italic'           => '<span class="bold italic">$2</span>',
		'bold'                  => '<span class="bold">$2</span>',
		'italic'                => '<span class="italic">$2</span>',
		'underline'             => '<span class="underline">$2</span>'
	);

	public function lex($str) {
		if (!$str) return;

		$styles = array(
			'bold_italic_underline',
			'bold_underline',
			'italic_underline',
			'bold_italic',
			'bold',
			'italic',
			'underline',
		);

		$str = preg_replace(Parser::$patterns['note_inline'], self::$patterns['note'], $str);
 		$str = str_replace("\n", self::$patterns['line_break'], $str);

		// replace escaped special chars
		$str = str_replace('\\*', '[star]', $str);
		$str = str_replace('\\_', '[underline]', $str);

		foreach ($styles as $style) {
			$regex = Parser::$patterns[$style];

			if (preg_match($regex, $str)) {
				$str = preg_replace($regex, self::$patterns[$style], $str);
			}
		}

		// revert replacement of escaped special chars
		$str = str_replace('[star]',      '*', $str);
		$str = str_replace('[underline]', '_', $str);

		return $str;
	}
}
