<?php
/*
 * Copyright (c) 2013, Christoph Mewes, http://www.xrstf.de
 *
 * This file is released under the terms of the MIT license. You can find the
 * complete text in the attached LICENSE file or online at:
 *
 * http://www.opensource.org/licenses/mit-license.php
 */

class Parser {
	public static $patterns = array(

		'title_page' => '/^((?:title|credit|author[s]?|source|notes|draft date|date|contact|copyright)\:)/im',

		'scene_heading' => '/^((?:\*{0,3}_?)?(?:(?:int|ext|est|i\/e)[. ]).+)|^(?:\.(?!\.+))(.+)/i',
		'scene_number'  => '/( *#(.+)# *)/',

		'transition' => '/^(FADE OUT\.)|^((?:(?:FADE|CUT) TO BLACK|.+ CUT|PULL BACK TO REVEAL.*)\:|.+ TO\:)|^(?:> *)(.+)/',

		'dialogue'      => '/^([A-Z*_ÄÖÜ]+[0-9A-ZÄÖÜß (._\-´`’\')]*)(\^?)?(?:\n(?!\n+))([\s\S]+)/',
		'parenthetical' => '/^(\(.+\))$/',

		'action'   => '/^(.+)/m',
		'centered' => '/^(?:> *)(.+)(?: *<)(\n.+)*/m',

		'section'  => '/^(#+)(?: *)(.*)/',
		'synopsis' => '/^(?:\=(?!\=+) *)(.*)/',

		'note'                => '/^(?:\[{2}(?!\[+))(.+)(?:\]{2}(?!\[+))$/',
		'note_inline'         => '/(?:\[{2}(?!\[+))([\s\S]+?)(?:\]{2}(?!\[+))/',
		'boneyard'            => '/(^\/\*|^\*\/)$/m',
		'boneyard_singleline' => '/^\/\*(.+)\*\/$/m',

		'page_break' => '/^\={3,}$/m',
		'line_break' => '/^ {2}$/m',

		'emphasis'              => '/(_|\*{1,3}|_\*{1,3}|\*{1,3}_)(.+)(_|\*{1,3}|_\*{1,3}|\*{1,3}_)/',
		'bold_italic_underline' => '/(_{1}\*{3}(?=.+\*{3}_{1})|\*{3}_{1}(?=.+_{1}\*{3}))(.+?)(\*{3}_{1}|_{1}\*{3})/',
		'bold_underline'        => '/(_{1}\*{2}(?=.+\*{2}_{1})|\*{2}_{1}(?=.+_{1}\*{2}))(.+?)(\*{2}_{1}|_{1}\*{2})/',
		'italic_underline'      => '/(?:_{1}\*{1}(?=.+\*{1}_{1})|\*{1}_{1}(?=.+_{1}\*{1}))(.+?)(\*{1}_{1}|_{1}\*{1})/',
		'bold_italic'           => '/(\*{3}(?=.+\*{3}))(.+?)(\*{3})/',
		'bold'                  => '/(\*{2}(?=.+\*{2}))(.+?)(\*{2})/',
		'italic'                => '/(\*{1}(?=.+\*{1}))(.+?)(\*{1})/',
		'underline'             => '/(_{1}(?=.+_{1}))(.+?)(_{1})/',

		'splitter'     => '/\n{2,}/',
		'cleaner'      => '/^\n+|\n+$/',
		'standardizer' => '/\r\n|\r/',
		'whitespacer'  => '/^\t+|^ {3,}/m'
	);

	public function __construct(Inliner $inliner = null) {
		$this->inliner = $inliner ?: new Inliner();
	}

	public function parse($script) {
		$tokens     = $this->tokenize($script);
		$html       = array();
		$titlePage  = array();
		$title      = null;
		$countScene = 0;

		foreach ($tokens as $token) {
			$text = null;

			if (isset($token['text'])) {
				$text = $this->inliner->lex($token['text']);
			}

			switch ($token['type']) {
				case 'title':
					$title_page[] = '<h1>'.$text.'</h1>';
					$title        = preg_replace('/<(?:.|\n)*?>/', '', str_replace('<br />', ' ', $text));
					break;

				case 'credit':     $titlePage[] = '<p class="credit">'    .$text.'</p>'; break;
				case 'author':     $titlePage[] = '<p class="authors">'   .$text.'</p>'; break;
				case 'authors':    $titlePage[] = '<p class="authors">'   .$text.'</p>'; break;
				case 'source':     $titlePage[] = '<p class="source">'    .$text.'</p>'; break;
				case 'notes':      $titlePage[] = '<p class="notes">'     .$text.'</p>'; break;
				case 'draft_date': $titlePage[] = '<p class="draft-date">'.$text.'</p>'; break;
				case 'date':       $titlePage[] = '<p class="date">'      .$text.'</p>'; break;
				case 'contact':    $titlePage[] = '<p class="contact">'   .$text.'</p>'; break;
				case 'copyright':  $titlePage[] = '<p class="copyright">' .$text.'</p>'; break;

				//case 'scene_heading': $countScene++; $html[] = '<h3'.($token['scene_number'] ? ' id="'.$token['scene_number'].'">' : '>').'<span>'.$countScene.'</span>'.$text.'<span>'.$countScene.'</span></h3>'; break;
				case 'scene_heading': $countScene++; $html[] = '<h3'.($token['scene_number'] ? ' id="'.$token['scene_number'].'">' : '>').$text.'</span></h3>'; break;
				case 'transition':    $html[] = '<p class="transition">'.$text.'</p>'; break;

				case 'dual_dialogue_begin': $html[] = '<div class="dual-dialogue">'; break;
				case 'dialogue_begin':      $html[] = '<div class="dialogue'.($token['dual'] ? ' '.$token['dual'] : '').'">'; break;
				case 'character':           $html[] = '<h4>'.$text.'</h4>'; break;
				case 'parenthetical':       $html[] = '<p class="parenthetical">'.$text.'</p>'; break;
				case 'dialogue':            $html[] = '<p>'.$text.'</p>'; break;
				case 'dialogue_end':        $html[] = '</div>'; break;
				case 'dual_dialogue_end':   $html[] = '</div>'; break;

				case 'section':  $html[] = '<p class="section" data-depth="'.$token['depth'].'">'.$text.'</p>'; break;
				case 'synopsis': $html[] = '<p class="synopsis">'.$text.'</p>'; break;

				case 'note':           $html[] = '<!-- '.$text.'-->'; break;
				case 'boneyard_begin': $html[] = '<!-- '; break;
				case 'boneyard_end':   $html[] = ' -->'; break;

				case 'action':   $html[] = '<p>'.$text.'</p>'; break;
				case 'centered': $html[] = '<p class="centered">'.$text.'</p>'; break;

				case 'page_break': $html[] = '</div><div class="page">'; break;
				case 'line_break': $html[] = '<br />'; break;
			}
		}

		return array(
			'title' => $title,
			'html'  => array(
				'title_page' => implode('', $titlePage),
				'script'     => '<div class="page">'.implode('', $html).'</div>',
			),
			'tokens' => $tokens
		);
	}

	public function tokenize($script) {
		$regex  = self::$patterns;
		$src    = $this->lex($script);
		$src    = preg_split($regex['splitter'], $src);
		$tokens = array();
		$dual   = false;

		foreach (array_reverse($src) as $line) {
			// title page
			if (preg_match($regex['title_page'], $line)) {
				$match = preg_replace($regex['title_page'], "\n$1", $line);
				$match = preg_split($regex['splitter'], $match);
				$match = array_reverse($match);

				foreach ($match as $m) {
					$parts = preg_replace($regex['cleaner'], '', $m);
					$parts = preg_split('/\:\n*/', $parts);

					$tokens[] = array(
						'type' => str_replace(' ', '_', strtolower(trim($parts[0]))),
						'text' => trim($parts[1])
					);
				}

				continue;
			}

			// scene headings
			if (preg_match($regex['scene_heading'], $line, $match)) {
				$text = $match[1] ?: $match[2];
				$meta = null;

				if (strpos($text, '  ') !== strlen($text) - 2) {
					if (preg_match($regex['scene_number'], $text, $match)) {
						$match = $match[2];
						$text  = preg_replace($regex['scene_number'], '', $text);
					}

					$tokens[] = array(
						'type'         => 'scene_heading',
						'text'         => $text,
						'scene_number' => $meta
					);
				}

				continue;
			}

			// centered
			if (preg_match($regex['centered'], $line, $match)) {
				$text     = preg_replace('/>|</', '', $match[0]);
				$tokens[] = array('type' => 'centered', 'text' => $text);

				continue;
			}

			// transitions
			if (preg_match($regex['transition'], $line, $match)) {
				$text     = $match[1] ?: $match[2] ?: $match[3];
				$tokens[] = array('type' => 'transition', 'text' => $text);

				continue;
			}

			// dialogue blocks - characters, parentheticals and dialogue
			if (preg_match($regex['dialogue'], $line, $match)) {
				if (strpos($match[1], '  ') !== strlen($match[1]) - 2) {
					// we're iterating from the bottom up, so we need to push these backwards
					if ($match[2]) {
						$tokens[] = array('type' => 'dual_dialogue_end');
					}

					$tokens[] = array('type' => 'dialogue_end');

					$parts = array_reverse(preg_split("/(\(.+\))(?:\\n+)/", $match[3], null, PREG_SPLIT_DELIM_CAPTURE));

					foreach ($parts as $text) {
						if (mb_strlen($text) > 0) {
							$tokens[] = array(
								'type' => preg_match($regex['parenthetical'], $text) ? 'parenthetical' : 'dialogue',
								'text' => $text
							);
						}
					}

					$tokens[] = array('type' => 'character', 'text' => trim($match[1]));
					$tokens[] = array('type' => 'dialogue_begin', 'dual' => $match[2] ? 'right' : ($dual ? 'left' : null));

					if ($dual) {
						$tokens[] = array('type' => 'dual_dialogue_begin');
					}

					$dual = !!$match[2];
					continue;
				}
			}

			// section
			if (preg_match($regex['section'], $line, $match)) {
				$tokens[] = array('type' => 'section', 'text' => $match[2], 'depth' => strlen($match[1]));
				continue;
			}

			// synopsis
			if (preg_match($regex['synopsis'], $line, $match)) {
				$tokens[] = array('type' => 'synopsis', 'text' => $match[1]);
				continue;
			}

			// notes
			if (preg_match($regex['note'], $line, $match)) {
				$tokens[] = array('type' => 'note', 'text' => $match[1]);
				continue;
			}

			// boneyard
			if (preg_match($regex['boneyard'], $line, $match)) {
				$tokens[] = array('type' => $match[0][0] === '/' ? 'boneyard_begin' : 'boneyard_end');
				continue;
			}

			// boneyard
			if (preg_match($regex['boneyard_singleline'], $line, $match)) {
				$tokens[] = array('type' => 'boneyard', 'text' => trim($match[1]));
				continue;
			}

			// page breaks
			if (preg_match($regex['page_break'], $line)) {
				$tokens[] = array('type' => 'page_break');
				continue;
			}

			// line breaks
			if (preg_match($regex['line_break'], $line)) {
				$tokens[] = array('type' => 'line_break');
				continue;
			}

			$tokens[] = array('type' => 'action', 'text' => $line);
		}

		return array_reverse($tokens);
	}

	protected function lex($script) {
		$script = preg_replace(self::$patterns['boneyard'],     "\n$1\n", $script);
		$script = preg_replace(self::$patterns['standardizer'], "\n",     $script);
		$script = preg_replace(self::$patterns['cleaner'],      '',       $script);
		$script = preg_replace(self::$patterns['whitespacer'],  '',       $script);

		return $script;
	}
}
