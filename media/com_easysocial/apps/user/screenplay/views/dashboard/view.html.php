<?php
/**
* @package		%PACKAGE%
* @subpackge	%SUBPACKAGE%
* @copyright	Copyright (C) 2010 - 2012 %COMPANY_NAME%. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*
* %PACKAGE% is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

/**
 * Profile Embed view for Textbook app
 *
 * @since	1.0
 * @access	public
 */
class ScreenplayViewDashboard extends SocialAppsView
{
	/**
	 * This method is invoked automatically and must exist on this view.
	 *
	 * The contents displayed here will be returned via an AJAX call from the system.
	 *
	 * @since	1.0.0
	 * @access	public
	 * @param	int		The user id that is currently being viewed.
	 * @return 	void
	 */
	public function display( $userId )
	{
		// Requires the viewer to be logged in to access this app
		Foundry::requireLogin();

		// We want the user object from EasySocial so we can do funky stuffs.
		$user 	= Foundry::user( $userId );

		// Since we are on the canvas page, we have the flexibility to change the page title.
		if( $user->isViewer() ) {
			$title 	= JText::_( 'APP_SCREENPLAY_YOUR_SCRIPTS' );
		}
		else {
			$title	= JText::sprintf( 'APP_SCREENPLAY_OTHER_SCRIPTS' , $user->getName() );
		}

		// Set the page title. You can use JFactory::getDocument()->setTitle( 'title' ) as well.
		Foundry::page()->title( $title );

		// Load up the model
		$model 	= $this->getModel( 'Screenplay' );

		// Get the list of screenplay created by the user.
		//$result = $model->getItemsNoContent( $userId );
		$params = $this->app->getParams();
		$options = array('limit' => (int) $params->get('total', 10));
		$result = $model->getItemsNoContent($userId, $options);

		$pagination = $model->getPagination();
		$pagination->setVar('option', 'com_easysocial');
		$pagination->setVar('view', 'dashboard');
		$pagination->setVar('layout', 'default');
		$pagination->setVar('id', $user->getAlias());
		$pagination->setVar('appId', $this->app->getAlias());

		foreach ($result as $play) {
			$comments = FD::model( 'Comments' );
			$comment_options = array( 'element' => 'screenplay.user.create' , 'uid' => $play->id );
			/*if ($this->stream_id) {
				$options['stream_id'] = $this->stream_id;
			}*/
			$play->comments	= $comments->getCommentCount($comment_options);
		}

		// Assign the screenplay to the theme files.
		// This option is totally optional, you can use your own theme object to output files.
		$this->set( 'screenplay' , $result );
		$this->set( 'user'		, $user );
		$this->set('pagination', $pagination);

		// If you use the built in theme manager, the namespace is relative to the following folder,
		// /media/com_easysocial/apps/user/textbook/themes/default

		$namespace 	= 'dashboard/default';

		// Output the contents
		echo parent::display( $namespace );
	}
}