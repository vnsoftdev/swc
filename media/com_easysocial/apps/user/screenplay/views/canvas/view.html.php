<?php
/**
* @package		%PACKAGE%
* @subpackge	%SUBPACKAGE%
* @copyright	Copyright (C) 2010 - 2012 %COMPANY_NAME%. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*
* %PACKAGE% is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

/**
 * Canvas view for Screenplay app
 *
 * @since	1.0
 * @access	public
 */
class ScreenplayViewCanvas extends SocialAppsView
{
	/**
	 * This method is invoked automatically and must exist on this view.
	 *
	 * @since	1.0.0
	 * @access	public
	 * @param	int		The user id that is currently being viewed.
	 * @return 	void
	 */
	public function display( $userId )
	{
		// Requires the viewer to be logged in to access this app
		Foundry::requireLogin();

		// We want the user object from EasySocial so we can do funky stuffs.
		$user 	= Foundry::user( $userId );
		$editor = JFactory::getEditor();

		$id 	= JRequest::getInt( 'cid' );
		//if ($id == 0) $id = 20;
		$appid 	= JRequest::getInt( 'id' );
		$sublayout 	= JRequest::getString( 'sublayout' );
		
		// Since we are on the canvas page, we have the flexibility to change the page title.
		if( $user->isViewer() ) {
			$title 	= JText::_( 'APP_SCREENPLAY_YOUR_SCRIPTS' );
		}
		else {
			$title	= JText::sprintf( 'APP_SCREENPLAY_OTHER_SCRIPTS' , $user->getName() );
		}

		// Set the page title. You can use JFactory::getDocument()->setTitle( 'title' ) as well.
		Foundry::page()->title( $title );

		// Load up the model
		$model 	= $this->getModel( 'Screenplay' );

		// Get the screenplay created by the user.
		$result = $this->getTable('Screenplay');
		$play = $model->getItem($id);
		$result->loadData($play);
		$scenes = $model->getScenes($id);
		//ML: debug
		//var_dump($result); die;

		$url = FRoute::apps( array( 'layout' => 'canvas' , 'id' => '159:screenplay' , 'cid' => $id , 'uid' => $user->getAlias() , 'type' => SOCIAL_TYPE_USER ), false );
		// Apply comments for the screenplay
		$comments	= FD::comments( $id , 'screenplay' , 'create', SOCIAL_TYPE_USER, array('url'=>$url) );

		// Apply likes for the screenplay
		$likes 		= FD::likes()->get( $id , 'screenplay', 'create', 'user' );

		// Retrieve the params
		$params 	= $this->app->getParams();

		$isOwner = false;
		if ($user->id == $result->created_by) $isOwner = true;
		
		// Assign the screenplay to the theme files.
		// This option is totally optional, you can use your own theme object to output files.
		$this->set( 'screenplay'	, $result );
		$this->set( 'user'			, $user );
		$this->set( 'likes'			, $likes );
		$this->set( 'comments'		, $comments );
		$this->set( 'params'		, $params );
		$this->set( 'editor'		, $editor );
		$this->set( 'isOwner'		, $isOwner );
		$this->set( 'scenes'		, $scenes);

		// If you use the built in theme manager, the namespace is relative to the following folder,
		// /media/com_easysocial/apps/user/textbook/themes/default

		$document = JFactory::getDocument();
		$document->addStyleSheet(JUri::root() . '/media/com_easysocial/apps/user/screenplay/assets/css/fountain.css');

		switch ($sublayout) {
			case 'create':
				FD::page()->title(JText::_('APP_SCREENPLAY_CREATE_FORM_TITLE'));
				break;
			case 'edit':
				if (!id || !$result) { //something wrong here
					FD::info()->set(false, JText::_('APP_SCREENPLAY_RETRY_MSG'), SOCIAL_MSG_ERROR);
					return $this->redirect(JRoute::_('index.php?option=com_easysocial&view=dashboard&appId=159:screenplay'));
				}
				FD::page()->title(JText::_('APP_SCREENPLAY_EDIT_FORM_TITLE'));
				break;
			case 'preview':
				break;
			case 'chart':
				break;
		}
		
		if ($sublayout) {
			$namespace 	= 'canvas/' . $sublayout;
		} else {
			$namespace 	= 'canvas/default';
			$result->addHit();
		}

		// Output the contents
		echo parent::display( $namespace );
	}
}