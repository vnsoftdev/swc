<?php
defined('_JEXEC') or die;

class ScreenplayHelper {
	public function getSceneHeading($script) {
		$result = array();
		$count = array();
		
		foreach ($script['tokens'] as $token) {
			//if (($token['type'] == 'scene_heading') && (!in_array($token['text'], $result))) {
			if ($token['type'] == 'scene_heading') {
				if (array_search($token['text'], $result) == FALSE) {
					$result[] = $token['text'];
					$count[] = 1;
				} else {
					$count[array_search($token['text'], $result)] += 1;
				}
			}
		}

		for ($i=0; $i<count($result); $i++) {
			$result[$i] = $result[$i] . " (" . $count[$i] . ")";
		}

		return $result;
	}

	public function getPage($script, $p = 0) {
		$result = explode("<br />", $script);

		return $result[$p];
	}
}
?>