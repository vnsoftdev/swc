<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2012 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

// Import main table to assis this table.
Foundry::import( 'admin:/tables/table' );

/**
 * Tasks object relation mapper.
 *
 * @since	1.0
 * @author	Mark Lee <mark@stackideas.com>
 */
class ScreenplayTableScreenplay extends SocialTable
{
	/**
	 * The unique screenplay id.
	 * @var	int
	 */
	public $id 		= null;

	/**
	 * The screenplay title.
	 * @var	string
	 */
	public $title 	= null;

	/**
	 * The screenplay content.
	 * @var	string
	 */
	public $content = null;

	/**
	 * The owner of the screenplay.
	 * @var	int
	 */
	public $created_by	= null;

	/**
	 * The created date.
	 * @var	datetime
	 */
	public $created = null;

	/**
	 * The state of the mapping
	 * @var int
	 */
	public $state		= null;

	/**
	 * Number of views on the screenplay.
	 * @var	int
	 */
	public $hits = null;

	/**
	 * Number of comments on the scrrenplay.
	 * @var	int
	 */
	public $comments = null;

	/**
	 * Number of scenes in the screenplay
	 * @var	int
	 */
	public $scenes = null;
	/**
	 * The screenplay uid related with #__social_privacy_items.
	 * @var	int
	 */
	public $uid = null;

	/**
	 * Class Constructor.
	 *
	 * @since	1.0
	 * @access	public
	 */
	public function __construct(& $db )
	{
		parent::__construct( '#__social_apps_screenplay' , 'id' , $db );
	}

	/**
	 * Allows the caller to check on some of the required items
	 *
	 * @since	1.2
	 * @access	public
	 * @param	Array
	 * @return	boolean		True if success, false otherwise.
	 */
	public function check()
	{
		if( empty( $this->title ) )
		{
			$this->setError( JText::_( 'Please enter a title for your article.' ) );
			return false;
		}

		if( empty( $this->content ) )
		{
			$this->setError( JText::_( 'Please enter some contents for your article.' ) );
			return false;
		}

		if( empty( $this->created_by ) )
		{
			$this->setError( JText::_( 'Please specify an author for this article.' ) );
			return false;
		}

		return true;
	}

	/**
	 * Publishes into the stream
	 *
	 * @since	1.0
	 * @access	public
	 * @param	string	The verb to be used on the stream
	 * @return
	 */
	public function createStream( $verb = 'create' )
	{
		// Add activity logging when a new schedule is created
		// Activity logging.
		$stream				= Foundry::stream();
		$streamTemplate		= $stream->getTemplate();

		// Set the actor.
		$streamTemplate->setActor( $this->created_by , SOCIAL_TYPE_USER );

		// Set the context.
		$streamTemplate->setContext( $this->id , 'screenplay' );

		// Set the verb.
		$streamTemplate->setVerb( $verb );

		$streamTemplate->setPublicStream( 'screenplay.view' );

		//ML:debug
		//var_dump($streamTemplate); die;
		
		// Create the stream data.
		return $stream->add( $streamTemplate );
	}
	
	/**
	 * Override parent's behavior to delete the item.
	 *
	 * @since	1.0
	 * @access	public
	 * @return
	 */
	public function delete( $pk = null )
	{
		$state 	= parent::delete( $pk );

		if( $state )
		{
			// Delete stream items
			FD::stream()->delete( $this->id , 'screenplay' );

			// Delete comments
			FD::comments( $this->id , 'screenplay', 'create' )->delete();

			// Delete likes
			FD::likes()->delete( $this->id , 'screenplay', 'create' );
		}

		return $state;
	}
	
	public function loadData($play = null)
	{
		if (!$play) return false;

		$this->id = $play->id;
		$this->title = $play->title;
		$this->content = $play->content;
		$this->created_by = $play->created_by;
		$this->created = $play->created;
		$this->hits = $play->hits;
		$this->comments = $play->comments;
		$this->scenes = $play->scenes;
		
		return true;
	}
	
	public function addComment()
	{
		$this->comments = $this->comments + 1;
	}
}
