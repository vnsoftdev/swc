define(['handlebars'], function(Handlebars) {

    this["JST"] = this["JST"] || {};

    this["JST"]["templates/layout.hbs"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};
        var buffer = "",
            stack1, helper, functionType = "function",
            escapeExpression = this.escapeExpression,
            self = this;

        function program1(depth0, data) {

            var buffer = "",
                stack1;
            buffer += " ";
            stack1 = helpers['if'].call(depth0, (depth0 && depth0.view), {
                hash: {},
                inverse: self.noop,
                fn: self.program(2, program2, data),
                data: data
            });
            if (stack1 || stack1 === 0) {
                buffer += stack1;
            }
            return buffer;
        }

        function program2(depth0, data) {

            var buffer = "",
                stack1, helper;
            buffer += "\n		<li class=\"menu-item ";
            if (helper = helpers.name) {
                stack1 = helper.call(depth0, {
                    hash: {},
                    data: data
                });
            } else {
                helper = (depth0 && depth0.name);
                stack1 = typeof helper === functionType ? helper.call(depth0, {
                    hash: {},
                    data: data
                }) : helper;
            }
            buffer += escapeExpression(stack1) +
                " ";
            if (helper = helpers['class']) {
                stack1 = helper.call(depth0, {
                    hash: {},
                    data: data
                });
            } else {
                helper = (depth0 && depth0['class']);
                stack1 = typeof helper === functionType ? helper.call(depth0, {
                    hash: {},
                    data: data
                }) : helper;
            }
            buffer += escapeExpression(stack1) +
                "\" plugin=\"";
            if (helper = helpers.name) {
                stack1 = helper.call(depth0, {
                    hash: {},
                    data: data
                });
            } else {
                helper = (depth0 && depth0.name);
                stack1 = typeof helper === functionType ? helper.call(depth0, {
                    hash: {},
                    data: data
                }) : helper;
            }
            buffer += escapeExpression(stack1) +
                "\">\n			<img src=\"";
            if (helper = helpers.static_path) {
                stack1 = helper.call(depth0, {
                    hash: {},
                    data: data
                });
            } else {
                helper = (depth0 && depth0.static_path);
                stack1 = typeof helper === functionType ? helper.call(depth0, {
                    hash: {},
                    data: data
                }) : helper;
            }
            buffer += escapeExpression(stack1) +
                "gfx/icons/";
            if (helper = helpers.name) {
                stack1 = helper.call(depth0, {
                    hash: {},
                    data: data
                });
            } else {
                helper = (depth0 && depth0.name);
                stack1 = typeof helper === functionType ? helper.call(depth0, {
                    hash: {},
                    data: data
                }) : helper;
            }
            buffer += escapeExpression(stack1) +
                ".svg\" />\n			<span>";
            if (helper = helpers.title) {
                stack1 = helper.call(depth0, {
                    hash: {},
                    data: data
                });
            } else {
                helper = (depth0 && depth0.title);
                stack1 = typeof helper === functionType ? helper.call(depth0, {
                    hash: {},
                    data: data
                }) : helper;
            }
            buffer += escapeExpression(stack1) +
                "</span>\n		</li>";
            return buffer;
        }

        function program4(depth0, data) {

            var stack1;
            stack1 = helpers['if'].call(depth0, (depth0 && depth0.view), {
                hash: {},
                inverse: self.noop,
                fn: self.program(5, program5, data),
                data: data
            });
            if (stack1 || stack1 === 0) {
                return stack1;
            } else {
                return '';
            }
        }

        function program5(depth0, data) {

            var buffer = "",
                stack1, helper;
            buffer += "\n		<img class=\"tool ";
            if (helper = helpers['class']) {
                stack1 = helper.call(depth0, {
                    hash: {},
                    data: data
                });
            } else {
                helper = (depth0 && depth0['class']);
                stack1 = typeof helper === functionType ? helper.call(depth0, {
                    hash: {},
                    data: data
                }) : helper;
            }
            buffer += escapeExpression(stack1) +
                "\" src=\"";
            if (helper = helpers.static_path) {
                stack1 = helper.call(depth0, {
                    hash: {},
                    data: data
                });
            } else {
                helper = (depth0 && depth0.static_path);
                stack1 = typeof helper === functionType ? helper.call(depth0, {
                    hash: {},
                    data: data
                }) : helper;
            }
            buffer += escapeExpression(stack1) +
                "gfx/icons/";
            if (helper = helpers.name) {
                stack1 = helper.call(depth0, {
                    hash: {},
                    data: data
                });
            } else {
                helper = (depth0 && depth0.name);
                stack1 = typeof helper === functionType ? helper.call(depth0, {
                    hash: {},
                    data: data
                }) : helper;
            }
            buffer += escapeExpression(stack1) +
                ".svg\" plugin=\"";
            if (helper = helpers.name) {
                stack1 = helper.call(depth0, {
                    hash: {},
                    data: data
                });
            } else {
                helper = (depth0 && depth0.name);
                stack1 = typeof helper === functionType ? helper.call(depth0, {
                    hash: {},
                    data: data
                }) : helper;
            }
            buffer += escapeExpression(stack1) +
                "\" />\n		";
            return buffer;
        }

        function program7(depth0, data) {

            var buffer = "",
                stack1, helper;
            buffer += "<div class=\"plugin-content\" plugin=\"";
            if (helper = helpers.name) {
                stack1 = helper.call(depth0, {
                    hash: {},
                    data: data
                });
            } else {
                helper = (depth0 && depth0.name);
                stack1 = typeof helper === functionType ? helper.call(depth0, {
                    hash: {},
                    data: data
                }) : helper;
            }
            buffer += escapeExpression(stack1) +
                "\">";
            if (helper = helpers.view) {
                stack1 = helper.call(depth0, {
                    hash: {},
                    data: data
                });
            } else {
                helper = (depth0 && depth0.view);
                stack1 = typeof helper === functionType ? helper.call(depth0, {
                    hash: {},
                    data: data
                }) : helper;
            }
            if (stack1 || stack1 === 0) {
                buffer += stack1;
            }
            buffer += "</div>";
            return buffer;
        }

        buffer += "<!--- out of content -->\n<div id=\"back\" style=\"opacity:0; width: 100%; height: 100%; position: absolute; top: 0; left: 0\"></div>\n<!--- main menu -->\n<div class=\"menu\" style=\"display:none\">\n	<ul class=\"selector\">\n		";
        stack1 = helpers.each.call(depth0, (depth0 && depth0.plugins), {
            hash: {},
            inverse: self.noop,
            fn: self.program(1, program1, data),
            data: data
        });
        if (stack1 || stack1 === 0) {
            buffer += stack1;
        }
        buffer += "</ul>\n</div>\n<div class=\"footer\"></div>\n<!--- plugin content -->\n<div class=\"content\" style=\"display:none\">\n	<div class=\"top-bar\">\n		";
        stack1 = helpers.each.call(depth0, (depth0 && depth0.plugins), {
            hash: {},
            inverse: self.noop,
            fn: self.program(4, program4, data),
            data: data
        });
        if (stack1 || stack1 === 0) {
            buffer += stack1;
        }
        buffer += "\n		\n		<div class=\"right-icons\">\n			<img class=\"close-content panel-icon\" src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/close.svg\" />\n			<img class=\"expand panel-icon\" src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/expand.svg\" />\n		</div>\n		\n	</div>\n	<div class=\"plugin-contents\">\n		";
        stack1 = helpers.each.call(depth0, (depth0 && depth0.plugins), {
            hash: {},
            inverse: self.noop,
            fn: self.program(7, program7, data),
            data: data
        });
        if (stack1 || stack1 === 0) {
            buffer += stack1;
        }
        buffer += "</div>\n</div>\n<div id=\"tooltip\"></div>";
        return buffer;
    });

    this["JST"]["templates/plugins/dev/fquerysandbox.hbs"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};



        return "<h1>FQuery sandbox</h1>\r\n\r\n<div id=\"sandbox\"></div>\r\n\r\n<p>Open dev tools and start with a snippet:</p>\r\n\r\n<pre>\r\nvar data = window.data,\r\n    fquery = window.fquery,\r\n    fhelper = window.fhelpers,\r\n    h = fhelpers.fq,\r\n    tokens = data.parsed.tokens,\r\n    lines = data.parsed.lines,\r\n	queries = window.queries;\r\n</pre>\r\n\r\n<h2>Build a - scenes with length</h2>\r\n\r\n<p>Create a new query. For each heading set name of the scene and save reference to the selection.</p>\r\n\r\n<pre>\r\nvar query = fquery('id');\r\nquery.enter(h.is('scene_heading'), function (token, fq) {\r\n    fq.current = fq.select(token);\r\n	fq.current.lines = 0;\r\n	fq.current.scene = token.text;\r\n});\r\n</pre>\r\n\r\n<p>For any other token add numberof lines.</p>\r\n<pre>\r\nquery.enter(query.not(h.is('scene_heading')), function (item, fq) {\r\n    if (fq.current) {\r\n        fq.current.lines += item.lines.length;\r\n    }\r\n});\r\n</pre>\r\n\r\n<p>Run the query and print returned results.</p>\r\n<pre>\r\nvar result = query.run(tokens);\r\nresult.forEach(function(item, i){\r\n    var pages = item.lines / data.config.print().lines_per_page;\r\n    console.log('#' + i + ' ' + item.scene + ' ' + helper.format_time(pages) + ' (' + item.lines + ' lines)') \r\n});\r\n</pre>\r\n\r\n<p>Console:</p>\r\n<pre>\r\n#0 INT. MATTHEW'S BEDROOM - DAY 00:12 (11 lines) test:24\r\n#1 INT. MAIN ROOM - DAY 01:34 (85 lines) test:24\r\n#2 EXT. PARK PLAYGROUND - DAY 00:08 (7 lines) test:24\r\n#3 INT. LIVING ROOM - DAY 02:00 (108 lines) test:24\r\n#4 INT. LIVING ROOM - DAY 06:17 (339 lines) test:24\r\n#5 EXT. PARK PLAYGROUND - DAY 00:29 (26 lines) test:24\r\n#6 EXT. HILLTOP - DAY 00:09 (8 lines) test:24\r\n#7 EXT. TRAIN STATION - DAY 01:37 (87 lines) test:24\r\n#8 EXT. OLD TRAIN STATION 01:56 (104 lines) \r\n</pre>\r\n\r\n<h2>Display a a chart</h2>\r\n\r\n<p>Add you query to queries.js module or use <em>result</em> variable from the previous script.</p>\r\n\r\n<pre>\r\nvar chart = {},\r\n    // query = queries.locations_breakdown,\r\n    result = query.run(data.parsed.tokens);\r\n\r\nchart.render = function(id, data, config) {\r\n    $(id).empty();\r\n    if (data.length &lt; 1) {\r\n	   $(id).append('&lt;p class=\"error\"&gt;Sorry, there is not enough data to display the chart. Add at least ??? to your script.&lt;/p&gt;');\r\n	   return;\r\n    };\r\n};\r\n\r\n\r\nchart.render('#sandbox', result, {});\r\n</pre>";
    });

    this["JST"]["templates/plugins/dev/test.hbs"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};



        return "<h1>Performance test</h1>\r\n<p>Parse\r\n	<input id=\"test-parse-times\" type=\"input\" value=\"10\"/>times:\r\n	<button id=\"test-parse-perform\">perform</button>\r\n</p>\r\n<ul id=\"test-parse-results\"></ul>\r\n<script>\r\n	require(['plugins/dev/test', 'jquery'], function(test, $) {\r\n\r\n			$('#test-parse-perform').click(function() {\r\n				var result = test.parse_times(parseInt($('#test-parse-times').val()));\r\n				$('#test-parse-results').empty();\r\n				result.forEach(function(r) {\r\n					$('#test-parse-results').append('<li>' + r.action + ': ' + r.avg);\r\n				});\r\n			});\r\n\r\n		});\r\n</script>";
    });

    this["JST"]["templates/plugins/editor.hbs"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};
        var buffer = "",
            stack1, helper, functionType = "function",
            escapeExpression = this.escapeExpression;


        buffer += "<h1 style=\"float:left\">Fountain Editor\n	<span class=\"info-icon\" section=\"editor-info\" />\n</h1>\n<div class=\"header-menu\">\n		<span>auto-save: <a href=\"#\" action=\"auto-save\" title=\"Auto-save to cloud \"><img class=\"auto-save-icon sync-icon icon small-icon\" /></a>&nbsp;|&nbsp;</span>\n		<span>auto-reload: <a href=\"#\" action=\"sync-fountain\" title=\"Auto-reload from cloud/disk\"><img class=\"auto-reload-icon sync-icon icon small-icon\" /></a>&nbsp;|&nbsp;</span>\n		<span>save .fountain: <a href=\"#\" action=\"save-fountain\"><img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/download.svg\" class=\"icon small-icon\" title=\"Download Fountain file\" /></a></span>\n		<span><a href=\"#\" action=\"save-dropbox-fountain\"><img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/dropbox.svg\" class=\"icon small-icon\" title=\"Upload Fountain file to Dropbox\" /></a></span>\n		<span><a href=\"#\" action=\"save-gd-fountain\"><img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/gd.svg\" class=\"icon small-icon\" title=\"Upload Fountain file to Google Drive\" /></a></span>\n	</div><div style=\"clear:both\"/>\n<p class=\"info-content\" section=\"editor-info\" style=\"clear:both\">\n	Just a basic fountain editor. Use Ctrl-Space for auto-complete. Go to <a href=\"http://fountain.io\" target=\"_blank\">fountain.io</a> for more details about Fountain format.<br/>\n	Use auto-save to automatically save your changes to the cloud every 3 seconds.<br />\n	Use auto-reload to reload the script from the cloud/disk to see PDF, facts & stats changes.\n</p>\n<textarea id=\"editor-textarea\" placeholder=\"Code goes here...\" class=\"to-the-bottom\"></textarea>\n<!-- scripts -->\n<script>\n	require(['jquery', 'plugins/editor', 'utils/layout', 'utils/common'], function($, editor, layout, common) {\n		editor.create_editor($('#editor-textarea').get(0));\n\n		var editor_content = $('.plugin-content[plugin=\"editor\"]');\n		\n		var sync_on_icon = common.data.static_path + 'gfx/icons/other/sync.svg',\n		    sync_off_icon = common.data.static_path + 'gfx/icons/other/no-sync.svg',\n			update_sync_layout = function() {\n				$('.auto-reload-icon')\n					.attr('src', editor.is_sync() ? sync_on_icon : sync_off_icon)\n					.attr('title', editor.is_sync() ? 'Turn auto-reload off' : 'Turn auto-reload on');\n				$('.auto-save-icon')\n					.attr('src', editor.is_auto_save() ? sync_on_icon : sync_off_icon)\n					.attr('title', editor.is_auto_save() ? 'Turn auto-save off' : 'Turn auto-save on');\n				$('.CodeMirror').css('opacity', editor.is_sync() ? 0.5 : 1);\n			};\n		\n		$('a[action=\"sync-fountain\"]').click(function(){\n			if (editor.is_sync()) {\n				editor.toggle_sync();\n				$.prompt('Synchronization turned off.', {\n					buttons: {'Keep content': true, 'Load version before sync': false},\n					submit: function(e,v) {\n						if (!v) {\n							editor.restore();\n						}\n					}\n				});\n			}\n			else {\n				editor.store();\n				$.prompt(\"You can start writing in your editor. Content will be synchronized with ’afterwriting! PDF preview, facts and stats will be automatically updated.\", {\n					buttons: {'OK': true, 'Cancel': false},\n					submit: function(e,v) {\n						if (v) {\n							editor.toggle_sync();\n						}\n					}\n				});\n			}\n		});\n\n		$('a[action=\"auto-save\"]').click(function(){\n			if (editor.is_sync()) {\n				$.prompt('This will turn auto-reload off. Do you wish to continue?', {\n					buttons: {'Yes': true, 'No': 'false'},\n					submit: function(e,v) {\n						if (v) {\n							editor.toggle_auto_save();\n						}\n					}\n				});\n			}\n			else {\n				editor.toggle_auto_save();\n			}\n		});\n		\n		editor.activate.add(function() {\n			if (editor.sync_available()) {\n				$('a[action=\"sync-fountain\"]').parent().show();\n			}\n			else {\n				$('a[action=\"sync-fountain\"]').parent().hide();\n			}\n\n			if (editor.auto_save_available()) {\n				$('a[action=\"auto-save\"]').parent().show();\n			}\n			else {\n				$('a[action=\"auto-save\"]').parent().hide();\n			}\n			update_sync_layout();\n		});\n		\n		editor.toggle_sync.add(function() {\n			update_sync_layout();\n		});\n\n		editor.toggle_auto_save.add(function(){\n			update_sync_layout();\n		});\n\n		function update_auto_save_icon() {\n\n			if (editor.is_auto_save()) {\n				if (editor.pending_changes() || editor.save_in_progress()) {\n					$('.auto-save-icon').addClass('in-progress');\n				}\n				else {\n					$('.auto-save-icon').removeClass('in-progress');\n				}\n\n				if (editor.save_in_progress()) {\n					$('.auto-save-icon').addClass('rotate');\n				}\n				else {\n					$('.auto-save-icon').removeClass('rotate');\n				}\n			}\n			else {\n				$('.auto-save-icon').removeClass('rotate');\n                $('.auto-save-icon').removeClass('in-progress');\n			}\n		}\n\n		editor.save_in_progress.add(update_auto_save_icon);\n		editor.pending_changes.add(update_auto_save_icon);\n\n		var resize = function() {\n\n			if (layout.small) {\n				editor.set_size(\"auto\", editor_content.height() - 70);\n			} else {\n				editor.set_size(\"auto\", editor_content.height() - 100);\n			}\n		};\n\n		resize();\n		$(window).resize(resize);\n	});\n</script>";
        return buffer;
    });

    this["JST"]["templates/plugins/facts.hbs"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};



        return "<style>\r\n	.plugin-content[plugin=\"facts\"] li span {\r\n		font-weight: bold;\r\n	}\r\n	\r\n</style>\r\n<h1>Facts</h1>\r\n<ul>\r\n	<li>Title:\r\n		<span id=\"facts-title\"></span>\r\n	</li>\r\n	<li>Pages:\r\n		<span id=\"facts-pages\"></span>\r\n	</li>\r\n	<li>Time:\r\n		<span id=\"facts-time\"></span>\r\n	</li>\r\n	<li>Scenes:\r\n		<span id=\"facts-scenes\"></span>\r\n	</li>\r\n	<li>Action time:\r\n		<span id=\"facts-action-time\"></span>\r\n	</li>\r\n	<li>Dialogue time:\r\n		<span id=\"facts-dialogue-time\"></span>\r\n	</li>\r\n	<li>Primary characters:\r\n		<span id=\"facts-primary-characters\"></span>\r\n	</li>\r\n	<li>Secondary characters:\r\n		<span id=\"facts-secondary-characters\"></span>\r\n	</li>\r\n</ul>\r\n<div id=\"facts-time-of-speaking-container\">\r\n<h2>Time of speaking</h2>\r\n<span class=\"content-expander expand-dialogue show-all-dialogues\" style=\"margin-left: 20px\">Top 10 characters shown. Click here to show all&nbsp;\r\n	<span class=\"nof-dialogues\"></span>.</span>\r\n<span class=\"content-expander expand-dialogue show-top-dialogues\" style=\"display:none; margin-left: 20px\">All&nbsp;\r\n	<span class=\"nof-dialogues\"></span>&nbsp;characters shown. Click here to show Top 10 only.</span>\r\n<ol id=\"facts-characters\"></ol>\r\n</div>\r\n<div id=\"facts-locations-contaner\">\r\n<h2>Locations</h2>\r\n<span class=\"content-expander expand-locations show-all-locations\" style=\"margin-left: 20px\">Top 10 locations shown. Click here to show all&nbsp;\r\n	<span class=\"nof-locations\"></span>.</span>\r\n<span class=\"content-expander expand-locations show-top-locations\" style=\"display:none; margin-left: 20px\">All\r\n	<span class=\"nof-locations\"></span>&nbsp;locations shown. Click here to show Top 10 only.</span>\r\n<ol id=\"facts-locations\"></ol>\r\n</div>\r\n<!-- scripts -->\r\n<script>\r\n	require(['jquery', 'plugins/facts', 'utils/helper'], function($, facts, helper) {\r\n		var handlers_added = false;\r\n\r\n		facts.refresh.add(function() {\r\n			var facts_data = facts.data.facts;\r\n			$('#facts-title').html(facts_data.title.replace(/\\*/g, '').replace(/_/g, '').replace(/\\n/g, ' / ') || '-');\r\n						\r\n			var	pages_text = facts_data.pages.toFixed(2);			\r\n			var eights = helper.eights(facts_data.pages),\r\n				eights_text = eights ? ' ~ ' + eights  : '';\r\n			pages_text += eights_text;\r\n			if (facts.each_scene_on_new_page()) {				\r\n				pages_text += ' ~ ' + facts_data.filled_pages.toFixed(2) + ' without page breaks';\r\n			}\r\n			$('#facts-pages').html(pages_text);\r\n			\r\n			$('#facts-time').html(helper.format_time(facts_data.filled_pages));\r\n			$('#facts-scenes').html(facts_data.scenes + ' (action only: ' + facts_data.action_scenes + ', with dialogue: ' + facts_data.dialogue_scenes + ')');\r\n			$('#facts-action-time').html(helper.format_time(facts_data.action_time));\r\n			$('#facts-dialogue-time').html(helper.format_time(facts_data.dialogue_time));\r\n\r\n			var primary_characters = facts.get_characters_by_level(1).map(function(ch){return ch.name}).join(', ');\r\n			$('#facts-primary-characters').html(primary_characters || '-');\r\n			var secondary_characters = facts.get_characters_by_level(2).map(function(ch){return ch.name}).join(', ');\r\n			$('#facts-secondary-characters').html(secondary_characters || '-');\r\n			\r\n			$('#facts-characters').empty();\r\n			var character_scenes;\r\n			for (var i = 0; i < facts_data.characters.length; i++) {\r\n				character_scenes = facts_data.characters[i].scenes.length;\r\n				character_scenes = character_scenes.toString() + (character_scenes === 1 ? ' scene' : ' scenes');\r\n				\r\n				$('#facts-characters').append('<li class=\"' + (i >= 10 ? 'expandable' : '') + '\">' + facts_data.characters[i].name + ' (' + helper.format_time(facts_data.characters[i].time) + ', ' + character_scenes + ')</li>');\r\n			}\r\n\r\n			$('#facts-locations').empty();\r\n			for (var i = 0; i < facts_data.locations.length; i++) {\r\n				$('#facts-locations').append('<li class=\"' + (i >= 10 ? 'expandable' : '') + '\">' + facts_data.locations[i].name + ' (' + facts_data.locations[i].count + ')</li>');\r\n			}\r\n			$('.nof-dialogues').html(facts_data.characters.length);\r\n			$('.nof-locations').html(facts_data.locations.length);\r\n\r\n\r\n			if (facts_data.characters.length === 0) {\r\n				$('#facts-time-of-speaking-container').hide();\r\n			}\r\n			else {\r\n				$('#facts-time-of-speaking-container').show();\r\n			}\r\n			\r\n			if (facts_data.characters.length > 10) {\r\n				$('.show-all-dialogues').show();\r\n				$('.show-top-dialogues').hide();\r\n				$('#facts-characters li.expandable').hide();\r\n			} else {\r\n				$('.expand-dialogue').hide();\r\n			}\r\n			\r\n			if (facts_data.locations.length === 0) {\r\n				$('#facts-locations-contaner').hide();\r\n			}\r\n			else {\r\n				$('#facts-locations-contaner').show();\r\n			}\r\n\r\n			if (facts_data.locations.length > 10) {\r\n				$('.show-all-locations').show();\r\n				$('.show-top-locations').hide();\r\n				$('#facts-locations li.expandable').hide();\r\n			} else {\r\n				$('.expand-locations').hide();\r\n			}\r\n\r\n			if (!handlers_added) {\r\n				$('.expand-dialogue').click(function() {\r\n					if ($(this).hasClass('show-all-dialogues')) {\r\n						$('.show-all-dialogues').hide();\r\n						$('.show-top-dialogues').show();\r\n					} else {\r\n						$('.show-all-dialogues').show();\r\n						$('.show-top-dialogues').hide();\r\n					}\r\n					$('#facts-characters li.expandable').slideToggle({\r\n						duration: 200\r\n					});\r\n				});\r\n				$('.expand-locations').click(function() {\r\n					if ($(this).hasClass('show-all-locations')) {\r\n						$('.show-all-locations').hide();\r\n						$('.show-top-locations').show();\r\n					} else {\r\n						$('.show-all-locations').show();\r\n						$('.show-top-locations').hide();\r\n					}\r\n					$('#facts-locations li.expandable').slideToggle({\r\n						duration: 200\r\n					});\r\n				});\r\n				handlers_added = true;\r\n			}\r\n		});\r\n\r\n	});\r\n</script>";
    });

    this["JST"]["templates/plugins/info.hbs"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};
        var buffer = "",
            stack1, helper, functionType = "function",
            escapeExpression = this.escapeExpression;


        buffer += "<h1>About</h1>\n<p>\n	<em>'afterwriting labs</em>&nbsp;is a place where you can play with some screenwriting tools. You can open screenplays written in <a href=\"http://fountain.io/\" target=\"_blank\"><strong>Fountain format</strong></a> or <a href=\"http://www.finaldraft.com/\" target=\"_blank\"><strong>Final Draft</strong></a> (it will be converted to fountain). You can also use one of the\n	<a class=\"switch\" href=\"#\" plugin=\"open\">samples</a>.</p>\n<p>You can use it offline too! (sorry, no Dropbox/Google Drive support for offline version). Just download this\n	<a href=\"afterwriting.zip\" id=\"download-link\">file</a>, unzip and double click on afterwriting.html.</p>\n<p>\n	It's a client-side only app. That means your screenplay is never sent out of your PC.\n</p>\n<p>\n<h2>Contact</h2>\n<p><a href=\"http://twitter.com/afterwriting\" target=\"_blank\" title=\"twitter.com/afterwriting\"><img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/twitter.svg\" class=\"icon\" />@afterwriting</a></p>\n<p><a href=\"http://blog.afterwriting.com\" target=\"_blank\" title=\"blog.afterwriting.com\"><img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/blogger.svg\" class=\"icon\" />blog.afterwriting.com</a></p>\n<p><a href=\"mailto:contact@afterwriting.com\" title=\"contact@afterwriting.com\"><img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/email.svg\" class=\"icon\" />contact@afterwriting.com</a></p>\n<p><a href=\"http://about.me/piotr.jamroz\" target=\"_blank\"><img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/about.me.svg\" class=\"icon\" />about.me</a>\n<p><a href=\"https://github.com/ifrost/afterwriting-labs\" target=\"_blank\"><img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/github.svg\" class=\"icon\" />GitHub</a>\n</p>\n<h1>What's inside?</h1>\n<p>Available plugins:</p>\n<ul>\n	<li>\n		<img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/open.svg\" class=\"plugin-icon\" />&nbsp;/&nbsp;\n		<img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/save.svg\" class=\"plugin-icon\" />&nbsp;\n		<em>Open / Save</em>&nbsp;- open a .fountain or .fdx file from disk, Dropbox or use a sample, save as .fountain or as PDF.</li>\n	<li>\n		<img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/settings.svg\" class=\"plugin-icon\" />\n		<em>Settings</em>&nbsp;- change paper size, print layout, etc.</li>\n	<li>\n		<img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/editor.svg\" class=\"plugin-icon\" />\n		<em>Editor</em>&nbsp;- a basic editor for\n		<a href=\"http://fountain.io/\" target=\"_blank\">.fountain files</a>\n	</li>\n	<li>\n		<img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/preview.svg\" class=\"plugin-icon\" />\n		<em>Preview</em>&nbsp;- PDF preview (a browser plugin required)</li>\n	<li>\n		<img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/facts.svg\" class=\"plugin-icon\" />\n		<em>Facts</em>&nbsp;- facts about your screenplay (number of pages, scenes, characters, etc.)</li>\n	<li>\n		<img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/stats.svg\" class=\"plugin-icon\" />\n		<em>Useless Stats</em>&nbsp;- some statistics, don't take them too seriously!</li>\n</ul>\n<h1>Thanks</h1>\n<p>\n	<ul>\n		<li>Icons made by <a href=\"http://www.flaticon.com/authors/freepik\" title=\"Freepik\">Freepik</a> from <a href=\"http://www.flaticon.com\" title=\"Flaticon\">www.flaticon.com</a> is licensed by <a href=\"http://creativecommons.org/licenses/by/3.0/\" title=\"Creative Commons BY 3.0\">CC BY 3.0</a>\n		</li>\n		<li>Sample scripts:\n			<ul>\n				<li>\n					<em>Brick & Steel</em>&nbsp;by\n					<a href=\"http://prolost.com/about/\" target=\"_blank\">Stu Maschwitz</a>\n				</li>\n				<li>\n					<em>My Living Memory</em>&nbsp;&amp;&nbsp;<em>Priting Trouble</em>&nbsp;by\n					<a href=\"http://www.webring.org/l/rd?ring=indepfilm;id=59;url=http%3A%2F%2Fwww%2Ecvisual%2Ecom%2F\" target=\"_blank\">Dan Rahmel</a>\n				</li>				\n			</ul>\n		</li>\n		<li>DayRoman font by\n			<a href=\"http://apostrophiclab.pedroreina.net/\" target=\"_blank\">Apostrophic Laboratories</a>\n		</li>\n		<li>Courier Prime font by\n			<a href=\"http://quoteunquoteapps.com/courierprime/\" target=\"_blank\">Quote-Unquote Apps</a>\n		</li>\n		<li>JS libs:\n			<ul>\n				<li>\n					<a href=\"https://github.com/mattdaly/Fountain.js\" target=\"_blank\">Fountain.js</a>\n				</li>\n				<li>\n					<a href=\"https://github.com/eligrey/FileSaver.js\" target=\"_blank\">FileSaver.js</a>\n				</li>\n				<li>\n					<a href=\"http://github.com/jonnyreeves/js-logger\" target=\"_blank\">js-logger</a>\n				</li>\n				<li>\n					<a href=\"http://pdfkit.org/\" target=\"_blank\">PDFKit</a>\n				</li>\n				<li>\n					<a href=\"http://codemirror.net/\" target=\"_blank\">CodeMirror</a>\n				</li>\n				<li>\n					<a href=\"http://jquery.com/\" target=\"_blank\">jQuery</a>\n				</li>\n				<li>\n					<a href=\"http://d3js.org/\" target=\"_blank\">d3.js</a>\n				</li>\n				<li>\n					<a href=\"http://handlebarsjs.com/\" target=\"_blank\">Handlebars</a>\n				</li>\n				<li>\n					<a href=\"https://github.com/jrburke/almond\" target=\"_blank\">Almond</a>\n				</li>\n				<li>\n					<a href=\"http://requirejs.org/\" target=\"_blank\">RequireJS</a>\n				</li>\n				<li>\n					<a href=\"http://www.jstree.com/\" target=\"_blank\">jstree</a>\n				</li>\n				<li>\n					<a href=\"http://trentrichardson.com/Impromptu/\" target=\"_blank\">JQuery Impromptu</a>\n				</li>\n				<li>\n					<a href=\"https://mozilla.github.io/pdf.js/\" target=\"_blank\">PDF.js</a>\n				</li>\n			</ul>\n		</li>\n	</ul>\n</p>\n<script>\nrequire(['plugins/info'],function(info){\n	$('#download-link').click(info.download_clicked);\n});\n</script>";
        return buffer;
    });

    this["JST"]["templates/plugins/open.hbs"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};
        var buffer = "",
            stack1, helper, functionType = "function",
            escapeExpression = this.escapeExpression,
            self = this;

        function program1(depth0, data) {

            var buffer = "",
                stack1, helper;
            buffer += "\n	<li>\n		<img src=\"";
            if (helper = helpers.static_path) {
                stack1 = helper.call(depth0, {
                    hash: {},
                    data: data
                });
            } else {
                helper = (depth0 && depth0.static_path);
                stack1 = typeof helper === functionType ? helper.call(depth0, {
                    hash: {},
                    data: data
                }) : helper;
            }
            buffer += escapeExpression(stack1) +
                "gfx/icons/other/reuse.svg\" class=\"icon\" />Last editor's content:&nbsp;\n		<a href=\"#\" open-action=\"last\">" +
                escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.last_used)), stack1 == null || stack1 === false ? stack1 : stack1.title)), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) +
                "</a>&nbsp;(" +
                escapeExpression(((stack1 = ((stack1 = (depth0 && depth0.last_used)), stack1 == null || stack1 === false ? stack1 : stack1.date)), typeof stack1 === functionType ? stack1.apply(depth0) : stack1)) +
                ")</li>";
            return buffer;
        }

        buffer += "<h1>Start&nbsp;<div class=\"info-icon\" section=\"open-start\" /></h1>\n<p class=\"info-content\" section=\"open-start\">You can open a .fountain or .fdx file (it will be converted to Fountain), or use one of the samples below.</p>\n<ul class=\"no-bullets\">\n	<li>\n		<img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/new.svg\" class=\"icon\" />\n		<a href=\"#\" open-action=\"new\">Create new</a>\n	</li>\n	<li>\n		<img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/load.svg\" class=\"icon\" />\n		<a href=\"#\" open-action=\"open\">Load file</a>\n	</li>\n	<li>\n		<img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/dropbox.svg\" class=\"icon\" />\n		<a href=\"#\" open-action=\"dropbox\">Open from Dropbox</a>\n	</li>\n	<li>\n		<img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/gd.svg\" class=\"icon\" />\n		<a href=\"#\" open-action=\"googledrive\">Open from Google Drive</a>\n	</li>";
        stack1 = helpers['if'].call(depth0, ((stack1 = (depth0 && depth0.last_used)), stack1 == null || stack1 === false ? stack1 : stack1.date), {
            hash: {},
            inverse: self.noop,
            fn: self.program(1, program1, data),
            data: data
        });
        if (stack1 || stack1 === 0) {
            buffer += stack1;
        }
        buffer += "</ul>\n<h1>Samples</h1>\n<ul>\n	<li>\n		<a href=\"#\" open-action=\"sample\" value=\"brick_and_steel\">\n			<i>Brick & Steel</i>\n		</a>&nbsp;by Stu Maschwitz</li>\n	<li>\n		<a href=\"#\" open-action=\"sample\" value=\"my_living_memory\">\n			<i>My Living Memory</i>\n		</a>&nbsp;by Dan Rahmel</li>\n	<li>\n		<a href=\"#\" open-action=\"sample\" value=\"printing_trouble\">\n			<i>Printing Trouble</i>\n		</a>&nbsp;by Dan Rahmel</li>\n</ul>\n<div id=\"open-file-wrapper\"></div>\n<!-- interactions -->\n<script>\n	require(['jquery', 'plugins/open'], function($, open) {\n		var reset_file_input = function() {\n			$('#open-file-wrapper').empty();\n			$('#open-file-wrapper').html('<input id=\"open-file\" type=\"file\" style=\"display:none\" />');\n			$(\"#open-file\").change(function() {\n				var selected_file = $('#open-file').get(0).files[0];\n				open.open_file(selected_file);\n				reset_file_input();\n			});\n		}\n\n		$('a[open-action=\"open\"]').click(function() {\n			open.open_file_dialog()\n		});\n\n		$('a[open-action=\"new\"]').click(open.create_new);\n		$('a[open-action=\"sample\"]').click(function() {\n			var name = $(this).attr('value');\n			open.open_sample(name);\n		});\n		$('a[open-action=\"last\"]').click(open.open_last_used);\n		\n		open.open_file_dialog.add(function() {\n			$(\"#open-file\").click();\n		});\n		\n		$('a[open-action=\"googledrive\"]').click(open.open_from_google_drive);\n		$('a[open-action=\"dropbox\"]').click(open.open_from_dropbox);\n		\n		open.activate.add(function(){			\n			if (open.is_dropbox_available()) {\n				$('a[open-action=\"dropbox\"]').parent().show();\n			} else {\n				$('a[open-action=\"dropbox\"]').parent().hide();\n			}\n\n			if (open.is_google_drive_available()) {\n				$('a[open-action=\"googledrive\"]').parent().show();\n			} else {\n				$('a[open-action=\"googledrive\"]').parent().hide();\n			}\n		});\n\n		reset_file_input();\n\n	});\n</script>";
        return buffer;
    });

    this["JST"]["templates/plugins/preview.hbs"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};
        var buffer = "",
            stack1, helper, functionType = "function",
            escapeExpression = this.escapeExpression;


        buffer += "<h1 style=\"float:left\">Preview\n	<span class=\"info-icon\" section=\"preview-info\" />\n</h1>\n<div class=\"header-menu\">\n	<span>save .pdf: <a href=\"#\" action=\"save-pdf\"><img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/download.svg\" class=\"icon small-icon\" title=\"Download PDF\" /></a></span>\n	<span><a href=\"#\" action=\"save-dropbox-pdf\"><img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/dropbox.svg\" class=\"icon small-icon\" title=\"Upload PDF to Dropbox\" /></a></span>\n	<span><a href=\"#\" action=\"save-gd-pdf\"><img src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/other/gd.svg\" class=\"icon small-icon\" title=\"Upload PDF to Google Drive\" /></a></span>\n</div><div style=\"clear:both\"/>\n<p class=\"info-content\" section=\"preview-info\">Can't see anything? You need a PDF plugin in your browser. (You can download pdf from\n	<a class=\"switch\" href=\"#\" plugin=\"save\">here</a>)</p>\n<div id=\"pdf-preview-iframe-container\" class=\"to-the-bottom\" style=\"display:none\"></div>\n<div id=\"pdf-preview-pdfjs-container\" class=\"to-the-bottom\" style=\"overflow: auto\">\n	<div>\n		<a href=\"#\"><img id=\"zoomin\" src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/viewer/zoomin.svg\" class=\"icon small-icon\" title=\"Zoom in\" /></a>\n		<a href=\"#\"><img id=\"zoomout\" src=\"";
        if (helper = helpers.static_path) {
            stack1 = helper.call(depth0, {
                hash: {},
                data: data
            });
        } else {
            helper = (depth0 && depth0.static_path);
            stack1 = typeof helper === functionType ? helper.call(depth0, {
                hash: {},
                data: data
            }) : helper;
        }
        buffer += escapeExpression(stack1) +
            "gfx/icons/viewer/zoomout.svg\" class=\"icon small-icon\" title=\"Zoom out\" /></a>\n	</div>\n	<div id=\"pdfjs-viewer\" class=\"pdfjs-viewer to-the-bottom\"></div>\n</div>\n<!-- scripts -->\n<script>\n	require(['jquery', 'plugins/preview', 'modules/data', 'utils/pdfjsviewer'], function($, preview, data, pdfjs_viewer) {\n\n		$('#next').click(pdfjs_viewer.next);\n		$('#prev').click(pdfjs_viewer.prev);\n		$('#zoomin').click(pdfjs_viewer.zoomin);\n		$('#zoomout').click(pdfjs_viewer.zoomout);\n		pdfjs_viewer.set_container(document.getElementById('pdfjs-viewer'));\n\n		preview.refresh.add(function() {\n			if (data.config.pdfjs_viewer) {\n				$('#pdf-preview-iframe-container').hide();\n				$('#pdf-preview-pdfjs-container').show();\n			}\n			else {\n				$('#pdf-preview-iframe-container').show();\n				$('#pdf-preview-pdfjs-container').hide();\n			}\n\n			$('#pdf-preview-iframe-container').html('<p>Loading preview...</p><embed id=\"pdf-preview-iframe\" style=\"height: 100%; width: 100%; display:none\"  type=\"application/pdf\"></embed>');\n\n			setTimeout(function() {\n				preview.get_pdf(function (result) {\n					$(\"#pdf-preview-iframe-container p\").remove();\n					if (data.config.pdfjs_viewer) {\n						pdfjs_viewer.from_blob(result.blob);\n					}\n					else {\n						$(\"#pdf-preview-iframe\").attr('src', result.url).css('display', 'block');\n					}\n				});\n			}, 0);\n\n		});\n		preview.deactivate.add(function() {\n			$(\"#pdf-preview-iframe\").remove();\n		});\n	});\n</script>";
        return buffer;
    });

    this["JST"]["templates/plugins/save.hbs"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};



        return "<h1>Save</h1>\n<ul>\n	<li>as .fountain:\n		<a href=\"#\" action=\"save-fountain\">download</a>\n		<span>&nbsp;|&nbsp;\n			<a href=\"#\" action=\"save-dropbox-fountain\">send to Dropbox</a>\n		</span>\n		<span>&nbsp;|&nbsp;\n			<a href=\"#\" action=\"save-gd-fountain\">send to Google Drive</a>			\n			<div id=\"google-drive-div-fountain\"></div>\n			<div id=\"container\">\n			<div class=\"g-savetodrive\"\n  \n   data-filename=\"script.pdf\"\n   data-sitename=\"'afterwriting\">\n</div></div>\n		</span>\n	</li>\n	<li>as .pdf:\n		<a href=\"#\" action=\"save-pdf\">download</a>\n		<span>&nbsp;|&nbsp;\n			<a href=\"#\" action=\"save-dropbox-pdf\">send to Dropbox</a>\n		</span>\n		<span>&nbsp;|&nbsp;\n			<a href=\"#\" action=\"save-gd-pdf\">send to Google Drive</a>\n			<div id=\"google-drive-div-pdf\"></div>\n		</span>\n	</li>\n</ul>\n<!--- script -->\n<script>\n	require(['jquery', 'plugins/save'], function($, save) {\n		$(document).ready(function() {\n			$('a[action=\"save-fountain\"]').click(save.save_as_fountain);\n			$('a[action=\"save-dropbox-fountain\"]').click(save.dropbox_fountain);\n			$('a[action=\"save-gd-fountain\"]').click(save.google_drive_fountain);\n\n			$('a[action=\"save-pdf\"]').click(save.save_as_pdf);\n			$('a[action=\"save-dropbox-pdf\"]').click(save.dropbox_pdf);				\n			$('a[action=\"save-gd-pdf\"]').click(save.google_drive_pdf);\n\n			save.activate.add(function() {\n				if (!save.is_dropbox_available()) {\n					$('a[action=\"save-dropbox-pdf\"], a[action=\"save-dropbox-fountain\"]').parent().hide();\n				} else {\n					$('a[action=\"save-dropbox-pdf\"], a[action=\"save-dropbox-fountain\"]').parent().show();\n				}\n				if (!save.is_google_drive_available()) {\n					$('a[action=\"save-gd-pdf\"], a[action=\"save-gd-fountain\"]').parent().hide();\n				} else {\n					$('a[action=\"save-gd-pdf\"], a[action=\"save-gd-fountain\"]').parent().show();\n				}\n			});\n			\n\n		});\n	});\n</script>";
    });

    this["JST"]["templates/plugins/settings.hbs"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};



        return "<style>\n	.plugin-content[plugin=\"settings\"] .buttons {\n		float: right;\n	}\n	.plugin-content[plugin=\"settings\"] .buttons p {\n		margin: 0;\n	}\n	.plugin-content[plugin=\"settings\"] table {\n		margin: auto;\n		width: 100%;\n	}\n	.plugin-content[plugin=\"settings\"] tr td:nth-child(1) {\n		text-align: right;\n		border-right: 0;\n	}\n	.plugin-content[plugin=\"settings\"] tr td:nth-child(2) {\n		text-align: left;\n		width: 50%;\n		border-left-width: 0;\n	}\n	.settings-panel {\n		width: 90%;\n		margin: auto;\n	}\n	\n</style>\n<h1>Settings\n	<span class=\"info-icon\" section=\"settings-info\" />\n</h1>\n<p class=\"info-content\" section=\"settings-info\">You can change configuration here. Some settings (e.g. page size, double space between scenes) may affect statistics which are based on assumption that 1 page = 1 minute of a movie .</p>\n<div class=\"settings-panel\">\n	<table>\n		<tbody>\n			<tr>\n				<th colspan=\"2\">Print</th>\n			</tr>\n			<tr>\n				<td>Page size:</td>\n				<td>\n					<select setting=\"print_profile\">\n						<option value=\"a4\">A4</option>\n						<option value=\"usletter\">US letter</option>\n					</select>\n				</td>\n			</tr>\n			<tr>\n				<td>Print title page:</td>\n				<td>\n					<input setting=\"print_title_page\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Print sections:</td>\n				<td>\n					<input setting=\"print_sections\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Print synopsis:</td>\n				<td>\n					<input setting=\"print_synopsis\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Print notes:</td>\n				<td>\n					<input setting=\"print_notes\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Print scene headers:</td>\n				<td>\n					<input setting=\"print_headers\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Print action:</td>\n				<td>\n					<input setting=\"print_actions\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Print dialogue:</td>\n				<td>\n					<input setting=\"print_dialogues\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Header:</td>\n				<td>\n					<input setting=\"print_header\" type=\"text\">\n				</td>\n			</tr>\n			<tr>\n				<td>Footer:</td>\n				<td>\n					<input setting=\"print_footer\" type=\"text\">\n				</td>\n			</tr>\n			<tr>\n				<td>Watermark:</td>\n				<td>\n					<input setting=\"print_watermark\" type=\"text\">\n				</td>\n			</tr>\n			<tr>\n				<th colspan=\"2\">Layout</th>\n			</tr>\n			<tr>\n				<td>Split dialogue between pages:</td>\n				<td>\n					<input setting=\"split_dialogue\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Accept dual dialogue:</td>\n				<td>\n					<input setting=\"use_dual_dialogue\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Double space between scenes:</td>\n				<td>\n					<input setting=\"double_space_between_scenes\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Page break after a scene:</td>\n				<td>\n					<input setting=\"each_scene_on_new_page\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Prefix sections with numbers:</td>\n				<td>\n					<input setting=\"number_sections\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Embolden scene headers:</td>\n				<td>\n					<input setting=\"embolden_scene_headers\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Scene numbers:</td>\n				<td>\n					<select setting=\"scenes_numbers\">\n						<option value=\"none\">none</option>\n						<option value=\"left\">left</option>\n						<option value=\"right\">right</option>\n						<option value=\"both\">both</option>\n					</select>\n				</td>\n			</tr>\n			<tr>\n				<td>Scene continuation (the bottom of a page):</td>\n				<td>\n					<input setting=\"scene_continuation_bottom\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Scene continuation(the top of the next page):</td>\n				<td>\n					<input setting=\"scene_continuation_top\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<th colspan=\"2\">Text</th>\n			</tr>\n			<tr>\n				<td>Override <i>(MORE)</i> text to:</td>\n				<td>\n					<input setting=\"text_more\" type=\"text\">\n				</td>\n			</tr>\n			<tr>\n				<td>Override <i>(CONT'D)</i> text to:</td>\n				<td>\n					<input setting=\"text_contd\" type=\"text\">\n				</td>\n			</tr>\n			<tr>\n				<td>Override <i>CONTINUED</i> (scene continuation) text to:</td>\n				<td>\n					<input setting=\"text_scene_continued\" type=\"text\">\n				</td>\n			</tr>\n			<tr>\n				<th colspan=\"2\">Miscellaneous</th>\n			</tr>\n			<tr>\n				<td>Show background image:</td>\n				<td>\n					<input setting=\"show_background_image\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>Load last opened on startup:</td>\n				<td>\n					<input setting=\"load_last_opened\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<th colspan=\"2\">Statistics</th>\n			</tr>\n			<tr>\n				<td>Keep last scene slugline time of day if not specified:</td>\n				<td>\n					<input setting=\"stats_keep_last_scene_time\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>\n					<em>\"Who with who\"</em>max characters:</td>\n				<td>\n					<input setting=\"stats_who_with_who_max\" type=\"input\" style=\"width: 50px\">\n				</td>\n			</tr>\n			<tr>\n				<th colspan=\"2\">Experimental</th>\n			</tr>\n			<tr>\n				<td>GoogleDrive lazy loading:</td>\n				<td>\n					<input setting=\"cloud_lazy_loading\" type=\"checkbox\">\n				</td>\n			</tr>\n			<tr>\n				<td>JavaScript PDF viewer:</td>\n				<td>\n					<input setting=\"pdfjs_viewer\" type=\"checkbox\">\n				</td>\n			</tr>\n		</tbody>\n	</table>\n</div>\n<script>\n	require(['plugins/settings'], function(settings) {\n\n		var data_to_components = function() {\n			var c = settings.get_config();\n			$('*[setting=\"show_background_image\"]').prop('checked', c.show_background_image);\n			$('*[setting=\"print_title_page\"]').prop('checked', c.print_title_page);\n			$('*[setting=\"embolden_scene_headers\"]').prop('checked', c.embolden_scene_headers);\n			$('*[setting=\"load_last_opened\"]').prop('checked', c.load_last_opened);\n			$('*[setting=\"double_space_between_scenes\"]').prop('checked', c.double_space_between_scenes);\n			$('*[setting=\"each_scene_on_new_page\"]').prop('checked', c.each_scene_on_new_page);\n			$('*[setting=\"split_dialogue\"]').prop('checked', c.split_dialogue);\n			$('*[setting=\"print_sections\"]').prop('checked', c.print_sections);\n			$('*[setting=\"print_synopsis\"]').prop('checked', c.print_synopsis);\n			$('*[setting=\"print_notes\"]').prop('checked', c.print_notes);\n			$('*[setting=\"print_headers\"]').prop('checked', c.print_headers);\n			$('*[setting=\"print_actions\"]').prop('checked', c.print_actions);\n			$('*[setting=\"print_dialogues\"]').prop('checked', c.print_dialogues);\n			$('*[setting=\"number_sections\"]').prop('checked', c.number_sections);\n			$('*[setting=\"use_dual_dialogue\"]').prop('checked', c.use_dual_dialogue);\n			$('*[setting=\"stats_keep_last_scene_time\"]').prop('checked', c.stats_keep_last_scene_time);\n			$('*[setting=\"scene_continuation_top\"]').prop('checked', c.scene_continuation_top);\n			$('*[setting=\"scene_continuation_bottom\"]').prop('checked', c.scene_continuation_bottom);\n			$('*[setting=\"cloud_lazy_loading\"]').prop('checked', c.cloud_lazy_loading);\n			$('*[setting=\"pdfjs_viewer\"]').prop('checked', c.pdfjs_viewer);\n			$('*[setting=\"print_profile\"]').val(c.print_profile);\n			$('*[setting=\"stats_who_with_who_max\"]').val(c.stats_who_with_who_max);\n			$('*[setting=\"print_header\"]').val(c.print_header);\n			$('*[setting=\"print_footer\"]').val(c.print_footer);\n			$('*[setting=\"print_watermark\"]').val(c.print_watermark);\n			$('*[setting=\"scenes_numbers\"]').val(c.scenes_numbers);\n			$('*[setting=\"text_more\"]').val(c.text_more);\n			$('*[setting=\"text_contd\"]').val(c.text_contd);\n			$('*[setting=\"text_scene_continued\"]').val(c.text_scene_continued);\n		};\n\n		var components_to_data = function() {\n			var c = settings.get_config();\n			c.show_background_image = $('*[setting=\"show_background_image\"]').is(':checked');\n			c.print_title_page = $('*[setting=\"print_title_page\"]').is(':checked');\n			c.embolden_scene_headers = $('*[setting=\"embolden_scene_headers\"]').is(':checked');\n			c.load_last_opened = $('*[setting=\"load_last_opened\"]').is(':checked');\n			c.double_space_between_scenes = $('*[setting=\"double_space_between_scenes\"]').is(':checked');\n			c.each_scene_on_new_page = $('*[setting=\"each_scene_on_new_page\"]').is(':checked');\n			c.split_dialogue = $('*[setting=\"split_dialogue\"]').is(':checked');\n			c.print_sections = $('*[setting=\"print_sections\"]').is(':checked');\n			c.print_synopsis = $('*[setting=\"print_synopsis\"]').is(':checked');\n			c.print_notes = $('*[setting=\"print_notes\"]').is(':checked');\n			c.print_headers = $('*[setting=\"print_headers\"]').is(':checked');\n			c.print_actions = $('*[setting=\"print_actions\"]').is(':checked');\n			c.print_dialogues = $('*[setting=\"print_dialogues\"]').is(':checked');\n			c.number_sections = $('*[setting=\"number_sections\"]').is(':checked');\n			c.use_dual_dialogue = $('*[setting=\"use_dual_dialogue\"]').is(':checked');\n			c.stats_keep_last_scene_time = $('*[setting=\"stats_keep_last_scene_time\"]').is(':checked');\n			c.scene_continuation_top = $('*[setting=\"scene_continuation_top\"]').is(':checked');\n			c.scene_continuation_bottom = $('*[setting=\"scene_continuation_bottom\"]').is(':checked');\n			c.cloud_lazy_loading = $('*[setting=\"cloud_lazy_loading\"]').is(':checked');\n			c.pdfjs_viewer = $('*[setting=\"pdfjs_viewer\"]').is(':checked');\n			c.stats_who_with_who_max = parseInt($('*[setting=\"stats_who_with_who_max\"]').val());\n			c.print_header = $('*[setting=\"print_header\"]').val();\n			c.print_footer = $('*[setting=\"print_footer\"]').val();\n			c.print_watermark = $('*[setting=\"print_watermark\"]').val();\n			c.print_profile = $('*[setting=\"print_profile\"]').val();\n			c.scenes_numbers = $('*[setting=\"scenes_numbers\"]').val();\n			c.text_more = $('*[setting=\"text_more\"]').val();\n			c.text_contd = $('*[setting=\"text_contd\"]').val();\n			c.text_scene_continued = $('*[setting=\"text_scene_continued\"]').val();\n			settings.save();\n		};\n\n		$('.plugin-content[plugin=\"settings\"]').select('input, option').on('change keyup', function() {\n			$('.settings-save').removeClass('inactive');\n			components_to_data();\n		});\n\n		settings.activate.add(function() {\n			data_to_components();\n		});\n\n	});\n</script>";
    });

    this["JST"]["templates/plugins/stats.hbs"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};



        return "<style>\n	.plugin-content[plugin=\"stats\"] svg {\n		margin: 20px 0 20px 0;\n	}\n	\n</style>\n\n<h2 style=\"margin-top: 30px; margin-left: 0\">Who talks with who (by number of scenes)\n	<div class=\"info-icon\" section=\"stats-who\" />\n</h2>\n<p class=\"info-content\" section=\"stats-who\">Each character is represented by a circle (max. 10 characters). If characters are connected with a line that means they are talking in the same scene. Thicker the line - more scenes together. Hover the mouse cursor over a character circle to see how many dialogues scenes that character have with other characters.</p>\n<div id=\"who-with-who\" style=\"text-align: center\"></div>\n<h2 style=\"margin-top: 30px; margin-left: 0\">Script pulse\n	<div class=\"info-icon\" section=\"stats-tempo\" />\n</h2>\n<p class=\"info-content\" section=\"stats-tempo\">Short scenes and short action/dialogue blocks bump the tempo up. Long scenes and long blocks set it back.</p>\n<div id=\"stats-tempo\"></div>\n<h2 style=\"margin-top: 30px; margin-left: 0\">Scene length\n	<div class=\"info-icon\" section=\"stats-scene-length\" />\n</h2>\n<p class=\"info-content\" section=\"stats-scene-length\">Each bar represent one scene (white bars for day scenes, black bars for night scenes). Hover the mouse cursor over a bar to see estimated time of a scene. You can click on a bar to jump to selected scene in the editor.</p>\n<span>chart colours: </span><select id=\"stats-scene-length-type\">\n	<option value=\"day_night\">DAY/NIGHT</option>\n	<option value=\"int_ext\">INT./EXT.</option>\n</select>\n<div id=\"stats-scene-length\" width=\"100%\"></div>\n<h2 style=\"margin-top: 30px; margin-left: 0\">Locations breakdown\n	<div class=\"info-icon\" section=\"stats-locations-breakdown\" />\n</h2>\n<p class=\"info-content\" section=\"stats-locations-breakdown\">Blocks on the top strip represent amount of time spent in a location. If a location occurs more than once in the script, it's highlighted by a colour (white colour is used for each location occurringonly once).\n	<br />Pie chart below shows time distribution for each location. Mouse over the blocks to see corresponding data on the pie chart (and vice versa).</p>\n<div id=\"locations-breakdown\" />\n<h2 style=\"margin-top: 30px; margin-left: 0\">Page balance\n	<div class=\"info-icon\" section=\"stats-page-balance\" />\n</h2>\n<p class=\"info-content\" section=\"stats-page-balance\">Shows balance between action time and dialogue time on each page. Click on a page to jump to the editor.</p>\n<div id=\"stats-page-balance\"></div>\n<h2 style=\"margin-top: 30px; margin-left: 0\">Days and nights\n	<div class=\"info-icon\" section=\"stats-days-nights\" />\n</h2>\n<p class=\"info-content\" section=\"stats-days-nights\">Pie chart representing day vs night scenes breakdown. Hover over sections to see number of day/night scenes.</p>\n<div id=\"stats-days-and-nights\" style=\"text-align: center\"></div>\n<h2 style=\"margin-top: 30px; margin-left: 0\">INT. vs EXT.\n	<div class=\"info-icon\" section=\"stats-int-ext\" />\n</h2>\n<p class=\"info-content\" section=\"stats-int-ext\">Pie chart representing interior vs exterior scenes breakdown. Hover over sections to see number of int/ext scenes.</p>\n<div id=\"stats-int-ext\" style=\"text-align: center\"></div>\n<!--- scripts -->\n<script>\n	require(['jquery', 'd3', 'plugins/stats', 'utils/layout', 'utils/helper', 'modules/charts', ], function($, d3, stats, layout, helper, charts) {\n\n		var render = function() {\n			charts.spider_chart.render('#who-with-who', stats.data.who_with_who.characters, stats.data.who_with_who.links, {\n				label: 'name'\n			});\n\n			charts.bar_chart.render('#stats-scene-length', stats.data.scenes, {\n				tooltip: function(d) {\n					return d.header + ' (time: ' + helper.format_time(helper.lines_to_minutes(d.length)) + ')'\n				},\n				value: 'length',\n				color: function(d) {\n					if ($('#stats-scene-length-type').val() === \"int_ext\") {\n						if (d.location_type === 'mixed') {\n							return '#777777';\n						} else if (d.location_type === 'int') {\n							return '#ffffff';\n						} else if (d.location_type === 'ext') {\n							return '#000000';\n						} else if (d.location_type === 'other') {\n							return '#444444';\n						}\n					}\n\n					if (d.type == 'day') {\n						return '#ffffff';\n					} else if (d.type == 'night') {\n						return '#222222';\n					} else {\n						return '#777777';\n					}\n				},\n				bar_click: function(d) {\n					if (!layout.small) {\n						stats.goto(d.token.line);\n					}\n				}\n			});\n\n			charts.pie_chart.render('#stats-days-and-nights', stats.data.days_and_nights, {\n				tooltip: function(d) {\n					return d.data.label + ': ' + d.data.value + (d.data.value == 1 ? ' scene' : ' scenes')\n				},\n				value: 'value',\n				color: function(d) {\n					if (d.data.label == 'DAY') {\n						return '#ffffff';\n					} else if (d.data.label == 'NIGHT') {\n						return '#222222';\n					} else if (d.data.label == 'DAWN') {\n						return '#777777';\n					} else if (d.data.label == 'DUSK') {\n						return '#444444';\n					} else {\n						return '#aaaaaa';\n					}\n				}\n			});\n\n			var int_ext_labels = {\n				int: 'INT.',\n				ext: 'EXT.',\n				mixed: 'INT./EXT.',\n				other: 'OTHER'\n			};\n\n			charts.pie_chart.render('#stats-int-ext', stats.data.int_and_ext, {\n				tooltip: function(d) {\n					return int_ext_labels[d.data.label] + ': ' + d.data.value + (d.data.value == 1 ? ' scene' : ' scenes')\n				},\n				value: 'value',\n				color: function(d) {\n					if (d.data.label == 'mixed') {\n						return '#777777';\n					} else if (d.data.label == 'int') {\n						return '#ffffff';\n					} else if (d.data.label == 'ext') {\n						return '#000000';\n					} else if (d.data.label == 'other') {\n						return '#444444';\n					}\n				}\n			});\n\n			charts.page_balance_chart.render('#stats-page-balance', stats.data.page_balance, {\n				page_click: function(d) {\n					if (!layout.small) {\n						stats.goto(d.first_line.token.line);\n					}\n				}\n			});\n\n			charts.line_chart.render('#stats-tempo', stats.data.tempo, {\n				value: 'tempo',\n				tooltip: function(d, i) {\n					if (i === stats.data.tempo.length - 1) {\n						return '';\n					}\n					return d.scene + '<br />...' + d.line + '... ';\n				},\n				click: function(d) {\n					if (!layout.small) {\n						stats.goto(d.line_no);\n					}\n				}\n			});\n\n			charts.locations_breakdown.render('#locations-breakdown', stats.data.locations_breakdown);\n\n		}\n\n		stats.refresh.add(render);\n		$('#stats-scene-length-type').on('change', render);\n		\n		layout.toggle_expand.add(function() {\n			if (stats.is_active) {\n				render();\n			}\n		});\n\n	});\n</script>";
    });

    this["JST"]["templates/samples/brick_and_steel.fountain"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};


	
        return "Title:\r\n	_**BRICK & STEEL**_\r\n	_**FULL RETIRED**_\r\nCredit: Written by\r\nAuthor: Stu Maschwitz\r\nSource: Story by KTM\r\nDraft date: 1/27/2012\r\nContact:\r\n	Next Level Productions\r\n	1588 Mission Dr.\r\n	Solvang, CA 93463\r\n\r\nEXT. BRICK'S PATIO - DAY\r\n\r\nA gorgeous day.  The sun is shining.  But BRICK BRADDOCK, retired police detective, is sitting quietly, contemplating -- something.\r\n\r\nThe SCREEN DOOR slides open and DICK STEEL, his former partner and fellow retiree, emerges with two cold beers.\r\n\r\nSTEEL\r\nBeer's ready!\r\n\r\nBRICK\r\nAre they cold?\r\n\r\nSTEEL\r\nDoes a bear crap in the woods?\r\n\r\nSteel sits.  They laugh at the dumb joke.\r\n\r\nSTEEL\r\n(beer raised)\r\nTo retirement.\r\n\r\nBRICK\r\nTo retirement.\r\n\r\nThey drink long and well from the beers.\r\n\r\nAnd then there's a long beat.\r\nLonger than is funny.\r\nLong enough to be depressing.\r\n\r\nThe men look at each other.\r\n\r\nSTEEL\r\nScrew retirement.\r\n\r\nBRICK ^\r\nScrew retirement.\r\n\r\nSMASH CUT TO:\r\n\r\nINT. TRAILER HOME - DAY\r\n\r\nThis is the home of THE BOY BAND, AKA DAN and JACK.  They too are drinking beer, and counting the take from their last smash-and-grab.  Money, drugs, and ridiculous props are strewn about the table.\r\n\r\n			JACK\r\n		(in Vietnamese, subtitled)\r\n	*Did you know Brick and Steel are retired?*\r\n\r\n			DAN\r\n	Then let's retire them.\r\n	_Permanently_.\r\n\r\nJack begins to argue vociferously in Vietnamese (?), But mercifully we...\r\n\r\n				CUT TO:\r\n\r\nEXT. BRICK'S POOL - DAY\r\n\r\nSteel, in the middle of a heated phone call:\r\n\r\nSTEEL\r\nThey're coming out of the woodwork!\r\n(pause)\r\nNo, everybody we've put away!\r\n(pause)\r\nPoint Blank Sniper?\r\n\r\n.SNIPER SCOPE POV\r\n\r\nFrom what seems like only INCHES AWAY.  _Steel's face FILLS the *Leupold Mark 4* scope_.\r\n\r\nSTEEL\r\nThe man's a myth!\r\n\r\nSteel turns and looks straight into the cross-hairs.\r\n\r\nSTEEL\r\n(oh crap)\r\nHello...\r\n\r\nCUT TO:\r\n\r\n.OPENING TITLES\r\n\r\n> BRICK BRADDOCK <\r\n> & DICK STEEL IN <\r\n\r\n> BRICK & STEEL <\r\n> FULL RETIRED <\r\n\r\nSMASH CUT TO:\r\n\r\nEXT. WOODEN SHACK - DAY\r\n\r\nCOGNITO, the criminal mastermind, is SLAMMED against the wall.\r\n\r\nCOGNITO\r\nWoah woah woah, Brick and Steel!\r\n\r\nSure enough, it's Brick and Steel, roughing up their favorite usual suspect.\r\n\r\nCOGNITO\r\nWhat is it you want with me, DICK?\r\n\r\nSteel SMACKS him.\r\n\r\nSTEEL\r\nWho's coming after us?\r\n\r\nCOGNITO\r\nEveryone's coming after you mate!  Scorpio, The Boy Band, Sparrow, Point Blank Sniper...\r\n\r\nAs he rattles off the long list, Brick and Steel share a look.  This is going to be BAD.\r\n\r\nCUT TO:\r\n\r\nINT. GARAGE - DAY\r\n\r\nBRICK and STEEL get into Mom's PORSCHE, Steel at the wheel.  They pause for a beat, the gravity of the situation catching up with them.\r\n\r\nBRICK\r\nThis is everybody we've ever put away.\r\n\r\nSTEEL\r\n(starting the engine)\r\nSo much for retirement!\r\n\r\nThey speed off.  To destiny!\r\n\r\nCUT TO:\r\n\r\nEXT. PALATIAL MANSION - DAY\r\n\r\nAn EXTREMELY HANDSOME MAN drinks a beer.  Shirtless, unfortunately.\r\n\r\nHis minion approaches offscreen:\r\n\r\nMINION\r\nWe found Brick and Steel!\r\n\r\nHANDSOME MAN\r\nI want them dead.  DEAD!\r\n\r\nBeer flies.\r\n\r\n> BURN TO PINK.\r\n\r\n> THE END <\r\n";
    });

    this["JST"]["templates/samples/my_living_memory.fountain"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};



        return "title: My Living Memory \r\ncredit: by \r\nauthor: Dan Rahmel\r\ncopyright: (c) Copyright Dan Rahmel 2006\r\n\r\nFADE IN:\r\n \r\nINT. MATTHEW'S BEDROOM - DAY\r\n\r\nThe small bedroom is painted sky blue and has toys scattered all around. JOE sits on the floor with his 7 year-old son, MATTHEW. In cheap black shoes, a work shirt with his name on the patch, and oil-stained pants, Joe is dressed for work. \r\n\r\nThey play together. Assembling a puzzle. Matthew loads cargo with a small crane made with an erector set. Legos are used to create a fierce dinosaur. Occasionally Joe's wife, KATE, looks in on them. \r\n\r\nINT. MAIN ROOM - DAY\r\n\r\nClosing the door to his son's bedroom, Joe walks into the living room. The small house has a main room that combines the living room and dining area, along with an attached convenience kitchen off to the right. In the kitchen, Kate stands peeling potatoes.  \r\n\r\nJOE\r\nHe's down for his nap.\r\n\r\nKATE\r\nHe didn't argue?\r\n\r\nJOE\r\n(smiles)\r\nWhat do you think?\r\n\r\nKate looks at Joe skeptically.\r\n\r\nKATE\r\nI think you had to bribe him.\r\n\r\nJOE\r\nWe just had a man-to-man talk and he decided to lie down.\r\n\r\nKATE\r\nAnd?\r\n\r\nJOE\r\nAnd we both agreed that it'd be best if we went out for ice cream later. \r\n\r\nKate smiles to herself and returns to her peeling.\r\n\r\nKATE\r\nI wish you didn't have to go to work today. It is a Sunday. \r\n\r\nJOE\r\nBernard's short a driver. With everyone traveling over the holiday weekend... \r\n\r\nKATE\r\nI know. I just thought it would be nice if the three of us could go to the park together. \r\n\r\nJoe walks up behind Kate and embraces her.\r\n\r\nJOE\r\nNext weekend, I promise.\r\n\r\nHe kisses her cheek and heads for the door.\r\n\r\nJOE (CONT'D)\r\nI should be home by four.\r\n\r\nKATE\r\nWe'll be at the park most of the day.\r\n\r\nJOE\r\nCould you take some pictures?\r\n\r\nJoe points to a table in the living room that holds a digital camera and a photo printer. \r\n\r\nKATE\r\nSure.\r\n\r\nJOE\r\nAt least I won't miss the whole day.\r\n\r\nJoe turns at the door.\r\n\r\nJOE (CONT'D)\r\nI love you.\r\n\r\nKATE\r\nI know that, silly.\r\n\r\nEXT. PARK PLAYGROUND - DAY\r\n\r\nThe park playground is awash with children. Slipping down the slides, climbing the jungle gym, playing in the sandbox. Kate watches Matthew play and takes pictures of him with the camera. Although there are many other kids around, whenever Kate takes a picture, Matthew is playing alone in the frame.  \r\n\r\nINT. LIVING ROOM - DAY\r\n\r\nMATTHEW\r\nI'm tired, mommy.\r\n\r\nKATE\r\nThen go lie down. \r\n\r\nMatthew begins to dash for his room.\r\n\r\nKATE (CONT'D)\r\nDon't forget to wash up first.\r\n\r\nMatthew freezes, his face drops, and he sulks toward the bathroom. \r\n\r\nAt the printer table, Kate plugs in the digital camera. After she starts it printing the day's pictures, she walks to the answering machine on the kitchen counter. \r\n\r\nThe answering machine display shows three messages. She hits\r\nthe play button.\r\n\r\nMESSAGE MACHINE\r\n(serious)\r\nHello, this is Detective Burton and I'm calling for Mrs. Randolph. Please call me back at 555-1332 as soon as possible. \r\n\r\nLooking perplexed, Kate writes down the number. The next message begins. \r\n\r\nMESSAGE MACHINE (CONT'D)\r\n(urgent)\r\nMrs. Randolph, it's urgent that I speak to you. Please call me at 555-1332 or on my cell phone at 555-2312. \r\n\r\nKate writes down the cell number and begins to show signs of alarm. \r\n\r\nMESSAGE MACHINE (CONT'D)\r\n(resigned)\r\nMrs. Randolph, please call me as soon as you get this message. My cell phone number is 555-2312. \r\n\r\nShe grabs the phone and dials the cell phone number.\r\n\r\nBURTON\r\nHello?\r\n\r\nKATE\r\nHello? This is Kate Randolph. You left several messages on my machine. \r\n\r\nBURTON\r\nMrs. Randolph, I've been trying to reach you since this morning. \r\n\r\nKATE\r\nWhat's this about?\r\n\r\nBURTON\r\nYou're husband's been in an accident.\r\n\r\nKATE\r\nWhat?\r\n\r\nBURTON\r\nOn the freeway. I'm afraid a speeding car struck your husband while he was hooking up a tow. \r\n\r\nKATE\r\nWhere is Joe?\r\n\r\nBURTON\r\nMrs. Randolph, I'm sorry to have to be the one to tell you... there's never an easy way to break this... Mrs. Randolph your husband died at Washington Hospital this morning. \r\n\r\nKATE\r\n(shaking)\r\nThat's impossible. I just saw him a few hours ago. \r\n\r\nBURTON\r\nHe was taken to the hospital after the accident. He'd been alive when they left the scene, but by the time-- \r\n\r\nKATE\r\nI'm sorry, I can't talk right now.\r\n\r\nKate hangs up the phone violently and stands staring at the phone. She puts her hand to her mouth. A wound opens deep within her. She begins sobbing as she slowly slides to the kitchen floor. \r\n\r\nDISSOLVE TO:\r\n\r\nINT. LIVING ROOM - DAY\r\n\r\nThe front door opens and Kate, wearing all black, somberly enters the living room. Matthew has on a little suit and grasps her hand tightly. Behind Kate, her friend BETTY walks into the living room and closes the door. \r\n\r\nMATTHEW\r\nCan I go play with my toys?\r\n\r\nKATE\r\nGo on.\r\n\r\nShe gives him a tender little push.\r\n\r\nMATTHEW\r\nDo I have to wash up first?\r\n\r\nKATE\r\nNo, sweetie. Not today.\r\n\r\nHe runs for his room.\r\n\r\nBETTY\r\nHow are you holding up?\r\n\r\nKATE\r\nOh, I'm OK. Can I make you a cup of coffee? \r\n\r\nBETTY\r\nThanks, that would be nice.\r\n\r\nKate walks into the kitchen and starts the coffee machine.\r\n\r\nBETTY (CONT'D)\r\nYou know that Bob would have come if he'd been in the country. \r\n\r\nKATE\r\nI know. I'm just glad you were there.\r\n\r\nBETTY\r\nHow are you and Matthew set for money? If you need any help... \r\n\r\nKATE\r\nI haven't really even thought about that yet. It's been tight since Joe lost his trucking company. He'd just started driving the tow truck to pay the mortgage. I told him we should sell some of this stuff--\r\n(waves her hand at the TV, stereo, digital camera)\r\n-- but he wouldn't even talk about it. He let the life insurance lapse because he never thought... \r\n\r\nKate begins crying and Betty starts toward the kitchen.\r\n\r\nKATE (CONT'D)\r\n(motioning stop with her hand)\r\nNo, I'm all right. I'm all right. The coffee 'll be done in a minute. I'm going to check on Matthew. \r\n\r\nKate leaves the kitchen and goes into Matthew's bedroom. While waiting for Kate's return, Betty casually strolls around the living room. Off the mantle, she picks up a picture of Joe and Kate smiling into the camera. Another of Joe, Kate, and Matthew at Disneyland. \r\n\r\nBetty moves to the table that holds the digital camera and the printer. She picks up the pictures in the printer tray and begins to look through them. \r\n\r\nReturning from Matthew's room, Kate pours two cups of coffee. She brings the cups to the dinner table and sets them on coasters. \r\n\r\nKATE (CONT'D)\r\nMatthew still asks when his Daddy's coming home. \r\n\r\nBetty sits down across from her, looking through the pictures.\r\n\r\nBETTY\r\nAre these the last pictures of Joe and Matthew together?\r\n\r\nKATE\r\nWhat do you mean?\r\n\r\nBetty hands the pictures to Kate. When Kate sees the first picture, her face drains of blood and she drops her coffee cup to the floor. \r\n\r\nBETTY\r\nWhat is it?\r\n\r\nThe top picture, originally taken in the park with only Matthew, shows Joe standing directly behind him with a hand on Matthew's shoulder. His other hand points off into the distance. \r\n\r\nKate stares at the picture in horror, not knowing what to make of it. She looks through several of the others. They all show Joe in the frame standing next to Matthew. \r\n\r\nKATE\r\nI don't understand this.\r\n\r\nBETTY\r\nWhat's wrong?\r\n\r\nKATE\r\nJoe wasn't there the morning these were taken.\r\n\r\nBETTY\r\nWhat do you mean?\r\n\r\nKATE\r\nI mean that Joe wasn't in the park that day.\r\n\r\nBetty takes back some of the pictures and looks at them closely. \r\n\r\nBETTY\r\nCould you be mistaken? Maybe these pictures were left in the camera? Maybe you took them some other day? \r\n\r\nKATE\r\nNo, I cleared the camera before I left. Besides, Joe's never been to this park. \r\n\r\nKate drops the photo she's holding and her mind tries to find an explanation where there is none. Betty scrutinizes the picture she holds. \r\n\r\nBETTY\r\nWhy is he wearing those strange clothes? \r\n\r\nKate is barely able to break out of her catatonia to speak.\r\n\r\nKATE\r\nWhat do you mean?\r\n\r\nBETTY\r\nI never remember Joe with sideburns.\r\n\r\nKATE\r\nI don't think--\r\n\r\nBETTY\r\nI've never seen him wear clothes like this either.\r\n\r\nKate tentatively picks up one of the pictures and examines it. \r\n\r\nKATE\r\nThose aren't Joe's clothes.\r\n(startled)\r\nThat's not Joe.\r\n\r\nBETTY\r\nWhat do you mean?\r\n\r\nWith a suddenness that makes Betty jump, Kate rushes into another room. When she returns, she's carrying an old photo album. She sets the album on the table and feverishly looks through it until she comes to a particular page. \r\n\r\nKATE\r\nOh my God...\r\n\r\nBetty turns the album so she can see it. In an old photo stands the man from the pictures. Same clothes. Same sideburns. \r\n\r\nBETTY\r\nWho is that?\r\n\r\nKATE\r\nThat's Joe's father.\r\n\r\nBetty takes the photo album and begins paging through it.\r\n\r\nBETTY\r\nHe was a soldier.\r\n\r\nKATE\r\nA marine. He served two tours in Vietnam. His name was Harry. Oh God, I don't think I can handle this right now. \r\n\r\nBETTY\r\nWhy do you think he's appeared in these pictures? Tell me about him. \r\n\r\nKATE\r\nJoe never really knew him. He died when Joe was very young. \r\n\r\nBETTY\r\nHow did he die?\r\n\r\nKate stares forward in a daze.\r\n\r\nKATE\r\nHe'd just come back from Vietnam. He flew in to the airport and was supposed to be on the midnight train home that night. They found his body the next day. \r\n\r\nBetty pauses and looks down.\r\n\r\nBETTY\r\nSuicide?\r\n\r\nKate shakes her head.\r\n\r\nKATE\r\nNo. No. The policeman told Joe's mother that some men tried to mug him. Steal his things. Harry fought them off and got away, but he'd been stabbed. The early shift conductor found him behind the station. \r\n\r\nBETTY\r\nWhat happened to Joe and his mother?\r\n\r\nKATE\r\nThe military stepped in and took care of them. His father's benefits even paid for Joe's business college. Joe always wanted to know about his father. A few years ago, he went to see some of the men Harry served with when he was in Vietnam. They said he'd been wounded saving a whole platoon of men on his last tour. That's how he got the Silver Star. Those stories of his father, they meant so much to Joe... \r\n\r\nThe memory of Joe's loss breaks through again and she can\r\nbarely finish the story.\r\n\r\nKATE (CONT'D)\r\nI can't tell you how much Joe regretted that he never knew his father. Now Matthew will have the same rift in his life. \r\n\r\nBetty comes around the table and hugs Kate. Slowly Kate's tears subside. \r\n\r\nKATE (CONT'D)\r\nBut what is he doing in these pictures?\r\n\r\nBetty retakes her seat and picks up some of the pictures.\r\n\r\nBETTY\r\nMaybe he's trying to tell you something about Joe. \r\n\r\nKate looks more closely at the photos.\r\n\r\nKATE\r\nWhere is he pointing?\r\n\r\nBetty pages through several of the pictures.\r\n\r\nBETTY\r\nI don't know. Somewhere outside the photo.\r\n\r\nKATE\r\nHe's always pointing in the same direction.\r\n\r\nBETTY\r\nI think you're right.\r\n\r\nResolutely, Kate gets up from the table.\r\n\r\nKATE\r\nI need you to stay here a while.\r\n\r\nBETTY\r\nWhat?\r\n\r\nRushing around the room, Kate grabs her coat and unplugs the camera from the printer. \r\n\r\nKATE\r\nI need you to watch Matthew for a while.\r\n\r\nBETTY\r\nWhere are you going?\r\n\r\nKATE\r\nBack to the park. I need to see if I can understand what he's trying to tell me. \r\n\r\nBETTY\r\nDo you think this is a good idea? Maybe you should--\r\n\r\nKate looks Betty in the eye.\r\n\r\nKATE\r\nBetty, I've lost so much already. I need to know what this is about.\r\n\r\nBETTY\r\nGo on.\r\n\r\nKATE\r\nI'll be back as soon as I can.\r\n\r\nEXT. PARK PLAYGROUND - DAY\r\n\r\nOn the playground, Kate tries to position herself where she originally took the pictures. The viewfinder on the back of the camera shows nothing unusual. She moves to another place. Nothing. \r\n\r\nKate becomes increasingly frantic. She can't see anything but the normal slides, swing set, and sand box. She tries zooming in. She changes the exposure.\r\n\r\nShe adjusts every camera setting and still nothing. Finally, she takes a picture with the camera. She presses the retrieve button to display it. \r\n\r\nStanding in the frame, a man dead more than 30 years smiles back at her. She nearly drops the camera when the image appears. Shaking but regaining control of herself, Kate takes another picture. \r\n\r\nHarry points away from the playground off to the east. Kate follows the direction his finger is pointing and sees a hilltop. She takes a picture of the hill. \r\n\r\nWhen she displays the picture, Harry appears on top of the hill pointing down the other side. She breaks into a run. \r\n\r\nEXT. HILLTOP - DAY\r\n\r\nWhen Kate reaches the top of the hill, she looks down on the other side. \r\n\r\nKate takes a step backward when she sees that stretching below her is a train station. Gathering up her courage, she begins to walk toward it. \r\n\r\nEXT. TRAIN STATION - DAY\r\n\r\nThe train station is very modern. Passengers bustle around the platform. Trains come and go. The actual depot looks like a piece of modern art. Kate hesitatingly walks toward the station and looks around confused. \r\n\r\nAn older conductor approaches her.\r\n\r\nCONDUCTOR\r\nYou look a bit lost. Maybe I can be of help? \r\n\r\nKate motions around the train station.\r\n\r\nKATE\r\nThis is all new.\r\n\r\nThe conductor looks around the station with pride.\r\n\r\nCONDUCTOR\r\nIt was refurbished just two years ago. It does look good as new. \r\n\r\nKATE\r\nWhat's left over?\r\n\r\nCONDUCTOR\r\nExcuse me?\r\n\r\nKATE\r\nBefore it was refurbished. What's left of the original station? \r\n\r\nCONDUCTOR\r\nWell, it looked pretty much like this before they redid it. \r\n\r\nKATE\r\nThe design of the station. It doesn't look like something from the 1960s. \r\n\r\nCONDUCTOR\r\n(grins knowingly)\r\nAre you one of those architecture students from the university? \r\n\r\nKATE\r\nNo, I just wanted to know.\r\n\r\nCONDUCTOR\r\nIt doesn't look like it's from the 60s because this station's only been here for 15 years. \r\n\r\nKATE\r\nAnd before that?\r\n\r\nCONDUCTOR\r\nThe old train station?\r\n\r\nKATE\r\nIt was knocked down?\r\n\r\nCONDUCTOR\r\nNo, they just rerouted the track. The old station still sits next to the old track. They talked about making it a landmark at one time but-- \r\n\r\nKATE\r\nWhere is it?\r\n\r\nCONDUCTOR\r\nIt's down the hill there about a quarter mile.\r\n\r\nKATE\r\nThank you.\r\n\r\nCONDUCTOR\r\nYou're welcome--\r\n\r\nKate doesn't even let the conductor finish before she dashes\r\noff.\r\n\r\nEXT. OLD TRAIN STATION\r\n\r\nFrom the top of the hill, Kate stares down at a little station standing adjacent to some dead track. Once colored bright red, the paint has faded, although it still looks in good condition. The windows on the station have been boarded up. \r\n\r\nKate takes another picture. When she displays it, Harry stands pointing at the back of the station. \r\n\r\nSlowly, Kate walks down the hill, not really wanting to go any farther. Her stuttering progress indicates she fears the unknown she's about to confront. Is she walking into a graveyard? \r\n\r\nWhen she reaches the back of the station, she raises the camera and takes another picture. Harry kneels next to the lowest siding board of the building. \r\n\r\nKate moves closer to examine the board. A few of the nails are missing on the left side. She hesitantly reaches down and grabs hold of the board. With a little pressure, it bends backward. \r\n\r\nStartled, she jumps back when something falls out and lands on the ground. Kate carefully picks up an old envelope and turns it over in her hands. It's an old letter addressed to Catherine Randolph. \r\n\r\nThe gum used to hold the envelope flap closed is long gone. Kate raises the flap and takes out the papers inside. A letter is on top. She unfolds it and begins reading. \r\n\r\nHARRY (V.O.)\r\nMy dearest Catherine, I'm writing you this letter because as much as I want to, I don't think I'll be making it back. Things are crazy over here. We're preparing for an offensive that nobody except the top brass thinks we can win. I hope you'll forgive me for breaking my promise of returning home. \r\n\r\nKate stops reading for a moment as a single tear runs down her cheek. She sits on the ground and begins again. \r\n\r\nHARRY (V.O.) (CONT'D)\r\nI spend all of my time thinking about you and the boy. I know I haven't been able to provide you with everything I wanted to. My CO was a stockbroker before he came over to this insane place. He helped me invest all of the extra money I could scrounge together from my last few of paychecks. Maybe this can help pay for Joe's education. \r\n\r\nKate pulls aside the letter and gasps at the papers underneath. A stock certificate says IBM in large bold letters, 800 shares. Below the corporation name, the certificate is dated 1969. \r\n\r\nHARRY (CONT'D)\r\nI don't think I told you often enough how much I love you. I wish I could be there to help you raise our son. \r\n\r\nMary feels her heart melt.\r\n\r\nHARRY (CONT'D)\r\nI know Joe will grow up to become a good man. \r\n\r\nKATE\r\n(crying)\r\nHe did. He did become a good man, Harry.\r\n\r\nHARRY (V.O.)\r\nTell him that I love him. I'm sure he'll know, but tell him anyway. Wherever I am then, I'll be watching over you two. I love you with all my heart. Harry. \r\n\r\nThe camera sitting on the ground next to her activates itself and takes a picture. Kate picks up the camera and loads the photo. On it, Harry waves goodbye. As Kate watches, his image fades from the photo. \r\n\r\nKATE\r\nGoodbye Harry and thank you. Tell Joe that I'll try to raise Matthew to be as good of person as he was. \r\n\r\nAs Kate finishes speaking, the camera moves to an overhead POV. Slowly the camera pulls back. Kate's image grows smaller and smaller as the POV ascends into the sky. \r\n\r\n> FADE OUT.";
    });

    this["JST"]["templates/samples/printing_trouble.fountain"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};



        return "Title: Printing Trouble\r\nCredit: written by\r\nAuthor: Dan Rahmel\r\nCopyright: (c) Copyright Dan Rahmel 2006\r\n\r\nFADE IN:\r\n\r\nEXT. SKYSCRAPER - NIGHT\r\n\r\nIn the dark early morning, a glass and steel skyscraper towers over the empty downtown streets. A single lighted window resembles a cyclops eye looking out from the sleeping structure. \r\n\r\nINT. COPY ROOM - NIGHT\r\n\r\nThe single red light on the copier machine shines in the dark room. The button reads STANDBY. A LARGE HAND reaches in and strikes the green ACTIVATE button and a deep whirring sound begins as the copier wakes to life. \r\n\r\nINT. HALLWAY - NIGHT\r\n\r\nThe large hand is attached to an even larger man, MIKE. He shambles down the hallway toward the light that shines from a single office.\r\n\r\nINT. OFFICE - NIGHT\r\n\r\nFootball memorabilia and paperwork dominate the office. Game balls, a framed jersey with a blood stain on the shoulder, various trophies, and so on. Surfaces not covered by football junk or general office dreck hold a vast array of empty and semi-empty coffee cups.\r\n\r\nPerched on the edge of the vast oak desk is a weathered LASER PRINTER. It looks like it's seen as much time on the field as any football player.\r\n\r\nMike enters the room and pitches a folder atop a pile of folders next to a desk clock. The clock face reads 4:32.\r\n\r\nMIKE drops into the leather chair behind his desk. Sighing, he leans forward and grabs a sheet from the printer output tray. He carefully places the page into an open 3-ring binder that sits in the middle of the desk. He snaps the three rings of the binder closed with a CLACK. \r\n\r\nMIKE\r\nAll 623 pages.\r\n\r\nMike turns to the computer and bumps his coffee mug -- almost knocking it to the floor. He wearily pushes the mug into a less dangerous position and enters the final numbers with the computer keypad. \r\n\r\nMIKE (CONT'D)\r\nNow just gotta print the summary.\r\n\r\nHe works the mouse--\r\n\r\nMIKE (CONT'D)\r\nPrint this... \r\n\r\nChecks the printer status light--\r\n\r\nMIKE (CONT'D)\r\n...drop it off... \r\n\r\nHits the Ready button--\r\n\r\nMIKE (CONT'D)\r\n...go home... \r\n\r\nFlips out the paper output catch--\r\n\r\nMIKE (CONT'D)\r\n...take the day off... \r\n\r\nReturns his attention to the mouse and display--\r\n\r\nMIKE (CONT'D)\r\nsleep...\r\n\r\nMike looks at the wall clock that reads 4:50 AM.\r\n\r\nMIKE (CONT'D)\r\nGuess it's already Monday. The means Madden tonight. \r\n\r\nDramatically he hits two keys to begin printing. The printer warms up. He fidgets with the debris on his desk. \r\n\r\nHe twiddles his fingers and watches the screen display a variety of status bars such as 'Processing...', 'Spooling...' 'Preparing to print...'. \r\n\r\nMIKE (CONT'D)\r\n(whispering)\r\nJust this once, work right you bastard... I know you've printed all night...just this last page... \r\n\r\nThe printer beeps. Mike scans the printer status lights.\r\n\r\nMIKE (CONT'D)\r\nOut of paper. Crap!\r\n\r\nMike lurches out of his chair and heads out of the room. \r\n\r\nAs soon as Mike steps through the doorway, the printer audibly snickers behind him. \r\n\r\nPTEW! Printer spits out the cable in back.\r\n\r\nMike returns with a fresh ream of paper. He yanks the paper tray out of the printer. Surprisingly, there are still a few sheets in the tray. \r\n\r\nMIKE (CONT'D)\r\nI thought you said paper out.\r\n\r\nMike shoves the paper tray home and the Paper Out indicator is still lit. He pulls the paper tray again. \r\n\r\nHe tears open the new ream and shoves a stack of new paper into the tray. \r\n\r\nWhile Mike fixes the paper in the tray, the lights on the printer shimmer in a strange pattern. When he glances at the panel, the lights show the steady Paper Out indicator. \r\n\r\nTenderly, he slides the cartridge back into place.\r\n\r\nMIKE (CONT'D)\r\nThere ya go. Now print...please.\r\n\r\nWhirring sounds as the printer warms up again. Mike drops into his chair. \r\n\r\nFlashing on the computer screen.\r\n\r\nMIKE (CONT'D)\r\n(reads)\r\n'Printer not connected.' I just printed 600 pages?!? \r\n\r\nExamining the printer, Mike sees the end of the printer cable. \r\n\r\nMIKE (CONT'D)\r\nHow'd that happen...\r\n\r\nHe plugs the printer back in. \r\n\r\nMIKE (CONT'D)\r\nMaybe you're the only known victim of the Y2K bug. \r\n\r\nMike waits. Drinks from a mug sitting next to the monitor. The cold brackish coffee makes him wince and he slams the cup on the desk. (pause) With resignation, he picks up the cup and drinks again. He taps on his desk, waiting. \r\n\r\nThe page begins feeding through the printer. Mike's face brightens with elation. KRUMF. The printer eats the rest of the page. \r\n\r\nMIKE (CONT'D)\r\nWhat now?\r\n\r\nMike opens the printer, clears the jam. \r\n\r\nMIKE (CONT'D)\r\n(word-by-word)\r\nGive me my damn page!\r\n\r\nHe hits print again. A page starts printing. \r\n\r\nMIKE (CONT'D)\r\nOK...OK...\r\n\r\nMike sits with his eyes closed, praying for the right output. The page feeds perfectly out of the printer. Mike opens his eyes. \r\n\r\nMIKE (CONT'D)\r\nYes!\r\n\r\nHe grabs the sheet that was spit out. When he glances at it, the smudged page reads: \"Error: Offending Command. Please try to print again.\" \r\n\r\nMike steams for a moment, crumples the paper and throws it aside. He sharply exhales. He puts his hands together as if praying. \r\n\r\nMIKE (CONT'D)\r\nOh God of Printers, I'm begging you. I just need this page. I'll buy you one of the fancy gold-plated cables if you'll grant this humble request. \r\n\r\nHe hits print again. Half the page feeds. Mikes does 'come on' hand signals as he silently urges the paper out of the machine. \r\n\r\nThe page jams and crumples.\r\n\r\nMike sighs. He opens up the front feed door and hits print again. \r\n\r\nMIKE (CONT'D)\r\nMaybe if I oil the rollers...\r\n\r\n> FADE TO:\r\n\r\nINT. OFFICE\r\n\r\nClock reads 5:42\r\n\r\nSound of paper crumpling.\r\n\r\n> FADE TO:\r\n\r\nINT. OFFICE\r\n\r\nClock reads 6:37\r\n\r\n(O.C.) Mike shouts and curses the printer.\r\n\r\n> FADE TO:\r\n\r\nINT. OFFICE - DAY\r\n\r\nClock reads 7:05\r\n\r\nMike stares into space, his hair disheveled. Covered in toner he sits in a room filled with sheets of crumpled, half printed paper. \r\n\r\nThe INTERCOM on the phone buzzes.\r\n\r\nMike stares at the phone in horror. He glances up at the clock. He straightens his tie poorly. \r\n\r\nHesitantly, he hits the button to pick up. A voice booms from the intercom speaker. \r\n\r\nINTERCOM\r\nHow ya doing Mike?\r\n\r\nMIKE\r\nGood morning, sir.\r\n\r\nINTERCOM\r\nGood morning? Try fantastic morning! I can almost hear the birds chirping outside my window. They're telling me to roll up my sleeves and dig in. That's why we're here, isn't it? \r\n\r\nMIKE\r\nYes, I--\r\n\r\nINTERCOM\r\nI have to be honest with you, Mike...I've got a problem. The Willard report...supposed to be on my desk at 7... It's... already 7:05. Is there some issue I should know about? \r\n\r\nMIKE\r\nWell, the printer--\r\n\r\nINTERCOM\r\nMy old work horse? I loved that printer. Put PASSION behind every sheet of paper it fed. Do you know what I mean by doing your job with some PASSION, Mike? \r\n\r\nMIKE\r\nYes, I--\r\n\r\nINTERCOM\r\nIf only people would show that kind of passion -- Mike. So what's the problem? \r\n\r\nMIKE\r\n(beat)\r\nThere is no problem.\r\n\r\nINTERCOM\r\nNow that's what I want to here! So I can expect two copies of that report on my desk in...say...twenty minutes? \r\n\r\nMIKE\r\nI'll bring it over.\r\n\r\nINTERCOM\r\nKnew I could count on you, Mike.\r\n\r\nMike mimics 'Knew I could count on you.' He looks down at the printer, stares at the wall a moment, and then reaches for a pen and paper. \r\n\r\n> FADE TO:\r\n\r\nINT. OFFICE\r\n\r\nWe see a page with a hand written grid of numbers on lined paper. It's the first page of a report folder that Mike holds. Sweat beads on his upper lip. Mike slowly closes the report and makes the sign of the cross. \r\n\r\nHe turns to stare at the printer and his gaze is deadly.\r\n\r\nMIKE\r\nThat was the final straw.\r\n\r\n> CUT TO:\r\n\r\nEXT. STREET\r\n\r\nCar idles by the curb.\r\n\r\nINT. CAR\r\n\r\nMike engages in elaborate driving preparation. He pulls on a pair of leather racing gloves, checks all the car gauges, smooths down his hair, and revs the engine. The smile in his eyes spreads to his lips. \r\n\r\nHe puts the car into gear and hits the gas.\r\n\r\nEXT. STREET\r\n\r\nThe car barrels down the street.\r\n\r\nEXT. STREET\r\n\r\nThe printer sits in the middle of the street lane.\r\n\r\nEXT. STREET\r\n\r\nCar revs and gains speed.\r\n\r\nEXT. STREET\r\n\r\nThe printer sits.\r\n\r\nINT. CAR\r\n\r\nMike speeds up and laughs manically.\r\n\r\nEXT. STREET\r\n\r\nPRINTER\r\nUh-oh.\r\n\r\nINT. CAR\r\n\r\nMike's joy disappears. He gasps at what he sees through the windshield. \r\n\r\nEXT. STREET\r\n\r\nThe printer magically takes a short hop toward the other\r\nlane. It hops again.\r\n\r\nINT. CAR\r\n\r\nMIKE\r\nWhat the fu...\r\n\r\nMike floors it.\r\n\r\nEXT. STREET\r\n\r\nPrinter makes several hops and gets out of the way.\r\n\r\nEXT. STREET\r\n\r\nThe car goes by the spot previously occupied by the printer. Mike slams on the brakes. \r\n\r\nINT. CAR\r\n\r\nMIKE\r\nYou're not getting off that easy.\r\n\r\nEXT. CAR\r\n\r\nMike turns the car around for another pass. The car sprints toward the printer. \r\n\r\nINT. CAR\r\n\r\nMike focuses intently on the road ahead.\r\n\r\nEXT. STREET\r\n\r\nThe printer again hops out of the way and leaps onto the\r\ncurb.\r\n\r\nEXT. STREET\r\n\r\nThe car pulls to the curb. Mike pops the trunk and gets out of the car. He walks around to the back and grabs a large, heavy crowbar. \r\n\r\nMIKE\r\nEscape this you toner-sucking bastard.\r\n\r\nEXT. SIDEWALK\r\n\r\nMike walks menacingly toward the printer. In the gutter next to the printer is a glass bottle. Testing, Mike swings the end of the bar down on it. The bottle shatters easily. \r\n\r\nHe hefts the weight of the crowbar and smiles. He turns toward the printer. As he lifts the crowbar over his head, the printer mewls. \r\n\r\nMike looks confused.\r\n\r\nPrinter mewls again.\r\n\r\nMike slowly lowers the crowbar. He shakes the bar over the printer. \r\n\r\nMIKE\r\nWhat am I gonna do with you?\r\n\r\nMike sighs.\r\n\r\nEXT. SIDEWALK\r\n\r\nMike sits on the sidewalk next to the printer. He disgustedly throws the crowbar into the street. \r\n\r\nMIKE\r\nI need a new printer or I need a new boss... \r\n\r\nMINA walks up and a shadow falls across Mike.\r\n\r\nMINA\r\nIs anything wrong?\r\n\r\nMIKE\r\nNo...just trying to murder my printer.\r\n\r\nMINA\r\nOhhh....\r\n\r\nMIKE\r\nDon't worry about me. \r\n(whispering to Mina)\r\nGonna make it look like a suicide.\r\n\r\nMINA\r\nUmm... OK... well... you have a nice day.\r\n\r\nMIKE\r\nYeah... see you later.\r\n\r\nMike shakes his head.\r\n\r\nThe printer makes a noise like it's warming up. A sheet feeds into the output tray. Mike grabs the paper. It's a cartoon picture of Mina standing by the curb. \r\n\r\nA second picture prints of cartoon Mina bending over. Mike looks puzzled. \r\n\r\nA third picture prints and when Mike looks at it, he flinches. He looks again and flinches again. \r\n\r\nMIKE (CONT'D)\r\nYou must have been bored silly printing my stuff. \r\n\r\nA light bulb goes off in Mike's head. He smiles and glances at the printer. \r\n\r\nINT. OFFICE\r\n\r\nBehind his desk, Mike pats the printer happily as it spits out pages. Two folders sit closed in front of him. \r\n\r\nThe intercom buzzes. The voice on the intercom is grumpy.\r\n\r\n\r\nINTERCOM\r\nMike, do you have the Southworth report ready?\r\n\r\nMIKE\r\nSure do.\r\n\r\nINTERCOM\r\nI hope we won't have a repeat of our last problem.\r\n\r\nMIKE\r\nNope. Took me a little time to understand how you treated your old printer. \r\n\r\nMike looks at his computer screen that shows a web page for 'Horny Virgin Sluts.' \r\n\r\nMIKE (CONT'D)\r\nOnce I figured that out, I've dealt with it like you did and it hums right along. \r\n\r\nINTERCOM\r\nWhat do you mean by that?\r\n\r\nIgnoring the voice, Mike picks up the two folders.\r\n\r\nMIKE\r\nI've got the Southworth report in front of me. I'll bring it right over. \r\n\r\nINTERCOM\r\nDon't bother. I'll send Donna over to pick all three copies. \r\n\r\nMIKE\r\nThree copies...\r\n\r\nMike looks down and the intercom light is already off.\r\n\r\nMIKE (CONT'D)\r\nIt was supposed to be two copies, you dick... \r\n\r\nMike sighs, opens a drawer, and takes out an empty folder. Mike grabs all of three folders and heads for the copy room. \r\n\r\nAfter a few moments, a knock at the half open door.\r\n\r\nDONNA\r\nMike? Hello?\r\n\r\nDonna walks into the room. She spots the stack of paper in the printer. \r\n\r\nDONNA (CONT'D)\r\nThis must be it...\r\n\r\nShe reaches for it.\r\n\r\n> FADE TO BLACK.\r\n\r\nDONNA (V.O.) (CONT'D)\r\nThat's disgusting.\r\n\r\n> FADE OUT.\r\n";
    });

    this["JST"]["test/screenplays/client.fountain"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};



        return "Title: \r\n	_Screenplay title_\r\n	Second line\r\nCredit: written by\r\nAuthor: \r\n	Author 1,\r\n	**Author 2** & Author 3\r\nSource: script based on...\r\nNotes: \r\n	Additional notes (1)\r\n	Additional notes (2)\r\nDate: 01/12/2010\r\nContact:\r\n	e-mail: email@email.com\r\n    phone: +12 34 567 89\r\nCopyright: Licence info\r\n\r\n# Act 1: basic blocks\r\n\r\n## Sequence 1: scene heading, action, dialogue, parenthetical\r\n\r\nINT. BASIC BLOCKS - DAY\r\n\r\nAction. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action.\r\n\r\nAction. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action.\r\n\r\nHERO 1\r\nBlah blah blah blah.\r\n\r\nHERO 2\r\nBlah blah.\r\n(parenthetical)\r\nBlah blah.\r\n\r\nAction. action. action. action.\r\n\r\n## Sequence 2: boneyard and notes\r\n\r\nEXT. BASIC BLOCKS - NIGHT\r\n\r\nAction. Action. Action[[ (inline note)]]. Action.\r\n\r\n/*\r\n\r\nIgnored.\r\nIgnored. /* nested */\r\nIgnored.\r\n\r\n*/\r\n\r\nAction. action. action. action. /*ignored*/action. action. action. action. action. action. action. action. action. /*ignored /*nested*/ */action. action. action. action. action. action. action.\r\n\r\n[[Long long long long long long long long long long long long long long long long long long long long long long long long long long long long long...\r\n\r\n...multiline note]]\r\n\r\n## Sequence 3: Formatting\r\n\r\nINT. FORMATTING - DAY\r\n\r\nNormal test, _underline test_, *italic test*, **bold test**, ***bold and italic test***, _*underline and italic test*_, _**underline and bold test_**, _***underline, bold and italic test***_.\r\n\r\n_Long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long **(with nested bold)** long long long long underlined text._\r\n\r\n> CENTERED, **BOLD**, _UNDERLINE_ <\r\n\r\n> TRANSITION:\r\n\r\nPage break test...\r\n\r\n===\r\n\r\n.FORCED HEADER\r\n\r\nAction.\r\n\r\n# Act 2: Dialogue\r\n\r\nINT. SPLIT DIALOGUE - DUSK\r\n\r\nAction.\r\n\r\nHERO 1\r\nBlah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah. Blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah. Blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah.\r\n\r\nAction.\r\n\r\nINT. DUAL DIALOGUE - NIGHT\r\n\r\nAction action.\r\n\r\nHERO 1\r\nBlah!\r\n\r\nHERO 2 ^\r\nBlah!\r\n\r\nHERO 3\r\nBlah?\r\n\r\nAction action.\r\n\r\n# Act 3: edge cases\r\n\r\nINT. LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG SCENE HEADER.\r\n\r\nAction.\r\n\r\nVERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY LONG NAME\r\nHello, my name is Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Long Name.\r\n\r\n# Act 4: Misc\r\n\r\n## Latin characters\r\n\r\nINT. LATIN TEST - DAWN\r\n\r\nLatin characters.\r\n\r\nPOLISH\r\nPchnąć w tę łódź jeża lub osiem skrzyń fig.\r\n\r\nGERMAN\r\nFalsches Üben von Xylophonmusik quält jeden größeren Zwerg.\r\n\r\nNORWEGIAN\r\nBlåbærsyltetøy\r\n\r\nSPANISH\r\nEl pingüino Wenceslao hizo kilómetros bajo exhaustiva lluvia y frío, añoraba a su querido cachorro.\r\n\r\n## Forced elements\r\n\r\n.WNĘTRZE. FORCED ELEMENTS - NIGHT\r\n\r\n!INT. ACTION\r\n\r\n@McCABE\r\n~Happy birthday to you!\r\n\r\n> FADE TO GREEN.\r\n\r\n## Sections and synopsis\r\n\r\n= Section synopsis\r\n\r\n### Subsection\r\n\r\n= Subsection synopsis\r\n\r\n= Additional synopsis\r\n\r\nINT. SCENE - DAY\r\n\r\n= Scene synopsis\r\n\r\nAction.\r\n\r\n# Act 5: Variables\r\n\r\nINT. $LOCATION - DAY\r\n\r\n$PROTAGONIST enters the $location. $Antagonist attacks him.\r\n\r\n$ANTAGONIST\r\nAaaaa!\r\n\r\n$Protagonists kills $Antagonist.\r\n\r\n$BOND.LAST\r\nMy name is $bond.last, $bond.name.\r\n\r\n> THE END.\r\n";
    });

    this["JST"]["test/screenplays/fdx/header_note.fountain"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};



        return "\r\nINT. SCENE HEADING - DAY\r\n\r\n[[Note in scene heading.]]\r\n";
    });

    this["JST"]["test/screenplays/fdx/note.fountain"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};



        return "\r\nAction. Action. [[Note in action. Note. Note. Note.]]\r\n";
    });

    this["JST"]["test/screenplays/fdx/synopsis.fountain"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};



        return "\r\nINT. SCENE HEADING - DAY\r\n\r\n= Here is the synopsis.\r\n";
    });

    this["JST"]["test/screenplays/test.fountain"] = Handlebars.template(function(Handlebars, depth0, helpers, partials, data) {
        this.compilerInfo = [4, '>= 1.0.0'];
        helpers = this.merge(helpers, Handlebars.helpers);
        data = data || {};



        return "[[Script note.\r\n- blah blah blah\r\n- blah blah blah\r\n]]\r\n\r\nTitle: \r\n	_Screenplay title_\r\n	Second line\r\nCredit: written by\r\nAuthor: \r\n	Author 1,\r\n	**Author 2** & Author 3\r\nSource: script based on...\r\nNotes: \r\n	Additional notes (1)\r\n	Additional notes (2)\r\nDate: 01/12/2010\r\nContact:\r\n	e-mail: email@email.com\r\n    phone: +12 34 567 89\r\nCopyright: Licence info\r\n\r\n# Act 1: basic blocks\r\n\r\n## Sequence 1: scene heading, action, dialogue, parenthetical\r\n\r\nINT. BASIC BLOCKS - DAY\r\n\r\nAction. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action.\r\n\r\nAction. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action. action.\r\n\r\nHERO 1\r\nBlah blah blah blah.\r\n\r\nHERO 2\r\nBlah blah.\r\n(parenthetical)\r\nBlah blah.\r\n\r\nAction. action. action. action.\r\n\r\n## Sequence 2: boneyard and notes\r\n\r\nEXT. BASIC BLOCKS - NIGHT\r\n\r\nAction. Action. Action[[ (inline note)]]. Action.\r\n\r\n/*\r\n\r\nIgnored.\r\nIgnored. /* nested */\r\nIgnored.\r\n\r\n*/\r\n\r\nAction. action. action. action. /*ignored*/action. action. action. action. action. action. action. action. action. /*ignored /*nested*/ */action. action. action. action. action. action. action.\r\n\r\n[[Long long long long long long long long long long long long long long long long long long long long long long long long long long long long long...\r\n\r\n...multiline note]]\r\n\r\n## Sequence 3: Formatting\r\n\r\nINT. FORMATTING - DAY\r\n\r\nNormal test, _underline test_, *italic test*, **bold test**, ***bold and italic test***, _*underline and italic test*_, _**underline and bold test_**, _***underline, bold and italic test***_.\r\n\r\n_Long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long long **(with nested bold)** long long long long underlined text._\r\n\r\nEXT. ESCAPING FORMATTING - NIGHT\r\n\r\nWhat the f\\*ck! Escape\\_escape\\*escape. \r\n\r\n> CENTERED, **BOLD**, _UNDERLINE_ <\r\n\r\n> TRANSITION:\r\n\r\nPage break test...\r\n\r\n===\r\n\r\n.FORCED HEADER\r\n\r\nAction.\r\n\r\n# Act 2: Dialogue\r\n\r\nINT. SPLIT DIALOGUE - DUSK\r\n\r\nAction.\r\n\r\nHERO 1\r\nBlah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah. Blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah. Blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah blah.\r\n\r\nAction.\r\n\r\nINT. DUAL DIALOGUE - NIGHT\r\n\r\nAction action.\r\n\r\nHERO 1\r\nBlah!\r\n\r\nHERO 2 ^\r\nBlah!\r\n\r\nHERO 3\r\nBlah?\r\n\r\nAction action.\r\n\r\n# Act 3: edge cases\r\n\r\nINT. LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG LONG SCENE HEADER.\r\n\r\nAction.\r\n\r\nVERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY VERY LONG NAME\r\nHello, my name is Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Very Long Name.\r\n\r\n# Act 4: Misc\r\n\r\n## Latin characters\r\n\r\nINT. LATIN TEST - DAWN\r\n\r\nLatin characters.\r\n\r\nPOLISH\r\nPchnąć w tę łódź jeża lub osiem skrzyń fig.\r\n\r\nGERMAN\r\nFalsches Üben von Xylophonmusik quält jeden größeren Zwerg.\r\n\r\nNORWEGIAN\r\nBlåbærsyltetøy\r\n\r\nSPANISH\r\nEl pingüino Wenceslao hizo kilómetros bajo exhaustiva lluvia y frío, añoraba a su querido cachorro.\r\n\r\n## Forced elements\r\n\r\n.WNĘTRZE. FORCED ELEMENTS - NIGHT\r\n\r\n!INT. ACTION\r\n\r\n@McCABE\r\n~Happy birthday to you!\r\n\r\n> FADE TO GREEN.\r\n\r\n## Sections and synopsis\r\n\r\n= Section synopsis\r\n\r\n### Subsection\r\n\r\n= Subsection synopsis\r\n\r\n= Additional synopsis\r\n\r\nINT. SCENE - DAY\r\n\r\n= Scene synopsis\r\n\r\nAction.\r\n\r\n> THE END.\r\n";
    });

    return this["JST"];

});