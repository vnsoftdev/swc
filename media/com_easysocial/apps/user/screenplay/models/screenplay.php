<?php
/**
* @package		%PACKAGE%
* @subpackge	%SUBPACKAGE%
* @copyright	Copyright (C) 2010 - 2012 %COMPANY_NAME%. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*
* %PACKAGE% is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

// Import the model file from the core
Foundry::import( 'admin:/includes/model' );


class ScreenplayModel extends EasySocialModel
{
	/**
	 * Retrieves the screenplay stored from the database.
	 *
	 * @since	1.0
	 * @access	public
	 * @param	int		User's id. 
	 * @return	Array	A list of screenplay rowset.
	 */
	public function getItems( $userId, $options = array() )
	{
		// Loads up our very own database object.
		// It's an alias to JFactory::getDBO but it has been beautified with some cool functions.
		$db 	= Foundry::db();

		// Load up our own sql query manager
		$sql 	= $db->sql();

		// Retrieve items from the database.
		$sql->select( '#__social_apps_screenplay' );
		$sql->where( 'created_by' , $userId );
		$sql->where( 'state' , '1' );

		$limit 	= isset($options[ 'limit' ]) ? $options[ 'limit' ] : '';

		if ($limit) {
			$this->setState('limit', $limit);

			// Get the limitstart.
			$limitstart 	= $this->getUserStateFromRequest('limitstart', 0);
			$limitstart 	= ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

			$this->setState('limitstart', $limitstart);

			// Run pagination here.
			$this->setTotal($sql->getTotalSql());

			$result	= $this->getData($sql->getSql());
		}
		else
		{
			$db->setQuery($sql);
			$result 	= $db->loadObjectList();
		}		
		//$result = $db->loadObjectList();
		
		
		
		return $result;
	}
	public function getItemsNoContent( $userId, $options = array() )
	{
		$db 	= Foundry::db();
		$sql 	= $db->sql();

		$query = "SELECT * FROM #__social_apps_screenplay WHERE created_by=" . $userId . " AND state=1";
		$sql->raw($query);
		$db->setQuery( $sql );

		$limit 	= isset($options[ 'limit' ]) ? $options[ 'limit' ] : '';

		if ($limit) {
			$this->setState('limit', $limit);

			// Get the limitstart.
			$limitstart 	= $this->getUserStateFromRequest('limitstart', 0);
			$limitstart 	= ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

			$this->setState('limitstart', $limitstart);

			// Run pagination here.
			$this->setTotal($sql->getTotalSql());

			$result	= $this->getData($sql->getSql());
		}
		else
		{
			$db->setQuery($sql);
			$result 	= $db->loadObjectList();
		}
		
		$my = FD::user();
		$userId	= FD::makeArray($my->id);
		$options	= array(
				'userid' 		=> $userId,
				'list' 			=> $listId,
				'profileId' 	=> $profileId,
				'context' 		=> 'screenplay',
				'type' 			=> 'user',
				'viewer' 		=> $my->id,
				'startlimit'	=> $limitstart,
				'limit'			=> $limit
		);
	
		
		$model 		= FD::model('Stream');
		$resultStream = $model->getStreamData($options);
		
		foreach ($resultStream as &$row)
		{
			$relatedActivities = $model->getRelatedActivities( $row->id , 'screenplay', $my->id );
			foreach ($relatedActivities as $relatedItem)
			{
				$isExists = false;
				foreach($result as $search)
				{
					if ($search->id == $relatedItem->context_id)
					{
						$isExists = true;
						break;
					}
				}
				
				if(!$isExists)
				{
					$result[] = $this->getItem($relatedItem->context_id);
				}
				
			}
		}
		//$relatedActivities = $model->getRelatedActivities( $uid , 'screenplay', $my->id );
		/*echo "<pre>";
		print_r($result);
		echo "</pre>";*/
		//print_r($my->id);exit();
		return $result;
	}
	
	function toArray($obj)
	{
		$obj = (array) $obj;
		return $obj['id'];
	}

	public function getItem( $playId )
	{
		// Loads up our very own database object.
		// It's an alias to JFactory::getDBO but it has been beautified with some cool functions.
		$db 	= Foundry::db();

		// Load up our own sql query manager
		$sql 	= $db->sql();

		// Retrieve items from the database.
		$sql->select( '#__social_apps_screenplay' );
		$sql->where( 'id' , $playId );
		$sql->where( 'state' , '1' );
		
		$db->setQuery( $sql );

		$result = $db->loadObject();

		return $result;
	}
	
	public function getScenes( $playId )
	{
		$db 	= Foundry::db();

		// Load up our own sql query manager
		$sql 	= $db->sql();

		// Retrieve items from the database.
		$sql->select( '#__social_apps_screenplay_scenes' );
		$sql->where( 'screen_id' , $playId );
		$sql->where( 'state' , '1' );
		
		$db->setQuery( $sql );

		$result = $db->loadObjectList();

		return $result;
	}
	
	public function incComment( $playId )
	{
		$play = $this->getItem($playId);
		
		$db 	= Foundry::db();

		$sql 	= $db->sql();
		$query = 'UPDATE #__social_apps_screenplay SET comments=' . ($play->comments + 1) . ' WHERE id='. $playId . ' and state=1';
		$sql = raw($query);	
		$db->setQuery( $sql );

		return $db->query();
	}
}