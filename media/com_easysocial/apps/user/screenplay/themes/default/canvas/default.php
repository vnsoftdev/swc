<?php
/**
* @package		%PACKAGE%
* @subpackge	%SUBPACKAGE%
* @copyright	Copyright (C) 2010 - 2012 %COMPANY_NAME%. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*
* %PACKAGE% is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

$appid = $app->id . ":" . $app->alias;

//var_dump($screenplay); die;

require_once(JPATH_ROOT . "/media/com_easysocial/apps/user/screenplay/helper.php");
require_once(JPATH_ROOT . "/media/com_easysocial/apps/user/screenplay/Inliner.php");
require_once(JPATH_ROOT . "/media/com_easysocial/apps/user/screenplay/Parser.php");

$parser = new Parser();
$f_content = $parser->parse($screenplay->content);

//var_dump($f_content);
echo "<h2 class='script-title center'>" . $f_content[title] . "</h2>";
echo "<div id='script' class='dpi72'>" . $f_content[html][script] . "</div>";
/*$helper = new ScreenplayHelper();
var_dump($helper->getSceneHeading($f_content));*/

?>