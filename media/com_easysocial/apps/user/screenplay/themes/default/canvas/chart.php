<?php
/**
* @package		%PACKAGE%
* @subpackge	%SUBPACKAGE%
* @copyright	Copyright (C) 2010 - 2012 %COMPANY_NAME%. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*
* %PACKAGE% is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

$appid = $app->id . ":" . $app->alias;
//print_r($screenplay);exit();
//var_dump($screenplay); die;
//echo JPATH_ROOT;exit();
$my = FD::user();

?>
<div class="screenplay-item">
	<div class="script-header hg60">
		<h3 class="pull-left"><?php echo $screenplay->title;?></h3>
		<div class="pull-right btn-group">
			<a href="javascript:void(0);" data-bs-toggle="dropdown" class="dropdown-toggle_ loginLink btn btn-dropdown">
				<i class="icon-es-dropdown"></i>
			</a>

			<?php if ($isOwner) { ?>
			<ul class="dropdown-menu dropdown-menu-user messageDropDown">
				<li>
					<a href="<?php echo JRoute::_('index.php?option=com_easysocial&view=apps&layout=canvas&id=159:screenplay&sublayout=edit&cid='.$screenplay->id.'&uid=74:vnsoftdev-gmail-com&Itemid=122'); ?>" data-apps-screenplay-edit>
						<?php echo JText::_( 'APP_SCREENPLAY_EDIT_BUTTON' );?>
					</a>
				</li>
				<li data-friends-unfriend="">
					<a href="javascript:void(0);" data-apps-screenplay-delete data-id="<?php echo $screenplay->id;?>">
						<?php echo JText::_( 'APP_SCREENPLAY_DELETE_BUTTON' );?>
					</a>
				</li>
			</ul>
			<?php } ?>
		</div>
	</div>
			
	<div class="script-meta">
		<div class="script-owner">
			<i class="ies-user"></i> <a href="<?php echo $user->getPermalink(); ?>"><?php echo $user->getName();?></a>
		</div>
		
		<div class="script-hit">
			<i class="ies-eye"></i> <?php echo JText::sprintf( 'APP_SCREENPLAY_HITS' , $screenplay->hits ); ?>
		</div>

		<div class="script-comment">
			<i class="ies-comments"></i> <?php echo JText::sprintf( 'APP_SCREENPLAY_COMMENTS', $comments->getCount() ); ?>
		</div>

		<div class="script-scene">
			<i class="ies-file"></i> <?php echo JText::sprintf( 'APP_SCREENPLAY_SCENES', $screenplay->scenes ); ?>
		</div>

		<div class="script-date">
			<time datetime="<?php echo $this->html( 'string.date' , $screenplay->created ); ?>" class="script-date">
				<i class="ies-calendar"></i>&nbsp; <?php echo $this->html( 'string.date' , $screenplay->created , JText::_( 'DATE_FORMAT_LC4' ) ); ?>
			</time>
		</div>
	</div>
	
	<div class="clearfix"></div>

	<div class="script-content"><?php //echo nl2br($screenplay->content);?></div>
	
	   
	<iframe src="media/com_easysocial/apps/user/screenplay/bundle/dev.html?id=<?php echo $screenplay->id; ?>&userid=<?php echo $my->id ?>&baseurl=<?php echo JUri::base(); ?>&appid=<?php echo $appid; ?>&plugin=stats" width="100%" height="800px"></iframe>
	
	<div class="script-scenes">
		<?php foreach ($scenes as $scene) { ?>
			<div class="script-scene">
				<h4><?php echo $scene->title; ?></h4>
				<div class="script-scene-content"><?php echo $scene->content; ?></div>
			</div>
		<?php } ?>
	</div>
	
	<div class="script-actions">
		<div class="es-action-wrap">
			<ul class="fd-reset-list es-action-feedback">
				<li>
					<a href="javascript:void(0);" class="fd-small"><?php echo $likes->button();?></a>
				</li>
				<li>
					<?php //echo FD::sharing( array( 'url' => FRoute::apps( array( 'layout' => 'canvas' , 'customView' => 'item' , 'uid' => $group->getAlias() , 'type' => SOCIAL_TYPE_GROUP , 'id' => $appid , 'articleId' => $news->id ) , false ) , 'display' => 'dialog', 'text' => JText::_( 'COM_EASYSOCIAL_STREAM_SOCIAL' ) , 'css' => 'fd-small' ) )->getHTML( false ); ?>
				</li>
			</ul>
		</div>

		<div data-screenplay-counter class="es-stream-counter<?php echo empty( $likes->data ) ? ' hide' : '';?>">
			<div class="es-stream-actions"><?php echo $likes->toHTML(); ?></div>
		</div>

		<div class="es-stream-actions">
			<?php if( $params->get( 'allow_comments' , true ) && $comments ){ ?>
				<?php echo $comments->getHTML( array( 'hideEmpty' => false ) );?>
			<?php } ?>
		</div>
	</div>
</div>