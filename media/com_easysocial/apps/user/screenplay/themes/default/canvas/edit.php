<?php
/**
* @package		%PACKAGE%
* @subpackge	%SUBPACKAGE%
* @copyright	Copyright (C) 2010 - 2012 %COMPANY_NAME%. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*
* %PACKAGE% is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );
?>
<div class="es-content">
	<div class="es-content-wrap">
		<form action="<?php echo JRoute::_( 'index.php' );?>" method="post">
			<div class="app-screenplay screenplay-form app-user">
				<h3><?php echo JText::_( 'APP_SCREENPLAY_EDIT_FORM_TITLE' ); ?></h3>

				<hr />

				<div class="form-user">
					<input type="text" name="title" value="<?php echo $this->html( 'string.escape' , $screenplay->title );?>" placeholder="<?php echo JText::_( 'APP_SCREENPLAY_TITLE_PLACEHOLDER' , true );?>" class="form-control input-sm screenplay-title" />

					<div class="screenplay-content-caption clearfix">
						<?php echo JText::_('APP_SCREENPLAY_FORM_CONTENT'); ?>
					</div>
					<div class="editor-wrap fd-cf">
						<?php echo $editor->display( 'screenplay_content' , $screenplay->content , '100%', '400', '10', '5', null, null, 'com_easysocial' ); ?>
					</div>
				</div>

				<div class="form-actions">
					<a href="<?php echo JRoute::_('index.php?option=com_easysocial&view=dashboard&appId=159:screenplay');?>" class="pull-left btn btn-es-danger btn-large"><?php echo JText::_( 'COM_EASYSOCIAL_CANCEL_BUTTON' ); ?></a>
					<button type="submit" class="pull-right btn btn-es-primary btn-large" data-screenplay-save-button><?php echo JText::_( 'COM_EASYSOCIAL_SAVE_BUTTON' ); ?> &rarr;</button>
					<a href="#" class="pull-right btn btn-es-primary btn-large mr-10"><?php echo JText::_( 'COM_SCREENPLAY_ADD_SCENE_BUTTON' ); ?></a>
				</div>
			</div>

			<?php echo $this->html( 'form.token' ); ?>
			<input type="hidden" name="controller" value="apps" />
			<input type="hidden" name="task" value="controller" />
			<input type="hidden" name="appController" value="screenplay" />
			<input type="hidden" name="appTask" value="save" />
			<input type="hidden" name="appId" value="<?php echo $app->id;?>" />
			<input type="hidden" name="playId" value="<?php echo $screenplay->id;?>" />
			<input type="hidden" name="option" value="com_easysocial" />
		</form>
	</div>
</div>
