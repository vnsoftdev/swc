<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );
?>
<div class="row mb-10 mt-10">
	<div class="col-md-12">
		<h4><a href="<?php echo $permalink;?>"><?php echo $play->title; ?></a></h4>
		<div class="fd-small">
			<?php echo JText::sprintf( 'APP_SCREENPLAY_STREAM_CONTENT_META' , '<a href="' . $permalink . '">' . $play->title .'</a>' , $date->format( JText::_('COM_EASYSOCIAL_DATE_DMY') ) );?>
		</div>

		<p class="mb-10 mt-10 blog-description">
			<?php echo $content; ?>
		</p>

		<div>
			<a href="<?php echo $permalink;?>" class="mt-5"><?php echo JText::_( 'APP_SCREENPLAY_CONTINUE_READING' ); ?> &rarr;</a>
		</div>
	</div>
</div>
