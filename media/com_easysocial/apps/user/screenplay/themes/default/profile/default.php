<?php
/**
* @package		%PACKAGE%
* @subpackge	%SUBPACKAGE%
* @copyright	Copyright (C) 2010 - 2012 %COMPANY_NAME%. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*
* %PACKAGE% is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );
?>
<ul class="screenplay">
	<?php if( $screenplay ){ ?>
		<?php foreach( $screenplay as $script ){ ?>
		<li class="textboook-item">
			<div><?php echo $script->title;?></div>
			
			<p><?php echo $script->description;?></p>
		</li>
		<?php } ?>
	<?php } else { ?>
		<li>
			<?php echo JText::_( 'No screenplay found.' );?>
		</li>
	<?php } ?>
</ul>