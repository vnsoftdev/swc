<?php
/**
* @package		EasySocial
* @copyright	Copyright (C) 2010 - 2014 Stack Ideas Sdn Bhd. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
* EasySocial is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

//ML:debug
//var_dump($play); die;

$appid = $app->id . ":" . $app->alias;
//echo $user->getAlias();exit();
//print_r($play);exit();
?>
<div class="screenplay-item" data-apps-screenplay-item data-play-id="<?php echo $play->id;?>" data-user-id="<?php echo $user->id;?>">
	<div class="script-header hg40">
		<h4 class="pull-left script-title">
			<a href="<?php echo FRoute::apps( array( 'layout' => 'canvas' , 'id' => $appid , 'cid' => $play->id , 'uid' => $user->getAlias() , 'type' => SOCIAL_TYPE_USER ) );?>" ><?php echo $play->title; ?></a>
		</h4>
		<div class="action-icons">
			<a class="icon-edit" href="<?php echo JRoute::_('index.php?option=com_easysocial&view=apps&layout=canvas&id='.$appid.'&sublayout=edit&cid='.$play->id.'&uid='.$user->getAlias()); ?>"></a>
			<a class="icon-eye" href="<?php echo FRoute::apps( array( 'layout' => 'canvas', 'sublayout' => 'preview', 'id' => $appid , 'cid' => $play->id , 'uid' => $user->getAlias() , 'type' => SOCIAL_TYPE_USER ) );?>"></a>
			<a class="icon-folder-open" href="#"></a>
			<a class="glyphicon glyphicon-list-alt" href="<?php echo FRoute::apps( array( 'layout' => 'canvas', 'sublayout' => 'facts', 'id' => $appid , 'cid' => $play->id , 'uid' => $user->getAlias() , 'type' => SOCIAL_TYPE_USER ) );?>"></a>
			<a class="icon-chart" href="<?php echo FRoute::apps( array( 'layout' => 'canvas', 'sublayout' => 'chart', 'id' => $appid , 'cid' => $play->id , 'uid' => $user->getAlias() , 'type' => SOCIAL_TYPE_USER ) );?>"></a>
		</div>
		<div class="pull-right btn-group">
			<a href="javascript:void(0);" data-bs-toggle="dropdown" class="dropdown-toggle_ loginLink btn btn-dropdown">
				<i class="icon-es-dropdown"></i>
			</a>
			<?php if ($user->id == $play->created_by) { ?>
			<ul class="dropdown-menu dropdown-menu-user messageDropDown">
				<li>
					<a href="<?php echo JRoute::_('index.php?option=com_easysocial&view=apps&layout=canvas&id='.$appid.'&sublayout=edit&cid='.$play->id.'&uid='.$user->getAlias()); ?>" data-apps-screenplay-edit>
						<?php echo JText::_( 'APP_SCREENPLAY_EDIT_BUTTON' );?>
					</a>
				</li>
				<li>
					<a href="javascript:void(0);" data-apps-screenplay-delete>
						<?php echo JText::_( 'APP_SCREENPLAY_DELETE_BUTTON' );?>
					</a>
				</li>
			</ul>
			<?php } ?>
		</div>
	</div>
	
	<div class="script-meta">
		<div class="script-owner">
			<i class="ies-user"></i> <a href="<?php echo $user->getPermalink(); ?>"><?php echo $user->getName();?></a>
		</div>
		
		<div class="script-hit">
			<i class="ies-eye"></i> <?php echo JText::sprintf( 'APP_SCREENPLAY_HITS' , $play->hits ); ?>
		</div>

		<div class="script-comment">
			<i class="ies-comments"></i> <?php echo JText::sprintf( 'APP_SCREENPLAY_COMMENTS', $play->comments ); ?>
		</div>

		<div class="script-scene">
			<i class="ies-file"></i> <?php echo JText::sprintf( 'APP_SCREENPLAY_SCENES', $play->scenes ); ?>
		</div>

		<div class="script-date">
			<time datetime="<?php echo $this->html( 'string.date' , $play->created ); ?>" class="script-date">
				<i class="ies-calendar"></i>&nbsp; <?php echo $this->html( 'string.date' , $play->created , JText::_( 'DATE_FORMAT_LC4' ) ); ?>
			</time>
		</div>

		<div class="script-privacy" data-screenplay-privacy><?php echo FD::privacy()->form($play->uid, "screenplay", $this->my->id, 'screenplay.view', false,$play->uid); ?></div>
	</div>
</div>
