
EasySocial.require()
.script( 'site/users/item' )
.done(function($)
{
	$( '[data-apps-screenplay-item]' ).implement( EasySocial.Controller.Users.Item.Screenplay );

})