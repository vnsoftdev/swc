<?php
/**
* @package		%PACKAGE%
* @subpackge	%SUBPACKAGE%
* @copyright	Copyright (C) 2010 - 2012 %COMPANY_NAME%. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*
* %PACKAGE% is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

?>

<div class="app-screenplay" data-dashboard-app-screenplay data-app-id="<?php echo $app->id; ?>">
	<div class="es-filterbar">
		<div class="h5 pull-left filterbar-title"><?php echo JText::_( 'APP_SCREENPLAY_DASHBOARD_TITLE' ); ?></div>

		<?php $appid = $app->id . ":" . $app->alias; ?>
		<a class="btn btn-es-primary btn-sm pull-right" href="<?php echo FRoute::apps( array( 'layout' => 'canvas' , 'id' => $appid , 'sublayout' => 'create' , 'uid' => $user->getAlias() ) );?>" data-app-screenplay-create><?php echo JText::_( 'APP_SCREENPLAY_NEW_BUTTON' ); ?></a>
	</div>

	<div class="screenplay-items app-contents-data">
		<?php if( $screenplay ){ ?>
			<?php foreach( $screenplay as $play ){ ?>
				<?php echo $this->loadTemplate( 'apps/user/screenplay/dashboard/item' , array( 'app' => $app , 'play' => $play , 'appId' => $app->id , 'user' => $user ) ); ?>
			<?php } ?>
		<?php } else { ?>
			<div>
				<?php echo JText::_( 'APP_SCREENPLAY_EMPTY' );?>
			</div>
		<?php } ?>
		
		<div class="es-pagination-footer">
			<?php echo $pagination->getListFooter( 'site' ); ?>
		</div>
	</div>
</div>