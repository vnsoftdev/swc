<?php
/**
 * @version		$Id: plg_easysocial_user_questions.php 01 2012-04-21 11:37:09Z maverick $
 * @package		CoreJoomla.easysocial
 * @subpackage	Modules.site
 * @copyright	Copyright (C) 2009 - 2013 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined( '_JEXEC' ) or die;

// We want to import our app library
Foundry::import( 'admin:/includes/apps/apps' );

class SocialUserAppQuestions extends SocialAppItem
{
	public function __construct()
	{
		parent::__construct();
	}

	public function onPrepareStream( SocialStreamItem &$item, $includePrivacy = true )
	{
		if( $item->context !== 'appcontext' )
		{
			return;
		}
	}

	public function onPrepareActivityLog( SocialStreamItem &$item, $includePrivacy = true )
	{
	}

	public function onAfterLikeSave( &$likes )
	{
	}

	public function onAfterCommentSave( &$comment )
	{
	}

	public function onNotificationLoad( $item )
	{
	}
	
}
