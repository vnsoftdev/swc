<?php
/**
 * @version		$Id: answers.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla16.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2010 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.controller');

class CommunityAnswersControllerAnswers extends JControllerLegacy {

	function __construct(){

		parent::__construct();
		
		$this->registerDefaultTask('get_answers_list');
		$this->registerTask('publish_item', 'publish_item');
		$this->registerTask('unpublish_item', 'unpublish_item');
		$this->registerTask('publish', 'publish_list');
		$this->registerTask('unpublish', 'unpublish_list');
		$this->registerTask('remove', 'delete');
		$this->registerTask('get_answer', 'get_answer');
	}
	
	public function get_answers_list(){
		
		$view = $this->getView('answers', 'html');
		$model = $this->getModel('answers');
		$category_model = $this->getModel('categories');
		$users_model = $this->getModel('users');
		
		$view->setModel($model, true);
		$view->setModel($category_model, false);
		$view->setModel($users_model, false);
		$view->display();
	}
	
	function publish_item(){
		
		$return = $this->change_state(1);
		
		if($return == 1){
			
			echo json_encode(array('data'=>1));
		} else {
			
			$msg = $return == 0 ? JText::_('MSG_ERROR') : JText::_('MSG_NO_ITEM_SELECTED');
			echo json_encode(array('error'=>$msg));
		}
		
		jexit();
	}
	
	function unpublish_item(){
		
		$return = $this->change_state(0);
		
		if($return == 1){
			
			echo json_encode(array('data'=>1));
		} else {
			
			$msg = $return == 0 ? JText::_('MSG_ERROR') : JText::_('MSG_NO_ITEM_SELECTED');
			echo json_encode(array('error'=>$msg));
		}
		
		jexit();
	}
	
	function publish_list(){
		
		$return = $this->change_state(1);
		$msg = $return == 1 ? JText::_('MSG_SUCCESS') : ($return == 0 ? JText::_('MSG_ERROR') : JText::_('MSG_NO_ITEM_SELECTED'));
		
		$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers', false), $msg);
	}
	
	function unpublish_list(){
		
		$return = $this->change_state(0);
		$msg = $return == 1 ? JText::_('MSG_SUCCESS') : ($return == 0 ? JText::_('MSG_ERROR') : JText::_('MSG_NO_ITEM_SELECTED'));
		
		$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers', false), $msg);
	}
	
	function change_state($state){

		$app = JFactory::getApplication();
		$ids = $app->input->getArray(array('cid'=>'array'));
		
		if(!empty($ids['cid'])){
				
			$model = $this->getModel('answers');
			JArrayHelper::toInteger($ids['cid']);
				
			if($model->publish_answers($state, $ids['cid'])){
		
				return 1;
			} else {

				return 0;
			}
		}
		
		return -1;
	}
	
	function delete(){

		$app = JFactory::getApplication();
		$ids = $app->input->post->getArray(array('cid'=>'array'));
		
		if(!empty($ids['cid'])){
		
			$model = $this->getModel('answers');
			JArrayHelper::toInteger($ids['cid']);
		
			if($model->delete_answers($ids['cid'])){
		
				$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers', false), JText::_('MSG_SUCCESS'));
			} else {
		
				$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers', false), JText::_('MSG_ERROR'));
			}
		} else {
		
			$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers', false), JText::_('MSG_NO_ITEM_SELECTED'));
		}
	}
	
	public function get_answer(){

		$app = JFactory::getApplication();
		$model = $this->getModel('answers');
		
		$id = $app->input->getInt('answer_id', 0);
		
		if($id > 0){
			
			$answer = $model->get_answer_details($id);
			
			if(!empty($answer)){
			
				echo json_encode(array('answer'=>$answer));
			} else {
				
				echo json_encode(array('error'=>JText::_('MSG_ERROR')));
			}
		} else {
				
			echo json_encode(array('error'=>JText::_('MSG_ERROR')));
		}
		
		jexit();
	}
	
	public function update_answer(){

		$app = JFactory::getApplication();
		$model = $this->getModel('answers');
		$params = JComponentHelper::getParams(A_APP_NAME);

		$answer_id = $app->input->post->getInt('answer_id', 0);
		$description = CJFunctions::get_clean_var('description', true);

		if($answer_id > 0 && !empty($description)){

			if($model->update_answer($answer_id, $description)){

				$data = CJFunctions::process_html($description, ($params->get('default_editor') == 'bbcode'));
				echo json_encode(array('content'=>$data));
			} else {

				echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
			}
		} else {

			echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
		}
		
		jexit();
	}
}