<?php
/**
 * @version		$Id: migration.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.controller');

class CommunityAnswersControllerMigration extends JControllerLegacy {

	function __construct(){

		parent::__construct();
		
		$this->registerDefaultTask('get_migrations_page');
		$this->registerTask('simanswers', 'start_simanswer_migration');
	}
	
	function get_migrations_page(){
		
		$model = $this->getModel('migration');
		$view = $this->getView('migration', 'html');
		$view->setModel($model, true);
		$view->display();
	}
	
	function start_simanswer_migration(){
		
		$model = $this->getModel('migration');
		
		if($model->migrate_sim_answers()){
			
			echo json_encode(array('data'=>1));
		} else {
			
			echo json_encode(array('error'=>JText::_('MSG_ERROR').'| Error: '.$model->getError()));
		}
		
		jexit();
	}
}