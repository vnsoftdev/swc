<?php
/**
 * @version		$Id: dashboard.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.controller');

class CommunityAnswersControllerDashboard extends JControllerLegacy {

	function __construct(){

		parent::__construct();
		
		$this->registerDefaultTask('get_dashboard');
	}
	
	function get_dashboard(){
		
		$model = $this->getModel('dashboard');
		$view = $this->getView('dashboard', 'html');
		$view->setModel($model, true);
		
		$view->display();
	}
}