<?php
/**
 * @version		$Id: questions.php 01 2011-12-12 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.controller');

class CommunityAnswersControllerQuestions extends JControllerLegacy {

	function __construct(){

		parent::__construct();
		
		$this->registerDefaultTask('get_questions_list');
		$this->registerTask('publish_item', 'publish_item');
		$this->registerTask('unpublish_item', 'unpublish_item');
		$this->registerTask('publish', 'publish_list');
		$this->registerTask('unpublish', 'unpublish_list');
		$this->registerTask('remove', 'delete');
		$this->registerTask('refresh', 'refresh');
		$this->registerTask('edit', 'edit_question');
		$this->registerTask('save', 'save_question');
		$this->registerTask('cancel', 'cancel');
	}
	
	public function get_questions_list(){
		
		$view = $this->getView('questions', 'html');
		$model = $this->getModel('questions');
		$category_model = $this->getModel('categories');
		$users_model = $this->getModel('users');
		
		$view->setModel($model, true);
		$view->setModel($category_model, false);
		$view->setModel($users_model, false);
		$view->assign('action', 'list');
		$view->display();
	}

	public function edit_question(){
	
		$view = $this->getView('questions', 'html');
		$model = $this->getModel('questions');
		$category_model = $this->getModel('categories');
	
		$view->setModel($model, true);
		$view->setModel($category_model, false);
		$view->assign('action', 'form');
		$view->display();
	}
	
	public function save_question(){
		
		$app = JFactory::getApplication();
		$model = $this->getModel('questions');
		$item = new stdClass();
		
		$item->id = $app->input->getInt('cid', 0);
		$item->catid = $app->input->getInt('catid', 0);
		$item->title = $app->input->getString('title');
		$item->alias = $app->input->getString('alias');
		$item->description = CJFunctions::get_clean_var('description', true);
		
		if($item->id && $item->catid && !empty($item->title) && $model->save_question($item)){
			
			$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=questions', false), JText::_('MSG_SUCCESS'));
		} else {

			$app->enqueueMessage(JText::_('MSG_ERROR'), 'error');
			
			$view = $this->getView('questions', 'html');
			$category_model = $this->getModel('categories');
				
			$view->setModel($model, true);
			$view->setModel($category_model, false);
			$view->assign('action', 'form');
			$view->assign('item', $item);
			$view->display();
		}
	}
	
	public function publish_item(){
		
		$return = $this->change_state(1);
		
		if($return == 1){
			
			echo json_encode(array('data'=>1));
		} else {
			
			$msg = $return == 0 ? JText::_('MSG_ERROR') : JText::_('MSG_NO_ITEM_SELECTED');
			echo json_encode(array('error'=>$msg));
		}
		
		jexit();
	}
	
	public function unpublish_item(){
		
		$return = $this->change_state(0);
		
		if($return == 1){
			
			echo json_encode(array('data'=>1));
		} else {
			
			$msg = $return == 0 ? JText::_('MSG_ERROR') : JText::_('MSG_NO_ITEM_SELECTED');
			echo json_encode(array('error'=>$msg));
		}
		
		jexit();
	}
	
	public function publish_list(){
		
		$return = $this->change_state(1);
		$msg = $return == 1 ? JText::_('MSG_SUCCESS') : ($return == 0 ? JText::_('MSG_ERROR') : JText::_('MSG_NO_ITEM_SELECTED'));
		
		$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=questions', false), $msg);
	}
	
	public function unpublish_list(){
		
		$return = $this->change_state(0);
		$msg = $return == 1 ? JText::_('MSG_SUCCESS') : ($return == 0 ? JText::_('MSG_ERROR') : JText::_('MSG_NO_ITEM_SELECTED'));
		
		$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=questions', false), $msg);
	}
	
	public function change_state($state){

		$app = JFactory::getApplication();
		$ids = $app->input->getArray(array('cid'=>'array'));
		
		if(!empty($ids['cid'])){
				
			$model = $this->getModel('questions');
			JArrayHelper::toInteger($ids['cid']);
				
			if($model->publish_questions($state, $ids['cid'])){
		
				return 1;
			} else {

				return 0;
			}
		}
		
		return -1;
	}
	
	public function delete(){

		$app = JFactory::getApplication();
		$ids = $app->input->post->getArray(array('cid'=>'array'));
		
		if(!empty($ids['cid'])){
		
			$model = $this->getModel('questions');
			JArrayHelper::toInteger($ids['cid']);
		
			if($model->delete_questions($ids['cid'])){
		
				$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=questions', false), JText::_('MSG_SUCCESS'));
			} else {
		
				$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=questions', false), JText::_('MSG_ERROR'));
			}
		} else {
		
			$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=questions', false), JText::_('MSG_NO_ITEM_SELECTED'));
		}
	}
	
	public function refresh(){

		$model = $this->getModel('categories');
		$model->refresh_categories();
		
		$model = $this->getModel('users');
		$model->refresh_user_stats();
		
		$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=questions', false), JText::_('MSG_SUCCESS'));
	}
	
	public function cancel(){
		
		$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=questions', false), JText::_('MSG_CANCELLED'));
	}
}