<?php
/**
 * @version		$Id: questions.php 01 2011-12-12 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');


jimport('joomla.application.component.controller');

class CommunityAnswersControllerUsers extends JControllerLegacy {

	function __construct(){

		parent::__construct();
		
		$this->registerDefaultTask('get_users_list');
		$this->registerTask('refresh', 'refresh');
		$this->registerTask('remove', 'remove_users');
	}
	
	public function get_users_list(){
		
		$view = $this->getView('users', 'html');
		$model = $this->getModel('users');
		
		$view->setModel($model, true);
		$view->display();
	}
    
    function refresh(){
    	
        $model = $this->getModel('users');
        $model->refresh_user_stats();
        $this->setRedirect('index.php?option='.A_APP_NAME.'&view=users', JText::_('MSG_USERS_REFRESHED'));
    }
	
	function remove_users(){

		$model = $this->getModel('users');
		$app = JFactory::getApplication();
		$ids = $app->input->getArray(array('cid'=>'array'));
		JArrayHelper::toInteger($ids['cid']);
		
		if(!empty($ids['cid']) && $model->delete($ids['cid'])){
		
			$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=users', false), JText::_('MSG_SUCCESS'));
		} else {
		
			$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=users', false), JText::_('MSG_ERROR'));
		}
	}
}