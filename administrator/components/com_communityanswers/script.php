<?php
/**
 * @version		$Id: script.php 01 2012-09-22 11:37:09Z maverick $
 * @package		CoreJoomla.CommunityAnswers
 * @subpackage	Components.site
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die('Restricted access');
defined('DS') or define('DS', DIRECTORY_SEPARATOR);
 
class com_communityanswersInstallerScript{
	
	function install($parent){
		
		$parent->getParent()->setRedirectURL('index.php?option=com_communityanswers');
	}
 
	function uninstall($parent){
		
		echo '<p>' . JText::_('COM_COMMUNITYANSWERS_UNINSTALL_TEXT') . '</p>';
	}
 
	function update($parent) {
		
		$db = JFactory::getDBO();
		
		if(method_exists($parent, 'extension_root')) {
			
			$sqlfile = $parent->getPath('extension_root').DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'install.mysql.utf8.sql';
		} else {
			
			$sqlfile = $parent->getParent()->getPath('extension_root').DIRECTORY_SEPARATOR.'sql'.DIRECTORY_SEPARATOR.'install.mysql.utf8.sql';
		}
		
		// Don't modify below this line
		$buffer = file_get_contents($sqlfile);
		
		if ($buffer !== false) {
			
			jimport('joomla.installer.helper');
			$queries = JInstallerHelper::splitSql($buffer);
			
			if (count($queries) != 0) {
				
				foreach ($queries as $query){
					
					$query = trim($query);
					
					if ($query != '' && $query{0} != '#') {
						
						$db->setQuery($query);
						
						try{
							$db->query();
						} catch(Exception $e){}
					}
				}
			}
		}
		
		JFactory::getApplication()->enqueueMessage(
			'<p>' . JText::sprintf('COM_COMMUNITYANSWERS_UPDATE_TEXT', $parent->get('manifest')->version) . '</p>');
		$parent->getParent()->setRedirectURL('index.php?option=com_communityanswers&view=dashboard');
	}
 
	function preflight($type, $parent) {
		
		echo '<p>' . JText::_('COM_COMMUNITYANSWERS_PREFLIGHT_' . $type . '_TEXT') . '</p>';
	}
 
	function postflight($type, $parent) {

		$db = JFactory::getDbo();
		
		$update_queries[] = 'ALTER IGNORE TABLE `#__answers_responses` ADD COLUMN `parent_id` int(10) unsigned NOT NULL DEFAULT \'0\'';
		$update_queries[] = 'ALTER IGNORE TABLE `#__answers_attachments` ADD COLUMN `downloads` int(10) unsigned NOT NULL DEFAULT \'0\'';
		$update_queries[] = 'ALTER IGNORE TABLE `#__answers_users` ADD COLUMN `karma` int(10) unsigned NOT NULL DEFAULT \'0\'';
		$update_queries[] = 'ALTER IGNORE TABLE `#__answers_users` ADD COLUMN `subid` varchar(12)';
		$update_queries[] = 'ALTER IGNORE TABLE `#__answers_tags` ADD COLUMN `description` MEDIUMTEXT';
		$update_queries[] = 'ALTER IGNORE TABLE `#__answers_categories` ADD COLUMN `description` MEDIUMTEXT';
		$update_queries[] = 'ALTER IGNORE TABLE `#__answers_categories` ADD COLUMN `language` VARCHAR(6) NOT NULL DEFAULT \'*\'';
		$update_queries[] = 'ALTER IGNORE TABLE `#__answers_ratings` ADD COLUMN `created` DATETIME NOT NULL DEFAULT \'0000-00-00 00:00:00\'';
		$update_queries[] = 'ALTER IGNORE TABLE `#__answers_categories` MODIFY COLUMN `nleft` INTEGER NOT NULL DEFAULT 0';
		$update_queries[] = 'ALTER IGNORE TABLE `#__answers_categories` MODIFY COLUMN `nright` INTEGER NOT NULL DEFAULT 0';

		$update_queries[] = 'ALTER IGNORE TABLE `#__answers_questions` ADD INDEX `idx_answers_questions_created_by`(`created_by`)';
		$update_queries[] = 'ALTER IGNORE TABLE `#__answers_responses` ADD INDEX `idx_answers_responses_created_by`(`created_by`)';
		$update_queries[] = 'ALTER IGNORE TABLE `#__answers_responses` ADD COLUMN `reply_type` TINYINT(3) UNSIGNED NOT NULL DEFAULT \'0\'';
		
		// Perform all queries - we don't care if it fails
		foreach( $update_queries as $query ) {
				
			$db->setQuery( $query );
			
			try{
			
				$db->query();
			}catch(Exception $e){}
		}
		
		jimport('joomla.filesystem.file');
		jimport('joomla.filesystem.folder');
		
		if(!JFolder::exists(JPATH_ROOT.DS.'media'.DS.'communityanswers'.DS.'attachments') && JFolder::create(JPATH_ROOT.DS.'media'.DS.'communityanswers'.DS.'attachments'))
		{
			
			touch(JPATH_ROOT.DS.'media'.DS.'communityanswers'.DS.'index.html');
			touch(JPATH_ROOT.DS.'media'.DS.'communityanswers'.DS.'attachments'.DS.'index.html');
		}
		
		// Move easysocial app
		if(JFolder::exists(JPATH_ROOT.'/media/com_communityanswers/easysocial/questions'))
		{
			JFolder::copy(JPATH_ROOT.'/media/com_communityanswers/easysocial', JPATH_ROOT.'/media/com_easysocial/apps/user', null, true);
		}
		
		$query = 'select count(*) from #__answers_categories where parent_id=0 and title='.$db->quote('Root');
		$db->setQuery($query);
		$count = $db->loadResult();
		
		if(!$count){
			
			$query = 'insert into #__answers_categories(title, alias, parent_id, nleft, nright, norder) values ('.$db->quote('Root').','.$db->quote('root').', 0, 1, 2, 1)';
			$db->setQuery($query);
			
			if($db->query()){
				
				$root = $db->insertid();
				$query = 'update #__answers_categories set parent_id='.$root.' where parent_id=0 and id!='.$root;
				$db->setQuery($query);
				
				if($db->query()){
					
					// CJLib includes
					$cjlib = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_cjlib'.DIRECTORY_SEPARATOR.'framework.php';
					
					if(file_exists($cjlib)){
					
						require_once $cjlib;
					}else{
					
						die('CJLib (CoreJoomla API Library) component not found. Please download and install it to continue.');
					}
					
					CJLib::import('corejoomla.framework.core');
					CJLib::import('corejoomla.nestedtree.core');
					
					$tree = new CjNestedTree($db, '#__answers_categories');
					
					if(!$tree->rebuild()){
						
						echo 'An error occurred while upgrading categories table. Please contact corejoomla support at support@corejoomla.com for assistance.';
					}
				}
			}else{
				echo 'An error occurred while upgrading categories table. Please contact corejoomla support at support@corejoomla.com for assistance.';
			}
		}
		
		echo '<p>' . JText::_('COM_COMMUNITYANSWERS_POSTFLIGHT_' . $type . '_TEXT') . '</p>';
	}
	
	private function migrate_categories($db){
	
		$query = $db->getQuery(true);
		$query->select('count(*) as row_count')->from('#__answers_categories')->where('migrate_id = 0');
		$db->setQuery($query);
	
		try {
	
			$count = $db->loadResult();
	
			if($count > 0){ // table exists, upgrade it
	
				$api = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_cjlib'.DIRECTORY_SEPARATOR.'framework.php';
					
				if(!file_exists($api)) {
	
					echo '<p style="color: red"><strong>Please install CjLib component to use Community Answers.</strong></p>';
				} else {
	
					require_once $api;
	
					CJLib::import('corejoomla.framework.core');
					CJLib::import('corejoomla.nestedtree.core');
	
					$tree = new CjNestedTree($db, '#__answers_categories');
					$tree = $tree->get_tree();
	
					if(count($tree)){
	
						$basePath = JPATH_ADMINISTRATOR . '/components/com_categories';
						require_once $basePath . '/models/category.php';
	
						$config = array( 'table_path' => $basePath . '/tables');
						$catmodel = new CategoriesModelCategory($config);
	
						foreach ($tree as $category){
	
							if($category['migrate_id'] == 0) {
									
								if(!$this->do_migration($db, $category, $catmodel, $category['alias'], 0, 1, 0)){
										
									echo 'An error occurred while migrating categories, please contact support.';
									return false;
								}
							}
						}
	
						return true;
					}
				}
			}
		} catch (Exception $e) {}
	
		return false;
	}
	
	private function do_migration($db, &$node, $catmodel, $path, $parent_id, $level, $parent_id){
	
		$catData = array(
				'id' => 0,
				'parent_id' => $parent_id,
				'level' => $level,
				'path' => $path,
				'extension' => 'com_communityanswers',
				'title' => $node['title'],
				'alias' => $node['alias'],
				'description' => '',
				'published' => 1,
				'language' => '*');
	
		$status = $catmodel->save( $catData);
		$migrate_id = $catmodel->getState('category.id');
	
		if(!$status || !$migrate_id){
	
			JError::raiseWarning(500, JText::_('Unable to create default content category!'));
			return false;
		}
	
		$node['migrate_id'] = $migrate_id;
	
		try {
				
			$query = $db->getQuery(true);
			$query->update('#__answers_categories')->set('migrate_id = '.$migrate_id)->where('id = '.$node['id']);
			$db->setQuery($query);
			$db->query();
				
			$query = $db->getQuery(true);
			$query->update('#__answers_questions')->set('catid = '.$migrate_id)->where('catid = '.$node['id']);
			$db->setQuery($query);
			$db->query();
		}catch (Exception $e){
				
			return false;
		}
	
		if(!empty($node['children'])){
				
			foreach ($node['children'] as $child) {
	
				if($child['migrate_id'] == 0) {
						
					if(!$this->do_migration($db, $child, $catmodel, $path.'/'.$child['alias'], $child['id'], $level + 1, $migrate_id)){
	
						return false;
					}
				}
			}
		}
	
		return true;
	}
}