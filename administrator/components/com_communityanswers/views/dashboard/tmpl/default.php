<?php
/**
 * @version		$Id: default.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
$app = JFactory::getApplication();
$params = JComponentHelper::getParams(A_APP_NAME);

$approval_link = JUri::root().'index.php?option='.A_APP_NAME.'&view=answers&task=approve&secret=';
$disapproval_link = JUri::root().'index.php?option='.A_APP_NAME.'&view=answers&task=disapprove&secret=';
?>
<div class="row-fluid">
	<?php if(APP_VERSION < 3):?>
	<div class="span2">
		<div class="sidebar-nav">
			<ul class="nav nav-tabs nav-stacked">
				<li class="active">
					<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=categories')?>">
						<i class="fa fa-folder-open-o"></i> <?php echo JText::_('COM_COMMUNITYANSWERS_CATEGORIES');?>
					</a>
				</li>
				<li>
					<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=questions')?>">
						<i class="fa fa-question-circle"></i> <?php echo JText::_('COM_COMMUNITYANSWERS_QUESTIONS');?>
					</a>
				</li>
				<li>
					<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers')?>">
						<i class="fa fa-comments-o"></i> <?php echo JText::_('COM_COMMUNITYANSWERS_ANSWERS');?>
					</a>
				</li>
				<li>
					<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=users')?>">
						<i class="fa fa-user"></i> <?php echo JText::_('COM_COMMUNITYANSWERS_USERS');?>
					</a>
				</li>
				<li>
					<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=migration')?>">
						<i class="fa fa-thumbs-o-up"></i> <?php echo JText::_('COM_COMMUNITYANSWERS_MIGRATION');?>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<?php endif;?>
	<div class="span<?php echo APP_VERSION < 3 ? 7 : 9;?>">
		<table class="table table-striped table-hover table-bordered">
			<caption><h4><?php echo JText::_('LBL_LATEST_QUESTIONS');?></h4></caption>
			<thead>
				<tr>
					<th><?php echo JText::_('LBL_TITLE')?></th>
					<th width="20%"><?php echo JText::_('LBL_USERNAME')?></th>
					<th width="20%"><?php echo JText::_('LBL_CATEGORY');?></th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($this->latest_questions)):?>
				<?php foreach ($this->latest_questions as $item):?>
				<tr>
					<td>
						<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=questions&task=edit&cid='.$item->id);?>">
							<?php echo CJFunctions::escape($item->title);?>
						</a>
					</td>
					<td><div class="tooltip-hover" title="<?php echo $item->username;?>"><?php echo CJFunctions::escape($item->name);?></div></td>
					<td><?php echo CJFunctions::escape($item->category);?></td>
				</tr>
				<?php endforeach;?>
				<?php endif;?>
			</tbody>
		</table>
	
		<table class="table table-striped table-hover table-bordered">
			<caption><h4><?php echo JText::_('LBL_PENDING_QUESTIONS');?></h4></caption>
			<thead>
				<tr>
					<th><?php echo JText::_('LBL_TITLE')?></th>
					<th width="20%"><?php echo JText::_('LBL_USERNAME')?></th>
					<th width="50px"><?php echo JText::_('LBL_APPROVE');?></th>
					<th width="50px"><?php echo JText::_('LBL_DISAPPROVE');?></th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($this->pending_questions)):?>
				<?php foreach ($this->pending_questions as $item):?>
				<tr>
					<td>
						<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=questions&task=edit&cid='.$item->id);?>">
							<?php echo CJFunctions::escape($item->title);?>
						</a>
					</td>
					<td><div class="tooltip-hover" title="<?php echo $item->username;?>"><?php echo CJFunctions::escape($item->name);?></div></td>
					<td style="text-align: center">
						<a class="btn btn-mini btn-success" href="<?php echo $approval_link.$item->secret.'&type=1'?>" target="_blank">
							<i class="fa fa-check-square-o"></i>
						</a>
					</td>
					<td style="text-align: center">
						<a class="btn btn-mini btn-danger" href="<?php echo $disapproval_link.$item->secret.'&type=1'?>" target="_blank">
							<i class="fa fa-remove"></i>
						</a>
					</td>
				</tr>
				<?php endforeach;?>
				<?php endif;?>
			</tbody>
		</table>
		
		<table class="table table-striped table-hover table-bordered">
			<caption><h4><?php echo JText::_('LBL_PENDING_ANSWERS');?></h4></caption>
			<thead>
				<tr>
					<th><?php echo JText::_('LBL_TITLE')?></th>
					<th width="20%"><?php echo JText::_('LBL_USERNAME')?></th>
					<th width="50px"><?php echo JText::_('LBL_APPROVE');?></th>
					<th width="50px"><?php echo JText::_('LBL_DISAPPROVE');?></th>
				</tr>
			</thead>
			<tbody>
				<?php if(!empty($this->pending_answers)):?>
				<?php foreach ($this->pending_answers as $item):?>
				<tr>
					<td>
						<div>
							<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=questions&task=edit&cid='.$item->question_id);?>">
								<?php echo CJFunctions::escape($item->title);?>
							</a>
						</div>
						<div><?php echo CJFunctions::process_html($item->description, $params->get('bbcode', 1) == 1);?></div>
					</td>
					<td><div class="tooltip-hover" title="<?php echo $item->username;?>"><?php echo CJFunctions::escape($item->name);?></div></td>
					<td style="text-align: center">
						<a class="btn btn-mini btn-success" href="<?php echo $approval_link.$item->secret.'&type=2'?>" target="_blank"><i class="fa fa-check-square-o"></i> </a>
					</td>
					<td style="text-align: center">
						<a class="btn btn-mini btn-danger" href="<?php echo $disapproval_link.$item->secret.'&type=2'?>" target="_blank"><i class="fa fa-remove"></i> </a>
					</td>
				</tr>
				<?php endforeach;?>
				<?php endif;?>
			</tbody>
		</table>
	</div>
	<div class="span3">
		<table class="table table-hover table-striped table-bordered">
			<caption><h4>Version Information</h4></caption>
			<tbody>
				<tr>
					<td>Installed Version:</td>
					<td><?php echo A_APP_VERSION;?></td>
				<tr>
				<?php if(!empty($this->version)):?>
				<tr>
					<td>Latest Version:</td>
					<td><?php echo $this->version['version'];?></td>
				</tr>
				<tr>
					<td>Release Date:</td>
					<td><?php echo $this->version['released'];?></td>
				</tr>
				<tr>
					<td style="text-align: center;">
						<?php if($this->version['status'] == 1):?>
						<a href="http://www.corejoomla.com/downloads.html" target="_blank" class="btn btn-danger">
							<i class="fa fa-download"></i> <span style="color: white">Download</span>
						</a>
						<?php else:?>
						<a href="#" class="btn btn-success"><i class="fa fa-check-square-o"></i> <span style="color: white">Up-to date</span></a>
						<?php endif;?>
					</td>
					<td>
						<?php //echo LiveUpdate::getIcon(); ?>
					</td>
				</tr>
				<?php endif;?>
			</tbody>
		</table>
		
		<div class="well">
			<strong>If you use Community Answers, please post a rating and a review at the Joomla Extension Directory</strong>
			<div style="text-align: center; margin-top: 10px;">
				<a class="btn btn-primary" href="http://extensions.joomla.org/extensions/communication/question-a-answers/21724" target="_blank">
					<i class="fa fa-share-alt"></i> <span style="color: white">Post Your Review</span>
				</a>
			</div>
		</div>
		
		<div class="well">
			<div><strong>Credits: </strong></div>
			<div>Community Answers is a free software released under Gnu/GPL license. Copyright© 2009-12 corejoomla.com</div>
			<div>Core Components: Bootstrap, jQuery and ofcourse Joomla®.</div>
			<div>Few icons are taken from icomoon (MIT).</div>
		</div>
	</div>
</div>