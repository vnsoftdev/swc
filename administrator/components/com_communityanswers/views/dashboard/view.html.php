<?php
/**
 * @version		$Id: view.html.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
jimport ( 'joomla.application.component.view' );

class CommunityAnswersViewDashboard extends JViewLegacy {
	
	protected $params;
	
	function display($tpl = null) {
		
		JToolBarHelper::title(JText::_('COM_COMMUNITYANSWERS_DASHBOARD'), 'logo.png');
		
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$version = null;
		
		$latest_questions = $model->get_latest_questions();
		$pending_questions = $model->get_pending_questions();
		$pending_answers = $model->get_pending_answers();
		$version = $app->getUserState(A_APP_NAME.'.VERSION');
		
		$this->assignRef('latest_questions', $latest_questions);
		$this->assignRef('pending_questions', $pending_questions);
		$this->assignRef('pending_answers', $pending_answers);
		$this->assignRef('version', $version);
		
		parent::display($tpl);
	}	
}