<?php
/**
 * @version		$Id: view.html.php 01 2012-06-30 11:37:09Z maverick $
 * @package		CoreJoomla.Quiz
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Import Joomla! libraries
jimport( 'joomla.application.component.view');

class CommunityAnswersViewMigration extends JViewLegacy {
	
	function display($tpl = null) {
		
		JToolBarHelper::title(JText::_('COM_COMMUNITYANSWERS_MIGRATION'), 'logo.png');
		$model = $this->getModel('migration');
		$simanswers = $model->check_simanswers_installed();
		
		$this->assign('simanswers_installed', $simanswers);
		parent::display($tpl);
	}
}
?>