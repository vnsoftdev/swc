<?php
/**
 * @version		$Id: default.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
?>
<div class="row-fluid">
	<div class="span2">
		<div class="sidebar-nav">
			<ul class="nav nav-tabs nav-stacked">
				<li>
					<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=categories')?>">
						<i class="fa fa-folder-open-o"></i> <?php echo JText::_('COM_COMMUNITYANSWERS_CATEGORIES');?>
					</a>
				</li>
				<li>
					<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=questions')?>">
						<i class="fa fa-question-circle"></i> <?php echo JText::_('COM_COMMUNITYANSWERS_QUESTIONS');?>
					</a>
				</li>
				<li>
					<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers')?>">
						<i class="fa fa-comments-o"></i> <?php echo JText::_('COM_COMMUNITYANSWERS_ANSWERS');?>
					</a>
				</li>
				<li>
					<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=users')?>">
						<i class="fa fa-user"></i> <?php echo JText::_('COM_COMMUNITYANSWERS_USERS');?>
					</a>
				</li>
				<li class="active">
					<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=migration')?>">
						<i class="fa fa-thumbs-o-up"></i> <?php echo JText::_('COM_COMMUNITYANSWERS_MIGRATION');?>
					</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="span10">
		<?php if($this->simanswers_installed):?>
		<div class="well">
			<h2 class="page-header no-margin-top">Sim Answers</h2>
			<div class="sim-answers-step1">
				<div class="alert alert-info"><i class="fa fa-info-circle"></i> SimAnswers migration is a lossy process and not everythis is upgraded. Though we tried our best to move maximum possible data from SimAnswers, there is always a room for improvement. If you feel there is still something can be possibly migrated, please do contact us.</div>
				<div class="alert alert-block"><i class="fa fa-warning"></i> Any data available in Community Answers will be deleted before migrating. Having a full database backup is a good idea to avoid data loss.</div>
				<div><button class="btn btn-primary btn-migrate-simanswers" type="button"><?php echo JText::_('LBL_MIGRATE_SIMANSWERS')?></button></div>
				
				<div class="migration-progress hide">
					<strong>Please wait while we are migrating your Sim Answers data to Community Answers.</strong>
					<div class="progress progress-striped active" style="width: 400px;">
						<div class="bar" style="width: 100%;"></div>
					</div>
				</div>
			</div>
			
			<div class="sim-answers-step2 hide">
				<p class="lead">Congratulations. Data migration successfully completed. Follow below steps to complete installation.</p>
				<ol>
					<li>Go to Categories tab and click refresh button. If you don't see categories, do it couple of more times. Still facing issues, please contact us.</li>
					<li>Go to Users tab and click refresh button to sync users.</li>
					<li>Go to Questions tab and click refresh button to sync question statistics.</li>
					<li>Check and verify component settings from Options button on toolbar.</li>
				</ol>
			</div>
			
			<div style="display: none;" id="url-migrate-simanswer"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=migration&task=simanswers');?></div>
		</div>
		<?php else:?>
		<div class="alert alert-info"><i class="fa fa-info-circle"></i> You do not have any other compatible Q&A components installed to migrate from.</div>
		<?php endif;?>
	</div>
</div>