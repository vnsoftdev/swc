<?php
/**
 * @version		$Id: default.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
?>
<form id="adminForm" name="adminForm" method="post" action="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=users');?>">
	<div class="well well-small clearfix">
		<div class="input-append">
			<input name="search" type="text" value="<?php echo CJFunctions::escape($this->lists['search']);?>">
			<button class="btn" type="button"><?php echo JText::_('LBL_SEARCH');?></button>
		</div>
	</div>
	
	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th width="20"><?php echo JText::_( '#' ); ?></th>
				<th width="20"><input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" /></th>
				<th><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_NAME' ), 'u.name', $this->lists['order_dir'], $this->lists['order']); ?></th>
				<th width="20%"><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_USERNAME' ), 'u.username', $this->lists['order_dir'], $this->lists['order']); ?></th>
				<th width="5%"><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_QUESTIONS' ), 'au.questions', $this->lists['order_dir'], $this->lists['order']); ?></th>
				<th width="5%"><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_ACCEPTED' ), 'au.accepted', $this->lists['order_dir'], $this->lists['order']); ?></th>
				<th width="5%"><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_ANSWERS' ), 'au.answers', $this->lists['order_dir'], $this->lists['order']);?></th>
				<th width="5%"><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_BEST_ANSWERS' ), 'au.best_answers', $this->lists['order_dir'], $this->lists['order']);?></th>
				<th width="10%"><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_LAST_VISITED' ), 'u.lastvisitDate', $this->lists['order_dir'], $this->lists['order']);?></th>
				<th width="20"><?php echo JText::_('ID');?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($this->users as $i=>$row):?>
			<tr>
				<td><?php echo $this->pagination->getRowOffset( $i ); ?></td>
				<td><?php echo JHTML::_( 'grid.id', $i, $row->id );?></td>
				<td><?php echo $this->escape($row->name)?></td>
				<td><?php echo $this->escape($row->username); ?></td>
				<td><?php echo $row->questions?></td>
				<td><?php echo $row->accepted; ?></td>
				<td><?php echo $row->answers ?></td>
				<td><?php echo $row->best_answers; ?></td>
				<td><?php echo $row->lastvisitDate;?></td>
				<td><?php echo $row->id;?></td>
			</tr>
			<?php endforeach;?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="10" align="center"><?php echo $this->pagination->getListFooter(); ?></td>
			</tr>
		</tfoot>
	</table>
	
	<input type="hidden" name="task" value="list" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php if($this->lists['order']) echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php if($this->lists['order_dir']) echo $this->lists['order_dir']; ?>" />
</form>