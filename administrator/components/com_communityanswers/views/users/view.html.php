<?php
/**
 * @version		$Id: view.html.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
jimport ( 'joomla.application.component.view' );

class CommunityAnswersViewUsers extends JViewLegacy {
	
	protected $params;
	
	function display($tpl = null) {
		
		JToolBarHelper::title(JText::_('COM_COMMUNITYANSWERS_USERS'), 'logo.png');
		
		$model = $this->getModel();
		
		$result = $model->get_users();
		
		$this->assignRef('users', $result['users']);
		$this->assignRef('pagination', $result['pagination']);
		$this->assignRef('lists', $result['lists']);
		
		JToolbarHelper::deleteList();
		JToolbarHelper::divider();
		JToolBarHelper::custom('refresh', 'refresh.png', 'refresh.png', JText::_('LBL_REFRESH'), false, false);
		
		parent::display($tpl);
	}	
}