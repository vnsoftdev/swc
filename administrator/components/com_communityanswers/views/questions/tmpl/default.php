<?php
/**
 * @version		$Id: default.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
?>
<form id="adminForm" action="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=questions&task=list');?>" method="post" name="adminForm">
	<div class="well well-small clearfix">
		<div class="pull-right">
			<select name="state" size="1" onchange="this.form.submit();">
				<option value="3"<?php echo $this->lists['state'] == 3 ? ' selected="selected"' : '';?>><?php echo JText::_('JALL');?></option>
				<option value="1"<?php echo $this->lists['state'] == 1 ? ' selected="selected"' : '';?>><?php echo JText::_('LBL_PUBLISHED');?></option>
				<option value="0"<?php echo $this->lists['state'] == 0 ? ' selected="selected"' : '';?>><?php echo JText::_('LBL_UNPUBLISHED');?></option>
				<option value="2"<?php echo $this->lists['state'] == 2 ? ' selected="selected"' : '';?>><?php echo JText::_('LBL_PENDING');?></option>
			</select>
			<select name="catid" size="1" onchange="this.form.submit();">
				<option><?php echo JText::_('LBL_FILTER_BY_CATEGORY');?></option>
				<?php if(!empty($this->categories)):?>
				<?php foreach($this->categories as $catid=>$title):?>
				<option value="<?php echo $catid;?>" <?php echo ($this->lists['catid'] == $catid) ? 'selected="selected"' : '';?>><?php echo CJFunctions::escape($title);?></option>
				<?php endforeach;?>
				<?php endif;?>
			</select>
			<select name="uid" size="1" onchange="this.form.submit();">
				<option><?php echo JText::_('LBL_FILTER_BY_USER');?></option>
				<?php if(!empty($this->users)):?>
				<?php foreach($this->users as $user):?>
				<option value="<?php echo $user->id?>"<?php echo $this->lists['uid'] == $user->id ? ' selected="selected"' : ''?>><?php echo CJFunctions::escape($user->name);?></option>
				<?php endforeach;?>
				<?php endif;?>
			</select>
		</div>
		<div class="input-append">
			<input name="search" type="text" value="<?php echo CJFunctions::escape($this->lists['search']);?>">
			<button class="btn" type="button"><?php echo JText::_('LBL_SEARCH');?></button>
		</div>
	</div>
	
	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th width="20"><?php echo JText::_( '#' ); ?></th>
				<th width="20"><input type="checkbox" name="toggle" value="" onclick="Joomla.checkAll(this);" /></th>
				<th><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_TITLE' ), 'a.title', $this->lists['order_dir'], $this->lists['order']); ?></th>
				<th width="10%"><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_CATEGORY' ), 'c.title', $this->lists['order_dir'], $this->lists['order']); ?></th>
				<th width="8%"><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_USERNAME' ), 'u.username', $this->lists['order_dir'], $this->lists['order']); ?></th>
				<th width="12%"><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_CREATED_ON' ), 'a.created', $this->lists['order_dir'], $this->lists['order']); ?></th>
				<th width="4%"><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_ANSWERS' ), 'a.answers', $this->lists['order_dir'], $this->lists['order']);?></th>
				<th width="4%"><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_HITS' ), 'a.hits', $this->lists['order_dir'], $this->lists['order']);?></th>
				<th width="4%"><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_PUBLISHED' ), 'a.published', $this->lists['order_dir'], $this->lists['order']); ?></th>
				<th width="4%"><?php echo JHTML::_( 'grid.sort', JText::_( 'LBL_SOLVED' ), 'a.accepted_answer', $this->lists['order_dir'], $this->lists['order']);?></th>
				<th width="20"><?php echo JText::_('ID');?></th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($this->questions as $i=>$row):?>
			<tr>
				<td><?php echo $this->pagination->getRowOffset( $i ); ?></td>
				<td><?php echo JHTML::_( 'grid.id', $i, $row->id );?></td>
				<td><a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=questions&task=edit&cid='.$row->id)?>"><?php echo $this->escape($row->title);?></a></td>
				<td><?php echo $this->escape($row->category); ?></td>
				<td><?php echo $row->created_by ? $this->escape($row->username) : $row->user_name . ' ('.JText::_('LBL_GUEST').')'; ?></td>
				<td><?php echo JHTML::Date($row->created, JText::_('DATE_FORMAT_LC2')); ?></td>
				<td><?php echo $this->escape($row->answers); ?></td>
				<td><?php echo $this->escape($row->hits); ?></td>
				<td class="center">
					<a 
						class="btn btn-mini <?php echo $row->published == 1 ? 'btn-success' : ($row->published == 2 ? 'btn-warning' : 'btn-danger');?> tooltip-hover btn-publish" 
						title="<?php echo $row->published == 1 ? JText::_('LBL_PUBLISHED') : ($row->published == 2 ? JText::_('LBL_PENDING') : JText::_('LBL_UNPUBLISHED'));?>"
						href="#"
						onclick="return false;">
						<i class="<?php echo $row->published == 1 ? 'fa fa-check-square-o' : 'fa fa-trash-o'; ?>"></i>
					</a>
					<input type="hidden" name="question_id" value="<?php echo $row->id?>">
				</td>
				<td class="center">
					<a class="btn btn-mini <?php echo $row->solved == 1 ? 'btn-success' : 'btn-danger'?>" href="#" onclick="return false;">
						<i class="<?php echo $row->solved == 1 ? 'fa fa-check-square-o' : 'fa fa-times'; ?>"></i>
					</a>
				</td>
				<td><?php echo $row->id;?></td>
			</tr>
			<?php endforeach;?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="11"><?php echo $this->pagination->getListFooter(); ?></td>
			</tr>
		</tfoot>
	</table>
	
	<input type="hidden" name="task" value="list" />
	<input type="hidden" name="boxchecked" value="0" />
	<input type="hidden" name="filter_order" value="<?php if($this->lists['order']) echo $this->lists['order']; ?>" />
	<input type="hidden" name="filter_order_Dir" value="<?php if($this->lists['order_dir']) echo $this->lists['order_dir']; ?>" />
</form>

<div style="display: none;">
	<div id="url-publish-question"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=questions&task=publish_item');?></div>
	<div id="url-unpublish-question"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=questions&task=unpublish_item');?></div>
</div>
			
<div id="message-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3 id="myModalLabel"><?php echo JText::_('LBL_ALERT');?></h3>
	</div>
	<div class="modal-body"></div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('LBL_CLOSE');?></button>
	</div>
</div>
