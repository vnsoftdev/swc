<?php
/**
 * @version		$Id: default.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

$editor = $this->params->get('default_editor', 'bbcode');
?>
<div class="container-fluid question-wrapper nospace-left no-space-left no-space-right">
	<div class="row-fluid">
		<div class="span8">
			<form class="adminForm form-horizontal" id="adminForm" action="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=questions&task=save')?>" method="post">
				<div class="control-group">
					<label class="control-label" for="input-title"><?php echo JText::_('LBL_TITLE');?><sup>*</sup></label>
					<div class="controls">
						<input type="text" name="title" id="input-title" class="required input-xxlarge" value="<?php echo CJFunctions::escape($this->item->title);?>">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input-alias"><?php echo JText::_('LBL_ALIAS');?><sup>*</sup></label>
					<div class="controls">
						<input type="text" name="alias" id="input-alias" class="required input-xxlarge" value="<?php echo CJFunctions::escape($this->item->alias);?>">
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input-alias"><?php echo JText::_('LBL_PUBLISHED');?><sup>*</sup></label>
					<div class="controls">
						<select name="published" size="1">
							<option value="1"<?php echo $this->item->published == 1 ? 'selected="selected"' : '';?>><?php echo JText::_('LBL_PUBLISHED');?></option>
							<option value="0"<?php echo $this->item->published == 0 ? 'selected="selected"' : '';?>><?php echo JText::_('LBL_UNPUBLISHED');?></option>
							<option value="2"<?php echo $this->item->published == 2 ? 'selected="selected"' : '';?>><?php echo JText::_('LBL_PENDING');?></option>
						</select>
					</div>
				</div>
				<div class="control-group">
					<label class="control-label" for="input-alias"><?php echo JText::_('LBL_CATEGORY');?><sup>*</sup></label>
					<div class="controls">
						<select name="catid" size="1">
							<?php if(!empty($this->categories)):?>
							<?php foreach($this->categories as $catid=>$title):?>
							<option value="<?php echo $catid;?>"<?php echo ($this->item->catid == $catid) ? ' selected="selected"' : '';?>>
								<?php echo CJFunctions::escape($title);?>
							</option>
							<?php endforeach;?>
							<?php endif;?>
						</select>
					</div>
				</div>
				
				<div class="control-group">
					<label class="control-label" for="answer"><?php echo JText::_('LBL_DESCRIPTION');?></label>
					<div class="controls">
						<?php echo CJFunctions::load_editor($editor, 'description', 'description', $this->item->description, '5', '40', '100%', '200px', '', 'width: 99%;'); ?>
					</div>
				</div>
				
				<input type="hidden" name="task" value="save">
				<input type="hidden" name="cid" value="<?php echo $this->item->id;?>">
			</form>
		</div>
	</div>
</div>