<?php
/**
 * @version		$Id: view.html.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
jimport ( 'joomla.application.component.view' );

class CommunityAnswersViewQuestions extends JViewLegacy {
	
	protected $params;
	
	function display($tpl = null) {
		
		JToolBarHelper::title(JText::_('COM_COMMUNITYANSWERS_QUESTIONS'), 'logo.png');
		
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$categories_model = $this->getModel('categories');
		
		/********************************** PARAMS *****************************/
		$this->params = JComponentHelper::getParams(A_APP_NAME);
		/********************************** PARAMS *****************************/
		
		switch ($this->action){
			
			case 'list':
				
				$result = $model->get_questions();
				$questions = !empty($result['questions']) ? $result['questions'] : array();
				$categories = $categories_model->get_categories_tree();
				
				$users_model = $this->getModel('users');
				$users = $users_model->get_all_active_users();
				
				$this->assignRef('questions', $questions);
				$this->assignRef('categories', $categories);
				$this->assignRef('users', $users);
				$this->assignRef('pagination', $result['pagination']);
				$this->assignRef('lists', $result['lists']);
				
				JToolBarHelper::custom('refresh', 'refresh.png', 'refresh.png', JText::_('LBL_REFRESH'), false, false);
				JToolBarHelper::divider();
				JToolBarHelper::publish();
				JToolBarHelper::unpublish();
				JToolBarHelper::deleteList();
				
				break;
				
			case 'form':
				
				$id = $app->input->getInt('cid', 0);
				if($id <= 0) return CJFunctions::throw_error(JText::_('JERROR_ALERTNOAUTH'), 403);
				
				if(empty($this->item)){
					
					$question = $model->get_question($id);
					if(empty($question)) return CJFunctions::throw_error(JText::_('JERROR_ALERTNOAUTH'), 403);
					
					$this->assignRef('item', $question);
				}
				
				$categories = $categories_model->get_categories_tree();
				$this->assignRef('categories', $categories);
				
				JToolBarHelper::save();
				JToolBarHelper::cancel();
				
				$tpl = 'form';
				
				break;
		}
		
		parent::display($tpl);
	}	
}