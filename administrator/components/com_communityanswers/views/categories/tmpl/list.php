<?php
/**
 * @version		$Id: list.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.Quiz
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

defined('_JEXEC') or die('Restricted access');
?>
<form name="adminForm" id="adminForm" action="index.php?option=<?php echo A_APP_NAME;?>&view=categories" method="post">
	<?php
	if (count($this->categories) > 0){
	?>
	<table class="table table-hover table-condensed table-striped adminlist">
	    <thead>
	        <tr>
	        	<th width="20px">#</th>
	            <th><?php echo JText::_('LBL_CATEGORY');?></th>
	            <th width="50px"><?php echo JText::_('LBL_QUESTIONS');?></th>
	            <th width="50px"><?php echo JText::_('LBL_ANSWERS');?></th>
	            <th width="25px"></th>
	            <th width="25px"></th>
	            <th width="50px"><?php echo JText::_('LBL_EDIT');?></th>
	            <th width="50px"><?php echo JText::_('LBL_DELETE');?></th>
	            <th width="50px">ID</th>
	        </tr>
	    </thead>
	    <tbody>
	    	<?php 
	    	$db = JFactory::getDbo();
	    	$tree = new CjNestedTree($db, '#__answers_categories');
	    	$content = '';
	    	static $row_num = 0;
	    	
	    	$base_uri = 'index.php?option='.A_APP_NAME.'&view=categories';
	    	$fields = array();
	    		    	
	    	$fields[] = array('header'=>JText::_('LBL_CATEGORY'), 'name'=>'title', 'type'=>'category', 'align'=>'left', 
	    					'src'=>$base_uri.'&task=edit', 'id'=>true, 'value'=>null);
	    	$fields[] = array('header'=>JText::_('LBL_QUESTIONS'), 'name'=>'questions', 'type'=>'text', 'align'=>'center');
	    	$fields[] = array('header'=>JText::_('LBL_ANSWERS'), 'name'=>'answers', 'type'=>'text', 'align'=>'center');
	    	$fields[] = array('header'=>'', 'name'=>null, 'type'=>'up', 'align'=>'center', 'src'=>$base_uri.'&task=move_up', 'id'=>true, 
							'value'=>'<i class="fa fa-arrow-up"></i>', 'attribs'=>array('class'=>'btn btn-mini tooltip-hover', 'title'=>JText::_('LBL_MOVE_UP')));
	    	$fields[] = array('header'=>'', 'name'=>null, 'type'=>'down', 'align'=>'center', 'src'=>$base_uri.'&task=move_down', 'id'=>true, 
							'value'=>'<i class="fa fa-arrow-down"></i>', 'attribs'=>array('class'=>'btn btn-mini tooltip-hover', 'title'=>JText::_('LBL_MOVE_DOWN')));
	    	$fields[] = array('header'=>JText::_('LBL_EDIT'), 'name'=>null, 'type'=>'link', 'align'=>'center', 'src'=>$base_uri.'&task=edit', 'id'=>true, 'value'=>null);
	    	$fields[] = array('header'=>JText::_('LBL_DELETE'), 'name'=>null, 'type'=>'link', 'align'=>'center', 'src'=>$base_uri.'&task=delete', 'id'=>true, 'value'=>null);
	    	$fields[] = array('header'=>JText::_('ID'), 'name'=>'id', 'type'=>'text', 'align'=>'center');
	    	
	    	echo $tree->get_tree_table($content, $this->categories, $fields);
	    	?>
	    </tbody>
	</table>
	<?php
	}else{
	    echo 'No categories found';
	}
	?>
    <input type="hidden" name="task" value="add">
</form>
