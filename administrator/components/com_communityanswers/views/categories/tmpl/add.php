<?php
/**
 * @version		$Id: add.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.Quiz
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die('Restricted access');
?>
<form name="adminForm" id="adminForm" action="index.php?option=<?php echo A_APP_NAME;?>&view=categories" method="post">
	<div class="col100">
		<table class="table table-hover table-striped">
			<tr>
				<th width="150px"><label class="hasTip" title="<?php echo JText::_('LBL_TITLE'); ?>" for="category"><?php echo JText::_('LBL_TITLE'); ?></label></th>
				<td><input class="text_area" type="text" name="title" id="title" size="32" maxlength="250" value="<?php echo $this->category['title'];?>" /></td>
			</tr>
			<tr>
				<th width="150px"><label class="hasTip" title="<?php echo JText::_('LBL_ALIAS'); ?>" for="category"><?php echo JText::_('LBL_ALIAS'); ?></label></th>
				<td><input class="text_area" type="text" name="alias" id="alias" size="32" maxlength="250" value="<?php echo $this->category['alias'];?>" /></td>
			</tr>
			<tr>
				<th><label class="hasTip" title="<?php echo JText::_('LBL_PARENT_CATEGORY'); ?>" for="category"><?php echo JText::_( 'LBL_PARENT_CATEGORY' ); ?>:</label></th>
				<td>
					<select name="category" id="category">
						<?php if(!empty($this->categories)):?>
						<?php foreach($this->categories as $catid=>$title):?>
						<option value="<?php echo $catid;?>" <?php echo ($this->category['parent_id'] == $catid) ? 'selected="selected"' : '';?>><?php echo CJFunctions::escape($title);?></option>
	                    <?php endforeach;?>
	                    <?php endif;?>
					</select>
				</td>
			</tr>
            <tr>
            	<td colspan="2"><hr><?php echo JText::_('MSG_PERMISSION_HELP')?><hr></td>
            </tr>
            <tr>
                <td width="100" align="right" class="key">
                    <span class="editlinktip hasTip" title="<?php echo JText::_( 'LBL_PERMISSION_VIEW_QUESTION_DESC' );?>">
                    	<?php echo JText::_( 'LBL_PERMISSION_VIEW_QUESTION' ); ?>:
                    </span>
                </td>
                <td>
                	<?php echo 
                		CJFunctions::get_user_groups_tree('permission_view','permission_view'.'[]', 
							(!empty($this->category) && !empty($this->category['permission_view'])) ? explode(",",$this->category['permission_view']) : array());?>
                </td>
            </tr>
            <tr>
                <td width="100" align="right" class="key">
                    <span class="editlinktip hasTip" title="<?php echo JText::_( 'LBL_PERMISSION_ASK_QUESTION_DESC' );?>">
                    	<?php echo JText::_( 'LBL_PERMISSION_ASK_QUESTION' ); ?>:
                    </span>
                </td>
                <td>
                	<?php echo 
                		CJFunctions::get_user_groups_tree('permission_ask','permission_ask'.'[]',(!empty($this->category) && 
							!empty($this->category['permission_ask'])) ? explode(",",$this->category['permission_ask']) : array());?>
                </td>
            </tr>
            <tr>
                <td width="100" align="right" class="key">
                     <span class="editlinktip hasTip" title="<?php echo JText::_( 'LBL_PERMISSION	_ANSWER_QUESTION_DESC' );?>">
                     	<?php echo JText::_( 'LBL_PERMISSION_ANSWER_QUESTION' ); ?>:
                     </span>
                </td>
                <td>
                	<?php echo 
                		CJFunctions::get_user_groups_tree('permission_answer','permission_answer'.'[]',(!empty($this->category) && 
							!empty($this->category['permission_answer'])) ? explode(",",$this->category['permission_answer']) : array());?>
                </td>
            </tr>
		</table>
	</div>
    <input type="hidden" name="id" value="<?php echo $this->category['id'];?>">
    <input type="hidden" name="view" value="categories">
    <input type="hidden" name="task" value="add">
</form>