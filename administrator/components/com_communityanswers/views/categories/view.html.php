<?php
/**
 * @version		$Id: view.html.php 01 2012-06-30 11:37:09Z maverick $
 * @package		CoreJoomla.Quiz
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

// Import Joomla! libraries
jimport( 'joomla.application.component.view');

class CommunityAnswersViewCategories extends JViewLegacy {
	
	function display($tpl = null) {
		
		JToolBarHelper::title(JText::_('COM_COMMUNITYANSWERS_CATEGORIES'), 'logo.png');
		$model = $this->getModel('categories');
		
	        
        if($this->getLayout() == 'list') {
        	
            JToolBarHelper::custom('refresh', 'refresh.png', 'refresh.png', JText::_('LBL_REFRESH'), false, false);
            JToolBarHelper::addNew();
        	
            $categories = $model->get_categories();
            $this->assignRef('categories',$categories);
            
        }else if($this->getLayout() == 'add') {
        	
            JToolBarHelper::save();
            JToolBarHelper::cancel();
        	
            $id = JRequest::getVar('id', 0, '', 'int');
            $category = array();
            
            if($id){
            	
                $category = $model->get_category($id);
            } else{
            	
            	$category['id'] = $category['num_questions'] = $category['num_answers'] = $category['parent_id'] = 0;
            	$category['title'] = $category['alias'] = '';
            }

            $this->assignRef('category', $category);
            $categories = $model->get_categories_tree(0, true);
            $this->assignRef('categories', $categories);
        }
        
		parent::display($tpl);
	}
}
?>