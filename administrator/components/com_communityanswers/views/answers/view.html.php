<?php
/**
 * @version		$Id: view.html.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
jimport ( 'joomla.application.component.view' );
jimport( 'joomla.application.component.helper' );

class CommunityAnswersViewAnswers extends JViewLegacy {
	
	protected $params;
	
	function display($tpl = null) {
		
		JToolBarHelper::title(JText::_('COM_COMMUNITYANSWERS_ANSWERS'), 'logo.png');
		
		$model = $this->getModel();
		$categories_model = $this->getModel('categories');
		$users_model = $this->getModel('users');
		$this->params = JComponentHelper::getParams(A_APP_NAME);
		
		$result = $model->get_answers();
		$answers = !empty($result['answers']) ? $result['answers'] : array();
		$categories = $categories_model->get_categories_tree();
		$users = $users_model->get_all_active_users();
		
		$this->assignRef('answers', $answers);
		$this->assignRef('categories', $categories);
		$this->assignRef('users', $users);
		$this->assignRef('pagination', $result['pagination']);
		$this->assignRef('lists', $result['lists']);
		
		JToolBarHelper::custom('refresh', 'refresh.png', 'refresh.png', JText::_('LBL_REFRESH'), false, false);
		JToolBarHelper::divider();
		JToolBarHelper::publish();
		JToolBarHelper::unpublish();
		JToolBarHelper::deleteList();
		
		parent::display($tpl);
	}	
}