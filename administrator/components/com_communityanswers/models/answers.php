<?php
/**
 * @version		$Id: answers.php 01 2011-01-12 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class CommunityAnswersModelAnswers extends JModelLegacy {
	
	function __construct() {
		 
		parent::__construct();
	}
	
	public function get_answers(){
	
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
	
		$filter_order = $app->getUserStateFromRequest( A_APP_NAME.'.answers.filter_order', 'filter_order', 'r.created', 'cmd' );
		$filter_order_dir = $app->getUserStateFromRequest( A_APP_NAME.'.answers.filter_order_dir', 'filter_order_Dir', 'DESC', 'word' );
		$limitstart = $app->getUserStateFromRequest( A_APP_NAME.'.answers.limitstart', 'limitstart', '', 'int' );
		$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit', 20), 'int');
	
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
	
		$catid = $app->input->post->getInt('catid', 0);
		$userid = $app->input->post->getInt('uid', 0);
		$state = $app->input->post->getInt('state', 3);
		$search = $app->input->post->getString('search', '');
	
		$wheres = array();
		$return = array();
	
		if($catid){
			 
			$wheres[] = 'q.catid = '.$catid;
		}
	
		if($userid){
			 
			$wheres[] = 'r.created_by = '.$userid;
		}
	
		if($state >= 0 && $state < 3){
			 
			$wheres[] = 'r.published = '.$state;
		}
	
		if(!empty($search)){
			 
			$wheres[] = 'r.description like \'%'.$this->_db->escape($search).'%\'';
		}
	
		$where = ((count($wheres) > 0) ? ' where ('.implode(' ) and ( ', $wheres).')' : '');
		$order = ' order by ' . $filter_order . ' ' . $filter_order_dir;
	
		$query = '
        		select
					q.id as question_id, q.title, q.alias, q.accepted_answer,
					r.id, r.description, r.created_by, r.created, r.published, r.user_name, 
					c.title as category, c.alias as cat_alias,
					u.name, u.username, case when r.created_by > 0 then r.email else u.email end as email
        		from
					#__answers_responses as r
				left join
        			#__answers_questions as q on r.question_id = q.id
        		left join
        			#__answers_categories as c ON q.catid = c.id
        		left join
        			#__users u ON r.created_by = u.id'.
	        	$where.
	        	$order;
	
		$this->_db->setQuery($query, $limitstart, $limit);
		$return['answers'] = $this->_db->loadObjectList();
	
		/************ pagination *****************/
		$query = '
        		select
        			count(*)
        		from
        			#__answers_responses r
				left join
					#__answers_questions q on r.question_id = q.id
        		left join
        			#__answers_categories c on q.catid = c.id
        		left join
        			#__users u on r.created_by = u.id
        		'.$where;
	
		jimport('joomla.html.pagination');
		$this->_db->setQuery($query);
		$total = $this->_db->loadResult();
	
		$return['pagination'] = new JPagination( $total, $limitstart, $limit );
		/************ pagination *****************/
	
		$return['lists'] = array(
				'limitstart'=>$limitstart,
				'limit'=>$limit,
				'order'=>$filter_order,
				'order_dir'=>$filter_order_dir,
				'catid'=>$catid,
				'search'=>$search,
				'uid'=>$userid,
				'state'=>$state);
	
		return $return;
	}
	
	public function publish_answers($state, $ids){
		
		$query = 'update #__answers_responses set published = '.$state.' where id in ('.implode(',', $ids).')';
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			$query = '
					update 
						#__answers_categories 
					set 
						answers = answers '.($state == 1 ? ' + ' : ' - ').' 1 
					where 
						id in (
								select 
									catid 
								from 
									#__answers_questions 
								where 
									id in (select question_id from #__answers_responses where id in ('.implode(',', $ids).') ) )'.
									($state == 0 ? ' and answers > 0' : '');
			
			$this->_db->setQuery($query);
			$this->_db->query();
			
			$query = '
					update 
						#__answers_users
					set
						answers = answers '.($state == 1 ? ' + ' : ' - ').' 1
					where
						id in (select created_by from #__answers_responses where id in ('.implode(',', $ids).') )'.
						($state == 0 ? ' and answers > 0' : '');

			$this->_db->setQuery($query);
			$this->_db->query();
			
			return true;
		}
		
		return false;
	}
	
	public function delete_answers($ids){

		$query = '
					update
						#__answers_categories
					set
						answers = answers - 1
					where
						id in (
							select 
								catid 
							from 
								#__answers_questions 
							where 
								id in (select question_id from #__answers_responses where id in ('.implode(',', $ids).') ) )
						and answers > 0';
		
		$this->_db->setQuery($query);
		$this->_db->query();
		
		$query = '
					update
						#__answers_users
					set
						answers = answers - 1
					where
						id in (select created_by from #__answers_responses where id in ('.implode(',', $ids).') ) and answers > 0';
			
		$this->_db->setQuery($query);
		$this->_db->query();
		
		$query = 'delete from #__answers_responses where id in ('.implode(',', $ids).')';
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			$query = 'delete from #__answers_subscribes where subscription_id in ('.implode(',', $ids).') and subscription_type = 2';
			$this->_db->setQuery($query);
			$this->_db->query();
			
			$query = 'select attachment from #__answers_attachments where item_id in ('.implode(',', $ids).') and item_type = 2';
			$this->_db->setQuery($query);
			$attachments = $this->_db->loadColumn();
			
			if(!empty($attachments)){
				
				foreach ($attachments as $attachment){
				
					if(JFile::exists(CA_ATTACHMENTS_PATH.$attachment)){
						
						JFile::delete(CA_ATTACHMENTS_PATH.$attachment);
					}
				}
				
				$query = 'delete from #__answers_attachments where item_id in ('.implode(',', $ids).') and item_type = 2';
				$this->_db->setQuery($query);
				$this->_db->query();
			}

			return true;
		}
		
		return false;
	}
	
	public function get_answer_details($answer_id){
		
		$query = '
				select 
					a.id, a.question_id, a.created_by, a.created, a.parent_id, a.description, a.published,
					q.title, q.alias, q.catid
				from
					#__answers_responses a
				left join
					#__answers_questions q on a.question_id = q.id
				where 
					a.id = '.$answer_id;
		
		$this->_db->setQuery($query);
		
		$answer = $this->_db->loadObject();
		
		return $answer;
	}
	
	public function update_answer($answer_id, $description){
	
		$user = JFactory::getUser();
		
		$query = 'update #__answers_responses set description = '.$this->_db->quote($description).' where id = '.$answer_id;
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			return true;
		}
		
		return false;
	}
}