<?php
/**
 * @version		$Id: categories.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class CommunityAnswersModelMigration extends JModelLegacy {

	protected $_error = '';
	
	function __construct() {
	
		parent::__construct();
	}
	
	public function getError($i = NULL, $toString = true){
	
		return $this->_error;
	}
	
	public function setError($error){
	
		$this->_error = $error;
	}
		
	public function check_simanswers_installed(){
		
		$result = 0;
		$query = 'show tables like \'%'.$this->_db->escape('_simanswers_answers').'\'';
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
		
			$result = (int)$this->_db->getNumRows();
		}
		
		return $result > 0;
	}
	
	public function migrate_sim_answers(){
		
		$queries = array();
		
		// First make sure no data exists.
		$queries[] = 'delete from #__answers_questions';
		$queries[] = 'delete from #__answers_responses';
		$queries[] = 'delete from #__answers_categories';
		$queries[] = 'delete from #__answers_ratings';
		
		$queries[] = '
				insert into
					#__answers_categories (id, title, description, parent_id)
				select 
					id, `name`, description, `parent`
				from
					#__simanswers_categories';
		
		$queries[] = '
				insert into
					#__answers_questions (id, title, description, catid, created_by, created, published)
				select
					qid, title, details, catid, creator, FROM_UNIXTIME(cdate), 1
				from
					#__simanswers_questions';
		
		$queries[] = '
				insert into
					#__answers_responses (id, question_id, description, created_by, created, source)
				select
					aid, qid, answer, creator, FROM_UNIXTIME(cdate), sources
				from
					#__simanswers_answers';

		$queries[] = '
				insert into
					#__answers_responses (question_id, parent_id, description, created_by, created)
				select
					qid, aid, feedback, creator, FROM_UNIXTIME(cdate)
				from
					#__simanswers_answers
				where
					feedback is not null and feedback != \'\'';
		
		$queries[] = '
				update
					#__answers_questions q
				set
					accepted_answer = (select aid from #__simanswers_answers a where a.qid = q.id and a.best = 1 limit 1)';
		
		$queries[] = '
				insert into
					#__answers_ratings (user_id, answer_id, rating, created)
				select
					uid, aid, case when direction = \'up\' then 1 else 0 end, FROM_UNIXTIME(cdate)
				from
					#__simanswers_thumbs';
		
		$queries[] = '
				update 
					#__answers_responses r
				set
					thumbup = (select count(*) from #__simanswers_thumbs t where t.aid = r.id and t.direction = \'up\'),
					thumbdown = (select count(*) from #__simanswers_thumbs t where t.aid = r.id and t.direction != \'up\')';
		
		foreach ($queries as $query){
			
			$this->_db->setQuery($query);
			
			if(!$this->_db->query()) {
				
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}
		
		// upgrade category root
		$query = 'insert into #__answers_categories(title, alias, parent_id, nleft, nright, norder) values(\'Root\',\'root\', 0, 1, 2, 1)';
		$this->_db->setQuery($query);

		if(!$this->_db->query()){
		
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		
		$rootcat = (int)$this->_db->insertid();
		$query = 'update #__answers_categories set parent_id = '.$rootcat.' where parent_id = 0 and id != '.$rootcat;
		$this->_db->setQuery($query);

		if(!$this->_db->query()){
		
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
		
		// create category aliases
		$query = 'select id, title from #__answers_categories';
		$this->_db->setQuery($query);
		$categories = $this->_db->loadObjectList();
		
		if(!empty($categories)){
			
			$query = 'update #__answers_categories set alias = case id ';
			
			foreach($categories as $category){
				
				$query = $query.' when '.$category->id.' then '.$this->_db->quote(JFilterOutput::stringURLUnicodeSlug($category->title));
			}
			
			$query = $query.' end';
			$this->_db->setQuery($query);
			
			if(!$this->_db->query()){

				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}
		
		// create question aliases
		$query = 'select id, title from #__answers_questions';
		$this->_db->setQuery($query);
		$categories = $this->_db->loadObjectList();
		
		if(!empty($categories)){
				
			$query = 'update #__answers_questions set alias = case id ';
				
			foreach($categories as $category){
		
				$query = $query.' when '.$category->id.' then '.$this->_db->quote(JFilterOutput::stringURLUnicodeSlug($category->title));
			}
				
			$query = $query.' end';
			$this->_db->setQuery($query);
				
			if(!$this->_db->query()){
		
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}
		
		return true;
	}
}