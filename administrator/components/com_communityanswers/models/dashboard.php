<?php
/**
 * @version		$Id: categories.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class CommunityAnswersModelDashboard extends JModelLegacy {
	
	function __construct() {
		 
		parent::__construct();
	}

	public function get_latest_questions(){
	
		$query = '
				select
					a.id, a.title, a.alias, a.created_by, a.created,
					u.name, u.username,
					c.title as category
				from
					#__answers_questions a
				left join
					#__answers_categories c on a.catid = c.id
				left join
					#__users u on a.created_by = u.id
				order by
					a.created desc';
	
		$this->_db->setQuery($query, 0, 5);
		$questions = $this->_db->loadObjectList();
	
		return $questions;
	}
	
	public function get_pending_questions(){
		
		$query = '
				select 
					a.id, a.title, a.alias, a.created_by, a.created,
					u.name, u.username,
					p.secret, p.status
				from
					#__answers_questions a
				left join
					#__answers_approval p on p.item_id = a.id and p.item_type = 1
				left join
					#__users u on a.created_by = u.id
				where
					a.published = 2
				order by 
					a.created desc';
		
		$this->_db->setQuery($query, 0, 5);
		$questions = $this->_db->loadObjectList();
		
		return $questions;
	}
	
	public function get_pending_answers(){
		
		$query = '
				select
					q.title, q.alias, q.id as question_id, 
					a.description, a.created_by, a.created,
					u.name, u.username,
					p.secret, p.status
				from
					#__answers_responses a
				left join
					#__answers_questions q on a.question_id = q.id
				left join
					#__answers_approval p on p.item_id = a.id and p.item_type = 2
				left join
					#__users u on a.created_by = u.id
				where 
					a.published = 2
				order by
					a.created desc';
		
		$this->_db->setQuery($query, 0, 5);
		$answers = $this->_db->loadObjectList();
		
		return $answers;
	}
	
	public function get_statistics(){
		
		$return = array();
		
		$query = 'select count(*) from #__answers_questions where published = 1';
		$this->_db->setQuery($query);
		$return['questions'] = (int)$this->_db->loadResult();
		
		$query = 'select count(*) from #__answers_responses where published = 1';
		$this->_db->setQuery($query);
		$return['answers'] = (int)$this->_db->loadResult();
		
		$query = 'select count(*) from #__answers_questions where accepted_answer > 0';
		$this->_db->setQuery($query);
		$return['resolved'] = (int)$this->_db->loadResult();
		
		
	}
}