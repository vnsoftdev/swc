<?php
/**
 * @version		$Id: categories.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class CommunityAnswersModelCategories extends JModelLegacy {
	
	function __construct() {
		 
		parent::__construct();
	}
	
	function get_categories($node=0){
	
		$tree = new CjNestedTree($this->_db, '#__answers_categories');
		return $tree->get_tree($node);
	}
	
	function get_categories_tree($node = 0, $include_root=false){
		 
		$tree = new CjNestedTree($this->_db, '#__answers_categories');
		return $tree->get_selectables($node, '.... ', $include_root);
	}
	
	function get_category($id){
		 
		$tree = new CjNestedTree($this->_db, '#__answers_categories');
		return $tree->get_node($id);
	}
	
	function delete($id){
		 
		$tree = new CjNestedTree($this->_db, '#__answers_categories');
		return $tree->delete($id);
	}
	
	function save(){
		 
		$post = JFactory::getApplication()->input->post;
		
		$id = $post->getInt('id', 0);
		$title = trim($post->getString('title', null));
		$alias = trim($post->getString('alias', null));
		$parent_id = $post->getInt('category', 0);
		$description = CJFunctions::get_clean_var('description', true);
		$permissions = $post->getArray(array('permission_view'=>'array', 'permission_ask'=>'array', 'permission_answer'=>'array'));
		
		$permission_view = $permissions['permission_view'];
		$permission_ask = $permissions['permission_ask'];
		$permission_answer = $permissions['permission_answer'];
		
		JArrayHelper::toInteger($permission_view);
		JArrayHelper::toInteger($permission_ask);
		JArrayHelper::toInteger($permission_answer);
		
		if(!$alias){
			 
			$alias = CJFunctions::get_unicode_alias($title);
		}else{
			 
			$alias = CJFunctions::get_unicode_alias($alias);
		}
	
		if(!empty($title) && $parent_id){

			$tree = new CjNestedTree(
						$this->_db, 
						'#__answers_categories', 
						array('alias', 'description', 'permission_view', 'permission_ask', 'permission_answer'));
			 
			if($id > 0){

				if(count($permission_view) > 0 && !empty($permission_view)){
					$permission_view = implode(',', $permission_view);
					$permission_view = $this->_db->quote($permission_view);
				}else{
					$permission_view = 'null';
				}
				
				if(count($permission_ask) > 0 && !empty($permission_ask)){
					$permission_ask = implode(',', $permission_ask);
					$permission_ask = $this->_db->quote($permission_ask);
				}else{
					$permission_ask = 'null';
				}
				
				if(count($permission_answer) > 0 && !empty($permission_answer)){
					$permission_answer = implode(',', $permission_answer);
					$permission_answer = $this->_db->quote($permission_answer);
				}else{
					$permission_answer = 'null';
				}
				
				$query = '
						update 
							#__answers_categories 
						set 
							title = '.$this->_db->quote($title).', 
							alias = '.$this->_db->quote($alias).',
							description = '.$this->_db->quote($description).',
		        			permission_view='.$permission_view.',
		        			permission_ask='.$permission_ask.',
		        			permission_answer='.$permission_answer.'
						where 
							id = '.$id;
				
				$this->_db->setQuery($query);
	
				if($this->_db->query()){
					 
					$category = $tree->get_node($id);
					 
					if($parent_id != $category['parent_id'] && $id != $parent_id){
	
						$tree->move($id, $parent_id);
					}
					
					$this->refresh_categories();
					return true;
				}
			} else {

				if(count($permission_view) > 0 && !empty($permission_view)){
					$permission_view = implode(',', $permission_view);
				}else{
					$permission_view = null;
				}
				
				if(count($permission_ask) > 0 && !empty($permission_ask)){
					$permission_ask = implode(',', $permission_ask);
				}else{
					$permission_ask = null;
				}
				
				if(count($permission_answer) > 0 && !empty($permission_answer)){
					$permission_answer = implode(',', $permission_answer);
				}else{
					$permission_answer = null;
				}
				
				if($tree->add($parent_id, $title, false, array($alias, $description, $permission_view, $permission_ask, $permission_answer))){
					 
					$this->refresh_categories();
					return true;
				}
			}
		}
	
		return false;
	}
	
	function movedown($id){
		 
		$query = 'select id, parent_id, norder from #__answers_categories where id='.$id;
		$this->_db->setQuery($query);
		$source = $this->_db->loadObject();
		 
		$query = '
				select 
					id, parent_id, norder from #__answers_categories
				where 
					parent_id='.$source->parent_id.' and norder>'.$source->norder.' 
				order by 
					norder 
				limit 
					1';
		
		$this->_db->setQuery($query);
		$target = $this->_db->loadObject();
		 
		if($target){
	
			$query = 'update #__answers_categories set norder='.$source->norder.' where id='.$target->id;
			$this->_db->setQuery($query);
	
			if($this->_db->query()){
				 
				$query = 'update #__answers_categories set norder='.$target->norder.' where id='.$source->id;
				$this->_db->setQuery($query);
				 
				if(!$this->_db->query()){
	
					$this->setError($this->_db->getErrorMsg());
				}
				 
				$tree = new CjNestedTree($this->_db, '#__answers_categories');
				return $tree->rebuild();
			}else{
				 
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}else{
	
			$this->setError($this->_db->getErrorMsg());
	
			return false;
		}
	}
	
	function moveup($id){
		 
		$query = 'select id, parent_id, norder from #__answers_categories where id='.$id;
		$this->_db->setQuery($query);
		$source = $this->_db->loadObject();
		 
		$query = '
				select 
					id, parent_id, norder 
				from 
					#__answers_categories
				where 
					parent_id='.$source->parent_id.' and norder<'.$source->norder.' 
				order by 
					norder desc 
				limit 
					1';
		
		$this->_db->setQuery($query);
		$target = $this->_db->loadObject();
		 
		if($target){
	
			$query = 'update #__answers_categories set norder='.$source->norder.' where id='.$target->id;
			$this->_db->setQuery($query);
	
			if($this->_db->query()){
				 
				$query = 'update #__answers_categories set norder='.$target->norder.' where id='.$source->id;
				$this->_db->setQuery($query);
				 
				if(!$this->_db->query()){
	
					$this->setError($this->_db->getErrorMsg());
				}
				 
				$tree = new CjNestedTree($this->_db, '#__answers_categories');
				return $tree->rebuild();
			}else{
				 
				$this->setError($this->_db->getErrorMsg());
				return false;
			}
		}else{
	
			$this->setError($this->_db->getErrorMsg());
			return false;
		}
	}
	
	function sort($id, $new_parent){
		 
		$query = "select nleft, nright from #__answers_categories where id=" . $new_parent;
		$this->_db->setQuery($query);
		$parent = $this->_db->loadObject();
	
		if($parent->nleft && $parent->nright){
			 
			$query = '
					update 
						#__answers_categories
					set 
						parent_id='.$new_parent.' where id='.$id.' and not('.$parent->nleft.' between nleft and nright)
						and not('.$parent->nright.' between nleft and nright)';
		}else{
			 
			$query = 'update #__answers_categories set parent_id=' . $new_parent . ' where id='.$id;
		}
	
		$this->_db->setQuery($query);
	
		if($this->_db->query()){
			 
			$tree = new CjNestedTree($this->_db, '#__answers_categories');
			return $tree->rebuild();
		}else{
			 
			return false;
		}
	}
	
	/**
	 * @deprecated
	 * @return boolean
	 */
	function rebuild_categories(){
		 
		$this->refresh_categories();
		return true;
	}
	
	function refresh_categories(){
		
		$query = '
				update 
					#__answers_questions q
				set 
					answers = (select count(*) from #__answers_responses r where r.question_id = q.id and r.published = 1 and r.parent_id = 0)';
		
		$this->_db->setQuery($query);
		$this->_db->query();
		 
		$query = '
    		SELECT
    			parent.id, SUM(items.answers) as item_count
    		FROM
    			#__answers_categories AS node ,
    			#__answers_categories AS parent,
    			#__answers_questions AS items
    		WHERE
    			node.nleft BETWEEN parent.nleft AND parent.nright
    			AND node.id = items.catid
    			AND items.published = 1
    		GROUP BY
    			parent.id
    		ORDER BY
    			node.nleft;
    	';
		 
		$this->_db->setQuery($query);
		$nodes = $this->_db->loadAssocList();
		 
		if(!empty($nodes)){
		
			foreach ($nodes as $node) {
				 
				$query = '
    				update
    					#__answers_categories
    				set
    					answers = '.$node['item_count'].'
    				where
    					id = '.$node['id'];
				 
				$this->_db->setQuery($query);
				 
				if(!$this->_db->query()){
		
					return false;
				}
			}
		}
		
		$tree = new CjNestedTree($this->_db, '#__answers_categories', array('alias'));
		$tree->update_category_counts('#__answers_questions', 'questions');
		$tree->rebuild();
// 		$categories = $tree->get_tree();
	
// 		$this->update_category_counts($categories);

		return true;
	}
	
	function update_category_counts($nodes, $catids = array()){
		 
		foreach ( $nodes as $id => $node ) {
			
			if( !empty($node['children'] ) ) {
				 
				$this->update_category_counts($node['children'], $catids);
			}
	
			if( !empty($catids) ) {
				 
				$query = '
					update 
						#__answers_categories 
					set 
						questions = ( select count(*) from #__answers_questions where catid in ( '.implode(',', $catids).' ) )';
				
				$this->_db->setQuery($query);
				$this->_db->query();
			}
	
			$catids[] = $node['id'];
		}
	}
}
?>