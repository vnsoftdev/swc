<?php
/**
 * @version		$Id: questions.php 01 2011-01-12 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.model');

class CommunityAnswersModelQuestions extends JModelLegacy {
	
	function __construct() {
		 
		parent::__construct();
	}
	
	public function get_questions(){
		
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		
		$filter_order = $app->getUserStateFromRequest( A_APP_NAME.'.questions.filter_order', 'filter_order', 'a.created', 'cmd' );
		$filter_order_dir = $app->getUserStateFromRequest( A_APP_NAME.'.questions.filter_order_dir', 'filter_order_Dir', 'DESC', 'word' );
		$limitstart = $app->getUserStateFromRequest( A_APP_NAME.'.questions.limitstart', 'limitstart', '', 'int' );
		$limit = $app->getUserStateFromRequest('global.list.limit', 'limit', $app->getCfg('list_limit', 20), 'int');

		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		
        $catid = $app->input->post->getInt('catid', 0);
        $userid = $app->input->post->getInt('uid', 0);
        $state = $app->input->post->getInt('state', 3);
        $search = $app->input->post->getString('search', '');
        
        $wheres = array();
        $return = array();
        
        if($catid){
        	
        	$wheres[] = 'a.catid = '.$catid;
        }
        
        if($userid){
        	
        	$wheres[] = 'a.created_by = '.$userid;
        }
        
        if($state >= 0 && $state < 3){
        	
        	$wheres[] = 'a.published = '.$state;
        }
        
        if(!empty($search)){
        	
        	$wheres[] = 'a.title like \'%'.$this->_db->escape($search).'%\'';
        }
        
        $where = ((count($wheres) > 0) ? ' where ('.implode(' ) and ( ', $wheres).')' : '');
        $order = ' order by ' . $filter_order . ' ' . $filter_order_dir;
        
        $query = '
        		select 
        			a.id, a.title, a.alias, a.description, a.created_by, a.created, a.catid, a.published, a.hits, a.user_name,
        			a.featured, a.answers, a.accepted_answer > 0 as solved, 
        			c.title as category, c.alias as calias,
					u.name, u.username, case when a.created_by > 0 then u.email else a.email end as email
        		from 
        			#__answers_questions a
        		left join 
        			#__answers_categories c ON a.catid=c.id 
        		left join 
        			#__users u ON a.created_by=u.id'.
        		$where.
        		$order;
        
        $this->_db->setQuery($query, $limitstart, $limit);
        $return['questions'] = $this->_db->loadObjectList();
        
        /************ pagination *****************/
        $query = '
        		select 
        			count(*) 
        		from 
        			#__answers_questions a 
        		left join 
        			#__answers_categories c on a.catid = c.id 
        		left join 
        			#__users u on a.created_by = u.id
        		'.$where;
        
        jimport('joomla.html.pagination');
        $this->_db->setQuery($query);
        $total = $this->_db->loadResult();
        
        $return['pagination'] = new JPagination( $total, $limitstart, $limit );
        /************ pagination *****************/
        
        $return['lists'] = array(
        		'limitstart'=>$limitstart, 
        		'limit'=>$limit, 
        		'order'=>$filter_order, 
        		'order_dir'=>$filter_order_dir,
        		'catid'=>$catid,
        		'search'=>$search,
        		'uid'=>$userid,
        		'state'=>$state);
        
        return $return;
	}
	
	public function get_question($id){
		
		$query = '
				select 
					id, title, alias, description, catid, created_by, created, published
				from
					#__answers_questions
				where
					id = '.$id;
		
		$this->_db->setQuery($query);
		$question = $this->_db->loadObject();
		
		return $question;
	}
	
	public function save_question($question){
		
		$alias = !empty($question->alias) 
					? JFilterOutput::stringURLUnicodeSlug($question->alias) 
					: JFilterOutput::stringURLUnicodeSlug($question->title);
		
		$query = '
				update 
					#__answers_questions
				set
					title = '.$this->_db->quote($question->title).',
					alias = '.$this->_db->quote($alias).',
					catid = '.$question->catid.',
					description = '.$this->_db->quote($question->description).'
				where
					id = '.$question->id;	

		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			return true;
		}
		
		return false;
	}
	
	public function publish_questions($state, $ids){
		
		$ids = implode(',', $ids);
		
		$query = 'update #__answers_questions set published = '.$state.' where id in ('.$ids.')';
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			$query = '
					update 
						#__answers_categories 
					set 
						questions = questions '.($state == 1 ? ' + ' : ' - ').' 1 
					where 
						id in (select catid from #__answers_questions where id in ('.$ids.') )'.($state == 0 ? ' and questions > 0' : '');
			
			$this->_db->setQuery($query);
			$this->_db->query();
			
			$query = '
					update 
						#__answers_users
					set
						questions = questions '.($state == 1 ? ' + ' : ' - ').' 1
					where
						id in (select created_by from #__answers_questions where id in ('.$ids.') )'.($state == 0 ? ' and questions > 0' : '');

			$this->_db->setQuery($query);
			$this->_db->query();
			
			$query = 'update #__answers_activities set published = '.$state.' where item_id in ('.($ids).') and activity_type in (1, 3)';
			$this->_db->setQuery($query);
			$this->_db->query();
			
			$query = 'update #__answers_activities set published = '.$state.' where item_id in (select id from #__answers_responses where question_id in ('.$ids.')) and activity_type in (2,4,5)';
			$this->_db->setQuery($query);
			$this->_db->query();
			
			return true;
		}
		
		return false;
	}
	
	public function delete_questions($ids){

		$ids = implode(',', $ids);
		
		$query = '
					update
						#__answers_categories
					set
						questions = questions - 1
					where
						id in (select catid from #__answers_questions where id in ('.$ids.') )';
		
		$this->_db->setQuery($query);
		$this->_db->query();
		
		$query = '
					update
						#__answers_users
					set
						questions = questions - 1
					where
						questions > 0 and id in (select created_by from #__answers_questions where id in ('.$ids.') )';
			
		$this->_db->setQuery($query);
		$this->_db->query();

		$query = 'delete from #__answers_activities where item_id in ('.($ids).') and activity_type in (1, 3)';
		$this->_db->setQuery($query);
		$this->_db->query();
			
		$query = 'delete from #__answers_activities where item_id in (select id from #__answers_responses where question_id in ('.$ids.')) and activity_type in (2,4,5)';
		$this->_db->setQuery($query);
		$this->_db->query();
		
		$query = 'delete from #__answers_questions where id in ('.$ids.')';
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			$query = 'delete from #__answers_responses where question_id in ('.$ids.')';
			$this->_db->setQuery($query);
			$this->_db->query();
			
			$query = 'delete from #__answers_subscribes where subscription_id in ('.$ids.') and subscription_type = 1';
			$this->_db->setQuery($query);
			$this->_db->query();
			
			$query = 'delete from #__answers_tagmap where item_id in ('.$ids.')';
			$this->_db->setQuery($query);
			$this->_db->query();

			$query = 'select attachment from #__answers_attachments where item_id in ('.$ids.') and item_type = 1';
			$this->_db->setQuery($query);
			$attachments = $this->_db->loadColumn();
			
			if(!empty($attachments)){
				
				foreach ($attachments as $attachment){
				
					if(JFile::exists(CA_ATTACHMENTS_PATH.$attachment)){
						
						JFile::delete(CA_ATTACHMENTS_PATH.$attachment);
					}
				}
				
				$query = 'delete from #__answers_attachments where item_id in ('.$ids.') and item_type = 1';
				$this->_db->setQuery($query);
				$this->_db->query();
			}

			return true;
		}
		
		return false;
	}
}