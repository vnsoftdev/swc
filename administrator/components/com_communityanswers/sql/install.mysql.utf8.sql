-- $Id: install.mysql.utf8.sql 01 2011-01-11 20:14:11Z Maverick $
CREATE TABLE IF NOT EXISTS  `#__answers_approval` (
  `item_id` int(10) unsigned NOT NULL,
  `item_type` int(10) unsigned NOT NULL,
  `secret` varchar(128) NOT NULL,
  `status` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`secret`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS  `#__answers_attachments` (
  `id` INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  `item_id` int(10) unsigned DEFAULT NULL,
  `item_type` int(10) unsigned NOT NULL,
  `attachment` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS  `#__answers_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `description` MEDIUMTEXT,
  `parent_id` int(10) unsigned NOT NULL,
  `nleft` int(10) NOT NULL DEFAULT '0',
  `nright` int(10) NOT NULL DEFAULT '0',
  `nlevel` int(10) unsigned NOT NULL DEFAULT '0',
  `norder` int(10) unsigned NOT NULL DEFAULT '0',
  `questions` int(10) unsigned NOT NULL DEFAULT '0',
  `answers` int(10) unsigned NOT NULL DEFAULT '0',
  `language` VARCHAR(6) NOT NULL DEFAULT '*',
  `permission_ask` varchar(128) DEFAULT NULL,
  `permission_answer` varchar(128) DEFAULT NULL,
  `permission_view` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `jos_answers_categories_nleft` (`nleft`),
  KEY `jos_answers_categories_parent_id` (`parent_id`),
  KEY `jos_answers_categories_nright` (`nright`),
  KEY `jos_answers_categories_nlevel` (`nlevel`)
)  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS  `#__answers_questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` mediumtext,
  `answers` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `accepted_answer` int(10) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `ip_address` varchar(39) NOT NULL DEFAULT '0.0.0.0',
  `user_name` varchar(64) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_answers_questions_created_by` (`created_by`)
)  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS  `#__answers_responses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(10) unsigned NOT NULL DEFAULT '0',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `reply_type` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `description` mediumtext NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `thumbup` int(10) unsigned NOT NULL DEFAULT '0',
  `thumbdown` int(10) unsigned NOT NULL DEFAULT '0',
  `source` mediumtext,
  `published` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `ip_address` varchar(39) NOT NULL DEFAULT '0.0.0.0',
  `user_name` varchar(64) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_answers_responses_created_by` (`created_by`)
)  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__answers_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `questions` int(10) unsigned NOT NULL DEFAULT '0',
  `answers` int(10) unsigned NOT NULL DEFAULT '0',
  `accepted` int(10) unsigned NOT NULL DEFAULT '0',
  `best_answers` int(10) unsigned NOT NULL DEFAULT '0',
  `karma` int(10) unsigned NOT NULL DEFAULT '0',
  `last_visit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `current_visit` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `session_data` text,
  `subid` varchar(12),
  PRIMARY KEY (`id`)
)  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__answers_ratings` (
  `user_id` int(10) unsigned DEFAULT NULL,
  `answer_id` int(10) unsigned NOT NULL,
  `rating` tinyint(3) NOT NULL DEFAULT '0',
  `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'
)  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS  `#__answers_subscribes` (
  `subscriber_id` int(10) unsigned NOT NULL DEFAULT '0',
  `subscription_type` int(10) unsigned NOT NULL DEFAULT '1',
  `subscription_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`subscriber_id`,`subscription_id`)
)  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS  `#__answers_tagmap` (
  `tag_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  PRIMARY KEY (`tag_id`,`item_id`),
  KEY `IDX_ANSWERS_TAGSMAP_ITEMID` (`item_id`)
)  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS  `#__answers_tags` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `tag_text` varchar(50) NOT NULL DEFAULT '0',
  `alias` varchar(50) NOT NULL,
  `description` MEDIUMTEXT,
  PRIMARY KEY (`id`),
  UNIQUE KEY `IDX_ANSWERS_TAGS_TAGTEXT` (`tag_text`)
)  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS  `#__answers_tags_stats` (
  `tag_id` int(11) NOT NULL,
  `num_items` int(10) unsigned NOT NULL,
  PRIMARY KEY (`tag_id`)
)  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS  `#__answers_categories_map` (
  `cid` INTEGER UNSIGNED NOT NULL,
  `gid` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`cid`, `gid`)
)  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS  `#__answers_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(512) NOT NULL,
  `description` mediumtext NOT NULL,
  `activity_type` int(10) unsigned NOT NULL,
  `item_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `url` varchar(512) NOT NULL,
  `published` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS  `#__answers_bounties` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question_id` int(10) unsigned NOT NULL,
  `bounty_points` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `expiry_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) DEFAULT CHARSET=utf8;