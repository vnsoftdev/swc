<?php
/**
 * @version		$Id: communityanswers.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla16.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2010 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

$user = JFactory::getUser();
$app = JFactory::getApplication();

$view = $app->input->getCmd('view','dashboard');
$task = $app->input->getCmd('task');

// CJLib includes
$cjlib = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_cjlib'.DIRECTORY_SEPARATOR.'framework.php';
if(file_exists($cjlib)){

	require_once $cjlib;
}else{

	die('CJLib (CoreJoomla API Library) component not found. Please <a href="http://www.corejoomla.com/downloads/required-extensions.html" target="_blank">download it here</a> and install it to continue.');
}

// /*** Handle live updates first **/
// require_once JPATH_COMPONENT_ADMINISTRATOR.DS.'liveupdate'.DS.'liveupdate.php';

// if($view == 'liveupdate') {

// 	LiveUpdate::handleRequest();
// 	return;
// }
// /*** Handle live updates first **/

CJLib::import('corejoomla.framework.core');
CJLib::import('corejoomla.nestedtree.core');
CJLib::import('corejoomla.ui.bootstrap');
CJFunctions::load_jquery(array('libs'=>array('fontawesome')));

require_once JPATH_ROOT.DS.'components'.DS.'com_communityanswers'.DS.'helpers'.DS.'constants.php';
require_once JPATH_COMPONENT.DS.'helpers'.DS.'helper.php';

if (!$user->authorise('core.manage', A_APP_NAME)) {
	
	return CJFunctions::throw_error(JText::_('JERROR_ALERTNOAUTHOR'), 403);
}

if( JFile::exists( JPATH_COMPONENT.DS.'controllers'.DS.$view.'.php' ) ){
	
    require_once (JPATH_COMPONENT.DS.'controllers'.DS.$view.'.php');
}else{
	
	return CJFunctions::throw_error('View '. JString::ucfirst($view) . ' not found!', 500);
}

$classname = 'CommunityAnswersController' . JString::ucfirst($view);
$controller = new $classname;

JSubMenuHelper::addEntry(JText::_('COM_COMMUNITYANSWERS_DASHBOARD'), 'index.php?option='.A_APP_NAME.'&amp;view=dashboard', $view == 'dashboard');
JSubMenuHelper::addEntry(JText::_('COM_COMMUNITYANSWERS_CATEGORIES'), 'index.php?option='.A_APP_NAME.'&amp;view=categories', $view == 'categories');
JSubMenuHelper::addEntry(JText::_('COM_COMMUNITYANSWERS_QUESTIONS'), 'index.php?option='.A_APP_NAME.'&amp;view=questions', $view == 'questions');
JSubMenuHelper::addEntry(JText::_('COM_COMMUNITYANSWERS_ANSWERS'), 'index.php?option='.A_APP_NAME.'&amp;view=answers', $view == 'answers');
JSubMenuHelper::addEntry(JText::_('COM_COMMUNITYANSWERS_USERS'), 'index.php?option='.A_APP_NAME.'&amp;view=users', $view == 'users');
JSubMenuHelper::addEntry(JText::_('COM_COMMUNITYANSWERS_MIGRATION'), 'index.php?option='.A_APP_NAME.'&amp;view=migration', $view == 'migration');

JToolBarHelper::preferences(A_APP_NAME);

$document = JFactory::getDocument();
$document->addStyleSheet(CJLIB_URI.'/framework/assets/cj.framework.css');
$document->addStyleSheet(JURI::base(true).'/components/'.A_APP_NAME.'/assets/css/cj.answers.admin.min.css');
$document->addScript(JURI::base(true).'/components/'.A_APP_NAME.'/assets/js/cj.answers.admin.min.js');

/********************************* VERSION CHECK *******************************/
if(empty($task)){
	
	$version = $app->getUserState(A_APP_NAME.'.VERSION');
	
	if(!$version){
			
		$version = CJFunctions::get_component_update_check(A_APP_NAME, A_APP_VERSION);
		$v = array();
		$v['connect'] = (int)$version['connect'];
		$v['version'] = (string)$version['version'];
		$v['released'] = (string)$version['released'];
		$v['changelog'] = (string)$version['changelog'];
		$v['status'] = (int)$version['status'];
		
		if($version['connect'] == 1){
		
			$app->setUserState(A_APP_NAME.'.VERSION', $v);
		}
	}
	
	if(!empty($version['status']) && $version['status'] == 1 && !empty($version['version'])) {
		
		echo '<div class="alert alert-success">
				<button type="button" class="close" data-dismiss="alert">&times;</button>
				Community Answers '.$version['version'].' is now available, please <a target="_blank" href="http://www.corejoomla.com/downloads.html">download it here</a>.
			</div>';
	}
}
/********************************* VERSION CHECK *******************************/

// Perform the Request task
$controller->execute( $task );
$controller->redirect();

if(empty($task)){
	
	echo '<input type="hidden" value="'.$view.'" id="cjanswers_page_id">';
	echo '<div class="center">';
	echo '<div><small>Version: '.A_APP_VERSION.' | Community Answers is developed by <a target="_blank" href="http://www.corejoomla.com">corejoomla.com</a> and is licensed under Gnu/GPL.</small></div>';
	echo '<div><strong>If you use Community Answers, please please post a rating and a review at the <a target="_blank" href="http://extensions.joomla.org/extensions/communication/question-a-answers/21724">Joomla! Extensions Directory.</a>';
	echo '</div>';
}