<?php
/**
 * @package        MijoVideos
 * @copyright      2009-2014 Miwisoft LLC, miwisoft.com
 * @license        GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined('_JEXEC') or die;

class MijovideosModelUpload extends MijovideosModel {

	public function __construct() {
		parent::__construct('upload', 'videos');
	}

	public function getTree() {
		$basePath = str_replace(DIRECTORY_SEPARATOR, '/', JPATH_ROOT.'/');

		// Get the list of folders
		jimport('joomla.filesystem.folder');
		$folders = JFolder::folders(JPATH_ROOT, '.', true, true, array('.svn', 'CVS', '.DS_Store', '__MACOSX'));

		$tree = array();

		foreach ($folders as $folder) {
			$folder   = str_replace(DIRECTORY_SEPARATOR, '/', $folder);
			$name     = substr($folder, strrpos($folder, '/') + 1);
			$relative = str_replace($basePath, '', $folder);
			$absolute = $folder;
			$path     = explode('/', $relative);
			$node     = (object)array('name' => $name, 'relative' => $relative, 'absolute' => $absolute);

			$tmp = & $tree;


			for ($i = 0, $n = count($path); $i < $n; $i++) {
				if (!isset($tmp['children'])) {
					$tmp['children'] = array();
				}

				if ($i == $n - 1) {
					// We need to place the node
					$tmp['children'][ $relative ] = array('data' => $node, 'children' => array());
					$fileList = JFolder::files($folder, '.', false, false, array('.svn', 'index.php', 'CVS', '.DS_Store', '__MACOSX'), array('^\..*', '.*~', '.xml', '.php', '.log', '.html', '.zip', '.txt'));
					if ($fileList !== false) {
						foreach ($fileList as $file) {
							if (substr($file, 0, 1) == '.' or strtolower($file) === 'index.html') {
								continue;
							}

							$file_types = explode('|', MijoVideos::getConfig()->get('allow_file_types'));
							$ext        = strtolower(JFile::getExt($file));
							if (!in_array($ext, $file_types)) {
								continue;
							}

							$temp           = new stdClass();
							$temp->name     = $file;
							$temp->absolute = str_replace(DIRECTORY_SEPARATOR, '/', JPath::clean($folder.'/'.$file));
							$temp->relative = str_replace($basePath, '', $temp->absolute);
							$tmp['children'][ $relative ]['children'][$file] = $temp;
						}
					}
					break;
				}

				if (array_key_exists($key = implode('/', array_slice($path, 0, $i + 1)), $tmp['children'])) {
					$tmp = & $tmp['children'][ $key ];
				}
			}
		}

		$tree['data'] = (object)array('name' => JText::_('COM_MEDIA_MEDIA'), 'relative' => '', 'absolute' => JPATH_ROOT);

		$fileList = JFolder::files($basePath, '.', false, false, array('.svn', 'index.php', 'CVS', '.DS_Store', '__MACOSX'), array('^\..*', '.*~', '.xml', '.php', '.log', '.html', '.zip', '.txt'));
		if ($fileList !== false) {
			foreach ($fileList as $file) {
				if (substr($file, 0, 1) == '.' or strtolower($file) === 'index.html') {
					continue;
				}

				$file_types = explode('|', MijoVideos::getConfig()->get('allow_file_types'));
				$ext        = strtolower(JFile::getExt($file));
				if (!in_array($ext, $file_types)) {
					continue;
				}

				$temp           = new stdClass();
				$temp->name     = $file;
				$temp->absolute = str_replace(DIRECTORY_SEPARATOR, '/', JPath::clean($basePath.'/'.$file));
				$temp->relative = str_replace($basePath, '', $temp->absolute);
				$tree['children'][$file] = $temp;
			}
		}
		return $tree;
	}
}