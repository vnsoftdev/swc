<?php
/**
 * @package		MijoVideos
 * @copyright	2009-2014 Miwisoft LLC, miwisoft.com
 * @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined('_JEXEC') or die('Restricted access');
?>

<script language="javascript" type="text/javascript">
	function upgrade() {	    
	    document.adminForm.view.value = 'upgrade';
		document.adminForm.submit();
	}
</script>

<form name="adminForm" id="adminForm" action="<?php echo MijoVideos::get('utility')->getActiveUrl(); ?>" method="post">
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr>
			<th>		
				<?php
                $pid = $this->config->get('pid');
				if(empty($pid)){
					if (MijoVideos::get('utility')->is30()) {
						$uri = (string) JUri::getInstance();
						$return = urlencode(base64_encode($uri));
						$link = MijoVideos::get('utility')->route('index.php?option=com_config&view=component&component=com_mijovideos&return='.$return);
						JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_MIJOVIDEOS_CPANEL_STATUS_NOTE_PERSONAL_ID', '<a href="'.$link.'">', '</a>'), 'error');
					}
					else {
						JFactory::getApplication()->enqueueMessage(JText::sprintf('COM_MIJOVIDEOS_CPANEL_STATUS_NOTE_PERSONAL_ID', '<a href="'.MijoVideos::get('utility')->route('index.php?option=com_config&view=component&component=com_mijovideos&tmpl=component').'" style="cursor:pointer" class="modal" rel="{handler: \'iframe\', size: {x: 875, y: 550}}">', '</a>'), 'error');
					}
				}
				?>	
			</th>
		</tr>
		<tr>
			<th>
				<?php
                $jusersync = $this->config->get('jusersync');
					if(empty($jusersync)){
						JError::raiseWarning('100', JText::sprintf('COM_MIJOVIDEOS_ACCOUNT_SYNC_WARN', '<a href="#" onclick="javascript : submitform(\'jusersync\')">', '</a>'));
					}
				?>
			</th>
		</tr>
		<tr>
            <?php
            $layout = 'user';
            if ($this->acl->canAdmin()) {
                $layout = 'admin';
            }

            echo $this->loadTemplate($layout);
            ?>
		</tr>
	</table>
	
	<input type="hidden" name="option" value="com_mijovideos" />
	<input type="hidden" name="view" value="mijovideos"/>
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="boxchecked" value="0" />
	<?php echo JHtml::_('form.token'); ?>
</form>