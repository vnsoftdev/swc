<?php
/**
 * @package        MijoVideos
 * @copyright      2009-2014 Miwisoft LLC, miwisoft.com
 * @license        GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined('_JEXEC') or die;


class MijovideosViewUpload extends MijovideosView {

	function display($tpl = null) {
		if ($this->_mainframe->isAdmin()) {
			$this->addToolbar();
		}

		$this->config = MijoVideos::getConfig();

		if ($this->config->get('upload_script') == 'fancy') {
			$this->document->addScript(JUri::root().'components/com_mijovideos/assets/js/Swiff.Uploader.js');
			$this->document->addScript(JUri::root().'components/com_mijovideos/assets/js/Fx.ProgressBar.js');
			$this->document->addScript(JUri::root().'components/com_mijovideos/assets/js/FancyUpload2.js');
			$this->document->addStyleSheet(JUri::root().'components/com_mijovideos/assets/css/fancyupload.css');
		}

		if ($this->config->get('upload_script') == 'dropzone') {
			$this->document->addScript(JUri::root().'components/com_mijovideos/assets/js/dropzone.js');
			$this->document->addStyleSheet(JUri::root().'components/com_mijovideos/assets/css/dropzone.css');
		}

		$checkbox   = 'false';
		$this->task = JRequest::getString('task', null);
		$jquery     = 'selectMode: 3});';

		if (empty($this->task)) {
			$checkbox = 'true';
			$jquery   = 'selectMode: 3,
							select: function(event, data) {
								// Get a list of all selected nodes, and convert to a key array:
								var selKeys = jQuery.map(data.tree.getSelectedNodes(), function(node) {
									return node.key;
								});

								// Get a list of all selected TOP nodes
								var selRootNodes = data.tree.getSelectedNodes(true);
								// ... and convert to a key array:
								var selRootKeys = jQuery.map(selRootNodes, function(node) {
									return node.key;
								});
							},
							dblclick: function(event, data) {
								data.node.toggleSelected();
							},
							keydown: function(event, data) {
								if( event.which === 32 ) {
									data.node.toggleSelected();
									return false;
								}
							}
					});
					jQuery("#btnDeselectAll").click(function() {
						jQuery("#mijovideos_folder_tree").fancytree("getTree").visit(function(node) {
							node.setSelected(false);
						});
						return false;
					});
					jQuery("#btnSelectAll").click(function() {
						jQuery("#mijovideos_folder_tree").fancytree("getTree").visit(function(node) {
							node.setSelected(true);
						});
						return false;
					});
					jQuery("#btnToggleSelect").click(function(){
						jQuery("#mijovideos_folder_tree").fancytree("getRootNode").visit(function(node){
							node.toggleSelected();
						});
						return false;
					});';
		}

		$this->document->addScriptDeclaration('
			jQuery(function() {
					jQuery("#mijovideos_folder_tree").fancytree({
							imagePath:\'\',
							checkbox: '.$checkbox.',
							'.$jquery.'
					Joomla.submitbutton = function(pressbutton) {
						jQuery("#mijovideos_folder_tree").fancytree("getTree").generateFormElements();
						Joomla.submitform(pressbutton);
					}
			});
		');

		$this->tree = $this->get('Tree');

		parent::display($tpl);
	}

	protected function addToolbar() {
		if ($this->_view == 'videos') {
			return;
		}

		JToolBarHelper::title(JText::_('COM_MIJOVIDEOS_UPLOAD_NEW_VIDEO'), 'mijovideos');

		$this->toolbar->appendButton('Popup', 'help1', JText::_('Help'), 'http://miwisoft.com/support/docs/mijovideos/how-to/how-to-upload-video-files?tmpl=component', 650, 500);
	}

	protected function getRecursiveFolders($folder) {
		$this->folders_id = null;
		$txt              = null;
		if (isset($folder['children']) && count($folder['children'])) {
			$tmp        = $this->tree;
			$this->tree = $folder;
			$txt        = $this->loadTemplate('server');
			$this->tree = $tmp;
		}
		return $txt;
	}
}