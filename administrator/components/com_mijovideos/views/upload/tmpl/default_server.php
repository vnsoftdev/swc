<?php
/**
 * @package		MijoVideos
 * @copyright	2009-2014 Miwisoft LLC, miwisoft.com
 * @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined('_JEXEC') or die; ?>

<ul style="display: none;">
	<?php foreach ($this->tree['children'] as $folder) { ?>
		<?php if (is_object($folder)) { ?>
			<li data-icon="<?php echo JUri::root(); ?>administrator/components/com_mijovideos/assets/images/icon-16-mijovideos.png" id="<?php echo $folder->relative; ?>">
				<?php echo $folder->name; ?>
			</li>
		<?php } else { ?>
			<li class="folder" id="<?php echo $folder['data']->relative; ?>">
				<?php echo $folder['data']->name; ?>
				<?php echo $this->getRecursiveFolders($folder); ?>
			</li>
		<?php } ?>
	<?php } ?>
</ul>