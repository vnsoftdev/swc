<?php
/*
* @package		MijoVideos
* @copyright	2009-2014 Miwisoft LLC, miwisoft.com
* @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
*/
# No Permission
defined('_JEXEC') or die ('Restricted access');

# Controller Class
class MijovideosControllerUpload extends MijovideosController {

	# Main constructer
	public function __construct() {
		parent::__construct('upload');
	}

	public function upload() {
		$upload = MijoVideos::get('upload');

		$dashboard = '';
		if (MijoVideos::isDashboard()) {
			$dashboard = '&dashboard=1';
		}

		// Add embed code
		if (!$upload->process()) {
			if (JRequest::getWord('format') != 'raw') {
				JError::raiseWarning(500, $upload->getError());
				$this->setRedirect('index.php?option=com_mijovideos&view=upload'.$dashboard);
			}
			else {
				$result = array(
					'status' => '0',
					'error'  => $upload->getError(),
					'code'   => 0
				);
			}
		}
		else {
			if (JRequest::getWord('format') != 'raw') {
				$this->_mainframe->enqueueMessage(JText::sprintf('COM_MIJOVIDEOS_SUCCESSFULLY_UPLOADED_X', $upload->_title));
				$this->setRedirect('index.php?option=com_mijovideos&view=videos&task=edit&cid[]='.$upload->_id.$dashboard);
			}
			else {
				$result = array(
					'success'  => 1,
					'id'       => $upload->_id,
					'href'     => MijoVideos::get('utility')->route('index.php?option=com_mijovideos&view=videos&task=edit&cid[]='.$upload->_id.$dashboard),
					'filename' => $upload->_filename
				);
			}
		}

		echo json_encode($result);
	}

	public function uberUpload() {
		$upload = MijoVideos::get('upload');

		$dashboard = '';
		if (MijoVideos::isDashboard()) {
			$dashboard = '&dashboard=1';
		}

		// Add embed code
		if (!$upload->uber()) {
			JError::raiseWarning(500, $upload->getError());
			$this->setRedirect('index.php?option=com_mijovideos&view=videos'.$dashboard);
		}
		else {
			$this->_mainframe->enqueueMessage(JText::sprintf('COM_MIJOVIDEOS_SUCCESSFULLY_UPLOADED_X', $upload->_title));
			$this->setRedirect('index.php?option=com_mijovideos&view=videos&task=edit&cid[]='.$upload->_id.$dashboard);
		}

		return $upload;
	}

	public function link_upload() {
		header('Content-type: text/javascript');
		MijoVideos::get('uber.ubr_link_upload');
		return;
	}

	public function set_progress() {
		header('Content-type: text/javascript');
		MijoVideos::get('uber.ubr_set_progress');
		return;
	}

	public function get_progress() {
		header('Content-type: text/javascript');
		MijoVideos::get('uber.ubr_get_progress');
		return;
	}

	public function convertToHtml5() {
		$video_id = JRequest::getInt('video_id');
		$filename = JRequest::getString('filename');

		$error = MijoVideos::get('utility')->backgroundTask($video_id, $filename);

		if (!$error) {
			$json = array(
				'success' => 1,
				'href'    => MijoVideos::get('utility')->route('index.php?option=com_mijovideos&view=videos&task=edit&cid[]='.$video_id)
			);
			echo json_encode($json);
			return true;
		}
		else {
			$json['error'] = JText::sprintf('COM_MIJOVIDEOS_ERROR_X_PROCESSING', 'frames');
			echo json_encode($json);
			return false;
		}

	}

	public function remoteLink() {
		$upload = MijoVideos::get('upload');

		$dashboard = '';
		if (MijoVideos::isDashboard()) {
			$dashboard = '&dashboard=1';
		}

		$upload->remoteLink();
		if (!count($upload->getErrors())) {
			if ($upload->_count > 1) {
				$this->_mainframe->enqueueMessage(JText::sprintf('COM_MIJOVIDEOS_SUCCESSFULLY_UPLOADED'));
				$this->setRedirect('index.php?option=com_mijovideos&view=videos'.$dashboard);
			}
			else {
				$this->_mainframe->enqueueMessage(JText::sprintf('COM_MIJOVIDEOS_SUCCESSFULLY_UPLOADED_X', $upload->_title));
				$redirect_url = MijoVideos::get('utility')->route('index.php?option=com_mijovideos&view=videos&task=edit&cid[]='.$upload->_id.$dashboard);
				$this->setRedirect($redirect_url);
			}
		}
		else {
			$this->_mainframe->enqueueMessage($upload->getError(0));
			$redirect_url = MijoVideos::get('utility')->route('index.php?option=com_mijovideos&view=upload'.$dashboard);
			$this->setRedirect($redirect_url);
		}

		return $upload;
	}

	public function serverImport() {
		$upload = MijoVideos::get('upload');

		$dashboard = '';
		if (MijoVideos::isDashboard()) {
			$dashboard = '&dashboard=1';
		}

		$upload->serverImport();
		if (!count($upload->getErrors())) {
			if ($upload->_count > 1) {
				$this->_mainframe->enqueueMessage(JText::sprintf('COM_MIJOVIDEOS_SUCCESSFULLY_UPLOADED'));
				$this->setRedirect('index.php?option=com_mijovideos&view=videos'.$dashboard);
			}
			else {
				$this->_mainframe->enqueueMessage(JText::sprintf('COM_MIJOVIDEOS_SUCCESSFULLY_UPLOADED_X', $upload->_title));
				$redirect_url = MijoVideos::get('utility')->route('index.php?option=com_mijovideos&view=videos&task=edit&cid[]='.$upload->_id.$dashboard);
				$this->setRedirect($redirect_url);
			}
		}
		else {
			$this->_mainframe->enqueueMessage($upload->getError(0));
			$redirect_url = MijoVideos::get('utility')->route('index.php?option=com_mijovideos&view=upload'.$dashboard);
			$this->setRedirect($redirect_url);
		}

		return $upload;
	}
}