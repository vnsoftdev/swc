<?php
/**
 * @package		MijoVideos
 * @copyright	2009-2014 Miwisoft LLC, miwisoft.com
 * @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined( '_JEXEC' ) or die ;

class MijovideosControllerCategories extends MijoVideosController {

	public function __construct($config = array()) {
		parent::__construct('categories');
	}

	public function save() {
		$this->trigger();
		parent::save();
	}

	public function publish() {
		$this->trigger(1);
		parent::publish();
	}

	public function unpublish() {
		$this->trigger(0);
		parent::unpublish();
	}

	protected function trigger($status = null) {
		if (is_null($status)) {
			$post = JRequest::get('post', JREQUEST_ALLOWRAW);
			$status = $post['published'];
		}
		$cid = JRequest::getVar('cid', array(), 'post');
		MijoVideos::get('utility')->trigger('onFinderChangeState', array('com_mijovideos.categories', $cid[0], $status), 'finder');
	}
}