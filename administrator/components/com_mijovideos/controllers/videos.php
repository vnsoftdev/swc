<?php
/**
 * @package		MijoVideos
 * @copyright	2009-2014 Miwisoft LLC, miwisoft.com
 * @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined( '_JEXEC' ) or die ;


class MijovideosControllerVideos extends MijoVideosController {

	public function __construct($config = array())	{
		parent::__construct('videos');
	}

    public function edit() {
        JRequest::setVar('hidemainmenu', 1);

        $view = $this->getView('Videos', 'edit');
	    $videos_model = $this->getModel('videos');
	    $view->setModel($videos_model, true);
	    $processes_model = $this->getModel('processes');
	    $view->setModel($processes_model);
	    if (JRequest::getWord('layout') == 'preview') {
		    $view->displayPreview('edit');
	    }
	    else {
		    $view->display('edit');
	    }
    }

	public function save() {
		$post       = JRequest::get('post', JREQUEST_ALLOWRAW);
        $cid = JRequest::getVar('cid', array(), 'post');

        if(!empty($cid[0])){
            $post['id'] = $cid[0];
        }

        $thumb_size = MijoVideos::get('utility')->getThumbSize($this->config->get('thumb_size'));

        # Thumb Image
        if (isset($_FILES['thumb_image']['name'])) {
            $fileExt = strtolower(JFile::getExt($_FILES['thumb_image']['name']));
            $supportedTypes = array('jpg', 'png', 'gif');
            if (in_array($fileExt, $supportedTypes)) {
                $fileName = hash('haval256,5', JString::strtolower($_FILES['thumb_image']['name'])) . '.' . $fileExt;
                $imagePath = MIJOVIDEOS_UPLOAD_DIR.'/images/'.$this->_context.'/'.$post['id'].'/orig/'.$fileName;
                $thumbPath = MIJOVIDEOS_UPLOAD_DIR.'/images/'.$this->_context.'/'.$post['id'].'/'.$thumb_size.'/'.$fileName;
                JFile::upload($_FILES['thumb_image']['tmp_name'], $imagePath);
                JFolder::create(MIJOVIDEOS_UPLOAD_DIR.'/images/'.$this->_context.'/'.$post['id'].'/'.$thumb_size.'/');
                MijoVideos::get('utility')->resizeImage($imagePath, $thumbPath, $thumb_size, $thumb_size, 95);
                $post['thumb'] = $fileName;
            }
        }

        $table = ucfirst($this->_component).ucfirst($this->_context);
        $row = MijoVideos::getTable($table);
        $row->load($post['id']);

        if (isset($post['del_thumb']) and $row->thumb) {
            if (JFile::exists(MIJOVIDEOS_UPLOAD_DIR.'/images/'.$this->_context.'/'.$post['id'].'/orig/'.$row->thumb)) {
                JFile::delete(MIJOVIDEOS_UPLOAD_DIR.'/images/'.$this->_context.'/'.$post['id'].'/orig/'.$row->thumb);
                //JFile::delete(MIJOVIDEOS_UPLOAD_DIR.'/images/'.$this->_context.'/'.$post['id'].'/orig');
            }

            if (JFile::exists(MIJOVIDEOS_UPLOAD_DIR.'/images/'.$this->_context.'/'.$post['id'].'/'.$thumb_size.'/'.$row->thumb)) {
                JFile::delete(MIJOVIDEOS_UPLOAD_DIR.'/images/'.$this->_context.'/'.$post['id'].'/'.$thumb_size.'/'.$row->thumb);
            }

            $post['thumb'] = '';
        }


        if (!$post['channel_id']) {
            $post['channel_id'] = MijoVideos::get('channels')->getDefaultChannel()->id;
        }

        $ret = $this->_model->store($post);

        if($ret){
            $msg = JText::_('COM_MIJOVIDEOS_VIDEO_SAVED');
        }
        else {
            $msg = JText::_('COM_MIJOVIDEOS_VIDEO_SAVE_ERROR');
        }

		MijoVideos::get('utility')->trigger('onFinderAfterSave', array('com_mijovideos.videos', $post['id'], null), 'finder');

		parent::route($msg, $post);

        return $msg;
	}

    public function copy() {
        # Check token
        JRequest::checkToken() or jexit('Invalid Token');

        $cid = JRequest::getVar('cid', array(), 'post');
        
        foreach ($cid as $id) {
        	$this->_model->copy($id);
        }
        
        $msg = JText::_('COM_MIJOVIDEOS_RECORD_COPIED');

        $this->setRedirect('index.php?option='.$this->_option.'&view='.$this->_context, $msg);

        return $msg;
    }
    
	public function delete() {
    	# Check token
		JRequest::checkToken() or jexit('Invalid Token');
		
		$cid = JRequest::getVar('cid', array(), 'post');
        JArrayHelper::toInteger($cid);

		# Action
        foreach ($cid as $id) {
            if (JFolder::exists(MIJOVIDEOS_UPLOAD_DIR.'/videos/'.$id)) {
                JFolder::delete(MIJOVIDEOS_UPLOAD_DIR.'/videos/'.$id);
            }
            if (JFolder::exists(MIJOVIDEOS_UPLOAD_DIR.'/images/videos/'.$id)) {
                JFolder::delete(MIJOVIDEOS_UPLOAD_DIR.'/images/videos/'.$id);

            }

	        MijoVideos::get('utility')->trigger('onFinderAfterDelete', array('com_mijovideos.videos', $id), 'finder');
        }

        $del_row = $this->deleteRecord($this->_table, $this->_model);
        $del_rel_row = $this->_model->delete($cid);

		if (!$del_row and !$del_rel_row) {
            $msg = JText::_('COM_MIJOVIDEOS_COMMON_RECORDS_DELETED_NOT');
		} else {
			$msg = JText::_('COM_MIJOVIDEOS_COMMON_RECORDS_DELETED');
		}

		$this->setRedirect('index.php?option='.$this->_option.'&view='.$this->_context, $msg);

        return $msg;
    }

    public function autoComplete(){
        $query = JRequest::getVar('query');
        $videos = json_encode($this->_model->autoComplete($query));
        echo $videos;
        exit();
    }

    public function createAutoFieldHtml(){
        $fieldid = JRequest::getInt('fieldid');
        $html = MijoVideos::get('fields')->createAutoFieldHtml($fieldid);
        echo $html;
        exit();
    }

    # Feature
    public function feature() {
        # Check token
        JRequest::checkToken() or jexit('Invalid Token');

        # Action
        self::updateField($this->_table, 'featured', 1, $this->_model);

	    # Trigger
	    $this->trigger();

        # Return
        self::route();
    }

    # Unfeature
    public function unfeature() {
        # Check token
        JRequest::checkToken() or jexit('Invalid Token');

        # Action
        self::updateField($this->_table, 'featured', 0, $this->_model);

	    # Trigger
	    $this->trigger();

        # Return
        self::route();
    }

	public function publish() {
		$this->trigger();
		parent::publish();
	}

	public function unpublish() {
		$this->trigger();
		parent::unpublish();
	}

	protected function trigger() {
		$cid = JRequest::getVar('cid', array(), 'post');
		MijoVideos::get('utility')->trigger('onFinderAfterSave', array('com_mijovideos.videos', $cid, null), 'finder');
	}
}