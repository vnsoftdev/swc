<?php
/**
* @package		%PACKAGE%
* @subpackge	%SUBPACKAGE%
* @copyright	Copyright (C) 2010 - 2012 %COMPANY_NAME%. All rights reserved.
* @license		GNU/GPL, see LICENSE.php
*
* %PACKAGE% is free software. This version may have been modified pursuant
* to the GNU General Public License, and as distributed it includes or
* is derivative of works licensed under the GNU General Public License or
* other free or open source software licenses.
* See COPYRIGHT.php for copyright notices and details.
*/
defined( '_JEXEC' ) or die( 'Unauthorized Access' );

// Import the model file from the core
Foundry::import( 'admin:/includes/model' );


class EasySocialModelScreenplay extends EasySocialModel
{
	public function updateCommentCount($playId, $trend = 1)
	{
		$db	= Foundry::db();
		
		$sql = $db->sql();
		$query = 'SELECT comments FROM #__social_apps_screenplay WHERE id='. $playId . ' and state=1';
		$sql->raw($query);	
		$db->setQuery( $sql );
		$newCount = $db->loadResult();
		
		$newCount = $newCount + $trend;
		$query = 'UPDATE #__social_apps_screenplay SET comments=' . $newCount . ' WHERE id='. $playId . ' and state=1';
		$sql->raw($query);	
		$db->setQuery( $sql );
		$db->query();
	}
	
	public function getScreenByUser($user_id, $state = 1, $datelimit = '')
	{
		$db	= Foundry::db();
		
		$sql = $db->sql();
		$query = 'SELECT * FROM #__social_apps_screenplay WHERE created_by='. $user_id . ' and state='.$state;
		$sql->raw($query);	
		$db->setQuery( $sql );
		$data = $db->loadObjectList();
		
		if(!empty($data)){
			foreach ($data as &$d){
				$sql = $db->sql();
				$query = 'SELECT count(id) FROM #__social_likes WHERE type="screenplay.user.create" and uid='.$d->id;
				$sql->raw($query);	
				$db->setQuery( $sql );
				
				$count = $db->loadResult();
				
				$d->likes = $count;
			}
		}

		return $data;
	}
	
	public function getScreenManager($event_id , $user_id = 0 ){
		
		// Load up the event
        $event = FD::event($event_id);
		
		$db	= Foundry::db();
		
		$sql = $db->sql();
		$query = 'SELECT sc.*, es.id as event_screen_id, es.created as event_screen_date, u.name as created_name, es.point FROM #__social_events_screens as es LEFT JOIN #__social_apps_screenplay as sc ON es.screen_id = sc.id LEFT JOIN #__users as u ON u.id = sc.created_by WHERE es.event_id='. $event_id;
		if($user_id > 0){
			$query .= ' and es.user_id = '.$user_id;
		}
		$query .= '';
		
		$sql->raw($query);	
		$db->setQuery( $sql );
		$data = $db->loadObjectList();
		
		if(!empty($data)){
			foreach ($data as &$d){
				$sql = $db->sql();
				$query = 'SELECT count(id) FROM #__social_likes WHERE type="screenplay.user.create" and uid='.$d->id.' AND created BETWEEN "'.$event->meta->start.'" AND "'.$event->meta->end.'"';
				$sql->raw($query);	
				$db->setQuery( $sql );
				
				$count = $db->loadResult();
				
				$d->likes = $count;
			}
		}
		
		return $data;
		//echo '<pre>'.print_r($data, true).'</pre>';die;
	}
}