<?php
defined('_JEXEC') or die;

class CswViewTest extends JViewLegacy {

	function display($tpl=null) {
		// Assign data to the view
		$this->msg = $this->get('Msg');

		// Display the view
		parent::display($tpl);
	}
}
?>