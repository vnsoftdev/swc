<?php
/**
 * @package     corejoomla.administrator
 * @subpackage  com_cjlib
 *
 * @copyright   Copyright (C) 2009 - 2014 corejoomla.com. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
require_once JPATH_ROOT.'/components/com_cjforum/helpers/constants.php';

class CjLibUserApi 
{
	public static function getUserAvatar($profileApp, $avatarApp, $userId, $name, $height = 48, $email = null, $attribs = array(), $imgAttribs = array())
	{
		$profileUrl = CjLibUserApi::getUserProfileUrl($profileApp, $userId, true);
		$avatarImage = CjLibUserApi::getUserAvatarImage($avatarApp, $userId, $name, $height, $email, false, $imgAttribs);
		
		return JHtml::link($profileUrl, $avatarImage, $attribs);
	}
	
	public static function getUserAvatarImage($app, $userId, $alt, $height = 48, $email = null, $urlOnly = true, $imgAttribs = array())
	{
		$db = JFactory::getDBO();
		$avatar = '';
		$userId = !empty($userId) ? $userId : 0;
		
		switch ( $app ) 
		{
			case 'cjforum':
				
				$api = JPATH_ROOT.'/components/com_cjforum/lib/api.php';
				if(file_exists($api))
				{
					require $api;
					$profileApi = CjForumApi::getProfileApi();
					$avatar = $profileApi->getUserAvatarImage($userId, $height);
					
					if($urlOnly == false)
					{
						$avatar = JHtml::image($avatar, $alt, $imgAttribs);
					}
				}
				
				break;
				
			case 'cjblog':
		
				$api = JPATH_ROOT.'/components/com_cjblog/api.php';
		
				if(file_exists($api))
				{
					include_once $api;
					$avatar = CjBlogApi::get_user_avatar_image($userId, $height);
					
					if($urlOnly == false) 
					{
						$avatar = JHtml::image($avatar, $alt, $imgAttribs);
					}
				}
				
				break;
		
			case 'jomsocial':
					
				require_once JPATH_ROOT.'/components/com_community/defines.community.php';
				require_once JPATH_ROOT.'/components/com_community/libraries/core.php';
				require_once JPATH_ROOT.'/components/com_community/helpers/string.php';
		
				$user = CFactory::getUser( $userId );
				$avatar = $user->getThumbAvatar();
				
				if($urlOnly == false)
				{
					$imgAttribs['height'] = $height.'px';
					$avatar = JHtml::image($avatar, $alt, $imgAttribs);
				}
		
				break;
		
			case 'cb':
		
				global $_CB_framework, $_CB_database, $ueConfig, $mainframe;

				$api = JPATH_ADMINISTRATOR.'/components/com_comprofiler/plugin.foundation.php';
		
				if (!is_file($api)) return;
				require_once ($api);
		
				cbimport ( 'cb.database' );
				cbimport ( 'cb.tables' );
				cbimport ( 'cb.field' );
				cbimport ( 'language.front' );
		
				outputCbTemplate( $_CB_framework->getUi() );
				
				//TODO: Here
				$imgAttribs['height'] = $height.'px';
				
				if($userId > 0)
				{
					$cbUser = CBuser::getInstance( $userId );
						
					if ( $cbUser !== null ) 
					{
						$avatar = $cbUser->getField( 'avatar', null, 'php', 'profile', 'list' );
						$alt = $cbUser->getField( 'name');
						$avatar = $avatar['avatar'];
						
						if($urlOnly == false)
						{
							$avatar = JHtml::image($avatar, $alt, $imgAttribs);
						}
					}
				} 
				else 
				{
					if ($height<=90) 
					{
						$avatar = $urlOnly ? selectTemplate().'images/avatar/tnnophoto_n.png' : Jhtml::image(selectTemplate().'images/avatar/tnnophoto_n.png', '', $imgAttribs);
					} 
					else 
					{
						$avatar = $urlOnly ? selectTemplate().'images/avatar/nophoto_n.png' : Jhtml::image(selectTemplate().'images/avatar/nophoto_n.png', '', $imgAttribs);
					}
				}
		
				break;
		
			case 'gravatar':
		
				if(null == $email && $userId > 0)
				{
					$strSql = 'SELECT email FROM #__users WHERE id=' . $userId;
					$db->setQuery($strSql);
					$email = $db->loadResult();
				}

				$avatar = 'https://www.gravatar.com/avatar/'.md5( strtolower( trim( $email ) ) ).'?s='.$height.'&d=mm&r=g';
				
				if($urlOnly == false)
				{
					$avatar = JHtml::image($avatar, $alt, $imgAttribs);
				}
		
				break;
		
			case 'kunena':
		
				if(CjLibUserApi::_initialize_kunena())
				{
					$class = 'avatar';
					$user = KunenaFactory::getUser($userId);
					$avatar = $user->getAvatarImage($class, $height, $height);
					
					if($urlOnly)
					{
						preg_match_all('/<img .*src=["|\']([^"|\']+)/i', $avatar, $matches);
						
						foreach ($matches[1] as $key=>$value) 
						{
							$avatar = $value;
							break;
						}
					}
				}
		
				break;
		
			case 'aup':
					
				$api_AUP = JPATH_SITE.'/components/com_alphauserpoints/helper.php';
		
				if ( file_exists($api_AUP)) 
				{
					require_once ($api_AUP);

					if($urlOnly)
					{
						$avatar = AlphaUserPointsHelper::getAvatarPath($userId);
					} 
					else 
					{
						$avatar = AlphaUserPointsHelper::getAupAvatar($userId, 0, $height, $height);
					}
				}
		
				break;
				
			case 'easysocial':
			
				$api = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_easysocial'.DS.'includes'.DS.'foundry.php';
			
				if( file_exists($api) ) 
				{
					require_once $api;
					$my = Foundry::user();
					$avatar = $my->getAvatar($height < 64 ? SOCIAL_AVATAR_SMALL : ($height < 128 ? SOCIAL_AVATAR_MEDIUM : SOCIAL_AVATAR_LARGE));
					$imgAttribs[] = array('style'=>'height: '.$height.'px');
					
					if($urlOnly == false)
					{
						$avatar = JHtml::image($avatar, $alt, $imgAttribs);
					}
				}
			
				break;
		}
		
		return $avatar;
	}
	
	public static function getUserProfileUrl($system, $userId = 0, $urlOnly = true, $name = 'Guest', $attribs = array())
	{
		$url = null;
		
		switch ( $system ) 
		{
			case 'cjforum':
				
				break;
			
			case 'cjblog':
				
				$api = JPATH_ROOT.'/components/com_cjblog/api.php';
				
				if(file_exists($api))
				{
					require_once $api;
					$url = CjBlogApi::get_user_profile_url($userId, 'name', false, $attribs);
				}
				
				break;
			
			case 'jomsocial':
				
				$jspath = JPATH_BASE.'/components/com_community/libraries/core.php';
				
				if(file_exists($jspath)) 
				{
					include_once($jspath);
					$url = CRoute::_('index.php? option=com_community&view=profile&userid='.$userId);
				}
				
				break;
				
			case 'cb':

				global $_CB_framework, $_CB_database, $ueConfig, $mainframe;
				
				$api = JPATH_ADMINISTRATOR.'/components/com_comprofiler/plugin.foundation.php';
				
				if (!is_file($api)) return;
				require_once ($api);
				
				cbimport ( 'cb.database' );
				cbimport ( 'cb.tables' );
				cbimport ( 'language.front' );
				cbimport ( 'cb.field' );
				
				$url = cbSef( 'index.php?option=com_comprofiler&amp;task=userProfile&amp;user=' . ( (int) $userId ) . getCBprofileItemid( true, false ) );
				
				break;
				
			case 'kunena':
				
				if(CJFunctions::_initialize_kunena() && $userId > 0) 
				{
					$user = KunenaFactory::getUser($userId);
					if ($user === false) break;
					$url = KunenaRoute::_('index.php?option=com_kunena&func=profile&userid='.$user->userid, true);
				}
				
				break;
				
			case 'aup':
				
				$api_AUP = JPATH_SITE.'/components/com_alphauserpoints/helper.php';
				
				if ( file_exists($api_AUP)) 
				{
					require_once ($api_AUP);
					$url = AlphaUserPointsHelper::getAupLinkToProfil($userId);
				}
				
				break;

			case 'easysocial':
				
				$api = JPATH_ADMINISTRATOR.DS.'components'.DS.'com_easysocial'.DS.'includes'.DS.'foundry.php';

				if( file_exists($api) ) {
					
					require_once $api;
					$my = Foundry::user($userId);
					$url = FRoute::profile(array( 'id' => $my->getAlias()));
					$name = $my->getName();
				}

				break;
		}

		if($url && ! $urlOnly)
		{
			$url = JHtml::link($url, $name, $attribs);
		}
		
		return (null == $url) ? $name : $url;
	}
	
	/**
	 * A private function that is used to initialize kunena app
	 * 
	 * @return boolean true if success, false if not compatible kunena installation found
	 */
	private static function _initialize_kunena()
	{
		if (!(class_exists('KunenaForum') && KunenaForum::isCompatible('2.0') && KunenaForum::installed())) 
		{
			return false;
		}
		KunenaForum::setup();
		
		return true;
	}
}