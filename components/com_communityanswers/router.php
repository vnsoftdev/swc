<?php
/**
 * @version		$Id: router.php 01 2012-12-07 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

/*
 * Function to convert a system URL to a SEF URL
*/
function CommunityAnswersBuildRoute(&$query) {
	
    static $items;

    $segments	= array();
    
    if(isset($query['task'])) {
    	
        $segments[] = $query['task'];
        unset($query['task']);
    }
    
    if(isset($query['id'])) {
    	
        $segments[] = $query['id'];
        unset($query['id']);
    }
    
    if(isset($query['catid'])) {
    	
        $segments[] = $query['catid'];
        unset($query['catid']);
    }
    
	unset($query['view']);
    
	return $segments;
}
/*
 * Function to convert a SEF URL back to a system URL
*/
function CommunityAnswersParseRoute($segments) {
	
    $vars = array();
    $app = JFactory::getApplication();
    
    $menu = JFactory::getApplication()->getMenu();
    $active = $menu->getActive();
    
    if($active){
    
    	$vars['view'] = $active->query['view'] == 'user' ? 'user' : 'answers';
    }
    
    if(count($segments) > 1) {
    	 
    	$vars['task']	= $segments[0];
    	
    	if($vars['view'] == 'answers'){
    		
    		switch($vars['task']){
    			
    			case 'tag':
    				
    				$vars['id']     = $segments[1];
    				break;
    				
    			default:
			    	$vars['id']     = $segments[1];
			    	$vars['catid']	= $segments[1];
			    	break;
    		}
    	}
    } else  if(count($segments) > 0){
    	
    	if(empty($task)){
    		
    		$vars['task']  = $segments[0];
    		$vars['id']	= $segments[0];
    	} else {
    		
    		$vars['id']	= $segments[0];
    	}
    }

    return $vars;
}
?>