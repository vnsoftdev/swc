<?php
/**
 * @version		$Id: header.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

$user = JFactory::getUser();
$itemid = CJFunctions::get_active_menu_id();
$user_itemid = CJFunctions::get_active_menu_id(true, 'index.php?option='.A_APP_NAME.'&view=user');
$answers_itemid = CJFunctions::get_active_menu_id(true, 'index.php?option='.A_APP_NAME.'&view=answers');
$catparam = !empty($catparam) ? $catparam : '';
$activity_count = AnswersHelper::get_new_activity_count();
?>

<?php if($this->params->get('show_toolbar', 1) == 1):?>
<?php echo CJFunctions::load_module_position('answers-common-above-navbar');?>
<div class="navbar">
	<div class="navbar-inner">
		<div class="header-container">

			<a class="btn btn-navbar navbar-btn" data-toggle="collapse" data-target=".ca-nav-collapse"> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span> 
				<span class="icon-bar"></span>
			</a>
			 
			<a class="brand navbar-brand" href="<?php echo JRoute::_(!empty($this->brand_url) ? $this->brand_url : '#');?>">
				<?php echo $this->escape($this->brand);?>
			</a>
			
			<?php if($this->params->get('enable_login_form', '1') == '1'):?>
			<ul class="nav pull-right">
				<?php if($user->guest):?>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="fa fa-user"></i> <b class="caret"></b>
					</a>
					
					<ul class="dropdown-menu">
						<li class="nav-header navbar-header"><?php echo JText::_('JLOGIN');?></li>
						<li class="padding-10">
							<form action="<?php echo JRoute::_('index.php', true, $this->params->get('usesecure')); ?>" method="post" id="login-form" class="form-horizontal">
								<div class="input-prepend" style="margin-bottom: 10px;">
									<span class="add-on"><i class="fa fa-user"></i></span>
									<input type="text" name="username" id="inputUsername" class="input-fix margin-bottom-10" placeholder="<?php echo JText::_('JGLOBAL_USERNAME');?>"/>
								</div>
								
								<div class="input-prepend" style="margin-bottom: 10px;">
									<span class="add-on"><i class="fa fa-lock"></i></span>
									<input type="password" name="password" class="input-fix margin-bottom-10" id="inputPassword" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD');?>"/>
								</div>
								
								<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
								<label class="checkbox"><input type="checkbox"/> <?php echo JText::_('COM_CJLIB_REMEMBER_ME');?></label>
								<?php endif; ?>
				
								<input type="hidden" name="option" value="com_users" />
								<input type="hidden" name="task" value="user.login" />
								<input type="hidden" name="return" value="<?php echo base64_encode(JUri::current()); ?>" />
								<?php echo JHtml::_('form.token'); ?>
								<button type="button" class="btn" data-dismiss="modal"><?php echo JText::_('JCANCEL');?></button>
								<button class="btn btn-primary" type="submit" onclick="jQuery(this).button('loading');"><?php echo JText::_('JLOGIN');?></button>
							</form>
						</li>
					</ul>
				</li>
				<?php else:?>
				<li style="vertical-align: middle;">
					<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=activities'.$answers_itemid);?>" style="padding: 2px 0;">
						<span class="label<?php echo $activity_count > 0 ? ' label-important' : ''?>" style="padding: 10px 15px 10px;">
							<?php echo $activity_count;?>
						</span>
					</a>
				</li>
				<li>
					<a class="tooltip-hover" href="#" onclick="document.cw_logout_form.submit();" title="<?php echo JText::_('JLOGOUT');?>" style="margin: 2px; padding: 0;">
						<span class="label label-success" style="padding: 10px 15px 10px;"><i class="fa fa-user fa-lg"></i></span>
					</a>
					<form id="cw_logout_form" name="cw_logout_form" action="<?php echo JRoute::_('index.php', true, $this->params->get('usesecure'));?>" method="post" style="display: none;">
						<input type="hidden" name="option" value="com_users"/> 
						<input type="hidden" name="task" value="user.logout"/> 
						<input type="hidden" name="return" value="<?php echo base64_encode(JUri::current());?>"/>
						<?php echo JHTML::_( 'form.token' ); ?>
					</form>
				</li>
				<?php endif;?>
			</ul>
			<?php endif;?>
			
			<div class="nav-collapse collapse ca-nav-collapse navbar-collapse">
				<ul class="nav">
					<li class="dropdown<?php echo $page_id >= 100 && $page_id < 200 ? ' active' : ''?>">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo JText::_('LBL_DISCOVER');?> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li class="nav-header"><?php echo JText::_('LBL_QUESTIONS');?></li>
							<li<?php echo $page_id == 101 ? ' class="active"' : '';?>>
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=trending'.$catparam.$answers_itemid);?>">
									<i class="fa fa-leaf"></i> <?php echo JText::_('LBL_TRENDING_QUESTIONS');?>
								</a>
							</li>
							<li<?php echo $page_id == 102 ? ' class="active"' : '';?>>
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=open'.$catparam.$answers_itemid);?>">
									<i class="fa fa-folder-open-o"></i> <?php echo JText::_('LBL_OPEN_QUESTIONS');?>
								</a>
							</li>
							<li<?php echo $page_id == 103 ? ' class="active"' : '';?>>
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=resolved'.$catparam.$answers_itemid);?>">
									<i class="fa fa-folder-o"></i> <?php echo JText::_('LBL_RESOLVED_QUESTIONS');?>
								</a>
							</li>
							<li class="divider"></li>
							<li class="nav-header"><?php echo JText::_('LBL_MORE_LISTINGS')?></li>
							<li<?php echo $page_id == 104 ? ' class="active"' : '';?>>
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=latest'.$catparam.$answers_itemid);?>">
									<i class="fa fa-tasks"></i> <?php echo JText::_('LBL_LATEST_QUESTIONS');?>
								</a>
							</li>
							<li<?php echo $page_id == 105 ? ' class="active"' : '';?>>
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=popular'.$catparam.$answers_itemid);?>">
									<i class="fa fa-eye"></i> <?php echo JText::_('LBL_MOST_POPULAR_QUESTIONS');?>
								</a>
							</li>
							<li<?php echo $page_id == 106 ? ' class="active"' : '';?>>
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=mostanswered'.$catparam.$answers_itemid);?>">
									<i class="fa fa-fire"></i> <?php echo JText::_('LBL_MOST_ANSWERED_QUESTIONS');?>
								</a>
							</li>
							
							<?php if($this->params->get('enable_leaderboard', 1) == 1):?>
							<li class="divider"></li>
							<li class="nav-header"><?php echo JText::_('COM_COMMUNITYANSWERS_USERS')?></li>
							<li <?php echo $page_id == 107 ? 'class="active"' : ''?>>
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=leaderboard'.$catparam.$answers_itemid);?>">
									<i class="fa fa-trophy"></i> <?php echo JText::_('COM_COMMUNITYANSWERS_LEADERBOARD');?>
								</a>
							</li>
							<?php endif;?>
							
							<li class="divider"></li>
							<li class="nav-header"><?php echo JText::_('LBL_SEARCH')?></li>
							<li <?php echo $page_id == 108 ? 'class="active"' : ''?>>
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=search'.$answers_itemid);?>">
									<i class="fa fa-search-plus"></i> <?php echo JText::_('LBL_ADVANCED_SEARCH');?>
								</a>
							</li>
						</ul>
					</li>
					<li <?php echo $page_id == 200 ? 'class="active"' : ''?>>
						<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=tags'.$answers_itemid);?>">
							<?php echo JText::_('LBL_TAGS');?>
						</a>
					</li>
					<?php if(!$user->guest && !empty($user_itemid)):?>
					<li class="dropdown<?php echo $page_id >= 300 && $page_id < 400  ? ' active' : '';?>">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo JText::_('LBL_MY_STUFF');?> <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li <?php echo $page_id == 301 ? 'class="active"' : ''?>>
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=user&task=questions'.$user_itemid);?>">
									<i class="fa fa-question-circle"></i> <?php echo JText::_('LBL_MY_QUESTIONS');?>
								</a>
							</li>
							<li <?php echo $page_id == 302 ? 'class="active"' : ''?>>
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=user&task=answers'.$user_itemid);?>">
									<i class="fa fa-info-circle"></i> <?php echo JText::_('LBL_MY_ANSWERS');?>
								</a>
							</li>
							<li class="divider"></li>
							<li <?php echo $page_id == 303 ? 'class="active"' : ''?>>
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=user&task=subscriptions'.$user_itemid);?>">
									<i class="fa fa-bookmark-o"></i> <?php echo JText::_('LBL_MY_SUBSCRIPTIONS');?>
								</a>
							</li>
						</ul>
					</li>
					<?php endif;?>
				</ul>
			</div>
		</div>
	</div>
</div>
<?php echo CJFunctions::load_module_position('answers-common-below-navbar');?>
<?php endif;?>