<?php
/**
 * @version		$Id: header.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

class AnswersHelper 
{
	public static function awardPoints($params, $userid, $action, $reference, $info, $points = 0)
	{
		$functions = null;

		switch ($params->get('points_system'))
		{
			case 'cjblog':
			case 'touch':

				$functions = array(
					'newqn'=>'com_communityanswers.newquestion',
					'newans'=>'com_communityanswers.newanswer',
					'bestans'=>'com_communityanswers.bestanswer',
					'voteup'=>'com_communityanswers.voteup',
					'votedown'=>'com_communityanswers.votedown',
					'votedownuser'=>'com_communityanswers.votedownuser',
					'bountyfrom'=>'com_communityanswers.bountyfrom',
					'bountywinner'=>'com_communityanswers.bountywinner');

				break;

			case 'aup':

				$functions = array(
					'newqn'=>'sysplgaup_newquestion',
					'newans'=>'sysplgaup_newanswer',
					'bestans'=>'sysplgaup_acceptedanswer',
					'voteup'=>'sysplgaup_voteup',
					'votedown'=>'sysplgaup_votedown',
					'votedownuser'=>'sysplgaup_votedownuser',
					'bountyfrom'=>'sysplugaup_bountyfrom',
					'bountywinner'=>'sysplugaup_bountywinner');

				break;

			case 'jomsocial':

				$functions = array(
					'newqn'=>'com_communityanswers.newquestion',
					'newans'=>'com_communityanswers.newanswer',
					'bestans'=>'com_communityanswers.acceptedanswer',
					'voteup'=>'com_communityanswers.voteup',
					'votedown'=>'com_communityanswers.votedown',
					'votedownuser'=>'com_communityanswers.votedownuser',
					'bountyfrom'=>'com_communityanswers.bountyfrom',
					'bountywinner'=>'com_communityanswers.bountywinner');

				break;

			case 'easysocial':
			
				$functions = array(
				'newqn'=>'com_communityanswers.newquestion',
				'newans'=>'com_communityanswers.newanswer',
				'bestans'=>'com_communityanswers.acceptedanswer',
				'voteup'=>'com_communityanswers.voteup',
				'votedown'=>'com_communityanswers.votedown',
				'votedownuser'=>'com_communityanswers.votedownuser',
				'bountyfrom'=>'com_communityanswers.bountyfrom',
				'bountywinner'=>'com_communityanswers.bountywinner');
			
				break;

			default:

				return false;
		}

		switch ($action)
		{
			case 1: // new question

				CJFunctions::award_points($params->get('points_system'), $userid, array(
					'points'=>$params->get('points_on_new_question'),
					'reference'=>$reference,
					'info'=>$info,
					'function'=>$functions['newqn'],
					'component'=>A_APP_NAME
				));

				break;

			case 2: // new answer

				CJFunctions::award_points($params->get('points_system'), $userid, array(
					'points'=>$params->get('points_on_new_answer'),
					'reference'=>$reference,
					'info'=>$info,
					'function'=>$functions['newans'],
					'component'=>A_APP_NAME
				));

				break;

			case 3: //best answer

				CJFunctions::award_points($params->get('points_system'), $userid, array(
					'points'=>$params->get('points_on_accepted_answer'),
					'reference'=>$reference,
					'info'=>$info,
					'function'=>$functions['bestans'],
					'component'=>A_APP_NAME
				));

				break;

			case 4: // vote up

				CJFunctions::award_points($params->get('points_system'), $userid, array(
					'points'=>$params->get('points_on_vote_up'),
					'reference'=>$reference,
					'info'=>$info,
					'function'=>$functions['voteup'],
					'component'=>A_APP_NAME
				));

				break;

			case 5: // vote down

				CJFunctions::award_points($params->get('points_system'), $userid, array(
					'points'=>$params->get('points_on_vote_down'),
					'reference'=>$reference,
					'info'=>$info,
					'function'=>$functions['votedown'],
					'component'=>A_APP_NAME
				));

				break;

			case 6: // points to voted down user

				CJFunctions::award_points($params->get('points_system'), $userid, array(
					'points'=>$params->get('points_on_vote_down_user'),
					'reference'=>$reference,
					'info'=>$info,
					'function'=>$functions['votedownuser'],
					'component'=>A_APP_NAME
				));
				
				break;
				
			case 7: // points deducted for bounty creation
				
				CJFunctions::award_points($params->get('points_system'), $userid, array(
					'points'=>$points,
					'reference'=>$reference,
					'info'=>$info,
					'function'=>$functions['bountyfrom'],
					'component'=>A_APP_NAME
				));
				
				break;

			case 8: // points awarded for winning bounty
				
				CJFunctions::award_points($params->get('points_system'), $userid, array(
					'points'=>$points,
					'reference'=>$reference,
					'info'=>$info,
					'function'=>$functions['bountywinner'],
					'component'=>A_APP_NAME
				));
	
				break;
		}
	}

    public static function stream_activity($action, $params, $question, $user_id, $url) 
    {
    	$link = JHtml::link($url, CJFunctions::escape($question->title));
    	$component = $params->get('activity_stream_type', 'none');
    	$profile = $params->get('user_avatar', 'none');
    	$title = null;
    	$command = null;
    	$description = '';
    	
    	switch ($component)
    	{
    		case 'jomsocial':
    			
    			switch ($action)
    			{
    				case 1: // New question
    					$title = JText::sprintf('LBL_STREAM_ACTOR_ASKED_QUESTION', '{actor}', $link);
    					$command = 'com_communityanswers.newquestion';
    					$description = $question->description;
    					break;
    					
    				case 2: // New answer
    					$title = JText::sprintf('LBL_STREAM_ACTOR_ANSWERED_QUESTION', '{actor}', $link);
    					$command = 'com_communityanswers.newanswer';
    					$description = $question->answer;
    					break;
    					
    				case 3: // Accepted answer
    					$title = JText::sprintf('LBL_STREAM_ACTOR_ACCEPTED_QUESTION', '{actor}', $link);
    					$command = 'com_communityanswers.acceptedanswer';
    					$description = $question->answer;
    					break;
    			}
    			
    			break;
    			
    		case 'easysocial':
    			
    			$username = CJFunctions::get_user_profile_url($profile, $user_id, $question->username, false);
    			
    			switch ($action)
    			{
    				case 1: // New question
    					$title = JText::sprintf('LBL_STREAM_ACTOR_ASKED_QUESTION', $username, $link);
    					$command = 'com_communityanswers.newquestion';
    					$description = $question->description;
    					break;
    						
    				case 2: // New answer
    					$title = JText::sprintf('LBL_STREAM_ACTOR_ANSWERED_QUESTION', $username, $link);
    					$command = 'com_communityanswers.newanswer';
    					$description = $question->answer;
    					break;
    						
    				case 3: // Accepted answer
    					$title = JText::sprintf('LBL_STREAM_ACTOR_ACCEPTED_QUESTION', $username, $link);
    					$command = 'com_communityanswers.acceptedanswer';
    					$description = $question->answer;
    					break;
    			}

    			break;
    	}
    	
    	if(!empty($title) && !empty($command))
    	{
	    	CJFunctions::stream_activity(
	    		$component,
	    		$user_id,
	    		array(
	    			'command' => $command,
	    			'component' => A_APP_NAME,
	    			'title' => $title,
	    			'href' => $url,
	    			'description' => $description,
	    			'length' => $params->get('stream_character_limit', 256),
	    			'icon' => 'components/'.A_APP_NAME.'/assets/images/answers.png',
	    			'group' => 'Answers',
	    			'item_id' => $question->id,
	    			'context' => 'questions'
	    		)
	    	);
    	}
    }
    
    public static function get_sanitize_urls($reference)
    {
    	require_once CJLIB_PATH.'/framework/lib_autolink.php';
    	$text = autolink_urls($reference, 50, ' rel="nofollow" target="_blank" class="muted"');
    	
    	return nl2br($text);
    }		
    
    public static function authorize_user($groups, $authgroups)
    {
		$flag = false;
		
		if(!empty($groups))
		{
			$groups = explode(',', $groups);
			$user = JFactory::getUser();
			
			foreach ($groups as $group)
			{
				if(in_array($group, $authgroups))
				{
					$flag = true;
					break;
				}
			}
		}else
		{
			$flag = true;
		}
		
		return $flag;
    }
    
    public static function get_category_tree($categories, $permission)
    {
    	$tree = array();
    	$authgroups = JFactory::getUser()->getAuthorisedGroups();
    	
    	if(!empty($categories))
    	{
    		AnswersHelper::recursively_build_tree($categories, $authgroups, $tree, $permission);
    	}
    	
    	return $tree;
    }
    
    private static function recursively_build_tree($categories, $authgroups, &$tree, $permission, $nlevel = 0)
    {
    	foreach($categories as $category)
    	{
    		if(!empty($category[$permission]))
    		{
    			$common_groups = array_intersect(explode(',', $category[$permission]), $authgroups);
    			if(empty($common_groups)) continue;
    		}
    		
    		$tree[$category['id']] = str_repeat('...', $nlevel).$category['title'];
    		
    		if(!empty($category['children']))
    		{
    			AnswersHelper::recursively_build_tree($category['children'], $authgroups, $tree, $permission, $nlevel + 1);
    		}
    	}
    }
    
    public static function get_new_activity_count()
    {
    	JLoader::import('joomla.application.component.model');
    	JLoader::import('notification', JPATH_ROOT.DS.'components'.DS.A_APP_NAME.DS.'models');
    	$model = JModelLegacy::getInstance( 'activities', 'CommunityAnswersModel' );
    	$count = $model->get_new_activity_count();
    	
    	return $count;
    }
    
    public static function get_activities()
    {
    	JLoader::import('joomla.application.component.model');
    	JLoader::import('activities', JPATH_ROOT.DS.'components'.DS.A_APP_NAME.DS.'models');
    	$model = JModelLegacy::getInstance( 'activities', 'CommunityAnswersModel' );
    	
    	$return = new stdClass();
    	$return->items = $model->getItemd();
    	$return->pagination = $model->getPagination();
    	$return->state = $model->getState();
    	
    	return $return;
    }
    
    public static function get_activity_title($activity)
    {
    	$link = JHtml::link($activity->url, CJFunctions::escape($activity->title));
    	
    	switch ($activity->activity_type)
    	{
    		case 1: // new answer
    			return JText::sprintf('COM_COMMUNITYANSWERS_NOTIF_NEW_ANSWER', $link);
    			
    		case 2: // new comment
    			return JText::sprintf('COM_COMMUNITYANSWERS_NOTIF_NEW_COMMENT', $link);
    			
    		case 3: // accepted answer
    			return JText::sprintf('COM_COMMUNITYANSWERS_NOTIF_ACCEPTED_ANSWER', $link);
    			
    		case 4: // voted up
    			return JText::sprintf('COM_COMMUNITYANSWERS_NOTIF_VOTED_UP', $link);
    			
    		case 5: // voted down
    			return JText::sprintf('COM_COMMUNITYANSWERS_NOTIF_VOTED_DOWN', $link);
    			
    		case 6: // new badge
    			break;
    	}
    }
    
    public static function get_activity_icon($type)
    {
    	switch ($type){
    		
    		case 1: return 'fa fa-reply text-info';
    		case 2: return 'fa fa-comments-o text-warning';
    		case 3: return 'fa fa-check-circle-o text-success';
    		case 4: return 'fa fa-thumbs-o-up text-success';
    		case 5: return 'fa fa-thumbs-o-down text-error';
    		case 6: return '';
    	}
    }
    
    public static function get_bounty_message($bounty)
    {
    	switch ($bounty->state)
    	{
    		case 1: // bounty is running
    			if(!empty($bounty->expiry_date) && $bounty->expiry_date != '0000-00-00 00:00:00')
    			{
    				return JText::sprintf('MSG_BOUNTY_RUNNING_WITH_EXPIRY', $bounty->bounty_points, CJFunctions::get_formatted_date($bounty->expiry_date));
    			} 
    			else 
    			{
    				return JText::sprintf('MSG_BOUNTY_RUNNING', $bounty->bounty_points);
    			}
    			
    		case 2: // bounty already awarded to the answerer
    			return JText::sprintf('MSG_BOUNTY_SUCCESS_AND_AWARDED', $bounty->bounty_points);
    			
    		case 3: // bounty expired as there are few unaccepted answers
    			return JText::sprintf('MSG_BOUNTY_REFUNDED_TO_ASKER', $bounty->bounty_points);
    			
    		case 4: // bounty expired with no answer with positive rating
    			return JText::sprintf('MSG_BOUNTY_EXPIRED_WITHOUT_RATED_ANSWERS', $bounty->bounty_points);
    			
    		case 5: // bounty expired with no answers, asker will get a refund for this
    			return JText::sprintf('MSG_BOUNTY_EXPIRED_WITHOUT_ANSWERS', $bounty->bounty_points);
    			
    		default:
    			return JText::_('MSG_BOUNTY_DISABLED');
    	}
    }
}