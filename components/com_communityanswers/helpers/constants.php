<?php
/**
 * @version		$Id: header.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

defined('A_APP_VERSION') or define('A_APP_VERSION', '3.1.0');
defined('A_APP_NAME') or define('A_APP_NAME', 'com_communityanswers');
defined('CA_MEDIA_URI') or define('CA_MEDIA_URI', JUri::root(true).'/media/'.A_APP_NAME);
defined('CA_ATTACHMENTS_PATH') or define('CA_ATTACHMENTS_PATH', JPATH_ROOT.DS.'media'.DS.'communityanswers'.DS.'attachments'.DS);
defined('CA_ATTACHMENTS_URI') or define('CA_ATTACHMENTS_URI', JUri::root(true).'/media/communityanswers/attachments/');