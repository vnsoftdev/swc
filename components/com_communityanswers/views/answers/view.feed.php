<?php
/**
 * @version		$Id: view.feed.php 01 2012-12-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view');
jimport( 'joomla.document.feed.feed' );

class CommunityAnswersViewAnswers extends JViewLegacy{
	
	protected $params;
	
	public function display($tpl = null){

		$app       = JFactory::getApplication();
		$doc       = JFactory::getDocument();
		$model	   = $this->getModel();
		$params    = $app->getParams();
		$feedEmail = $app->getCfg('feed_email', 'author');
		$siteEmail = $app->getCfg('mailfrom');
		$itemid    = CJFunctions::get_active_menu_id(false);
		$catid	   = 0;

		if(!$catid){
				
			$menuparams = $app->getMenu()->getParams( $itemid );
			$catid = (int)$menuparams->get('catid', 0);
			$app->input->set('id', $catid);
		}
		
		/********************************** PARAMS *****************************/
		$appparams = JComponentHelper::getParams(A_APP_NAME);
		$menuParams = new JRegistry;
		
		if ($active) {
		
			$menuParams->loadString($active->params);
		}
		
		$this->params = clone $menuParams;
		$this->params->merge($appparams);
		/********************************** PARAMS *****************************/
		
		$doc->link	= JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.($catid ? '&id='.$catid : ''));
		
		// Get some data from the model
		$app->input->set('limit', $app->getCfg('feed_limit'));
		$options = array('catid'=>$catid, 'limit'=>$app->input->get('limit'), 'limitstart'=>0, 'order'=>'a.created', 'order_dir'=>'desc');
		$return = $model->get_questions(1, $options, $this->params);
		
		if(empty($return['questions'])) return;
		
		foreach ($return['questions'] as $row)
		{
			// strip html from feed item title
			$title = $this->escape($row->title);
			$title = html_entity_decode($title, ENT_COMPAT, 'UTF-8');
		
			// Compute the article slug
			$row->slug = $row->alias ? ($row->id . ':' . $row->alias) : $row->id;
		
			// Url link to article
			$link = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$row->id.':'.$row->alias.'&Itemid='.$itemid);
		
			$description	= ($params->get('feed_summary', 0) ? CJFunctions::substrws($row->description, 250) : $row->description);
		
			// Load individual item creator class
			$item				= new JFeedItem;
			$item->title		= $title;
			$item->link			= $link;
			$item->date			= $row->created;
			$item->category		= $row->category_title;
			$item->author 		= $row->username;
			
			if ($feedEmail == 'site')
			{
				$item->authorEmail = $siteEmail;
			}
			elseif ($feedEmail === 'author')
			{
				$item->authorEmail = $row->email;
			}
		
			// Add readmore link to description if introtext is shown, show_readmore is true and fulltext exists
			if (!$params->get('feed_summary', 0) && $params->get('feed_show_readmore', 0) && $row->description)
			{
				$description .= '<p class="feed-readmore"><a target="_blank" href ="' . $item->link . '">' . JText::_('COM_CONTENT_FEED_READMORE') . '</a></p>';
			}
		
			// Load item description and add div
			$item->description	= '<div class="feed-description">'.$description.'</div>';
		
			// Loads item info into rss array
			$doc->addItem($item);
		}
	}
}