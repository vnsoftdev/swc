<?php
/**
 * @version		$Id: default.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

$itemid = CJFunctions::get_active_menu_id();
$user = JFactory::getUser();
$page_id = isset($this->page_id) ? $this->page_id : 101;

if(!empty($this->category)){
	
	$catparam = '&id='.$this->category['id'].':'.$this->category['alias'];
}
?>

<div id="cj-wrapper" class="cj-wrapper-main">
	
	<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'header.php';?>

	<div class="container-fluid question-wrapper">
		<div class="row-fluid">
			<div class="span12">
			
				<?php echo CJFunctions::load_module_position('answers-list-above-ask-form');?>
				
				<div class="well">

					<?php if(!empty($this->categories) || !empty($this->category)):?>
					<div class="clearfix page-header no-space-top margin-bottom-10">
						<h2 class="margin-right-10 no-space-top no-space-bottom pull-left">
							<?php echo JText::_('LBL_CATEGORIES').(!empty($this->category) ? ': <small>'.$this->escape($this->category['title']).'</small>' : '');?>
						</h2>
						
						<div class="header-icons">
							<?php if(!$user->guest && $user->authorise('answers.subscrcat', A_APP_NAME)):?>
							<?php if(isset($this->subscribed) && $this->subscribed):?>
							<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=unsubscribe'.$catparam.$itemid);?>" 
								title="<?php echo JText::_('LBL_UNSUBSCRIBE')?>" class="tooltip-hover btn-unsubscribe" onclick="return false;">
								<i class="fa fa-check-circle-o"></i>
							</a>
							<?php else:?>
							<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=subscribe'.$catparam.$itemid);?>" 
								title="<?php echo JText::_('LBL_SUBSCRIBE')?>" class="tooltip-hover btn-subscribe" onclick="return false;">
								<i class="fa fa-envelope-o"></i>
							</a>
							<?php endif;?>
							<?php endif;?>
							
							<?php if($this->params->get('enable_rss_feed', 0) == '1'):?>
							<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=feed&format=feed'.$catparam.$itemid);?>" 
								title="<?php echo JText::_('LBL_RSS_FEED')?>" class="tooltip-hover">
								<i class="fa fa-rss-square"></i>
							</a>
							<?php endif;?>
						</div>
					</div>					
					<?php endif;?>
	
					<?php if(!empty($this->page_description)):?>
					<div class="margin-bottom-10"><?php echo $this->page_description;?></div>
					<?php endif;?>
	
					<?php 
					if($this->params->get('display_cat_list', 1) == 1){
	
						echo CJFunctions::get_categories_table_markup($this->categories, array(
								'max_columns'=>$this->params->get('num_cat_list_columns', 3), 'max_children'=>0, 'base_url'=>$this->page_url, 'menu_id'=>$itemid,
								'stat_primary'=>'questions', 'stat_secondary'=>'answers', 'stat_tooltip'=>JText::_('LBL_CATEGORY_STATS_TOOLTIP')));
					} 
					?>

					<?php if($user->guest || $user->authorise('answers.ask', A_APP_NAME)):?>
					<div class="center margin-top-10">
						<form action="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=ask'.$itemid);?>" style="text-align: center;" method="post" class="no-margin-bottom">
							<div class="input-append center span12">
								<input type="text" name="input-ask" placeholder="<?php echo JText::_('LBL_ASK');?>">
								<button type="submit" class="btn"><?php echo JText::_('LBL_ASK_QUESTION');?></button>
							</div>
							<div class="center">
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=search'.$itemid)?>" class="muted">
									<?php echo JText::_('LBL_TRY_ADVANCED_SEARCH');?>
								</a>
							</div>
							<input type="hidden" name="catid" value="<?php echo !empty($this->category) ? $this->category['id'] : 0;?>">
						</form>
					</div>
					<?php endif;?>
				</div>
				
				<?php echo CJFunctions::load_module_position('answers-list-below-ask-form');?>
				
				<?php if($this->params->get('display_page_heading', 1) == 1):?>
				<h2 class="page-header"><?php echo $this->params->get('page_heading', '');?></h2>
				<?php endif;?>
				
				<?php if(!empty($this->items)):?>
				<?php foreach ($this->items as $item):?>
				<div class="media clearfix">
					<?php if($this->params->get('user_avatar') != 'none'):?>
					<div class="pull-left margin-right-10 avatar hidden-phone">
						<?php echo CJFunctions::get_user_avatar(
							$this->params->get('user_avatar'), 
							$item->created_by, 
							$this->params->get('user_display_name'), 
							$this->params->get('avatar_size'),
							$item->email,
							array('class'=>'thumbnail tooltip-hover', 'title'=>$item->username),
							array('class'=>'media-object', 'style'=>'height:'.$this->params->get('avatar_size').'px'));?>
					</div>
					<?php endif;?>
							
					<div class="pull-left hidden-phone thumbnail num-box<?php echo $item->accepted_answer > 0 ? ' accepted-block tooltip-hover' : '';?>" 
						title="<?php echo $item->accepted_answer > 0 ? JText::_('LBL_QUESTION_IS_SOLVED') : '';?>">
						<h2 class="num-header"><?php echo $item->answers;?></h2>
						<span class="muted"><?php echo $item->answers == 1 ? JText::_('TXT_ANSWER') : JText::_('TXT_ANSWERS');?></span>
					</div>
					
					<div class="media-body">
						<h4 class="media-heading">
							<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$item->id.':'.$item->alias.$itemid);?>">
								<?php if($item->bounty > 0 && $item->bounty_state == 1):?>
								<span class="label label-success">+<?php echo $item->bounty;?></span>
								<?php endif;?>
								<?php echo $this->escape($item->title)?>
							</a>
						</h4>
						<div class="muted">
							<small>
							<span class="label label-success visible-phone"><?php echo $item->answers == 1 ? JText::_('TXT_ONE_ANSWER') : JText::sprintf('TXT_N_ANSWERS', $item->answers);?></span>
							<?php 
							$category_name = JHtml::link(
								JRoute::_($this->page_url.'&id='.$item->catid.':'.$item->category_alias.$itemid),
								$this->escape($item->category_title));
							$user_name = $item->created_by > 0 
								? CJFunctions::get_user_profile_link($this->params->get('user_avatar'), $item->created_by, $this->escape($item->username))
								: $this->escape($item->username);
							$formatted_date = CJFunctions::get_formatted_date($item->created);
							
							echo JText::sprintf('LBL_QUESTION_META', $category_name, $user_name, $formatted_date, $item->hits);
							?>
							</small>
						</div>
						<div class="tags">
							<?php foreach($item->tags as $tag):?>
							<a title="<?php echo JText::sprintf('LBL_TAGGED_QUESTIONS', $this->escape($tag->tag_text));?>" class="tooltip-hover" 
								href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=tag&id='.$tag->tag_id.':'.$tag->alias.$itemid);?>">
								<span class="label"><?php echo $this->escape($tag->tag_text);?></span>
							</a>
							<?php endforeach;?>
						</div>
						<?php if($this->params->get('list_show_description') == 1):?>
						<div class="description">
							<?php echo CJFunctions::process_html($item->description, ($this->params->get('default_editor') == 'bbcode'), ($this->params->get('process_content_plugins', false) == '1'));?>
						</div>
						<?php endif;?>
					</div>
				</div>
				<?php endforeach;?>
				
				<?php echo CJFunctions::load_module_position('answers-list-above-pagination');?>
				
				<div class="row-fluid">
					<div class="span12">
						<?php 
						echo CJFunctions::get_pagination(
								$this->page_url.$catparam, 
								$this->pagination->get('pages.start'), 
								$this->pagination->get('pages.current'), 
								$this->pagination->get('pages.total'),
								$this->pagination->get('limit'),
								true
							);
						?>
					</div>
				</div>
		
				<?php else:?>
				<div class="alert alert-info"><?php echo JText::_('MSG_NO_RESULTS')?></div>
				<?php endif;?>
				
				<?php echo CJFunctions::load_module_position('answers-list-below-pagination');?>
			</div>
		</div>
		
		<div id="message-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 id="myModalLabel"><?php echo JText::_('LBL_ALERT');?></h3>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('LBL_CLOSE');?></button>
			</div>
		</div>
	</div>
</div>