<?php
/**
 * @version		$Id: default.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

$page_id = 107;
$i = 1;

$system = $this->params->get('user_avatar', 'none');
?>
<div id="cj-wrapper" class="cj-wrapper-main">
	
	<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'header.php';?>
	
	<div class="panel panel-default">
		<div class="panel-heading"><?php echo JText::_('COM_COMMUNITYANSWERS_LEADERBOARD');?></div>
		<ul class="list-group">
			<?php 
			if(!empty($this->items)){
				foreach ($this->items as $rank=>$item)
				{
					$username = $this->escape($item->username);
				?>
				<li class="list-group-item">
					<div class="media">
						<div class="panel panel-success text-center no-margin-bottom pull-left" style="min-width: 64px; min-height: 72px;">
							<h2 class="leader-rank"><?php echo $rank + 1;?></h2>
							<div class="muted"><?php echo JText::_('COM_COMMUNITYANSWERS_RANK');?></div>
						</div>
						
						<a href="<?php echo CJFunctions::get_user_profile_url($system, $item->id, $username, true)?>" class="pull-left thumbnail">
							<?php echo CJFunctions::get_user_avatar_image($system, $item->id, $username, 64, $item->email, false);?>
						</a>
						<div class="media-body">
							<h4 class="margin-top-5 margin-bottom-5"><?php echo $username;?></h4>
							<div class="muted margin-bottom-10"><strong><?php echo JText::sprintf('COM_COMMUNITYANSWERS_KARMA', $item->karma)?></strong></div>
							<div class="muted">
								<ul class="unstyled inline">
									<li class="no-pad-left"><i class="fa fa-comments"></i> <?php echo JText::sprintf('COM_COMMUNITYANSWERS_NUM_QUESTIONS', $item->questions);?></li>
									<li class="no-pad-left"><i class="fa fa-comments-o"></i> <?php echo JText::sprintf('COM_COMMUNITYANSWERS_NUM_ANSWERS', $item->answers);?></li>
									<li class="no-pad-left"><i class="fa fa-thumbs-o-up"></i> <?php echo JText::sprintf('COM_COMMUNITYANSWERS_NUM_BEST_ANSWERS', $item->best_answers);?></li>
								</ul>
							</div>
						</div>
						
					</div>
				</li>
				<?php
				}
			} 
			else
			{ 
			?>
			<li class="list-group-item">
				<div class="alert alert-info"><?php echo JText::_('MSG_NO_RESULTS'); ?></div>
			</li>
			<?php 
			}
			?>
		</ul>
	</div>
</div>