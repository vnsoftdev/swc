<?php
/**
 * @version		$Id: view.html.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
jimport ( 'joomla.application.component.view' );

class CommunityAnswersViewUsers extends JViewLegacy {
	
	protected $params;
	
	function display($tpl = null) {
		
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$active = $app->getMenu()->getActive();
		$pathway = $app->getPathway();
		
		/********************************** PARAMS *****************************/
		$appparams = JComponentHelper::getParams(A_APP_NAME);
		$menuParams = new JRegistry;
		
		if ($active) {
		
			$menuParams->loadString($active->params);
		}
		
		$this->params = clone $menuParams;
		$this->params->merge($appparams);
		/********************************** PARAMS *****************************/

		$limit = $this->params->get('list_length', $app->getCfg('list_limit', 20));
		$limitstart = $app->input->getInt('start', 0);
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		
		$itemid = CJFunctions::get_active_menu_id();
		$user = JFactory::getUser();
		
		switch($this->action){
	
			case 'leaderboard':
				
				$return = $model->get_users($limitstart, $limit, $this->params);
				
				$this->assignRef('items', $return['users']);
				$this->assignRef('pagination', $return['pagination']);
				
				$this->assign('task', null);
				$this->assign('brand', JText::_('COM_COMMUNITYANSWERS_LEADERBOARD'));
				$this->assign('brand_url', JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=leaderboard'.$itemid));
				$this->assign('page_url', 'index.php?option='.A_APP_NAME.'&view=answers&task=leaderboard'.$itemid);
				
				break;
		}
		
		$pathway->addItem($this->brand);
				
		parent::display($tpl);
	}
}