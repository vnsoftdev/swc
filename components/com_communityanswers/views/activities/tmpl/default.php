<?php
/**
 * @version		$Id: default.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Crosswords
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die;
$page_id = 304;
$last_notif_date = JFactory::getApplication()->getUserState('answers_last_viewed');
?>

<div id="cj-wrapper" class="cj-wrapper-main">
	<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'header.php';?>
	
	<div class="panel panel-default">
		<div class="panel-heading"><?php echo JText::_('LBL_NOTIFICATIONS');?></div>
		<ul class="list-group">
			<?php if(!empty($this->items) && is_array($this->items)):?>
			<?php foreach ($this->items as $item):?>
			<li class="list-group-item">
				<div class="media<?php echo CJFunctions::compare_string_dates($item->created, $last_notif_date) > 0 ? ' alert alert-info' : '';?>">
					<i class="<?php echo AnswersHelper::get_activity_icon($item->activity_type);?> fa-lg pull-left"></i>
					<div class="media-body">
						<strong class="media-heading"><?php echo AnswersHelper::get_activity_title($item);?></strong>
						<div class="muted margin-bottom-10"><?php echo strip_tags($item->description);?></div>
						<div class="muted"><?php echo CJFunctions::get_formatted_date($item->created);?></div>
					</div>
				</div>
			</li>
			<?php endforeach;?>
			<?php else:?>
			<li class="list-group-item">
				<div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo JText::_('MSG_NO_RESULTS')?></div>
			</li>
			<?php endif;?>
		</ul>
		<div class="panel-footer">
			<?php 
						echo CJFunctions::get_pagination(
								$this->page_url, 
								$this->pagination->get('pages.start'), 
								$this->pagination->get('pages.current'), 
								$this->pagination->get('pages.total'),
								$this->pagination->get('limit'),
								true
							);
						?>
		</div>
	</div>
</div>