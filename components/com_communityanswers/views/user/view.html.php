<?php
/**
 * @version		$Id: view.html.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
jimport ( 'joomla.application.component.view' );

class CommunityAnswersViewUser extends JViewLegacy {
	
	protected $params;
	
	function display($tpl = null) {
		
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$active = $app->getMenu()->getActive();
		$pathway = $app->getPathway();
		
		/********************************** PARAMS *****************************/
		$appparams = JComponentHelper::getParams(A_APP_NAME);
		$menuParams = new JRegistry;
		
		if ($active) {
		
			$menuParams->loadString($active->params);
		}
		
		$this->params = clone $menuParams;
		$this->params->merge($appparams);
		/********************************** PARAMS *****************************/

		$limit = $this->params->get('list_length', $app->getCfg('list_limit', 20));
		$limitstart = $app->input->getInt('start', 0);
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);
		
		$itemid = CJFunctions::get_active_menu_id();
		$user = JFactory::getUser();
		
		switch($this->action){
	
			case 'questions':
				
				$options = array('userid'=>$user->id, 'limit'=>$limit, 'limitstart'=>$limitstart, 'order'=>'a.created', 'order_dir'=>'desc');
				$return = $model->get_questions(1, $options, $this->params);
				
				$this->assignRef('items', $return['questions']);
				$this->assignRef('state', $return['state']);
				$this->assignRef('pagination', $return['pagination']);
				$this->assign('task', null);
				$this->assign('brand', JText::_('LBL_MY_QUESTIONS'));
				$this->assign('brand_url', JRoute::_('index.php?option='.A_APP_NAME.'&view=user&task=questions&id='.$user->id.':'.$user->username.$itemid));
				$this->assign('page_url', 'index.php?option='.A_APP_NAME.'&view=users&task=questions'.$itemid);
				
				break;
				
			case 'answers':
				
				$return  = $model->get_user_answers($user->id, $limitstart, $limit, $this->params);
				
				$this->assignRef('items', $return['answers']);
				$this->assignRef('pagination', $return['pagination']);
				$this->assign('task', null);
				$this->assign('brand', JText::_('LBL_MY_ANSWERS'));
				$this->assign('brand_url', JRoute::_('index.php?option='.A_APP_NAME.'&view=user&task=answers&id='.$user->id.':'.$user->username.$itemid));
				$this->assign('page_url', 'index.php?option='.A_APP_NAME.'&view=users&task=answers'.$itemid);
				
				$tpl ='answers';
				
				break;
				
			case 'subscriptions':

				$subscriptions = $model->get_user_subscriptions($user->id);
				$this->assignRef('items', $subscriptions);
				
				$this->assign('task', null);
				$this->assign('brand', JText::_('LBL_MY_SUBSCRIPTIONS'));
				$this->assign('brand_url', JRoute::_('index.php?option='.A_APP_NAME.'&view=user&task=subscriptions&id='.$user->id.':'.$user->username.$itemid));
				$this->assign('page_url', 'index.php?option='.A_APP_NAME.'&view=users&task=subscriptions'.$itemid);
				
				$tpl = 'subscriptions';
				
				break;
		}
		
		$pathway->addItem($this->brand);
				
		parent::display($tpl);
	}
}