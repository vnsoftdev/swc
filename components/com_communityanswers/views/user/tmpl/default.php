<?php
/**
 * @version		$Id: default.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

$page_id = 301;
$i = 1;
?>
<div id="cj-wrapper" class="cj-wrapper-main">
	
	<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'header.php';?>
	
	<div class="container-fluid question-wrapper">
		<div class="row-fluid">
			<div class="span12">
				<?php if(!empty($this->items)):?>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th><?php echo JText::_('LBL_QUESTION');?></th>
							<th><?php echo JText::_('LBL_ANSWERS');?></th>
							<th><?php echo JText::_('LBL_HITS');?></th>
							<th><?php echo JText::_('LBL_DATE');?></th>
							<th><?php echo JText::_('LBL_STATUS');?></th>
						</tr>
					</thead>
					<tbody>
						<?php foreach ($this->items as $item):?>
						<tr>
							<td><?php echo $this->pagination->get('limitstart') + ($i++);?></td>
							<td>
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$item->id.':'.$item->alias.$answers_itemid)?>">
									<?php echo $this->escape($item->title);?>
								</a>
							</td>
							<td><?php echo $item->answers;?></td>
							<td><?php echo $item->hits;?></td>
							<td nowrap="nowrap"><?php echo CJFunctions::get_localized_date($item->created);?></td>
							<td align="center" class="center">
								<button class="btn btn-mini <?php echo $item->published == 1 ? 'btn-success' : 'btn-danger'?> tooltip-hover" 
									title="<?php echo $item->published == 1 ? JText::_('LBL_PUBLISHED') : JText::_('LBL_UNPUBLISHED');?>">
									<i class="<?php echo $item->published == 1 ? 'fa fa-check-circle-o' : 'fa fa-ban';?>"></i>
								</button>
							</td>
						</tr>
						<?php endforeach;?>
					</tbody>
					<tfoot>
						<tr>
							<td colspan="6">
								<?php 
								echo CJFunctions::get_pagination(
										$this->page_url, 
										$this->pagination->get('pages.start'), 
										$this->pagination->get('pages.current'), 
										$this->pagination->get('pages.total'),
										JFactory::getApplication()->getCfg('list_limit', 20),
										true
									);
								?>
							</td>
						</tr>
					</tfoot>
				</table>
				<?php else:?>
				<div class="alert alert-info"><?php echo JText::_('MSG_NO_RESULTS'); ?></div>
				<?php endif;?>
			</div>
		</div>
	</div>
</div>