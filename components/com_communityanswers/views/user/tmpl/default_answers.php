<?php
/**
 * @version		$Id: default.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

$page_id = 302;
?>
<div id="cj-wrapper" class="cj-wrapper-main">
	
	<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'header.php';?>
	
	<div class="container-fluid question-wrapper">
		<div class="row-fluid">
			<div class="span12">
				<?php if(!empty($this->items)):?>
				<?php foreach($this->items as $item):?>
				<h3 class="page-header no-margin-bottom">
					<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$item->question_id.':'.$item->alias.$answers_itemid)?>">
						<?php echo $this->escape($item->title);?>
					</a>
				</h3>
				<div><?php echo CJFunctions::process_html($item->description, ($this->params->get('default_editor', 'bbcode') == 'bbcode'), false);?></div>
				<p class="muted"><?php echo JText::sprintf(
												'LBL_QUESTION_META', 
												$this->escape($item->category), 
												$this->escape($item->username), 
												CJFunctions::get_formatted_date($item->created),
												$item->hits)?></p>
				<?php endforeach;?>
				<?php else:?>
				<div class="alert alert-info"><?php echo JText::_('MSG_NO_RESULTS')?></div>
				<?php endif;?>
				
				<?php 
				echo CJFunctions::get_pagination(
						$this->page_url, 
						$this->pagination->get('pages.start'), 
						$this->pagination->get('pages.current'), 
						$this->pagination->get('pages.total'),
						JFactory::getApplication()->getCfg('list_limit', 20),
						true
					);
				?>
			</div>
		</div>
	</div>
</div>