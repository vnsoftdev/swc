<?php
/**
 * @version		$Id: default.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

$page_id = 100;
CJFunctions::load_jquery(array('libs'=>array('form', 'colorbox', 'social', 'validate')));

$user_name = $this->params->get('user_display_name', 'name');
$user_avatar = $this->params->get('user_avatar', 'none');

$user_profile_link = $this->item->created_by > 0 
	? CJFunctions::get_user_profile_link($user_avatar, $this->item->created_by, $this->escape($this->item->username))
	: $this->escape($this->item->username);
$formatted_date = CJFunctions::get_formatted_date($this->item->created);

if($this->params->get('enable_sharing_tools', 1) == 1){
	
	JFactory::getDocument()->addScript('//s7.addthis.com/js/300/addthis_widget.js');
}
?>
<div id="cj-wrapper" class="cj-wrapper-main">
	
	<?php 
	include_once JPATH_COMPONENT.DS.'helpers'.DS.'header.php';
	
	// include these after header only
	$editor = $user->authorise('answers.wysiwyg', A_APP_NAME) ? $this->params->get('default_editor', 'bbcode') : 'none';
	$category_name = JHtml::link(
			JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&id='.$this->item->catid.':'.$this->item->category_alias.$itemid),
			$this->escape($this->item->category_title));
	$question_url = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$this->item->id.':'.$this->item->alias.$itemid, true, -1);
	?>

	<div class="container-fluid question-wrapper">
	
		<div class="row-fluid">
			<div class="span12">
			
				<?php echo CJFunctions::load_module_position('answers-view-above-ask-form');?>
				
				<?php if($user->authorise('answers.ask', A_APP_NAME)):?>
				<div class="toggle-element">
					<div class="panel panel-default">
						<div class="panel-heading">
								<a class="panel-title cj-accordion-toggle" href="#askform" style="line-height: 20px;">
									<?php echo JText::_('TXT_HAVE_A_QUESTION_QUERY')?> <small><i class="fa fa-chevron-circle-down"></i></small>
								</a>
						</div>
						<div id="askform" class="text-center panel-body hide">
							<form action="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=ask'.$itemid);?>" method="post" class="no-margin-bottom">
								<div class="input-append center">
									<input type="text" name="input-ask" placeholder="<?php echo JText::_('LBL_ASK');?>">
									<button type="submit" class="btn"><?php echo JText::_('LBL_ASK_QUESTION');?></button>
								</div>
								<div class="center">
									<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=search'.$itemid)?>" class="muted">
										<?php echo JText::_('LBL_TRY_ADVANCED_SEARCH');?>
									</a>
								</div>
							</form>
						</div>
					</div>
				</div>
				<?php endif;?>
			
				<div class="reply-outer-wrapper panel panel-default">
					<div class="panel-body">
						<div class="media" style="overflow: visible;">
							<div class="pull-left margin-right-10 avatar hidden-phone">
								<?php echo CJFunctions::get_user_avatar(
										$this->params->get('user_avatar', 'none'), 
										$this->item->created_by, 
										$user_name,
										64,
										$this->item->email,
										array('class'=>'thumbnail'),
										array('class'=>'media-object'));?>
							</div>
							<div class="media-body" style="overflow: visible;">
								<h1 class="media-heading no-margin-top"><?php echo $this->escape($this->item->title);?></h1>
								<div class="description">
									<?php echo CJFunctions::process_html(
											$this->item->description, 
											$this->params->get('default_editor') == 'bbcode', 
											$this->params->get('process_content_plugins', false) == '1');?>
								</div>
								<div class="muted">
									<small><?php echo JText::sprintf('LBL_QUESTION_META', $category_name, $user_profile_link, $formatted_date, $this->item->hits);?></small>
								</div>
								<div class="tags margin-top-10">
									<?php foreach($this->item->tags as $tag):?>
									<a title="<?php echo JText::sprintf('LBL_TAGGED_QUESTIONS', $this->escape($tag->tag_text)).' - '.$this->escape($tag->description);?>" class="tooltip-hover" 
										href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=tag&id='.$tag->tag_id.':'.$tag->alias.$itemid);?>">
										<span class="label"><?php echo $this->escape($tag->tag_text);?></span>
									</a>
									<?php endforeach;?>
								</div>
						
								<?php if(!empty($this->item->attachments)):?>
								<div class="margin-top-20 well well-transparent well-small">
									<div class="page-header no-margin-top margin-bottom-10"><?php echo JText::_('LBL_ATTACHMENTS');?></div>
									<ul class="unstyled attachments-list clearfix">
										<?php foreach($this->item->attachments as $i=>$attachment):?>
										<?php if(in_array(strtolower(JFile::getExt($attachment->attachment)), array('jpg', 'gif', 'png', 'bmp', 'jpeg'))):?>
										<?php if($this->params->get('image_thumbnails', 0) == 0):?>
										<li>
											<a 
												href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=download&id='.$attachment->id.$itemid);?>"
												title="<?php echo JText::sprintf('TXT_DOWNLOADED_N_TIMES', $attachment->downloads);?>"
												class="gallery tooltip-hover"
												rel="nofollow">
												<span class="fa fa-picture-o"></span> <?php echo JText::sprintf('LBL_ATTACHMENT_NAME', $i + 1)?> 
												<span class="muted">(<?php echo $attachment->downloads;?>)</span>
											</a>
										</li>
										<?php endif;?>
										<?php else:?>
										<li>
											<a 
												href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=download&id='.$attachment->id.$itemid);?>" 
												title="<?php echo JText::sprintf('TXT_DOWNLOADED_N_TIMES', $attachment->downloads);?>"
												class="tooltip-hover"
												rel="nofollow">
												<span class="fa fa-download"></span> <?php echo JText::sprintf('LBL_ATTACHMENT_NAME', $i + 1)?>
												<span class="muted">(<?php echo $attachment->downloads;?>)</span>
											</a>
										</li>
										<?php endif;?>
										<?php endforeach;?>
									</ul>
									
									<?php
									if($this->params->get('image_thumbnails', 0) == 1){
										echo '<div class="row-fluid">'; 
										foreach($this->item->attachments as $i=>$attachment){
											if(in_array(strtolower(JFile::getExt($attachment->attachment)), array('jpg', 'gif', 'png', 'bmp', 'jpeg'))){
												?>
												<div class="span2">
													<a 
														href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=download&id='.$attachment->id.$itemid);?>"
														title="<?php echo JText::sprintf('TXT_DOWNLOADED_N_TIMES', $attachment->downloads);?>"
														class="gallery thumbnail tooltip-hover"
														rel="nofollow">
														<img src="<?php echo CA_ATTACHMENTS_URI.$attachment->attachment?>">
													</a>
												</div>
												<?php
											}
										}
										echo '</div>';
									}
									?>
								</div>
								<?php endif;?>
								
								<div class="question-replies reply-inner-wrapper">
									<?php foreach ($this->item->replies as $reply):?>
									<div class="answer-reply">
										<?php echo 
											$reply->description.' - '.
											CJFunctions::get_user_profile_link($user_avatar, $reply->created_by, $reply->username).' '.
											CJFunctions::get_formatted_date($reply->created);?>
										<?php if($user->authorise('answers.manage')):?>
										<a class="muted btn-unpublish-reply" onclick="return false;" 
											href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=unpublish_reply&id='.$reply->id.$itemid);?>">
											&nbsp;&bull;&nbsp;<?php echo JText::_('LBL_UNPUBLISH');?>
										</a>
										<a class="muted btn-delete-reply" onclick="return false;" 
											href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=delete_reply&id='.$reply->id.$itemid);?>">
											&bull;&nbsp;<?php echo JText::_('LBL_DELETE');?>
										</a>
										<?php endif;?>
									</div>
									<?php endforeach;?>
								</div>
							
								<input type="hidden" value="<?php echo $this->item->id;?>" id="question_id">
							</div>
						</div>
					</div>
								
					<?php if($this->item->accepted_answer == 0 && $this->params->get('enable_bounties', 1) == '1'):?>
					<?php if(!$user->guest && ($user->id == $this->item->created_by || $user->authorise('answers.manage', A_APP_NAME))):?>
					<ul class="list-group">
						<li class="list-group-item">
							<div class="bounty-details">
								<?php if(empty($this->item->bounty)):?>
								<a href="#" onclick="return false;" id="btn_start_bounty"><?php echo JText::_('LBL_START_BOUNTY');?></a>
								<?php else:?>
								<div class="<?php echo $this->item->bounty->state == 1 ? ' text-success' : '';?>">
									<i class="fa fa-gift"></i> <?php echo AnswersHelper::get_bounty_message($this->item->bounty);?>
									
									<?php if($this->item->bounty->state == 5):?>
									<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=refund_bounty&id='.$this->item->bounty->id.$itemid)?>">
										<?php echo JText::_('LBL_REFUND_POINTS'). ' | ';?>
									</a>
									<?php endif;?>
									
									<?php if($this->item->bounty->state == 1 || $this->item->bounty->state == 5):?>
									<a href="#" onclick="return false;" id="btn_start_bounty"><?php echo JText::_('LBL_EDIT_BOUNTY');?></a>
									<?php endif;?>
									
								</div>
								<?php endif;?>
							</div>
						</li>
					</ul>
					<?php elseif(!empty($this->item->bounty) && $this->item->bounty->state == 1):?>
					<ul class="list-group">
						<li class="list-group-item">
							<div class="bounty-details">
								<div class="<?php echo $this->item->bounty->state == 1 ? ' text-success' : '';?>">
									<i class="fa fa-gift"></i> <?php echo AnswersHelper::get_bounty_message($this->item->bounty);?>
								</div>
							</div>
						</li>
					</ul>
					<?php endif;?>
					<?php endif;?>
					
					<div class="question-controls panel-footer">
						<?php if((!$user->guest && $user->id == $this->item->created_by && $user->authorise('answers.edit', A_APP_NAME)) 
								|| $user->authorise('answers.manage', A_APP_NAME)):?>
						<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=edit&id='.$this->item->id.':'.$this->item->alias.$itemid);?>" 
							class="muted btn-edit-question"><?php echo JText::_('LBL_EDIT');?></a>
						<?php endif;?>
						
						<?php if($user->authorise('answers.subscrqn', A_APP_NAME)):?>
						&bull;
						<?php if($this->subscribed):?>
						<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=unsubscribe_qn&id='.$this->item->id.$itemid);?>" 
							class="muted btn-unsubscribe" onclick="return false;"><?php echo JText::_('LBL_UNSUBSCRIBE');?></a>
						<?php else:?>
						<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=subscribe_qn&id='.$this->item->id.$itemid);?>" 
							class="muted btn-subscribe" onclick="return false;"><?php echo JText::_('LBL_SUBSCRIBE');?></a>
						<?php endif;?>
						<?php endif;?>
						
						<?php if($user->authorise('answers.report', A_APP_NAME)):?>
						&bull;
						<a href="#" class="muted btn-report-question"><?php echo JText::_('LBL_REPORT');?></a>
						<?php endif;?>
						
						<?php if(!$user->guest):?>
						&bull;
						<a href="#" class="muted" id="btn-reply-qn" onclick="return false;"><?php echo JText::_('LBL_ASK_FOR_CLARIFICATION');?></a>
						<?php endif;?>
						
						<?php if($user->authorise('answers.manage', A_APP_NAME)):?>
						&bull;
						<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=unpublish_question&id='.$this->item->id.$itemid);?>" 
							class="muted btn-unpublish-question" onclick="return false;"><?php echo JText::_('LBL_UNPUBLISH');?></a>
						&bull;
						<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=delete_question&id='.$this->item->id.$itemid);?>" 
							class="muted btn-delete-question" onclick="return false;"><?php echo JText::_('LBL_DELETE');?></a>
						<?php endif;?>
					</div>
				</div>
				
				<?php if($this->params->get('enable_sharing_tools', 1) == 1):?>
				<div class="panel panel-default">
					<div class="panel-body">
						<p><?php echo JText::_('LBL_QUESTION_SOCIAL_SHARING_DESC');?></p>
						<div class="addthis_toolbox addthis_default_style ">
							<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
							<a class="addthis_button_tweet"></a>
							<a class="addthis_button_google_plusone" g:plusone:size="medium"></a>
							<a class="addthis_counter addthis_pill_style"></a>
						</div>
					</div>
				</div>
				<?php endif;?>
				
			</div>
		</div>
		
		<div class="row-fluid margin-top-20">
			<div class="span12">
			
				<?php if($this->item->authorize_answer == false):?>
				<div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo JText::_('MSG_ALREADY_ANSWERED');?></div>
				<?php endif;?>
	
				<?php echo CJFunctions::load_module_position('answers-view-below-question-details');?>
				
				<h3 class="page-header no-margin-top margin-bottom-10">
					<?php echo $this->item->answers == 1 ? JText::_('TXT_ONE_ANSWER') : JText::sprintf('TXT_N_ANSWERS', $this->item->answers);?>
				</h3>
				
				<?php if
				(
					(!$user->guest && $user->id == $this->item->created_by && $user->authorise('core.viewownqn', A_APP_NAME)) || 
					$user->authorise('answers.viewanswers', A_APP_NAME)
				):?>
				<div class="answers-wrapper">
					<?php if(!empty($this->item->responses)):?>
					<?php foreach($this->item->responses as $i=>$answer):?>
					<div class="reply-outer-wrapper answer-wrapper<?php echo $this->item->accepted_answer == $answer->id ? ' well accepted-answer' : '';?> clearfix" id="<?php echo $answer->id;?>">
						<?php if($this->item->accepted_answer == $answer->id):?>
						<h3 class="page-header margin-top-20 margin-bottom-10"><?php echo JText::_('LBL_ACCEPTED_ANSWER');?></h3>
						<?php endif;?>
						<div class="media">
							<div class="pull-left">
								<div class="thumbnail">
									<?php if($user->authorise('answers.rate', A_APP_NAME)):?>
									<div class="answer-rating-num center tooltip-hover" title="<?php echo JText::_('LBL_RATING');?>">
										<h2><?php echo $answer->thumbup - $answer->thumbdown;?></h2>
									</div>
									<div class="media-body thumbnail answer-rating-controls no-margin-bottom">
										<button class="btn tooltip-hover btn-dislike btn-mini" data-placement="right" title="<?php echo JText::_('LBL_UNLIKE')?>">
											<i class="fa fa-thumbs-o-down text-error"></i>
										</button>
										<button class="btn tooltip-hover btn-like btn-mini" data-placement="right" title="<?php echo JText::_('LBL_LIKE')?>">
											<i class="fa fa-thumbs-o-up text-success"></i>
										</button>
									</div>
									<?php else:?>
									<div class="answer-rating-num-readonly center tooltip-hover" title="<?php echo JText::_('LBL_RATING');?>">
										<h2><?php echo $answer->thumbup - $answer->thumbdown;?></h2>
									</div>
									<?php endif;?>
								</div>
								<?php if(!empty($this->item->bounty) && $this->item->accepted_answer == $answer->id):?>
								<div class="text-center margin-top-10">
									<div class="label label-success tooltip-hover" title="<?php echo JText::_('MSG_BOUNTY_WON')?>">
										+<?php echo $this->item->bounty->bounty_points?>
									</div>
								</div>
								<?php endif;?>
							</div>
							<div class="media-body">
								<div class="answer-content">
									<?php echo CJFunctions::process_html(
										$answer->description, 
										$this->params->get('default_editor') == 'bbcode', 
										$this->params->get('process_content_plugins', false) == '1
										');?>
								</div>
	
								<div class="answer-meta margin-top-20">
									<div class="media pull-right" style="min-width: 150px;">
										<?php if($user_avatar != 'none'):?>
										<div class="pull-left margin-right-10 hidden-phone">
											<?php echo CJFunctions::get_user_avatar($user_avatar, $answer->created_by, $user_name, 36, $answer->email, array());?>
										</div>
										<?php endif;?>
										<div class="media-body">
											<div class="media-heading">
												<?php echo CJFunctions::get_user_profile_link($user_avatar, $answer->created_by, $answer->username);?>
											</div>
											<div class="muted"><small><?php echo CJFunctions::get_formatted_date($answer->created);?></small></div>
										</div>
									</div>
									
									<div class="clearfix">
										<div>
											<?php if(!$user->guest):?>
											&bull;
											<a href="#" class="muted" id="btn-reply-answer" onclick="return false;"><?php echo JText::_('LBL_REPLY');?></a>
											
											<?php if(($user->id == $answer->created_by && $user->authorise('answers.edit', A_APP_NAME)) || $user->authorise('answers.manage', A_APP_NAME)):?>
											&bull;
											<a href="#" class="muted btn-edit-answer" onclick="return false;"><?php echo JText::_('LBL_EDIT');?></a>
											<?php endif;?>
											
											<?php endif;?>
											
											<?php if($user->authorise('answers.report', A_APP_NAME)):?>
											&bull;
											<a href="#" class="muted btn-report-answer" onclick="return false;"><?php echo JText::_('LBL_REPORT');?></a>
											<?php endif;?>
											
											<?php if($user->authorise('answers.manage', A_APP_NAME)):?>
											&bull;
											<a href="#" class="muted btn-unpublish-answer" onclick="return false;"><?php echo JText::_('LBL_UNPUBLISH');?></a>
											&bull;
											<a href="#" class="muted btn-delete-answer" onclick="return false;"><?php echo JText::_('LBL_DELETE');?></a>
											<?php endif;?>
										</div>
										<?php if(!$user->guest && ($user->authorise('answers.manage', A_APP_NAME) || 
												($answer->created_by != $this->item->created_by && $this->item->created_by == $user->id))):?>
										<div class="margin-top-20">
											<?php if($this->item->accepted_answer <= 0):?>
											<button type="button" class="btn btn-mini btn-invert btn-accept-answer"><?php echo JText::_('LBL_ACCEPT_ANSWER');?></button>
											<?php elseif($answer->id == $this->item->accepted_answer):?>
											<button type="button" class="btn btn-mini btn-invert btn-unaccept-answer"><?php echo JText::_('LBL_MARK_IT_OPEN');?></button>
											<?php endif;?>
										</div>
										<?php endif;?>
									</div>	
								</div>
								
								<div class="answer-replies reply-inner-wrapper">
									<?php foreach ($answer->replies as $reply):?>
									<div class="answer-reply">
										<?php echo 
											$reply->description.' - '.
											CJFunctions::get_user_profile_link($user_avatar, $reply->created_by, $reply->username).' '.
											CJFunctions::get_formatted_date($reply->created);?>
										<?php if($user->authorise('answers.manage')):?>
										<a class="muted btn-unpublish-reply" onclick="return false;" 
											href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=unpublish_reply&id='.$reply->id.$itemid);?>">
											&nbsp;&bull;&nbsp;<?php echo JText::_('LBL_UNPUBLISH');?>
										</a>
										<a class="muted btn-delete-reply" onclick="return false;" 
											href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=delete_reply&id='.$reply->id.$itemid);?>">
											&bull;&nbsp;<?php echo JText::_('LBL_DELETE');?>
										</a>
										<?php endif;?>
									</div>
									<?php endforeach;?>
								</div>
								
								<?php if($this->params->get('enable_references') == '1' && !empty($answer->source)):?>
								<div class="references">
									<h4 class="page-header no-margin-bottom"><?php echo JText::_('LBL_REFERENCES');?></h4>
									<small><?php echo AnswersHelper::get_sanitize_urls($answer->source);?></small>
								</div>
								<?php endif;?>
						
								<?php if(!empty($answer->attachments)):?>
								<div class="margin-top-20 well well-small well-transparent">
									<div class="page-header no-margin-top margin-bottom-10"><?php echo JText::_('LBL_ATTACHMENTS');?></div>
									<ul class="unstyled attachments-list clearfix">
										<?php foreach($answer->attachments as $i=>$attachment):?>
										<?php if(in_array(strtolower(JFile::getExt($attachment->attachment)), array('jpg', 'gif', 'png', 'bmp', 'jpeg'))):?>
										<?php if($this->params->get('image_thumbnails', 0) == 0):?>
										<li>
											<a 
												href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=download&id='.$attachment->id.$itemid);?>"
												title="<?php echo JText::sprintf('TXT_DOWNLOADED_N_TIMES', $attachment->downloads);?>"
												class="gallery tooltip-hover"
												rel="nofollow">
												<span class="fa fa-picture-o"></span> <?php echo JText::sprintf('LBL_ATTACHMENT_NAME', $i + 1)?> 
												<span class="muted">(<?php echo $attachment->downloads;?>)</span>
											</a>
										</li>
										<?php endif;?>
										<?php else:?>
										<li>
											<a 
												href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=download&id='.$attachment->id.$itemid);?>" 
												title="<?php echo JText::sprintf('TXT_DOWNLOADED_N_TIMES', $attachment->downloads);?>"
												class="tooltip-hover"
												rel="nofollow">
												<span class="fa fa-download"></span> <?php echo JText::sprintf('LBL_ATTACHMENT_NAME', $i + 1)?>
												<span class="muted">(<?php echo $attachment->downloads;?>)</span>
											</a>
										</li>
										<?php endif;?>
										<?php endforeach;?>
									</ul>
							
									<?php
									if($this->params->get('image_thumbnails', 0) == 1){
										echo '<div class="row-fluid margin-top-10">'; 
										foreach($answer->attachments as $i=>$attachment){
											if(in_array(strtolower(JFile::getExt($attachment->attachment)), array('jpg', 'gif', 'png', 'bmp', 'jpeg'))){
												?>
												<div class="span2">
													<a 
														href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=download&id='.$attachment->id.$itemid);?>"
														title="<?php echo JText::sprintf('TXT_DOWNLOADED_N_TIMES', $attachment->downloads);?>"
														class="gallery thumbnail tooltip-hover"
														rel="nofollow">
														<img src="<?php echo CA_ATTACHMENTS_URI.$attachment->attachment?>">
													</a>
												</div>
												<?php
											}
										}
										echo '</div>';
									}
									?>
								</div>
								<?php endif;?>
								
							</div>
						</div>
						<input type="hidden" name="answer_id" value="<?php echo $answer->id?>">
					</div>
					
					<?php echo CJFunctions::load_module_position('answers-view-after-answer'.($i+1));?>
					
					<?php endforeach;?>
					
					<?php elseif($user->authorise('answers.answer', A_APP_NAME)):?>
					<div class="alert alert-info no-answer-alert"><i class="fa fa-info-circle"></i> <?php echo JText::_('TXT_NO_ANSWERS')?></div>
					<?php endif;?>
				</div>
				<?php else:?>
				<div class="answers-wrapper">
					<p><?php echo JText::_('LBL_ANSWERS_HIDDEN');?></p>
				</div>
				<?php endif;?>
			</div>
		</div>
				
		<div class="row-fluid">
			<div class="span12">
			
				<?php echo CJFunctions::load_module_position('answers-view-above-answer-form');?>
				
				<?php if( $this->item->authorize_answer && ($this->item->accepted_answer == 0 || $this->params->get('answering_closed_questions') == 1) &&
							($user->authorise('answers.answer', A_APP_NAME) || $user->authorise('answers.manage', A_APP_NAME))):?>
				<form class="answer-form" action="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=answer'.$itemid)?>">
					<h3 class="page-header margin-bottom-20"><?php echo JText::_('LBL_YOUR_ANSWER');?></h3>
					
					<?php if($user->guest):?>
					<div class="control-group">
						<label class="control-label" for="input-name"><?php echo JText::_('LBL_NAME');?><sup>*</sup></label>
						<div class="controls"><input type="text" name="username" id="input-name" placeholder="<?php echo JText::_('LBL_NAME');?>" class="required"></div>
					</div>
					<div class="control-group">
						<label class="control-label" for="input-email"><?php echo JText::_('LBL_EMAIL');?><sup>*</sup></label>
						<div class="controls"><input type="text" name="email" id="input-email" placeholder="<?php echo JText::_('LBL_EMAIL');?>" class="required"></div>
					</div>
					<?php endif;?>
					
					<div class="control-group">
						<label class="control-label" for="answer"><?php echo JText::_('LBL_ANSWER');?><sup>*</sup></label>
						<div class="controls"><?php echo CJFunctions::load_editor($editor, 'answer', 'answer', '', '5', '40', '100%', '200px', '', 'width: 99%;'); ?></div>
					</div>
					
					<?php if($this->params->get('enable_references', 0) == 1):?>
					<div class="control-group">
						<label class="control-label" for="references"><?php echo JText::_('LBL_REFERENCES');?><sup></sup></label>
						<div class="controls">
							<textarea id="references" name="references" rows="3" cols="40" style="width: 99%" placeholder="<?php echo JText::_('TXT_REFERENCES')?>"></textarea>
						</div>
					</div>
					<?php endif;?>
					
					<?php if($user->authorise('answers.attachments', A_APP_NAME)):?>
					<div class="well well-small">
						<input type="button" class="btn btn-info btn-mini btn-attach-file" value="<?php echo JText::_('LBL_ATTACH_FILES');?>"/>
						<ul id="attachments-uploaded"></ul>
					</div>
					<?php endif;?>
					
					<?php if($user->guest):?>
					<div class="margin-top-10 margin-bottom-10">
						<div id="dynamic_recaptcha_1"></div>
						<?php 
						JHtml::_('behavior.framework');
						JPluginHelper::importPlugin('captcha');
						$dispatcher = JDispatcher::getInstance();
						$dispatcher->trigger('onInit','dynamic_recaptcha_1');
						?>
					</div>
					<?php endif;?>
	
					<div class="actions">
						<button class="btn" type="reset"><?php echo JText::_('LBL_RESET');?></button>
						<button class="btn btn-primary btn-save-answer" type="button"><?php echo JText::_('LBL_SUBMIT_ANSWER');?></button>
					</div>
					
					<input type="hidden" name="question_id" value="<?php echo $this->item->id;?>"/>
					<input type="hidden" name="answer_id" value="0"/>
					<input type="hidden" name="attachments" value=""/>
				</form>
				<?php elseif($user->guest):?>
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i> <?php echo JText::_('MSG_REGISTER_LOGIN')?>&nbsp;
					<?php if($this->params->get('enable_login_form', '1') == '1'):?>
					<a href="#" data-toggle="modal" data-target="#calogin-modal" onclick="return false;"><?php echo JText::_('LBL_CLICK_HERE_TO_LOGIN');?></a>
					<?php endif;?>
				</div>
				<?php endif;?>
			</div>
		</div>
		
		<div class="row-fluid margin-top-20">
			<div class="span12">
				<div class="toggle-element" id="suggestions_group">
					
					<?php if($this->params->get('related_questions_count', 0) > 0 && !empty($this->related_questions)):?>
					<div class="well well-small well-transparent margin-bottom-10 no-pad-left no-pad-right">
						<a class="cj-accordion-toggle margin-left-10" href="#related_questions_content"><i class="fa fa-chevron-circle-down"></i> <?php echo JText::_('LBL_RELATED_QUESTIONS');?></a>
						<div id="related_questions_content" class="cj-accordion-body">
							<div class="accordion-inner suggestions">
								<table class="table table-striped table-hover">
									<tbody>
										<?php foreach($this->related_questions as $question):?>
										<tr>
											<td>
												<span class="label tooltip-hover" title="<?php echo JText::sprintf('TXT_ANSWERS', $question->answers)?>" data-placement="right">
													<?php echo $question->answers?>
												</span>&nbsp;
												<?php if($question->accepted_answer > 0):?>
												<span class="tooltip-hover" data-placement="right" title="<?php echo JText::_('LBL_QUESTION_IS_SOLVED')?>">
													<i class="fa fa-check-circle-o"></i>
												</span>
												<?php endif;?>
												<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$question->id.':'.$question->alias.$itemid)?>">
													<?php echo $this->escape($question->title);?>
												</a>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<?php endif;?>
					
					<?php if($this->params->get('category_questions_count', 0) > 0 && !empty($this->category_questions)):?>
					<div class="well well-small well-transparent margin-bottom-10 no-pad-left no-pad-right">
						<a class="cj-accordion-toggle margin-left-10" href="#category_questions_content"><i class="fa fa-chevron-circle-down"></i> <?php echo JText::_('LBL_CATEGORY_QUESTIONS');?></a>
						<div id="category_questions_content" class="accordion-body" style="display: none;">
							<div class="accordion-inner suggestions">
								<table class="table table-striped table-hover">
									<tbody>
										<?php foreach($this->category_questions as $question):?>
										<tr>
											<td>
												<span class="label tooltip-hover" title="<?php echo JText::sprintf('TXT_ANSWERS', $question->answers)?>" data-placement="right">
													<?php echo $question->answers?>
												</span>&nbsp;
												<?php if($question->accepted_answer > 0):?>
												<span class="tooltip-hover" data-placement="right" title="<?php echo JText::_('LBL_QUESTION_IS_SOLVED')?>">
													<i class="fa fa-check-circle-o"></i>
												</span>
												<?php endif;?>
												<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$question->id.':'.$question->alias.$itemid)?>">
													<?php echo $this->escape($question->title);?>
												</a>
											</td>
										</tr>
										<?php endforeach;?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<?php endif;?>
					
				</div>
			</div>
		</div>
	
		<?php /*********************** DO NOT EDIT ANYTHING BELOW UNLESS YOU KNOW WHAT YOU ARE DOING *****************************/?>
		<div style="display: none;">
			<div class="reply-form">
				<textarea name="reply" rows="4" cols="60" class="span6"></textarea>
				<div class="controls">
					<button class="btn btn-mini btn-cancel-reply"><?php echo JText::_('LBL_CANCEL');?></button>
					<button class="btn btn-mini btn-primary btn-submit-reply"><?php echo JText::_('LBL_SUBMIT');?></button>
				</div>
			</div>
			
			<div id="tpl-answer-reply">
				<?php echo ' - '.CJFunctions::get_user_profile_link($user_avatar, $user->id, $user->$user_name).' '.CJFunctions::get_formatted_date('now');?>
			</div>
			
			<div id="tpl-answer">
				<div class="media">
					<div class="pull-left thumbnail">
						<div class="thumbnail answer-rating-num center"><h2>0</h2></div>
						<?php if($user->authorise('answers.rate', A_APP_NAME)):?>
						<div class="media-body answer-rating-controls">
							<button class="btn tooltip-hover btn-dislike btn-mini" data-placement="left" title="<?php echo JText::_('LBL_UNLIKE')?>">
								<i class="fa fa-thumbs-o-down"></i>
							</button>
							<button class="btn tooltip-hover btn-like btn-mini" data-placement="right" title="<?php echo JText::_('LBL_LIKE')?>">
								<i class="fa fa-thumbs-o-up"></i>
							</button>
						</div>
						<?php endif;?>
					</div>
					<div class="media-body">
						<div id="tpl-answer-description"></div>
		
						<div class="answer-meta margin-top-20">
							<div class="media pull-right">
								<?php if($user_avatar != 'none'):?>
								<div class="pull-left margin-right-10">
								<?php echo CJFunctions::get_user_avatar($user_avatar, $user->id, $user_name, 48, $user->email, array('class'=>'pull-left'));?>
								</div>
								<?php endif;?>
								<div class="media-body">
									<div class="media-heading"><?php echo $this->escape($user->$user_name);?></div>
									<div class="muted"><?php echo CJFunctions::get_formatted_date('now');?></div>
								</div>
							</div>
							
							<div>
								<?php if(!$user->guest):?>
								<a href="#" class="muted btn-reply-answer">&bull;&nbsp;<?php echo JText::_('LBL_REPLY');?></a>
								<?php if($user->authorise('answers.edit', A_APP_NAME) || $user->authorise('answers.manage', A_APP_NAME)):?>
								<a href="#" class="muted btn-edit-answer">&bull;&nbsp;<?php echo JText::_('LBL_EDIT');?></a>
								<?php endif;?>
								<?php endif;?>
								
								<?php if($user->authorise('answers.report', A_APP_NAME)):?>
								<a href="#" class="muted btn-report-answer">&bull;&nbsp;<?php echo JText::_('LBL_REPORT');?></a>
								<?php endif;?>
								
								<?php if($user->authorise('answers.manage', A_APP_NAME)):?>
								<a href="#" class="muted btn-unpublish-answer">&bull;&nbsp;<?php echo JText::_('LBL_UNPUBLISH');?></a>
								<a href="#" class="muted btn-delete-answer">&bull;&nbsp;<?php echo JText::_('LBL_DELETE');?></a>
								<?php endif;?>
							</div>
							
							<input type="hidden" name="answer_id" value="0">
						</div>
						
						<div class="answer-replies"></div>
					</div>
				</div>
				<?php if($user->authorise('answers.attachments', A_APP_NAME)):?>
				<div class="alert alert-info margin-top-20">
					<a class="close" data-dismiss="alert" href="#">&times;</a>
					<i class="fa fa-info-circle"></i> <?php echo JText::_('TXT_ATTACHMENTS_DELAYED_SHOWING');?>
				</div>
				<?php endif;?>
			</div>
			
			<div id="home_page_url"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid);?></div>
			<div id="url_get_answer"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=get_answer&id='.$this->item->id.$itemid)?></div>
			<div id="url_submit_reply"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=save_reply&id='.$this->item->id.$itemid)?></div>
			<div id="url_submit_answer"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=save_answer&id='.$this->item->id.$itemid)?></div>
			<div id="url_accept_answer"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=accept_answer&id='.$this->item->id.$itemid)?></div>
			<div id="url_unaccept_answer"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=unaccept_answer&id='.$this->item->id.$itemid)?></div>
			<div id="url_unpublish_answer"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=unpublish_answer&id='.$this->item->id.$itemid)?></div>
			<div id="url_delete_answer"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=delete_answer&id='.$this->item->id.$itemid)?></div>
			<div id="url_update_answer"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=update_answer&id='.$this->item->id.$itemid)?></div>
			<div id="url_like_answer"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=like&id='.$this->item->id.$itemid)?></div>
			<div id="url_dislike_answer"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=dislike&id='.$this->item->id.$itemid)?></div>
			<div id="url-report-answer"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=report_answer&id='.$this->item->id.$itemid);?></div>
			<div id="url-report-question"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=report_question&id='.$this->item->id.$itemid);?></div>
			<div id="url-save-bounty"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=save_bounty&id='.$this->item->id.$itemid);?></div>
			
			<input type="hidden" value="view" id="cjanswers_page_id">
		</div>
		
		<div id="message-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 id="myModalLabel"><?php echo JText::_('LBL_ALERT');?></h3>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('LBL_CLOSE');?></button>
			</div>
		</div>
		
		<div id="confirm-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 id="confirmModalLabel"><?php echo JText::_('LBL_ALERT');?></h3>
			</div>
			<div class="modal-body"><?php echo JText::_('MSG_CONFIRM')?></div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('LBL_CLOSE');?></button>
				<button class="btn btn-primary btn-confirm" aria-hidden="true"><?php echo JText::_('LBL_CONFIRM');?></button>
			</div>
		</div>	
		
		<?php if( $user->authorise('answers.manage', A_APP_NAME) || 
					(($this->item->accepted_answer == 0 || $this->params->get('answering_closed_questions') == 1) && ($user->authorise('answers.answer', A_APP_NAME)))):?>
		<div id="edit-answer-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="editModalLabel" aria-hidden="true" style="min-width: 700px;">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 id="editModalLabel"><?php echo JText::_('LBL_EDIT');?></h3>
			</div>
			<div class="modal-body">
				<div class="error"></div>
				<?php echo CJFunctions::load_editor($editor, 'answer-edit', 'answer-edit', '', '8', '40', '100%', '300px', '', 'width: 99%;'); ?>
				
				<?php if($this->params->get('enable_references', 0) == 1):?>
				<div class="margin-top-10">
					<label for="references"><?php echo JText::_('LBL_REFERENCES');?>:</label>
					<textarea name="references" rows="3" cols="40" class="input-xxlarge" placeholder="<?php echo JText::_('TXT_REFERENCES')?>"></textarea>
				</div>
				<?php endif;?>
			</div>
			<div class="modal-footer">
				<button class="btn btn-cancel" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('LBL_CANCEL');?></button>
				<button class="btn btn-primary btn-save" aria-hidden="true"><?php echo JText::_('LBL_SAVE');?></button>
			</div>
		</div>
		<?php endif;?>
		
		<?php if($user->authorise('answers.report', A_APP_NAME)):?>
		<div id="report-item-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="reportModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 id="reportModalLabel"><?php echo JText::_('LBL_REPORT');?></h3>
			</div>
			<div class="modal-body">
				<form class="report-form" action="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid);?>" method="post">
					<?php if($user->guest):?>
					<div class="control-group">
						<label class="control-label" for="inputName"><?php echo JText::_('LBL_NAME');?></label>
						<div class="controls"><input type="text" name="name" id="inputName" placeholder="<?php echo JText::_('LBL_NAME');?>"></div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputEmail"><?php echo JText::_('LBL_EMAIL');?></label>
						<div class="controls"><input type="text" name="email" id="inputEmail" placeholder="<?php echo JText::_('LBL_EMAIL');?>"></div>
					</div>
					<?php endif;?>
					<div class="control-group">
						<label class="control-label" for="inputReason"><?php echo JText::_('LBL_REPORT_REASON');?></label>
						<div class="controls"><input type="text" name="reason" id="inputReason" placeholder="<?php echo JText::_('LBL_REPORT_REASON');?>"></div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputDescription"><?php echo JText::_('LBL_DESCRIPTION');?></label>
						<div class="controls">
							<textarea name="description" id="inputDescription" placeholder="<?php echo JText::_('LBL_DESCRIPTION');?>" class="span4" cols="40" rows="5"></textarea>
						</div>
					</div>
					<input name="answer_id" value="" type="hidden">
					<input name="question_id" value="<?php echo $this->item->id?>" type="hidden">
				</form>
			</div>
			<div class="modal-footer">
				<button class="btn btn-cancel" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('LBL_CANCEL');?></button>
				<button class="btn btn-primary btn-submit" aria-hidden="true"><?php echo JText::_('JSUBMIT');?></button>
			</div>
		</div>	
		<?php endif;?>
		
		<div id="calogin-modal" class="modal hide fade no-space-bottom" tabindex="-1" role="dialog" aria-labelledby="cjLoginModalLabel" aria-hidden="true">
			<form action="<?php echo JRoute::_('index.php', true, $this->params->get('usesecure')); ?>" method="post" id="login-form" class="form-horizontal">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3 id="cjLoginModalLabel"><?php echo JText::_('JLOGIN');?></h3>
				</div>
				<div class="modal-body">
					<div class="control-group">
						<label class="control-label" for="inputUsername"><?php echo JText::_('LBL_USERNAME');?></label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="fa fa-user"></i></span>
								<input type="text" name="username" id="inputUsername" placeholder="<?php echo JText::_('LBL_USERNAME');?>" />
							</div>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="inputUsername"><?php echo JText::_('LBL_PASSWORD');?></label>
						<div class="controls">
							<div class="input-prepend">
								<span class="add-on"><i class="fa fa-lock"></i></span>
								<input type="password" name="password" id="inputPassword" placeholder="<?php echo JText::_('LBL_PASSWORD');?>" />
							</div>
						</div>
					</div>
					<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
					<div class="control-group">
						<div class="controls">
							<label class="checkbox"><input type="checkbox"/> <?php echo JText::_('LBL_REMEMBER_ME');?></label>
						</div>
					</div>
					<?php endif; ?>
					
					<ul class="unstyled">
						<li><a href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>"><?php echo JText::_('LBL_FORGOT_USERNAME'); ?></a></li>
						<li><a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>"><?php echo JText::_('LBL_FORGOT_PASSWORD'); ?></a></li>
						<?php
						$usersConfig = JComponentHelper::getParams('com_users');
						if ($usersConfig->get('allowUserRegistration')) : ?>
						<li><a href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>"><?php echo JText::_('LBL_REGISTER'); ?></a></li>
						<?php endif; ?>
					</ul>

					<input type="hidden" name="option" value="com_users" />
					<input type="hidden" name="task" value="user.login" />
					<input type="hidden" name="return" value="<?php echo base64_encode(JUri::current()); ?>" />
					<?php echo JHtml::_('form.token'); ?>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn" data-dismiss="modal"><?php echo JText::_('JCANCEL');?></button>
					<button class="btn btn-primary" type="submit"><?php echo JText::_('JLOGIN');?></button>
				</div>
			</form>
		</div>
		
		<?php if(!$user->guest && $this->params->get('enable_bounties', 1) == '1'):?>
		<?php
		$available_points = CJFunctions::get_user_points($this->params->get('points_system', 'none'), $this->item->created_by);
		JHTML::_( 'behavior.calendar' ); 
		?>
		<div id="bounty-modal" class="modal hide fade no-space-bottom" tabindex="-1" role="dialog" aria-labelledby="cjBountyModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 id="cjBountyModalLabel"><?php echo JText::_('LBL_START_BOUNTY');?></h3>
			</div>
			<div class="modal-body">
				<div class="error"></div>
				
				<div class="boounty-help margin-bottom-20"><?php echo JText::_('TXT_BOUNTY_HELP');?></div>
				<label><?php echo JText::_('LBL_BOUNTY_POINTS');?>: (<?php echo JText::sprintf('LBL_AVAILABLE_POINTS', $available_points);?>)</label>
				<input type="text" name="bounty-points" placeholder="<?php echo JText::_('LBL_BOUNTY_POINTS');?>"
					min="<?php echo !empty($this->item->bounty) ? $this->item->bounty->bounty_points : 1;?>" class="input-medium required"
					value="<?php echo !empty($this->item->bounty) ? $this->item->bounty->bounty_points : '';?>">
					
				<label><?php echo JText::_('LBL_BOUNTY_EXPIRY_DATE');?>: <i class="tooltip-hover fa fa-info-circle" title="<?php echo JText::_('MSG_BOUNTY_EXPIRY_HELP');?>"></i></label>
				<?php echo JHtml::_('calendar', (!empty($this->item->bounty) ? $this->item->bounty->expiry_date : ''), 
						'bounty-expiry', 'bounty-expiry', '%Y-%m-%d %H:%M:%S', array('placeholder' => 'YYYY-MM-DD HH:mm:ss')); ?>
			</div>
			<div class="modal-footer">
				<button class="btn btn-cancel" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('LBL_CANCEL');?></button>
				<button class="btn btn-primary btn-save-bounty" aria-hidden="true"><?php echo JText::_('LBL_SAVE');?></button>
			</div>
		</div>
		<?php endif;?>
			
		<form id="attach-files-form" action="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=attach&id='.$this->item->id.$itemid)?>" 
			enctype="multipart/form-data" style="position: absolute; top:-1000px;">
			<input type="file" name="input-attachment">
		</form>
	
	</div>
</div>