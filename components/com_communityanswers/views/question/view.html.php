<?php
/**
 * @version		$Id: view.html.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
jimport ( 'joomla.application.component.view' );

class CommunityAnswersViewQuestion extends JViewLegacy {
	
	protected $params;
	protected $print;
	protected $state;
	
	function display($tpl = null) {
		
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$document = JFactory::getDocument();
		$user = JFactory::getUser();
		
		$pathway = $app->getPathway();
		$active = $app->getMenu()->getActive();
		$this->print = $app->input->getBool('print');
		$page_heading = '';
		$question = null;
		
		/********************************** PARAMS *****************************/
		$appparams = JComponentHelper::getParams(A_APP_NAME);
		$menuParams = new JRegistry;
		
		if ($active) {
		
			$menuParams->loadString($active->params);
		}
		
		$this->params = clone $menuParams;
		$this->params->merge($appparams);
		/********************************** PARAMS *****************************/
		
		$itemid = CJFunctions::get_active_menu_id();
		
		switch($this->action){
			
			case 'question_details':
				
				$id = $app->input->getInt('id', 0);
				if(!$id) return CJFunctions::throw_error(JText::_('JERROR_ALERTNOAUTH'), 403);
				
				$question = $model->get_question_details(
						$id, 
						array('get_tags'=>true, 'get_answers'=>true, 'trigger_hit'=>true, 'get_attachments'=>true, 'get_bounty'=>true), 
						$this->params);
				
				if(!$question) return CJFunctions::throw_error(JText::_('JERROR_ALERTNOAUTH'), 403);
				
				/************************ RELATED QUESTIONS *********************************/
				if($this->params->get('related_questions_count', 0) > 0 && !empty($question->title)){
					
					$options = array(
							'search_params'=>array('q'=>$question->title, 'qt'=>0, 'type'=>0, 'all'=>0),
							'limit'=>$this->params->get('related_questions_count'), 'limitstart'=>0, 'order'=>'a.answers', 'order_dir'=>'desc', 'exclude_ids'=>array($id));
					
					$related = $model->get_questions(7, $options, $this->params);
					$this->assignRef('related_questions', $related['questions']);
				}
				/************************ RELATED QUESTIONS *********************************/
				
				/************************ CATEGORY QUESTIONS ********************************/
				if($this->params->get('category_questions_count', 0) > 0){
						
					$options = array(
							'catid'=>$question->catid, 'limit'=>$this->params->get('category_questions_count'), 
							'limitstart'=>0, 'order'=>'a.created', 'order_dir'=>'desc');
					$catqns = $model->get_questions(1, $options, $this->params);
					$this->assignRef('category_questions', $catqns['questions']);
				}
				/************************ CATEGORY QUESTIONS ********************************/
				
				$subscribed = $model->check_subscription(array(1), $user->id, $question->id);
				$breadcrumbs = $model->get_breadcrumbs($question->catid);
				
				if(!empty($breadcrumbs)){
					
					foreach($breadcrumbs as $id=>$breadcrumb){
						
						if($breadcrumb['parent_id'] > 0){
							
							$pathway->addItem($breadcrumb['title'], JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&id='.$breadcrumb['id'].':'.$breadcrumb['alias'].$itemid));
						}
					}
				}
				
				$pathway->addItem($question->title);
				
				// process content plugins on question and aswer descriptions
				if($this->params->get('process_content_plugins', 0) == 1){
				
					$this->process_content_plugins($question);
				}
				
				$this->assignRef('item', $question);
				$this->assign('subscribed', $subscribed);
				$this->assign('brand', JText::_('LBL_HOME'));
				$this->assign('brand_url', 'index.php?option='.A_APP_NAME.'&view=answers'.$itemid);
				
				$page_heading = $this->escape($question->title);
				break;
				
			default:
				
				return CJFunctions::throw_error(JText::_('JERROR_ALERTNOAUTH'), 403);
				
		}

		// set browser title
		$title = $this->params->get('page_title', $app->getCfg('sitename'));
		
		if ($app->getCfg('sitename_pagetitles', 0) == 1) {
		
			$document->setTitle(JText::sprintf('JPAGETITLE', $title, $page_heading));
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
		
			$document->setTitle(JText::sprintf('JPAGETITLE', $page_heading, $title));
		} else {
				
			$document->setTitle($page_heading);
		}
		
		// set meta description
		$document->setDescription($question->description);
		$document->setMetaData('og:title', $page_heading, true);
		$document->setMetaData('og:description', $question->description, true);
		
		// set meta keywords
		$keywords = array();
		foreach($this->item->tags as $tag){
			
			$keywords[] = $tag->tag_text;
		}
		
		$title_words = explode(' ', $question->title);
		
		foreach($title_words as $i=>$keyword){
			
			if(strlen($keyword) < 3){
				
				unset($title_words[$i]);
			}
		}
		
		$keywords = array_merge($keywords, $title_words);
		$metakey = implode(',', $keywords);
		
		if ($this->params->get('menu-meta_keywords')){
		
			$document->setMetadata('keywords', $this->params->get('menu-meta_keywords').','.$metakey);
		} else {
			
			$document->setMetadata('keywords', $metakey);
		}
		
		// set robots
		if ($this->params->get('robots')){
		
			$document->setMetadata('robots', $this->params->get('robots'));
		}
		
		// set nofollow if it is print
		if ($this->print){
		
			$document->setMetaData('robots', 'noindex, nofollow');
		}
		
		parent::display($tpl);
	}
    
    private function process_content_plugins(&$question){
    	
    	$app = JFactory::getApplication();
		$params = $app->getParams('com_content');
		
		$item = new JObject();
		$id = 0;
		$item->text = $question->description;
		$item->title = $question->title;
		
		CJFunctions::trigger_event('content', 'onContentPrepare', array ('com_content.article', &$item, &$params, 0));
		$question->description = $item->text;
		
		if(!empty($question->responses)){
			
			foreach($question->responses as &$answer){
				
				$item->text = $answer->description;
				CJFunctions::trigger_event('content', 'onContentPrepare', array ('com_content.article', &$item, &$params, 0));
				$answer->description = $item->text;
			}
		}
    }
}