<?php
/**
 * @version		$Id: form_content.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
$user = JFactory::getUser();
?>
<form class="question-form" method="post" 
	action="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=save_question'.$itemid);?>">
	<h2 class="page-header"><?php echo JText::_('TXT_NEW_QUESTION');?></h2>
	<p><?php echo JText::_('TXT_ASK_HELP');?></p>
	
	<label><?php echo JText::_('TITLE_QUESTION_TITLE');?><sup>*</sup>:</label> 
	<input type="text" name="title" value="<?php echo $this->escape($this->question->title)?>" 
		class="input-xxlarge qn-title required" placeholder="<?php echo JText::_('LBL_ASK');?>">
	
	<?php if($user->authorise('answers.manage', A_APP_NAME)):?>
	<label><?php echo JText::_('LBL_ALIAS');?>:</label> 
	<input type="text" name="alias" value="<?php echo $this->escape($this->question->alias)?>" 
		class="input-xxlarge qn-alias" placeholder="<?php echo JText::_('LBL_ALIAS');?>">
	<?php endif;?>
		
	<label><?php echo JText::_('LBL_DESCRIPTION');?>:</label> 
	<?php echo CJFunctions::load_editor(
				$editor, 'description', 'description', $this->question->description, '5', '40', '99%', '200px', '', 'width: 99%;'); ?>
	
	<label><?php echo JText::_('LBL_CATEGORY');?>:</label>
	<select name="catid" size="1" class="required">
		<option><?php echo JText::_('TXT_SELECT_CATEGORY');?></option>
		<?php foreach($this->categories as $id=>$title):?>
		<option value="<?php echo $id;?>" <?php echo ($this->question->catid == $id)?'selected="selected"':''?>>
			<?php echo $this->escape($title);?>
		</option>
		<?php endforeach;?>
	</select>
	
	<?php if($user->guest):?>
	<label><?php echo JText::_('LBL_NAME');?><sup>*</sup>:</label>
	<input type="text" id="username" name="username" class="input-xlarge required" 
		value="<?php echo $this->question->username;?>" placeholder="<?php echo JText::_('TXT_ENTER_YOUR_NAME');?>">
	
	<label><?php echo JText::_('LBL_EMAIL');?><sup>*</sup>:</label>
	<input type="text" id="email" name="email" class="input-xlarge required" 
		value="<?php echo $this->question->email;?>" placeholder="<?php echo JText::_('TXT_ENTER_YOUR_EMAIL');?>">
	<?php endif;?>
	
	<label><?php echo JText::_('LBL_TAGS');?>:</label>
	<input type="text" size="40" id="input-tags" name="input-tags" data-provide="typeahead" 
		class="input-xlarge" autocomplete="off" placeholder="<?php echo JText::_('TXT_FLDHLP_TAGS');?>">
	<div class="question-tags">
		<ul class="clearfix unstyled">
			<?php foreach($this->question->tags as $tag):?>
			<?php if(!empty($tag)):?>
			<li class="label">
				<a href="#" class="btn-remove-tag" onclick="return false;"><i class="fa fa-remove"></i></a>&nbsp;
				<span class="tag-item"><?php echo $this->escape($tag)?></span>
			</li>
			<?php endif;?>
			<?php endforeach;?>
		</ul>
		<input type="hidden" name="tags" value="">
	</div>
	
	<?php if($user->authorise('answers.attachments', A_APP_NAME)):?>
	<div class="well well-small attachments">
		<div class="alert" style="display: none;">
			<button type="button" class="close" data-dismiss="alert">&times;</button><span class="alert-content"></span>
		</div>
		<div>
			<input type="button" class="btn btn-info btn-mini btn-attach-file" value="<?php echo JText::_('LBL_ATTACH_FILES');?>"/>
			<small class="muted"> ( <?php echo JText::sprintf('TXT_ALLOWED_EXTENSIONS', $this->params->get('allowed_attachment_types'));?> )</small>
		</div>
		
		<ul id="attachments-uploaded">
			<?php if(!empty($this->question->attachments)):?>
			<?php foreach($this->question->attachments as $attachment):?>
			<li>
				<?php echo $attachment->attachment;?>&nbsp;
				<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=detach&id='.$attachment->id.$itemid)?>" 
					onclick="return false;" title="<?php echo JText::_('LBL_DELETE');?>" class="tooltip-hover btn-remove-attachment">
					<i class="fa fa-remove"></i>&nbsp;
				</a>
			</li>
			<?php endforeach;?>
			<?php endif;?>
		</ul>
	</div>
	<?php endif;?>
	
	<?php if(!$user->guest && $this->params->get('enable_bounties', 1) == '1'):?>
	<?php
	$available_points = CJFunctions::get_user_points($this->params->get('points_system', 'none'), (!empty($this->question->created_by) ? $this->question->created_by : $user->id));
	?>
	<div class="well well-small bounty">
		<h3 class="page-header"><?php echo JText::_('LBL_START_BOUNTY');?></h3>
		<p><?php echo JText::_('TXT_BOUNTY_HELP');?></p>
		
		<label><?php echo JText::_('LBL_BOUNTY_POINTS');?>: (<?php echo JText::sprintf('LBL_AVAILABLE_POINTS', $available_points);?>)</label>
		<input type="text" name="bounty-points" min="<?php echo $this->question->bounty->bounty_points;?>" class="input-medium"
			placeholder="<?php echo JText::_('LBL_BOUNTY_POINTS');?>" value="<?php echo $this->question->bounty->bounty_points;?>">
			
		<label><?php echo JText::_('LBL_BOUNTY_EXPIRY_DATE');?>: <i class="tooltip-hover fa fa-info-circle" title="<?php echo JText::_('MSG_BOUNTY_EXPIRY_HELP');?>"></i></label>
		<?php echo JHtml::_('calendar', $this->question->bounty->expiry_date, 'bounty-expiry', 'bounty-expiry', '%Y-%m-%d %H:%M:%S', array('placeholder' => 'YYYY-MM-DD HH:mm:ss')); ?>
	</div>
	<?php endif;?>
	
	<?php if($user->guest):?>
	<div class="margin-top-10">
		<div id="dynamic_recaptcha_1"></div>
		<?php 
		JHtml::_('behavior.framework');
		JPluginHelper::importPlugin('captcha');
		$dispatcher = JDispatcher::getInstance();
		$dispatcher->trigger('onInit','dynamic_recaptcha_1');
		?>
	</div>
	<?php endif;?>
	
	<?php if($this->question->id == 0):?>
	<div class="margin-top-10">
		<label class="checkbox"><input name="notify_answers" type="checkbox" value="1" checked="checked"> <?php echo JText::_('LBL_NOTIFY_NEW_ANSWEERS');?></label>
	</div>
	<?php endif;?>

	<div class="margin-top-20">
		<a class="btn" href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid)?>">
			<?php echo JText::_('LBL_CANCEL');?>
		</a>
		<button type="button" class="btn btn-primary btn-submit-form">
			<?php echo $this->question->id > 0 ? JText::_('JSAVE') : JText::_('LBL_ASK_QUESTION');?>
		</button>
	</div>
	
	<input type="hidden" name="id" value="<?php echo $this->question->id?>">
	<input type="hidden" name="attachments" value="<?php $this->question->attachments?>">
	<input type="hidden" value="form" id="cjanswers_page_id">
</form>