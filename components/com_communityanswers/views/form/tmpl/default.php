<?php
/**
 * @version		$Id: default.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

$page_id = 210;
$user = JFactory::getUser();

CJFunctions::load_jquery(array('libs'=>array('form', 'validate')));
$editor = $user->authorise('answers.wysiwyg', A_APP_NAME) ? $this->params->get('default_editor', 'bbcode') : 'none';
?>
<div id="cj-wrapper" class="cj-wrapper-main">
	
	<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'header.php';?>
	
	<div class="container-fluid question-wrapper">
		<div class="row-fluid">
			<div class="span12">
			
				<?php if($this->params->get('related_questions_count', 0) > 0 && !empty($this->related_questions)):?>
				<p><?php echo JText::_('TXT_RELATED_QUESTIONS_FOUND')?></p>
				<div class="toggle-element">
					<div class="well well-small well-transparent margin-bottom-10 no-pad-left no-pad-right">
						<a class="cj-accordion-toggle margin-left-10" href="#related_questions_content"><?php echo JText::_('LBL_RELATED_QUESTIONS');?></a>
						<div id="related_questions_content" class="cj-accordion-body">
							<div class="accordion-inner suggestions">
								<?php include_once 'suggestions.php';?>
							</div>
						</div>
					</div>
					
					<div class="well well-small well-transparent">
						<a class="cj-accordion-toggle" href="#form_content"><?php echo JText::_('LBL_STILL_CONTINUE_TO_QUESTION_FORM');?></a>
						<div id="form_content" class="cj-accordion-body hide">
							<div class="accordion-inner">
								<?php include_once 'form_content.php';?>
							</div>
						</div>
					</div>
				</div>
				<?php else:?>
				<?php include_once 'form_content.php';?>
				<?php endif;?>
				
			</div>
		</div>
	
		<div style="display: none;">		
			<span id="url-get-tags"><?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=get_tags'.$itemid)?></span>
		</div>
		
		<div id="message-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h3 id="myModalLabel"><?php echo JText::_('LBL_ALERT');?></h3>
			</div>
			<div class="modal-body"></div>
			<div class="modal-footer">
				<button class="btn" data-dismiss="modal" aria-hidden="true"><?php echo JText::_('LBL_CLOSE');?></button>
			</div>
		</div>
		
		<form id="attach-files-form" action="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=attach')?>" enctype="multipart/form-data" style="position: absolute; top:-1000px;">
			<input type="file" name="input-attachment">
		</form>
	</div>
</div>