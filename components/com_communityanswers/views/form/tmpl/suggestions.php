<?php
/**
 * @version		$Id: suggestions.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
?>
<table class="table table-striped table-hover">
	<tbody>
		<?php foreach($this->related_questions as $question):?>
		<tr>
			<td>
				<span class="label tooltip-hover" title="<?php echo JText::sprintf('TXT_ANSWERS', $question->answers)?>" 
					data-placement="right"><?php echo $question->answers?></span>&nbsp;
				<?php if($question->accepted_answer > 0):?>
				<span class="tooltip-hover" data-placement="right" title="<?php echo JText::_('LBL_QUESTION_IS_SOLVED')?>"><i class="fa fa-check-circle-o"></i></span>
				<?php endif;?>
				<a target="_blank" href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$question->id.':'.$question->alias.$itemid)?>">
					<?php echo $this->escape($question->title);?>
				</a>
			</td>
		</tr>
		<?php endforeach;?>
	</tbody>
</table>
