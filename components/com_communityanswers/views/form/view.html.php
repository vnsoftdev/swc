<?php
/**
 * @version		$Id: view.html.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();
jimport ( 'joomla.application.component.view' );

class CommunityAnswersViewForm extends JViewLegacy {

	protected $params;
	protected $print;
	protected $state;
	
	function display($tpl = null) {
		
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$active = $app->getMenu()->getActive();
		$pathway = $app->getPathway();
		$document = JFactory::getDocument();
		
		/********************************** PARAMS *****************************/
		$appparams = JComponentHelper::getParams(A_APP_NAME);
		$menuParams = new JRegistry;
		
		if ($active) {
		
			$menuParams->loadString($active->params);
		}
		
		$this->params = clone $menuParams;
		$this->params->merge($appparams);
		/********************************** PARAMS *****************************/
		
		if(empty($this->question)){
			
			$id = $app->input->getInt('id', 0);
			$question = new stdClass();
			
			if($id > 0){
				
				$question = $model->get_question_details(
						$id, 
						array('get_tags'=>true, 'get_answers'=>false, 'trigger_hit'=>false, 'get_attachments'=>true, 'get_bounty'=>true), 
						$this->params);
				
				$tags = array();
				
				if(!empty($question->tags)){
					
					foreach($question->tags as $tag){
					
						$tags[] = $tag->tag_text;
					}
				}
				
				$question->tags = $tags;
			} else {
				
				$question->catid = $app->input->getInt('catid', 0);
				$question->title = $app->input->post->getString('input-ask', '');
				$question->id = $question->published = 0;
				$question->alias = $question->description = $question->attachments = $question->username = $question->email = '';
				$question->tags = array();
				$question->created_by = JFactory::getUser()->id;
			}
			
			if(empty($question->bounty)){
				
				$question->bounty = new stdClass();
				$question->bounty->bounty_points = 0;
				$question->bounty->expiry_date = null;
			}
			
			/************************ RELATED QUESTIONS *********************************/
			if($question->id == 0 && !empty($question->title) && $this->params->get('form_related_count', 10) > 0){
				
				$options = array(
						'search_params'=>array('q'=>$question->title, 'qt'=>0, 'type'=>0, 'all'=>0),
						'limit'=>$this->params->get('form_related_count', 10), 'limitstart'=>0, 'order'=>'a.answers', 'order_dir'=>'desc');
					
				$return = $model->get_questions(7, $options, $this->params);
				$this->assignRef('related_questions', $return['questions']);
			}
			/************************ RELATED QUESTIONS *********************************/
			
			$this->assignRef( 'question', $question );
		} else {
				
			// validation failed and showing the form again
			$this->question->tags = explode(',', $this->question->tags);
			$this->question->attachments = $model->populate_attachments($this->question);
		}

		$categories = $model->get_categories();
		$tree = AnswersHelper::get_category_tree($categories, 'permission_ask');
		$this->assignRef( 'categories', $tree );
		
		$itemid = CJFunctions::get_active_menu_id();
		$this->assign('brand', JText::_('LBL_HOME'));
		$this->assign('brand_url', JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid));
		
		$page_heading = JText::_('LBL_ASK_QUESTION');
		
		// set browser title
		$this->params->set('page_heading', $this->params->get('page_heading', $page_heading));
		
		// add to pathway
		$pathway->addItem($page_heading);
		
		$title = $this->params->get('page_title', $app->getCfg('sitename'));
		
		if ($app->getCfg('sitename_pagetitles', 0) == 1) {
		
			$document->setTitle(JText::sprintf('JPAGETITLE', $title, $page_heading));
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
		
			$document->setTitle(JText::sprintf('JPAGETITLE', $page_heading, $title));
		} else {

			$document->setTitle($page_heading);
		}
		
		// set meta description
		if ($this->params->get('menu-meta_description')){
		
			$document->setDescription($this->params->get('menu-meta_description'));
		}
		
		// set meta keywords
		if ($this->params->get('menu-meta_keywords')){
		
			$document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}
		
		// set robots
		if ($this->params->get('robots')){
		
			$document->setMetadata('robots', $this->params->get('robots'));
		}
		
		// set nofollow if it is print
		if ($this->print){
		
			$document->setMetaData('robots', 'noindex, nofollow');
		}
		
		parent::display($tpl);
	}
}