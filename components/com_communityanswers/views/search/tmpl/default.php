<?php
/**
 * @version		$Id: default.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

$page_id = 108;
?>
<div id="cj-wrapper" class="cj-wrapper-main">

	<?php include_once JPATH_COMPONENT.DS.'helpers'.DS.'header.php';?>
	
	<div class="container-fluid question-wrapper">
		<div class="row-fluid">
			<div class="span12">
			
				<h2 class="page-header no-margin-top margin-bottom-10"><?php echo JText::_('LBL_ADVANCED_SEARCH');?></h2>
				
				<form action="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=search'.$itemid);?>" method="get">
					<input name="option" type="hidden" value="<?php echo A_APP_NAME;?>" />
					<input name="view" type="hidden" value="answers" />
					<input name="task" type="hidden" value="search" />

					<div class="well">
						<div class="row-fluid">
							<div class="span6">
								 <fieldset class="no-margin-top">
								 	<legend><?php echo JText::_('LBL_KEYWORDS');?></legend>
								 	<input name="q" type="text" class="span9" placeholder="<?php echo JText::_('LBL_KEYWORDS');?>">
								 	
								 	<select name="qt" class="span9">
								 		<option value="0"><?php echo JText::_('LBL_SEARCH_TITLE_ONLY');?></option>
								 		<option value="1"><?php echo JText::_('LBL_SEARCH_TITLE_DESC');?></option>
								 	</select>
								 	
								 	<label class="checkbox"><input type="checkbox" value="1" name="all"> <?php echo JText::_('LBL_SEARCH_ALL_WORDS');?></label>
								 </fieldset>
							</div>
							
							<div class="span6">
								<fieldset class="no-margin-top">
									<legend><?php echo JText::_('LBL_USERNAME')?></legend>
									<input name="u" type="text" class="span9" placeholder="<?php echo JText::_('LBL_USERNAME');?>">
									<label class="checkbox"><input name="m" type="checkbox" value="1"> <?php echo JText::_('LBL_EXACT_MATCH');?></label>
								</fieldset>
							</div>
						</div>
					</div>
					
					<div class="well">
						<div class="row-fluid">
							<div class="span6">
								<fieldset class="no-margin-top">
									<legend><?php echo JText::_('LBL_SEARCH_OPTIONS');?></legend>
									<label><?php echo JText::_('LBL_SEARCH_IN');?></label>
									<select name="type" size="1">
										<option value="0"><?php echo JText::_('LBL_ALL_QUESTIONS')?></option>
										<option value="1"><?php echo JText::_('LBL_OPEN_QUESTIONS')?></option>
										<option value="2"><?php echo JText::_('LBL_RESOLVED_QUESTIONS')?></option>
									</select>
									<label><?php echo JText::_('LBL_SEARCH_ORDER_BY');?></label>
									<select name="ord" size="1">
										<option value="0"><?php echo JText::_('LBL_DATE');?></option>
										<option value="1"><?php echo JText::_('LBL_ANSWERS');?></option>
										<option value="2"><?php echo JText::_('LBL_HITS');?></option>
										<option value="3"><?php echo JText::_('LBL_CATEGORY');?></option>
									</select>
									<label><?php echo JText::_('LBL_SEARCH_ORDER');?></label>
									<select name="dir" size="1">
										<option value="0"><?php echo JText::_('LBL_ASCENDING');?></option>
										<option value="1"><?php echo JText::_('LBL_DESCENDING');?></option>
									</select>
								</fieldset>
							</div>
							<div class="span6">
								<fieldset class="no-margin-top">
									<legend><?php echo JText::_('LBL_CATEGORIES')?></legend>
									<select name="cid[]" multiple="multiple" size="10">
										<?php foreach ($this->categories as $id=>$title):?>
										<option value="<?php echo $id;?>"><?php echo $this->escape($title);?></option>
										<?php endforeach;?>
									</select>
								</fieldset>
							</div>
						</div>
					</div>
					
					<div class="well">
						<div class="row-fluid">
							<div class="center">
								<a href="<?php echo JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid)?>" class="btn"><?php echo JText::_('LBL_CANCEL');?></a>
								<button class="btn btn-primary" type="submit"><i class="fa fa-search-plus"></i> <?php echo JText::_('LBL_SEARCH');?></button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>