<?php
/**
 * @version		$Id: answers.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.controller');

class CommunityAnswersControllerAnswers extends JControllerLegacy 
{
	function __construct()
	{
		parent::__construct();
		
		$this->registerDefaultTask('get_latest_questions');
		$this->registerTask('trending', 'get_trending_questions');
		$this->registerTask('popular', 'get_popular_questions');
		$this->registerTask('resolved', 'get_solved_questions');
		$this->registerTask('open', 'get_unsolved_questions');
		$this->registerTask('mostanswered', 'get_most_answered_questions');
		$this->registerTask('tag', 'get_tagged_questions');
		
		$this->registerTask('ask', 'get_question_form');
		$this->registerTask('edit', 'get_question_form');
		$this->registerTask('get_tags', 'get_tags');
		$this->registerTask('delete_tag', 'delete_tag');
		
		$this->registerTask('attach', 'attach_file');
		$this->registerTask('detach', 'delete_attachment');
		$this->registerTask('download', 'download_file');
		
		$this->registerTask('view', 'get_question_details');
		$this->registerTask('get_answer', 'get_answer');
		$this->registerTask('save_question', 'save_question');
		$this->registerTask('save_reply', 'save_reply');
		$this->registerTask('save_answer', 'save_answer');
		$this->registerTask('accept_answer', 'accept_answer');
		$this->registerTask('unaccept_answer', 'unaccept_answer');
		$this->registerTask('unpublish_question', 'unpublish_question');
		$this->registerTask('delete_question', 'delete_question');
		$this->registerTask('unpublish_answer', 'unpublish_answer');
		$this->registerTask('delete_answer', 'delete_answer');
		$this->registerTask('unpublish_reply', 'unpublish_reply');
		$this->registerTask('delete_reply', 'delete_reply');
		$this->registerTask('update_answer', 'update_answer');
		$this->registerTask('like', 'like_answer');
		$this->registerTask('dislike', 'dislike_answer');
		$this->registerTask('report_answer', 'report_answer');
		$this->registerTask('report_question', 'report_question');
		
		$this->registerTask('search', 'redirect_to_search');
		$this->registerTask('tags', 'get_tags_listing');
		$this->registerTask('get_tag', 'get_tag_details');
		$this->registerTask('save_tag', 'save_tag_details');
		
		$this->registerTask('approve', 'approve_items');
		$this->registerTask('disapprove', 'disapprove_items');
		
		$this->registerTask('subscribe', 'subscribe_category_feed');
		$this->registerTask('unsubscribe', 'unsubscribe_category_feed');
		$this->registerTask('subscribe_qn', 'subscribe_question_feed');
		$this->registerTask('unsubscribe_qn', 'unsubscribe_question_feed');
		
		$this->registerTask('feed', 'get_rss_feed');
		$this->registerTask('stats_widget', 'get_user_stats_widget');
		$this->registerTask('activities', 'get_activities');
		
		$this->registerTask('save_bounty', 'ajx_save_bounty');
		$this->registerTask('refund_bounty', 'refund_bounty_points');
		
		$this->registerTask('leaderboard', 'get_leaderboard');
	}

	public function get_trending_questions()
	{
		$model = $this->getModel('answers');
		$users_model = $this->getModel('users');
		$view = $this->getView('answers', 'html');
	
		$view->setModel($model, true);
		$view->setModel($users_model, false);
		$view->assign('action', 'trending_questions');
	
		$view->display();
	}
	
	public function get_latest_questions()
	{
		$model = $this->getModel('answers');
		$users_model = $this->getModel('users');
		$view = $this->getView('answers', 'html');
		
		$view->setModel($model, true);
		$view->setModel($users_model, false);
		$view->assign('action', 'latest_questions');
		
		$view->display();
	}
	
	public function get_solved_questions()
	{
		$model = $this->getModel('answers');
		$users_model = $this->getModel('users');
		$view = $this->getView('answers', 'html');
		
		$view->setModel($model, true);
		$view->setModel($users_model, false);
		$view->assign('action', 'solved_questions');
		
		$view->display();
		
	}
	
	public function get_unsolved_questions()
	{
		$model = $this->getModel('answers');
		$users_model = $this->getModel('users');
		$view = $this->getView('answers', 'html');
		
		$view->setModel($model, true);
		$view->setModel($users_model, false);
		$view->assign('action', 'unsolved_questions');
		
		$view->display();
	}

	public function get_popular_questions()
	{
		$model = $this->getModel('answers');
		$users_model = $this->getModel('users');
		$view = $this->getView('answers', 'html');
		
		$view->setModel($model, true);
		$view->setModel($users_model, false);
		$view->assign('action', 'popular_questions');
		
		$view->display();
	}
	
	public function get_most_answered_questions()
	{
		$model = $this->getModel('answers');
		$users_model = $this->getModel('users');
		$view = $this->getView('answers', 'html');
		
		$view->setModel($model, true);
		$view->setModel($users_model, false);
		$view->assign('action', 'most_answered_questions');
		
		$view->display();
	}

	public function get_tagged_questions()
	{
		$model = $this->getModel('answers');
		$users_model = $this->getModel('users');
		$view = $this->getView('answers', 'html');
		
		$view->setModel($model, true);
		$view->setModel($users_model, false);
		$view->assign('action', 'tagged_questions');
		
		$view->display();
	}
	
	public function get_question_form()
	{
		$user = JFactory::getUser();
		
		if($user->authorise('answers.ask', A_APP_NAME) || $user->authorise('answers.manage', A_APP_NAME))
		{
			$model = $this->getModel('answers');
			$view = $this->getView('form', 'html');
			
			$view->setModel($model, true);
			$view->display();
		} 
		else 
		{
			if($user->guest) 
			{
				$itemid = CJFunctions::get_active_menu_id();
				$redirect_url = base64_encode(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=ask'.$itemid));
				$this->setRedirect(JRoute::_("index.php?option=com_users&view=login".$itemid."&return=".$redirect_url), JText::_('MSG_USER_LOGIN'));
			} 
			else 
			{
				CJFunctions::throw_error(JText::_('JERROR_ALERTNOAUTHOR'), 403);
			}
		}
	}
	
	public function get_question_details()
	{
		$user = JFactory::getUser();
		
		if($user->authorise('answers.view', A_APP_NAME) || $user->authorise('answers.manage', A_APP_NAME))
		{
			$model = $this->getModel('answers');
			$users_model = $this->getModel('users');
			$view = $this->getView('question', 'html');
			
			$view->setModel($model, true);
			$view->setModel($users_model, false);
			$view->assign('action', 'question_details');
			
			$view->display();
		} 
		else if($user->guest)
		{
			$itemid = CJFunctions::get_active_menu_id();
			$redirect_url = base64_encode(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid));
			$this->setRedirect(JRoute::_("index.php?option=com_users&view=login".$itemid."&return=".$redirect_url), JText::_('MSG_USER_LOGIN'));
		} 
		else 
		{
			CJFunctions::throw_error(JText::_('JERROR_ALERTNOAUTHOR'), 403);
		}
	}
	
	public function save_question()
	{
		$user = JFactory::getUser();
		$itemid = CJFunctions::get_active_menu_id(true, 'index.php?option='.A_APP_NAME.'&view=answers');
		
		if(!$user->authorise('answers.ask', A_APP_NAME) && !$user->authorise('answers.manage', A_APP_NAME)) 
		{
			$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid), JText::_('JERROR_ALERTNOAUTHOR'));
			return;
		}
			
		$app = JFactory::getApplication();
		$model = $this->getModel('answers');
		$mail_model = $this->getModel('mail');
		$users_model = $this->getModel('users');
		$params = JComponentHelper::getParams(A_APP_NAME);
		
		$question = new stdClass();
		$post = $app->input->post;
		$allow_html = ($params->get('default_editor') != 'none' && $user->authorise('answers.wysiwyg', A_APP_NAME));
		
		$question->id = $post->getInt('id', 0);
		$question->catid = $post->getInt('catid', 0);
		$question->title = $post->getString('title', '');
		$question->description = CJFunctions::get_clean_var('description', $allow_html);
		$question->alias = $post->getString('alias', '');
		$question->tags = $post->getString('tags', '');
		$question->attachments = $post->getString('attachments', '');
		$question->username = $post->getString('username', '');
		$question->email = $post->getString('email', '');
		$question->notification = $post->getInt('notify_answers', 1);

		$bounty = new stdClass();
		$bounty->question_id = $question->id;
		$bounty->bounty_points = $post->getInt('bounty-points', 0);
		$bounty->expiry_date = $post->getString('bounty-expiry', null);
		
		$question->bounty = $bounty;
		
		if($user->guest && JPluginHelper::isEnabled('captcha'))
		{
			JPluginHelper::importPlugin('captcha');
			$dispatcher = JDispatcher::getInstance();
			$result = $dispatcher->trigger('onCheckAnswer',$post->getString('recaptcha_response_field'));
			
			if(!$result[0])
			{
				$app->enqueueMessage(JText::_('MSG_INVALID_CAPTCHA'));

				$view = $this->getView('form', 'html');
				$view->assignRef('question', $question);
				$view->setModel($model, true);
				$view->display();
				return;
			}
		}

		if(empty($question->title) || $question->catid <= 0 || ($user->guest && (empty($question->username) || empty($question->email))))
		{
			$app->enqueueMessage(JText::_('MSG_MISSING_REQUIRED'));
		} 
		elseif($question->id > 0)
		{
			if($model->update_question($question))
			{
				// update bounty if user saves it
				$this->save_bounty(false);
				
				$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$question->id.':'.$question->alias.$itemid));
				return;
			} 
			else 
			{
				$app->enqueueMessage(JText::_('MSG_ERROR_PROCESSING'));
			}
		} 
		else 
		{
			$question->published = $user->authorise('answers.aaquestion', A_APP_NAME) ? 1 : 2;
			CJFunctions::trigger_event('corejoomla', 'onBeforeSaveQuestion', $question);
			
			if($model->save_question($question))
			{
				CJFunctions::trigger_event('corejoomla', 'onAfterSaveQuestion', array(&$question, &$user));
				
				if($question->notification == 1 && !$user->guest)
				{
					$model->subscribe(1, $user->id, $question->id);
				}

				// Save bounty if one exists
				$bounty->question_id = $question->id;
				$this->save_bounty(false, $bounty);
								
				if($question->published == 1)
				{
					$qurl = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$question->id.':'.$question->alias.$itemid);
					
					if($user->id > 0)
					{
						if(file_exists(JPATH_ROOT.DS.'components'.DS.'com_cjblog'.DS.'api.php'))
						{
							require_once JPATH_ROOT.DS.'components'.DS.'com_cjblog'.DS.'api.php';
							$my = $users_model->get_user_details($user->id);
							
							// trigger badges rule
							CjBlogApi::trigger_badge_rule('com_communityanswers.num_questions', array('num_questions'=>$my->questions, 'ref_id'=>$question->id), $user->id);
						}
												
						// award points
						AnswersHelper::awardPoints($params, $user->id, 1, $question->id, JHtml::link($qurl, $question->title));
						
						// stream activity
						AnswersHelper::stream_activity(1, $params, $question, $user->id, $qurl);
					}

					// send mail to admins and subscribers
					$sitename = $app->getCfg('sitename');
					$subject = JText::sprintf('EMAIL_NEW_QUESTION_SUB', $sitename);
					$question_link = JHtml::link(
							JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$question->id.':'.$question->alias.$itemid, false, -1), 
							$question->title);
					$body = JText::sprintf('EMAIL_NEW_QUESTION_CONTENT', $user->name, $question->title, $question->description, $question_link, $sitename);
					
					if($params->get('admin_new_question_notification', 0) == 1){
					
						$admins = $model->get_admin_users($params->get('admin_user_groups', 0));
						
						if(!empty($admins))
						{
							$mail_model->add_messages_to_queue(
									2,
									$question->id, 
									$question->catid, 
									A_APP_NAME.'.newquestion', 
									$subject, 
									$body, 
									$params->get('mail-tpl-newquestion', 'mail-blue.tpl'), 
									$admins);
						}
					}
										
					$this->setRedirect($qurl, JText::_('MSG_QUESTION_SAVED'));
				} 
				else 
				{
					$from = $app->getCfg('mailfrom' );
					$fromname = $app->getCfg('fromname' );
					
					$approval_link = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=approve&type=1&secret='.$question->secret.$itemid, false, -1);
					$disapproval_link = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=disapprove&type=1&secret='.$question->secret.$itemid, false, -1);
					
					$admin_emails = $model->get_admin_emails($params->get('admin_user_groups', 0));
					
					if(!empty($admin_emails))
					{
						$body = JText::sprintf(
								'EMAIL_QUESTION_APPROVAL_BODY', 
								$question->username, 
								$question->title, 
								$question->description, 
								!empty($question->attachments) ? JText::_('JYES') : JText::_('JNO'), 
								JHtml::link($approval_link, JText::_('LBL_APPROVE_QUESTION')), 
								JHtml::link($disapproval_link, JText::_('LBL_DISAPPROVE_QUESTION')));
						
						CJFunctions::send_email($from, $fromname, $admin_emails, JText::_('EMAIL_QUESTION_APPROVAL_SUB'), $body, 1);
					}
											
					$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid), JText::_('MSG_SENT_FOR_APPROVAL'));
				}
				
				return;
			}
		}

		$view = $this->getView('form', 'html');
		$view->assignRef('question', $question);
		$view->setModel($model, true);
		$view->display();
	}
	
	public function get_answer()
	{
		$user = JFactory::getUser();
		
		if($user->authorise('answers.answer', A_APP_NAME) || $user->authorise('answers.manage', A_APP_NAME))
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
			
			$question_id = $app->input->getInt('id');
			$answer_id = $app->input->getInt('answer_id', 0);
			
			if($answer_id > 0 && $question_id > 0)
			{
				$answer = $model->get_answer_details($question_id, $answer_id);
				
				if(!empty($answer))
				{
					if($answer->created_by != $user->id && !$user->authorise('answers.manage', A_APP_NAME))
					{
						echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
					} 
					else 
					{
						echo json_encode(array('answer'=>$answer));
					}
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		} 
		else 
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		}
		
		jexit();
	}
	
	public function save_answer()
	{
		$user = JFactory::getUser();
		
		if($user->authorise('answers.answer', A_APP_NAME) || $user->authorise('answers.manage', A_APP_NAME))
		{
			$app = JFactory::getApplication();
			$params = JComponentHelper::getParams(A_APP_NAME);
			$model = $this->getModel('answers');
			$mail_model = $this->getModel('mail');
			$users_model = $this->getModel('users');
			$post = $app->input->post;

			$answer = new stdClass();
			$allow_html = ($params->get('default_editor') != 'none' && $user->authorise('answers.wysiwyg', A_APP_NAME));
			
			$answer->id = $post->getInt('answer_id', 0);
			$answer->question_id = $post->getInt('question_id', 0);
			$answer->description = CJFunctions::get_clean_var('answer', $allow_html);
			$answer->username = $post->getString('username', '');
			$answer->email = $post->getString('email', '');
			$answer->published = $user->authorise('answers.aaanswer', A_APP_NAME) ? 1 : 2;
			$answer->attachments = $post->getString('attachments', '');
			$answer->references = $params->get('enable_references', 0) == 1 ? $post->getString('references', '') : '';
			
			if($user->guest && JPluginHelper::isEnabled('captcha'))
			{
				JPluginHelper::importPlugin('captcha');
				$dispatcher = JDispatcher::getInstance();
				$result = $dispatcher->trigger('onCheckAnswer',$post->getString('recaptcha_response_field'));
					
				if(!$result[0])
				{
					echo json_encode(array('error'=>JText::_('MSG_INVALID_CAPTCHA')));
					jexit();
				}
			}
			
			if($answer->question_id <= 0 || empty($answer->description) || ($user->guest && (empty($answer->username) || empty($answer->email))))
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			} 
			elseif($user->guest && !JMailHelper::isEmailAddress($answer->email))
			{
				echo json_encode(array('error'=>JText::_('MSG_INVALID_EMAIL_ADDRESS')));
			} 
			elseif(!$user->guest && !$user->authorise('answers.multianswer', A_APP_NAME) && !$model->can_answer($user->id, $answer->question_id))
			{
				echo json_encode(array('error'=>JText::_('MSG_ALREADY_ANSWERED')));
			} 
			else if($model->save_answer($answer, $params) && $answer->id > 0)
			{
				CJFunctions::trigger_event('corejoomla', 'onAfterSaveAnswer', $answer);
				
				$question = $model->get_question_details($answer->question_id, array('get_tags'=>false, 'get_answers'=>false, 'trigger_hit'=>false), $params);
				$itemid = CJFunctions::get_active_menu_id();
				$url = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$question->id.':'.$question->alias.$itemid.'#'.$answer->id, false, -1);
				$qurl = JHtml::link($url, $question->title);
				
				if($answer->published == 1)
				{
					// process the description
					if($params->get('default_editor') == 'bbcode')
					{
						$answer->description = CJFunctions::process_html($answer->description, true, false);
					}
					
					if(!$user->guest)
					{
						// trigger badges rule
						if(file_exists(JPATH_ROOT.DS.'components'.DS.'com_cjblog'.DS.'api.php'))
						{
							require_once JPATH_ROOT.DS.'components'.DS.'com_cjblog'.DS.'api.php';
							$my = $users_model->get_user_details($user->id);
							
							if(!empty($my))
							{
								CjBlogApi::trigger_badge_rule(
									'com_communityanswers.num_answers', 
									array('num_answers'=>$my->answers, 'ref_id'=>$answer->id), 
									$user->id);
							}
						}
						
						// award points first
						AnswersHelper::awardPoints($params, $user->id, 2, $answer->id, $qurl);
						
						// stream the activity
						$question->answer = $answer->description;
						AnswersHelper::stream_activity(2, $params, $question, $user->id, $url);
					}
										
					// send mail to admins and subscribers
					$sitename = $app->getCfg('sitename');
					$subject = JText::_('EMAIL_NEW_ANSWER_SUB');
					$body = JText::sprintf('EMAIL_NEW_ANSWER_CONTENT', $question->title, $answer->description, $qurl, $sitename);
					
					$admins = $model->get_admin_users($params->get('admin_user_groups', 0));
					
					if($question->created_by == 0)
					{
						$asker = new stdClass();
						$asker->name = $question->username;
						$asker->email = $question->email;
						$asker->subid = 0;
						
						$admins = array_merge(array($asker), $admins);
					}
					
					if(!empty($admins))
					{
						$mail_model->add_messages_to_queue(
								1,
								$question->id, 
								$question->catid, 
								A_APP_NAME.'.newanswer', 
								$subject, 
								$body, 
								$params->get('mail-tpl-newanswer', 'mail-blue.tpl'),
								$admins);
					}
										
					echo json_encode(array('answer'=>$answer));
					
					// now add to activities
					$activity = new stdClass();
					$activity->type = 1;
					$activity->question_id = $answer->question_id;
					$activity->item_id = $answer->question_id;
					$activity->title = $question->title;
					$activity->url = $url;
					$activity->description = CJFunctions::substrws($answer->description, 255);
					
					$activity_model = $this->getModel('activities');
					$activity_model->save_activity($activity);
				} 
				else 
				{
					$from = $app->getCfg('mailfrom' );
					$fromname = $app->getCfg('fromname' );
					
					$approval_link = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=approve&type=2&secret='.$answer->secret.$itemid, false, -1);
					$disapproval_link = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=disapprove&type=2&secret='.$answer->secret.$itemid, false, -1);
					
					$admin_emails = $model->get_admin_emails($params->get('admin_user_groups', 0));
					
					if(!empty($admin_emails))
					{
						$body = JText::sprintf(
								'EMAIL_ANSWER_APPROVAL_BODY',
								$answer->username,
								$question->title,
								$answer->description,
								!empty($answer->attachments) ? JText::_('JYES') : JText::_('JNO'),
								JHtml::link($approval_link, JText::_('LBL_APPROVE_ANSWER')),
								JHtml::link($disapproval_link, JText::_('LBL_DISAPPROVE_ANSWER')));
							
						CJFunctions::send_email($from, $fromname, $admin_emails, JText::_('EMAIL_ANSWER_APPROVAL_SUB'), $body, 1);
					}
					
					echo json_encode(array('message'=>JText::_('MSG_ANSWER_SENT_FOR_APPROVAL')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
			}
		} 
		else 
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		}
		
		jexit();
	}
	
	public function save_reply()
	{
		$user = JFactory::getUser();
		
		if($user->guest || (!$user->authorise('answers.answer', A_APP_NAME) && !$user->authorise('answers.manage', A_APP_NAME)))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
			$params = JComponentHelper::getParams(A_APP_NAME);
				
			$question_id = $app->input->getInt('id');
			$answer_id = $app->input->post->getInt('answer_id', 0);
			$qn_id = $app->input->post->getInt('question_id', 0);
			$reply = $app->input->post->getString('reply');
			
			if(($answer_id > 0 || $qn_id > 0) && !empty($reply))
			{
				if($model->save_reply($question_id, $answer_id, $qn_id, $reply))
				{
					$answer = null;
					
					if(!$answer_id && $qn_id > 0)
					{
						$answer = $model->get_question_details($question_id, array('get_tags'=>false, 'get_answers'=>false, 'trigger_hit'=>false, 'get_attachments'=>false), $params);
						$answer->asker_id = $answer->created_by;
						$answer->question_id = $answer->id;
						$answer_id = $qn_id;
					} 
					else 
					{
						$answer = $model->get_answer_details($question_id, $answer_id);
					}
					
					$itemid = CJFunctions::get_active_menu_id();
					$url = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$answer->question_id.':'.$answer->alias.$itemid.'#'.$answer_id, false, -1);
					$qurl = JHtml::link($url, $answer->title);
					
					if(!empty($answer->email))
					{
						$from = $app->getCfg('mailfrom' );
						$fromname = $app->getCfg('fromname' );
						$site_name = $app->getCfg('sitename');
						$body = JText::sprintf('EMAIL_ANSWER_REPLY_BODY', $answer->username, $user->name, $answer->title, $qurl, $site_name);
						
						CJFunctions::send_email($from, $fromname, $answer->email, JText::_('EMAIL_ANSWER_REPLY_SUB'), $body, 1);
					}
					
					// now add to activity
					$activity = new stdClass();
					$activity->type = 2;
					$activity->answer_id = $answer_id;
					$activity->asker_id = $answer->asker_id;
					$activity->item_id = $answer_id;
					$activity->title = $answer->title;
					$activity->url = $url;
					$activity->description = CJFunctions::substrws($reply, 256);
						
					$activity_model = $this->getModel('activities');
					$activity_model->save_activity($activity);

					echo json_encode(array('reply'=>$reply));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
		
		jexit();
	}
	
	public function accept_answer()
	{
		$user = JFactory::getUser();
		
		if($user->guest || (!$user->authorise('answers.ask', A_APP_NAME) && !$user->authorise('answers.manage', A_APP_NAME)))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
			$users_model = $this->getModel('users');
			$mail_model = $this->getModel('mail');
			
			$question_id = $app->input->getInt('id');
			$answer_id = $app->input->getInt('answer_id', 0);
		
			if($answer_id > 0 && $question_id > 0)
			{
				if($model->accept_answer($question_id, $answer_id))
				{
					$answer = $model->get_answer_details($question_id, $answer_id);
					CJFunctions::trigger_event('corejoomla', 'onAfterAcceptAnswer', $answer);
					
					$params = JComponentHelper::getParams(A_APP_NAME);
					$itemid = CJFunctions::get_active_menu_id();
					$qurl = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$answer->question_id.':'.$answer->alias.$itemid.'#'.$answer_id, false, -1);
					$link = JHtml::link($qurl, CJFunctions::escape($answer->title));
					
					// process the description
					if($params->get('default_editor') == 'bbcode')
					{
						$answer->description = CJFunctions::process_html($answer->description, true, false);
					}

					if($answer->created_by > 0)
					{
						if(file_exists(JPATH_ROOT.DS.'components'.DS.'com_cjblog'.DS.'api.php'))
						{
							require_once JPATH_ROOT.DS.'components'.DS.'com_cjblog'.DS.'api.php';
							$my = $users_model->get_user_details($answer->created_by);
		
							if(!empty($my))
							{
								// trigger badges rule
								CjBlogApi::trigger_badge_rule('com_communityanswers.num_best_answers', array('num_answers'=>$my->best_answers, 'ref_id'=>$answer->question_id), $my->id);
							}
						}
											
						// award points first for the regular points, if any
						AnswersHelper::awardPoints($params, $answer->created_by, 3, $answer->question_id, $qurl);
						
						// now award bounty points, if any
						if($params->get('enable_bounties', 1) == '1')
						{
							$bounty = $model->get_bounty_of_question($question_id);
								
							if(!empty($bounty) && ($bounty->state == 1 || $bounty->state == 4) && $model->set_bounty_state($bounty->id, 2))
							{
								AnswersHelper::awardPoints($params, $answer->created_by, 8, $question_id, JText::sprintf('MSG_BOUNTY_WINNER', $bounty->bounty_points), $bounty->bounty_points);
							}
						}
						
						// now add to activity
						$activity = new stdClass();
						$activity->type = 3;
						$activity->answerer = $answer->created_by;
						$activity->item_id = $question_id;
						$activity->title = $answer->title;
						$activity->url = $qurl;
						$activity->description = CJFunctions::substrws($answer->description, 255);
						
						$activity_model = $this->getModel('activities');
						$activity_model->save_activity($activity);
					}
											
					// stream the activity
					$question = new stdClass();
					$question->answer = $answer->description;
					$question->title = $answer->title;
					AnswersHelper::stream_activity(3, $params, $question, $answer->created_by, $qurl);
						
					// send mail to admins and subscribers
					$sitename = $app->getCfg('sitename');
					$subject = JText::_('EMAIL_BEST_ANSWER_SUB');
					$body = JText::sprintf('EMAIL_BEST_ANSWER_CONTENT', $answer->title, $answer->description, JHtml::link($qurl, $answer->title), $sitename);
						
					$mail_model->add_messages_to_queue(
							1,
							$answer->question_id,
							$answer->catid,
							A_APP_NAME.'.newanswer',
							$subject,
							$body,
							$params->get('mail-tpl-newanswer', 'mail-blue.tpl'));
						
					echo json_encode(array('data'=>1));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
		
		jexit();
	}

	public function unaccept_answer()
	{
		$user = JFactory::getUser();
	
		if(!$user->authorise('answers.manage', A_APP_NAME))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
	
			$question_id = $app->input->getInt('id');
			$answer_id = $app->input->getInt('answer_id', 0);
	
			if($answer_id > 0 && $question_id > 0)
			{
				if($model->unaccept_answer($question_id, $answer_id))
				{
					echo json_encode(array('data'=>1));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
	
		jexit();
	}

	public function unpublish_question()
	{
		$user = JFactory::getUser();
	
		if($user->guest || !$user->authorise('answers.manage', A_APP_NAME))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
	
			$question_id = $app->input->getInt('id');
	
			if($question_id > 0)
			{
				if($model->unpublish_question($question_id))
				{
					echo json_encode(array('data'=>1));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
	
		jexit();
	}
	
	public function delete_question()
	{
		$user = JFactory::getUser();
	
		if($user->guest || !$user->authorise('answers.manage', A_APP_NAME))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
	
			$question_id = $app->input->getInt('id', 0);
	
			if($question_id > 0)
			{
				if($model->delete_question($question_id))
				{
					echo json_encode(array('data'=>1));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
	
		jexit();
	}
	
	public function unpublish_answer()
	{
		$user = JFactory::getUser();
		
		if($user->guest || !$user->authorise('answers.manage', A_APP_NAME))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
		
			$question_id = $app->input->getInt('id');
			$answer_id = $app->input->getInt('answer_id', 0);
		
			if($answer_id > 0 && $question_id > 0)
			{
				if($model->unpublish_answer($question_id, $answer_id))
				{
					echo json_encode(array('data'=>1));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
		
		jexit();
	}

	public function delete_answer()
	{
		$user = JFactory::getUser();
	
		if($user->guest || !$user->authorise('answers.manage', A_APP_NAME))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
	
			$question_id = $app->input->getInt('id', 0);
			$answer_id = $app->input->getInt('answer_id', 0);
	
			if($answer_id > 0 && $question_id > 0)
			{
				if($model->delete_answer($question_id, $answer_id))
				{
					echo json_encode(array('data'=>1));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
	
		jexit();
	}

	public function unpublish_reply()
	{
		$user = JFactory::getUser();
	
		if($user->guest || !$user->authorise('answers.manage', A_APP_NAME))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
	
			$answer_id = $app->input->getInt('id', 0);
	
			if($answer_id > 0)
			{
				if($model->unpublish_reply($answer_id))
				{
					echo json_encode(array('data'=>1));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
	
		jexit();
	}
	
	public function delete_reply()
	{
		$user = JFactory::getUser();
	
		if($user->guest || !$user->authorise('answers.manage', A_APP_NAME))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
	
			$answer_id = $app->input->getInt('id', 0);
	
			if($answer_id > 0)
			{
				if($model->delete_reply($answer_id))
				{
					echo json_encode(array('data'=>1));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
	
		jexit();
	}
	
	public function update_answer()
	{
		$user = JFactory::getUser();
	
		if($user->guest || (!$user->authorise('answers.edit', A_APP_NAME) && !$user->authorise('answers.manage', A_APP_NAME)))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
			$params = JComponentHelper::getParams(A_APP_NAME);
			$allow_html = ($params->get('default_editor') != 'none' && $user->authorise('answers.wysiwyg', A_APP_NAME));
	
			$question_id = $app->input->getInt('id');
			$answer_id = $app->input->getInt('answer_id', 0);
			$references = $params->get('enable_references', 0) == 1 ? $app->input->getString('references', 0) : '';
			$description = CJFunctions::get_clean_var('description', $allow_html);
	
			if($answer_id > 0 && $question_id > 0 && !empty($description))
			{
				if($model->update_answer($question_id, $answer_id, $description, $references))
				{
					$data = CJFunctions::process_html(
							$description, 
							($allow_html && $params->get('default_editor') == 'bbcode'),
							$params->get('process_content_plugins') == '1');
					echo json_encode(array('content'=>$data));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
	
		jexit();
	}
	
	public function like_answer()
	{
		$this->like_or_dislike_answer(1);
	}
	
	public function dislike_answer()
	{
		$this->like_or_dislike_answer(0);
	}
	
	public function like_or_dislike_answer($state)
	{
		$user = JFactory::getUser();
		
		if(!$user->authorise('answers.rate', A_APP_NAME))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
		
			$question_id = $app->input->getInt('id');
			$answer_id = $app->input->getInt('answer_id', 0);
		
			if($answer_id > 0 && $question_id > 0)
			{
				$result = $model->like_or_dislike_answer($state, $question_id, $answer_id);
				
				if(is_object($result))
				{
					$params = JComponentHelper::getParams(A_APP_NAME);
					$answer = $model->get_answer_details($question_id, $answer_id);
					
					if($state == 1)
					{
						AnswersHelper::awardPoints($params, $answer->created_by, 4, 0, CJFunctions::substrws($answer->description, 250));
					} 
					elseif($state == 0)
					{
						AnswersHelper::awardPoints($params, $answer->created_by, 5, 0, CJFunctions::substrws($answer->description, 250));
						AnswersHelper::awardPoints($params, $user->id, 6, $answer->id, CJFunctions::substrws($answer->description, 250));
					}
					
					// now add to activity
					$itemid = CJFunctions::get_active_menu_id();
					$qurl = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$question_id.':'.$answer->alias.$itemid.'#'.$answer_id);
					
					$activity = new stdClass();
					$activity->type = $state == 1 ? 4 : 5;
					$activity->question_id = $question_id;
					$activity->item_id = $answer_id;
					$activity->answerer = $answer->created_by;
					$activity->title = $answer->title;
					$activity->url = $qurl;
					$activity->description = '';
						
					$activity_model = $this->getModel('activities');
					$activity_model->save_activity($activity);

					echo json_encode(array('rating'=>($result->thumbup - $result->thumbdown)));
				} 
				elseif($result == -1)
				{
					echo json_encode(array('error'=>JText::_('ALREADY_RATED')));
				} 
				elseif($result == -2)
				{
					echo json_encode(array('error'=>JText::_('MSG_SELF_RATING')));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
		
		jexit();
	}
	
	public function report_answer()
	{
		$user = JFactory::getUser();
		
		if($user->authorise('answers.report', A_APP_NAME))
		{
			$app = JFactory::getApplication();
			
			$question_id = $app->input->getInt('id', 0);
			$answer_id = $app->input->getInt('answer_id', 0);
			$reason = $app->input->getString('reason', '');
			$description = $app->input->getString('description', '');
			
			if($question_id && $answer_id && !empty($description) && !empty($reason))
			{
				$model = $this->getModel('answers');
				$answer = $model->get_answer_details($question_id, $answer_id);
				
				if(!empty($answer))
				{
					$params = JComponentHelper::getParams(A_APP_NAME);
					$itemid = CJFunctions::get_active_menu_id();
					$url = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$answer->question_id.':'.$answer->alias.$itemid, false, -1);
					
					$subject = JText::sprintf('EMAIL_REPORT_SUB', $reason);
					$item_description = CJFunctions::process_html($answer->description, $params->get('default_editor') == 'bbcode');
					$body = JText::sprintf('EMAIL_REPORT_CONTENT', $user->name, $item_description, $reason, $description, $url);
					
					$from = $app->getCfg('mailfrom');
					$fromname = $app->getCfg('fromname' );
					$admin_emails = $model->get_admin_emails($params->get('admin_user_groups', 0));
						
					if(!empty($admin_emails))
					{
						CJFunctions::send_email($from, $fromname, $admin_emails, $subject, $body, 1);
					}
					
					echo json_encode(array('content'=>JText::_('MSG_REPORT_SENT')));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		} 
		else 
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		}
		
		jexit();
	}

	public function report_question()
	{
		$user = JFactory::getUser();
	
		if($user->authorise('answers.report', A_APP_NAME))
		{
			$app = JFactory::getApplication();
				
			$question_id = $app->input->getInt('id', 0);
			$reason = $app->input->getString('reason', '');
			$description = $app->input->getString('description', '');
				
			if($question_id && !empty($description) && !empty($reason))
			{
				$params = JComponentHelper::getParams(A_APP_NAME);
				$model = $this->getModel('answers');
				
				$question = $model->get_question_details(
						$question_id, 
						array('get_tags'=>false, 'get_answers'=>false, 'trigger_hit'=>false, 'get_attachments'=>false), 
						$params);
	
				if(!empty($question))
				{
					$itemid = CJFunctions::get_active_menu_id();
					$url = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$question->id.':'.$question->alias.$itemid, false, -1);
						
					$subject = JText::sprintf('EMAIL_REPORT_SUB', $reason);
					$item_description = CJFunctions::process_html($question->description, $params->get('default_editor') == 'bbcode');
					$item_description = '<p><strong>'.$question->title.'</strong></p>'.$item_description;
					$body = JText::sprintf('EMAIL_REPORT_CONTENT', $user->name, $item_description, $reason, $description, $url);
						
					$from = $app->getCfg('mailfrom');
					$fromname = $app->getCfg('fromname' );
					$admin_emails = $model->get_admin_emails($params->get('admin_user_groups', 0));
					
					if(!empty($admin_emails))
					{
						CJFunctions::send_email($from, $fromname, $admin_emails, $subject, $body, 1);
					}
						
					echo json_encode(array('content'=>JText::_('MSG_REPORT_SENT')));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		} 
		else 
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		}
	
		jexit();
	}
	
	public function attach_file()
	{
		$user = JFactory::getUser();
		$xhr = $_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest';
		if(!$xhr) echo '<textarea>';
		
		if($user->authorise('answers.attachments', A_APP_NAME))
		{
			$params = JComponentHelper::getParams(A_APP_NAME);
			$allowed_extensions = $params->get('allowed_attachment_types', '');
			$allowed_size = ((int)$params->get('max_attachment_size', 256))*1024;
			$input = JFactory::getApplication()->input;
			
			if(!empty($allowed_extensions))
			{
				$tmp_file = $input->files->get('input-attachment');
				$tmp_path = JPATH_ROOT.DS.'tmp'.DS.'answers'.DS;
				
				if($tmp_file['error'] > 0)
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				} 
				else 
				{
					$temp_file_path = $tmp_file['tmp_name'];
					$temp_file_name = $tmp_file['name'];
					$temp_file_ext = JFile::getExt($temp_file_name);
					
					if (!in_array(strtolower($temp_file_ext), explode(',', strtolower($allowed_extensions))))
					{
						echo json_encode(array('error'=>JText::_('MSG_INVALID_FILETYPE')));
					} 
					elseif ($tmp_file['size'] > $allowed_size)
					{ 
						echo json_encode(array('error'=>JText::_('MSG_MAX_SIZE_FAILURE')));
					} 
					else 
					{
						$file_name = CJFunctions::generate_random_key(25, 'abcdefghijklmnopqrstuvwxyz1234567890').'.'.$temp_file_ext;
							
						if(JFile::upload($temp_file_path, $tmp_path.$file_name))
						{
							echo json_encode(array('file_name'=>$file_name));
						} 
						else 
						{
							echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
						}
					}
				}			
			} 
			else 
			{
				echo '{"file_name": null}';
			}
			
		} 
		else 
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		}
		
		if(!$xhr) echo '</textarea>';
				
		jexit();
	}
	
	public function delete_attachment()
	{
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		$id = $app->input->getInt('id', 0);
		
		if($id > 0 && ($user->authorise('answers.attachments', A_APP_NAME) || $user->authorise('answers.manage', A_APP_NAME)))
		{
			$model = $this->getModel('answers');
			
			if($model->delete_attachment($id))
			{
				echo json_encode(array('data'=>1));
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
			}
		}
		
		jexit();
	}
	
	public function download_file()
	{
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$id = $app->input->getInt('id', 0);
		
		if($id > 0 && $user->authorise('answers.download', A_APP_NAME))
		{
			$model = $this->getModel('answers');
			$attachment = $model->get_attachment($id);
			
			if(!empty($attachment))
			{
				$extension = strtolower(JFile::getExt($attachment->attachment));
				
				if(in_array($extension, array('jpg', 'jpeg', 'gif', 'png', 'bmp')))
				{
					header('Content-type: image/'.$extension);
				} 
				else 
				{
					header("Content-Type: application/force-download");
					header("Content-Type: application/octet-stream");
					header("Content-Type: application/download");
					header("Content-Disposition: attachment; filename=".$attachment->attachment);
				}
								
				readfile(CA_ATTACHMENTS_PATH.$attachment->attachment);
			} 
			else 
			{
				echo JText::_('JERROR_ALERTNOAUTHOR');
			}
		} 
		else 
		{
			echo JText::_('JERROR_ALERTNOAUTHOR');
		}
		
		jexit();
	}
	
	public function redirect_to_search()
	{
		$app = JFactory::getApplication();
		$model = $this->getModel('answers');
		
		$search_keywords = $app->input->getString('q', null);
		$search_username = $app->input->getString('u', null);
		
		if(empty($search_keywords) && empty($search_username))
		{
			$view = $this->getView('search', 'html');
			
			$view->setModel($model, true);
			$view->display();
		} 
		else 
		{
			$users_model = $this->getModel('users');
			$view = $this->getView('answers', 'html');
			
			$view->setModel($model, true);
			$view->setModel($users_model, false);
			$view->assign('action', 'search');
			$view->display();
		}
	}
	
	public function get_tags_listing()
	{
		$model = $this->getModel('answers');
		$view = $this->getView('tags', 'html');
		
		$view->setModel($model, true);
		$view->display();
	}
	
	public function get_tag_details()
	{
		$user = JFactory::getUser();
		
		if(!$user->authorise('answers.manage', A_APP_NAME))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
		
			$tag_id = $app->input->getInt('tagid');
		
			if($tag_id)
			{
				$tag = $model->get_tag_details($tag_id);
				echo json_encode(array('tag'=>$tag));
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
		
		jexit();
	}

	public function delete_tag()
	{
		$user = JFactory::getUser();
	
		if($user->guest || !$user->authorise('answers.manage', A_APP_NAME))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
	
			$tag_id = $app->input->getInt('tagid', 0);
	
			if($tag_id > 0)
			{
				if($model->delete_tag($tag_id))
				{
					echo json_encode(array('data'=>1));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
	
		jexit();
	}
	
	public function save_tag_details()
	{
		$user = JFactory::getUser();
		
		if(!$user->authorise('answers.manage', A_APP_NAME))
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		} 
		else 
		{
			$app = JFactory::getApplication();
			$model = $this->getModel('answers');
		
			$tag = new stdClass();
			
			$tag->id = $app->input->getInt('tagid', 0);
			$tag->title = $app->input->getString('name', null);
			$tag->alias = $app->input->getString('alias', '');
			$tag->description = $app->input->getString('description', '');
		
			if($tag->id && !empty($tag->title))
			{
				if($model->save_tag_details($tag))
				{
					echo json_encode(array('tag'=>1));
				} 
				else 
				{
					echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
				}
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_MISSING_REQUIRED')));
			}
		}
		
		jexit();
	}
	
	public function get_tags()
	{
		$app = JFactory::getApplication();
		$model = $this->getModel('answers');
		$search = $app->input->getString('search');
		
		if(!empty($search))
		{
			$tags = $model->search_tags($search);
			echo json_encode(array('tags'=>$tags));
		} 
		else 
		{
			echo json_encode(array('tags'=>array()));
		}
				
		jexit();
	}
	
	public function approve_items()
	{
		$app = JFactory::getApplication();
		
		$itemid = CJFunctions::get_active_menu_id(true, 'index.php?option='.A_APP_NAME.'&view=answers');
		$type = $app->input->getInt('type', 0);
		$secret = $app->input->getCmd('secret', null);
		
		if(($type == 1 || $type == 2) && !empty($secret))
		{
			$model = $this->getModel('answers');
			$users_model = $this->getModel('users');
			$return = $model->approve_items(1, $type, $secret);
			$params = JComponentHelper::getParams(A_APP_NAME);
			
			if(!empty($return))
			{
				if($return->created_by > 0)
				{
					if($type == 1)
					{
						if(file_exists(JPATH_ROOT.DS.'components'.DS.'com_cjblog'.DS.'api.php'))
						{
							require_once JPATH_ROOT.DS.'components'.DS.'com_cjblog'.DS.'api.php';
							$my = $users_model->get_user_details($return->created_by);
					
							// trigger badges rule
							CjBlogApi::trigger_badge_rule(
								'com_communityanswers.num_questions', 
								array('num_questions'=>$my->questions, 'ref_id'=>$return->id), 
								$return->created_by);
						}
						
						$qurl = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$return->id.':'.$return->alias.$itemid);
							
						// award points
						AnswersHelper::awardPoints($params, $return->created_by, 1, $return->id, JHtml::link($qurl, $return->title));
							
						// stream activity
						AnswersHelper::stream_activity(1, $params, $return, $return->created_by, $qurl);
						
					} 
					elseif ($type == 2)
					{
						// trigger badges rule
						if(file_exists(JPATH_ROOT.DS.'components'.DS.'com_cjblog'.DS.'api.php'))
						{
							require_once JPATH_ROOT.DS.'components'.DS.'com_cjblog'.DS.'api.php';
							$my = $users_model->get_user_details($return->created_by);
								
							CjBlogApi::trigger_badge_rule(
								'com_communityanswers.num_answers',
								array('num_answers'=>$my->answers, 'ref_id'=>$return->id),
								$return->created_by);
						}
						
						$qurl = JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$return->question_id.':'.$return->alias.$itemid);
						
						// award points first
						AnswersHelper::awardPoints($params, $return->created_by, 2, $return->id, $qurl);
						
						// stream the activity
						$return->answer = $return->description;
						AnswersHelper::stream_activity(2, $params, $return, $return->created_by, $qurl);
					}
				}
				

				// send mail to admins and subscribers
				$sitename = $app->getCfg('sitename');
				$question_link = JHtml::link(
						JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=view&id='.$return->id.':'.$return->alias.$itemid, false, -1),
						$return->title);
				$mail_model = $this->getModel('mail');
				
				if($type == 1)
				{
					$subject = JText::sprintf('EMAIL_NEW_QUESTION_SUB', $sitename);
					$body = JText::sprintf('EMAIL_NEW_QUESTION_CONTENT', $return->username, $return->title, $return->description, $question_link, $sitename);
					$admins = array();
						
					if($params->get('admin_new_question_notification', 0) == 1)
					{
						$admins = $model->get_admin_users($params->get('admin_user_groups', 0));
					}

					$mail_model->add_messages_to_queue(
							2,
							$return->id,
							$return->catid,
							A_APP_NAME.'.newquestion',
							$subject,
							$body,
							$params->get('mail-tpl-newquestion', 'mail-blue.tpl'), 
							$admins);
					
					// send notification to the user that the question is approved.
					$subject = JText::sprintf('EMAIL_USER_QUESTION_APPROVED_SUB', $return->username);
					$body = JText::sprintf('EMAIL_USER_QUESTION_APPROVED_BODY', $return->username, $return->title, $question_link, $sitename);
					$from = $app->getCfg('mailfrom' );
					$fromname = $app->getCfg('fromname' );

					CJFunctions::send_email($from, $fromname, $return->email, $subject, $body, 1);
				} 
				elseif($type == 2)
				{
					$subject = JText::_('EMAIL_NEW_ANSWER_SUB');
					$body = JText::sprintf('EMAIL_NEW_ANSWER_CONTENT', $return->title, $return->description, $question_link, $sitename);
						
					$admins = array();
					
					if($params->get('admin_new_answer_notification', 0) == 1)
					{
						$admins = $model->get_admin_users($params->get('admin_user_groups', 0));
					}
						
					if($return->asker == 0 && $params->get('new_answer_notification', 0) == 1)
					{
						$asker = new stdClass();
						$asker->name = $return->asker_name;
						$asker->email = $return->asker_email;
						$asker->subid = 0;
					
						$admins = array_merge(array($asker), $admins);
					}

					$mail_model->add_messages_to_queue(
							1,
							$return->question_id,
							$return->catid,
							A_APP_NAME.'.newanswer',
							$subject,
							$body,
							$params->get('mail-tpl-newanswer', 'mail-blue.tpl'),
							$admins);
					
					// send notification to the user that the answer is approved.
					$subject = JText::sprintf('EMAIL_USER_ANSWER_APPROVED_SUB', $sitename);
					$body = JText::sprintf('EMAIL_USER_ANSWER_APPROVED_BODY', $return->username, $return->title, $question_link, $sitename);
					$from = $app->getCfg('mailfrom' );
					$fromname = $app->getCfg('fromname' );

					CJFunctions::send_email($from, $fromname, $return->email, $subject, $body, 1);
				}
				
				$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid), JText::_('MSG_POSTS_APPROVED'));
			} 
			else 
			{
				$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid), JText::_('MSG_ERROR_PROCESSING'));
			}
		} 
		else 
		{
			$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid), JText::_('MSG_INVALID_LINK'));
		}
	}
	
	public function disapprove_items()
	{
		$app = JFactory::getApplication();
		
		$itemid = CJFunctions::get_active_menu_id();
		$type = $app->input->getInt('type', 0);
		$secret = $app->input->getCmd('secret', null);
		
		if(($type == 1 || $type == 2) && !empty($secret))
		{
			$model = $this->getModel('answers');
			
			if($model->approve_items(0, $type, $secret))
			{
				$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid), JText::_('MSG_POSTS_DISAPPROVED'));
			} 
			else 
			{
				$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid), JText::_('MSG_ERROR_PROCESSING'));
			}
		} 
		else 
		{
			$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid), JText::_('MSG_INVALID_LINK'));
		}
	}
	
	public function subscribe_category_feed()
	{
		$user = JFactory::getUser();
		
		if(!$user->guest && $user->authorise('answers.subscrcat', A_APP_NAME))
		{
			$model = $this->getModel('answers');
			$app = JFactory::getApplication();
			
			$catid = $app->input->getInt('id', 0);
			$type = $catid > 0 ? 2 : 3;
			
			if($model->subscribe($type, $user->id, $catid))
			{
				echo json_encode(array('error'=>($type == 2 ? JText::_('MSG_CATEGORY_SUBSCRIBED') : JText::_('MSG_ALL_CATEGORIES_SUBSCRIBED'))));
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
			}
		} 
		else 
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		}
		
		jexit();
	}
	
	public function unsubscribe_category_feed()
	{
		$user = JFactory::getUser();
		
		if(!$user->guest && $user->authorise('answers.subscrcat', A_APP_NAME))
		{
			$model = $this->getModel('answers');
			$app = JFactory::getApplication();
			
			$catid = $app->input->getInt('id', 0);
			$type = $catid > 0 ? 2 : 3;
			
			if($model->unsubscribe($type, $user->id, $catid))
			{
				echo json_encode(array('error'=>($type == 2 ? JText::_('MSG_CATEGORY_UNSUBSCRIBED') : JText::_('MSG_ALL_CATEGORIES_UNSUBSCRIBED'))));
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
			}
		} 
		else 
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		}
		
		jexit();
	}
	
	public function subscribe_question_feed()
	{
		$user = JFactory::getUser();
		
		if(!$user->guest && $user->authorise('answers.subscrqn', A_APP_NAME))
		{
			$model = $this->getModel('answers');
			$app = JFactory::getApplication();
			
			$id = $app->input->getInt('id', 0);
			
			if($model->subscribe(1, $user->id, $id))
			{
				echo json_encode(array('error'=>JText::_('MSG_QUESTION_SUBSCRIBED')));
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
			}
		} 
		else 
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		}
		
		jexit();
	}
	
	public function unsubscribe_question_feed()
	{
		$user = JFactory::getUser();
		
		if(!$user->guest && $user->authorise('answers.subscrqn', A_APP_NAME))
		{
			$model = $this->getModel('answers');
			$app = JFactory::getApplication();
			
			$id = $app->input->getInt('id', 0);
			
			if($model->unsubscribe(1, $user->id, $id))
			{
				echo json_encode(array('error'=>JText::_('MSG_QUESTION_UNSUBSCRIBED')));
			} 
			else 
			{
				echo json_encode(array('error'=>JText::_('MSG_ERROR_PROCESSING')));
			}
		} 
		else 
		{
			echo json_encode(array('error'=>JText::_('JERROR_ALERTNOAUTHOR')));
		}
		
		jexit();
	}
	
	public function get_rss_feed()
	{
		$app = JFactory::getApplication();
		$view = $this->getView('answers', 'feed');
		$model = $this->getModel('answers');
		
		$app->input->set('format', 'feed');
		$app->input->set('type', 'rss');
		$view->setModel($model, true);
		$view->display();
	}
	
	public function get_user_stats_widget()
	{
		$app = JFactory::getApplication();
		$model = $this->getModel('users');
		
		$userid = $app->input->getInt('id', 0);
		$user = $model->get_user_details($userid);
		
		JFactory::getDocument()->addStyleSheet(CA_MEDIA_URI.'/css/cj.answers.widget.min.css');
		
		if(!empty($user))
		{
			$params = JComponentHelper::getParams(A_APP_NAME);
			$display_name = $params->get('user_display_name', 'name');
			$avatar = CJFunctions::get_user_avatar($params->get('user_avatar', 'none'), $userid, $display_name, 48);
			
			$html = '<div class="answer-stats-widget-container">';
				$html = '<div class="answer-stats-widget">';
					$html .= '<div class="answer-widget-avatar-wrapper clearfix">';
						$html .= '<div class="answer-widget-avatar">'.$avatar.'</div>';
						$html .= '<div class="answer-widget-username">'.$user->$display_name.'</div>';
					$html .= '</div>';
					$html .= '<hr/>';
					$html .= '<div class="answer-widget-stats-wrapper clearfix">';
						$html .= '<div class="answer-widget-stats-content">';
							$html .= '<h1>'.$user->questions.'</h1>';
							$html .= '<div class="answers-widget-stats-label">'.($user->questions == 1 ? JText::_('LBL_QUESTION') : JText::_('LBL_QUESTIONS')).'</div>';
						$html .= '</div>';
						$html .= '<div class="answer-widget-stats-content">';
							$html .= '<h1>'.$user->answers.'</h1>';
							$html .= '<div class="answers-widget-stats-label">'.($user->answers == 1 ? JText::_('LBL_ANSWER') : JText::_('LBL_ANSWERS')).'</div>';
						$html .= '</div>';
						$html .= '<div class="answer-widget-stats-content">';
							$html .= '<h1>'.$user->best_answers.'</h1>';
							$html .= '<div class="answers-widget-stats-label">'.($user->best_answers == 1 ? JText::_('LBL_BEST_ANSWER') : JText::_('LBL_BEST_ANSWERS')).'</div>';
						$html .= '</div>';
					$html .= '</div>';
				$html .= '</div>';
			$html .= '</div>';
				
			echo $html;
		} 
		else 
		{
			echo JText::_('MSG_NO_USER_FOUND');
		}
		
// 		jexit();
	}

	public function get_activities()
	{
	
		$user = JFactory::getUser();
		
		if($user->guest) 
		{
			$itemid = CJFunctions::get_active_menu_id();
			$redirect_url = base64_encode(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers&task=activities'.$itemid));
			$this->setRedirect(CJFunctions::get_login_url($redirect_url, $itemid), JText::_('MSG_USER_LOGIN'));
		}
		else 
		{
			$model = $this->getModel('activities');
			$view = $this->getView('activities', 'html');
			$view->setModel($model, true);
			$view->display();
		}
	}
	
	public function ajx_save_bounty()
	{
		$this->save_bounty(true);
	}

	public function save_bounty($jexit = true, $bounty = null)
	{
		$app = JFactory::getApplication();
		$model = $this->getModel('answers');
		$post = $app->input->post;

		if(empty($bounty))
		{
			$bounty = new stdClass();
			$bounty->question_id = $app->input->getInt('id', 0);
			$bounty->bounty_points = $post->getInt('bounty-points', 0);
			$bounty->expiry_date = $post->getString('bounty-expiry', null);
		}
		
		$message = null;
		
		if($bounty->question_id > 0 && $bounty->bounty_points >  0)
		{
			$rc = $model->save_bounty($bounty);

			if($rc == 0)
			{
				$message = JText::sprintf('MSG_BOUNTY_RUNNING', $bounty->bounty_points);
			} 
			elseif($rc == 1)
			{
				$message = JText::sprintf('MSG_BOUNTY_NOT_ENOUGH_POINTS', $bounty->bounty_points);
			} 
			elseif($rc == 2)
			{
				$message = JText::sprintf('MSG_BOUNTY_NOT_POINTS_LESS_THAN_EXISTING', $bounty->bounty_points);
			} 
			elseif($rc == -1)
			{
				// No message, silenty exit
			} 
			else 
			{
				$message = JText::_('MSG_ERROR_PROCESSING');
			}
			
			if(!empty($message))
			{
				if($jexit) 
				{
					echo json_encode(array((($rc == 0) ? 'info' : 'error') => $message)); 
				} 
				else 
				{
					$app->enqueueMessage($message);
				}
			}
		}
		
		if($jexit) jexit();
	}
	
	public function refund_bounty_points()
	{
		$model = $this->getModel('answers');
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$params = JComponentHelper::getParams(A_APP_NAME);
		
		if($user->guest || $params->get('enable_bounties', '1') != '1') 
		{
			$app->enqueueMessage(JText::_('MSG_ERROR_PROCESSING'));
			return;
		}
		
		$id = $app->input->getInt('id', 0);
		$rc = $model->refund_bounty_points($id);
		
		switch ($rc)
		{
			case 1:
				
				$app->enqueueMessage(JText::_('MSG_BOUNTY_POINTS_REFUNDED'));
				break;
				
			case 2:
				$app->enqueueMessage(JText::_('MSG_BOUNTY_IS_RUNNING'));
				break;
				
			case 3:
				$app->enqueueMessage(JText::_('MSG_BOUNTY_CANNOT_BE_REFUNDED'));
				break;
				
			case 4:
				$app->enqueueMessage(JText::_('MSG_BOUNTY_ALREADY_REFUNDED'));
				break;
				
			default:
				$app->enqueueMessage(JText::_('MSG_ERROR_PROCESSING'));
		}
		
		$itemid = CJFunctions::get_active_menu_id();
		$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid));
	}
	
	public function get_leaderboard()
	{
		$model = $this->getModel('users');
		$view = $this->getView('users', 'html');
		$view->setModel($model, true);
		$view->assign('action', 'leaderboard');
		$view->display();
	}
}