<?php
/**
 * @version		$Id: user.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

jimport('joomla.application.component.controller');

class CommunityAnswersControllerUser extends JControllerLegacy {
	
	function __construct(){
		
		parent::__construct();
		
		$this->registerDefaultTask('get_questions');
		$this->registerTask('answers', 'get_answers');
		$this->registerTask('unsubscribe', 'unsubscribe_all');
		$this->registerTask('subscriptions', 'get_user_subscriptions');
	}
	
	public function get_questions(){
		
		$model = $this->getModel('answers');
		$view = $this->getView('user', 'html');
		
		$view->setModel($model, true);
		$view->assign('action', 'questions');
		$view->display();
	}
	
	public function get_answers(){

		$model = $this->getModel('users');
		$view = $this->getView('user', 'html');
		
		$view->setModel($model, true);
		$view->assign('action', 'answers');
		$view->display();
	}
	
	public function get_user_subscriptions(){

		$model = $this->getModel('users');
		$view = $this->getView('user', 'html');
		
		$view->setModel($model, true);
		$view->assign('action', 'subscriptions');
		$view->display();
	}
	
	public function unsubscribe_all(){
		
		$app = JFactory::getApplication();
		$model = $this->getModel('users');
		
		$subid = $app->input->get('subid');
		$itemid = CJFunctions::get_active_menu_id(true, 'index.php?option='.A_APP_NAME.'&view=answers');
		
		if(!empty($subid) && $model->delete_subscriptions($subid)){
			
			$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid), JText::_('MSG_UNSUBSCRIBED_ALL'));
		} else {
			
			$this->setRedirect(JRoute::_('index.php?option='.A_APP_NAME.'&view=answers'.$itemid), JText::_('MSG_UNSUBSCRIBED_ALL_ERROR'));
		}
	}
}