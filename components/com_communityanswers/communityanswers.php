<?php
/**
 * @version		$Id: communityanswers.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die();

// CJLib includes
$cjlib = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_cjlib'.DIRECTORY_SEPARATOR.'framework.php';

if(file_exists($cjlib)){

	require_once $cjlib;
}else{

	die('CJLib (CoreJoomla API Library) component not found. Please download and install it to continue.');
}

CJLib::import('corejoomla.framework.core');
CJLib::import('corejoomla.nestedtree.core');
CJLib::import('corejoomla.template.core');

require_once JPATH_COMPONENT.DS.'helpers'.DS.'constants.php';
require_once JPATH_COMPONENT.DS.'helpers'.DS.'helper.php';

$app = JFactory::getApplication();
$params = JComponentHelper::getParams(A_APP_NAME);
$view = $app->input->getCmd('view', 'answers');
$task = $app->input->getCmd('task', '');

if( JFile::exists( JPATH_COMPONENT.DS.'controllers'.DS.$view.'.php' ) ){
	
    require_once (JPATH_COMPONENT.DS.'controllers'.DS.$view.'.php');
}else{
	
	return CJFunctions::throw_error('View '. JString::ucfirst($view) . ' not found!', 500);
}

if($task != 'feed'){

	if(($task == 'ask' || $task == 'view') && !JFactory::getUser()->guest && $params->get('enable_bounties', 1) == '1'){
		
		JHtml::_('behavior.framework');
	}
	
	if($params->get('disable_jquery_bootstrap', 0) == 0){
		
		if($params->get('disable_bootstrap', 0) == 0){
			
			CJLib::import('corejoomla.ui.bootstrap', true, false, true);
		}
		
		CJFunctions::load_jquery(array('libs'=>array('fontawesome')));
	}
	
	CJFunctions::load_component_language(A_APP_NAME);
	
	$document = JFactory::getDocument();
	CJFunctions::add_css_to_document($document, CJLIB_URI.'/framework/assets/cj.framework.css', true);
	CJFunctions::add_css_to_document($document, CA_MEDIA_URI.'/css/cj.answers.min.css', true);
	CJFunctions::add_script(CA_MEDIA_URI.'/js/cj.answers.min.js', true);
	
	// send out the messages
	if($view == 'answers'){
		
		// simulate the dummy cronjob
		require_once CJLIB_PATH.'/framework/virtualcron.php';
		$vcron = new virtualcron(30, 'cavcron.txt');
		
		if ($vcron->allowAction())
		{
			JLoader::import('joomla.application.component.model');
			JLoader::import('answers', JPATH_COMPONENT.'/models');

			$model = JModelLegacy::getInstance( 'answers', 'CommunityAnswersModel' );
			$model->check_expired_bounties();
		}
		
		CJFunctions::send_messages_from_queue();
	} 
}

$classname = 'CommunityAnswersController'.JString::ucfirst($view);
$controller = new $classname( );

if(empty($task)){

	$menu = $app->getMenu();
	$active = $menu->getActive();
	$menuParams = new JRegistry;

	if ($active) {

		$menuParams->loadString($active->params);
	}

	$task = $menuParams->get('menutask', '');
}

$controller->execute( $task );
$controller->redirect();
?>