<?php
/**
 * @version		$Id: mail.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

jimport('joomla.application.component.modelitem');

class CommunityAnswersModelMail extends JModelLegacy {

	function __construct() {

		parent::__construct ();
	}
	
	function add_messages_to_queue($mail_type, $asset_id, $catid, $asset_name, $subject, $body, $template, $custom_users = null){
		
		$where = '';
		$params = JComponentHelper::getParams(A_APP_NAME);
		
		switch ($mail_type){
			
			case 1: // new answer, question subscriptions
				
				$where = '
						(s.subscription_id = '.$asset_id.' and subscription_type = 1) or
						(s.subscription_id = '.$catid.' and subscription_type = 2) or 
						subscription_type = 3';
				break;
				
			case 2: // new question, category subscriptions
				$where = '
						(s.subscription_id = '.$catid.' and subscription_type = 2) or 
						subscription_type = 3';
				
				break;
		}
		
		$subscribers = array();
		
		if($mail_type != 1 || $params->get('new_answer_notification', 0) == 1){
			
			$query = '
					select 
						u.name, u.email, au.subid
					from 
						#__answers_subscribes s
					left join
						#__answers_users au on s.subscriber_id = au.id
					left join
						#__users u on s.subscriber_id = u.id 
					where 
						'.$where;
			
			$this->_db->setQuery($query);
			$subscribers = $this->_db->loadObjectList();
		}
		
		if(!empty($custom_users)){
			
			$subscribers = array_merge($custom_users, $subscribers);
		}
		
		if(!empty($subscribers)){

			$app = JFactory::getApplication();
			$preferences_link = JHtml::link(JRoute::_('index.php?option='.A_APP_NAME.'&view=user&task=subscriptions', false, -1), JText::_('LBL_PREFERENCES'));
			$site_name = $app->getCfg('sitename');
			$site_url = JUri::root();
			
			$msg_params = json_encode(array(
					'template'=>$template, 
					'placeholders'=>array(
							'{preferences}'=>$preferences_link,
							'{sitename}'=>$site_name,
							'{siteurl}'=>JUri::root(),
							'{title}'=>JText::_('EMAIL_TITLE')
							)));
			$created = JFactory::getDate()->toSql();
			
			$query = '
				insert into 
					'.T_CJ_MESSAGES.'(asset_id, asset_name, subject, description, params, created) values 
				(
					'.$asset_id.',
					'.$this->_db->quote($asset_name).',
					'.$this->_db->quote($subject).',
					'.$this->_db->quote($body).',
					'.$this->_db->quote($msg_params).',
					'.$this->_db->quote($created).'
				)';
			
			$this->_db->setQuery($query);
			
			if($this->_db->query()){
				
				$messageid = $this->_db->insertid();
				
				if($messageid > 0){
					
					foreach ($subscribers as $subscriber){

						$unsubscribe_link = JHtml::link(
								JRoute::_('index.php?option='.A_APP_NAME.'&view=user&task=unsubscribe&subid='.$subscriber->subid, false, -1),
								JText::_('LBL_UNSUBSCRIBE'));
						
						if($subscriber->subid <= 0) $unsubscribe_link = '';
						
						$params = json_encode(array('placeholders'=>array('{name}'=>$subscriber->name, '{unsubscribe}'=>$unsubscribe_link)));
						
						$inserts[] = '('.$this->_db->quote($subscriber->email).','.$messageid.', 0, 1,'.$this->_db->quote($params).','.$this->_db->quote($created).')';
					}
					
					$query = 'insert into '.T_CJ_MESSAGEQUEUE.'(to_addr, message_id, `status`, html, params, created) values '.implode(',', $inserts);
					$this->_db->setQuery($query);
					
					if($this->_db->query()){
						
						$sent = $this->_db->getAffectedRows();
						
						return $sent;
					}
				}
			}
		}
		
		return false;
	}
	
	public function delete_subscriptions($subid){
		
		$query = 'delete from #__answers_subscribes where user_id = (select id from #__answers_users where subid = '.$subid.')';
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			return true;
		}
		
		return false;
	}
}