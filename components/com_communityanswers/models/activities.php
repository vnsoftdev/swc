<?php
/**
 * @version		$Id: activities.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2013 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class CommunityAnswersModelActivities extends JModelList {

	public function __construct($config = array()){
		
		if (empty($config['filter_fields'])) {
			
			$config['filter_fields'] = array(
					'id', 'a.id',
					'title', 'a.title',
					'created', 'a.created',
					'created_by', 'a.created_by',
					'item_id', 'a.item_id',
					'activity_type', 'a.activity_type'
			);
		}
		
		parent::__construct($config);
	}
	
	protected function populateState($ordering = null, $direction = 'desc') {
		
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$params = JComponentHelper::getParams(A_APP_NAME);

		$limit = $params->get('list_length', $app->getCfg('list_limit', 20), 'uint');
		$this->setState('list.limit', $limit);
		
		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
	
		$published = $this->getUserStateFromRequest($this->context.'.filter.published', 'filter_published', '1');
		$this->setState('filter.published', $published);
		
		$user_id = $this->getUserStateFromRequest($this->context.'.filter.authorid', 'filter_authorid', $user->id);
		$this->setState('filter.author_id', $user_id);

		$ordering = 'a.created';
		
		// List state information.
		parent::populateState($ordering, $direction);
		
		$limitstart = $app->input->get('start', 0, 'uint');
		$this->setState('list.start', $limitstart);
	}
	
	protected function getStoreId($id = ''){
		
		// Compile the store id.
		$id	.= ':'.$this->getState('filter.search');
		$id	.= ':'.$this->getState('filter.published');
		$id	.= ':'.$this->getState('filter.author_id');
	
		return parent::getStoreId($id);
	}
	
	protected function _buildQuery(){
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		
		$query->select('a.id, a.title, a.description, a.created, a.url, a.activity_type');
		$query->from('#__answers_activities as a');
		
		return $query;
	}
	
	protected function _buildWhere(&$query) {
		
		$query->where('a.published = 1');
		
		$authorId = $this->getState('filter.author_id');
		if(is_numeric($authorId)){
			
			$query->where('a.created_by = ' . (int) $authorId);
		}
		
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			
			if (stripos($search, 'id:') === 0) {
				
				$query->where('a.id = '.(int) substr($search, 3));
			} else {
				
				$search = $db->Quote('%'.$db->escape($search, true).'%');
				$query->where('(a.title LIKE '.$search.' OR a.description LIKE '.$search.')');
			}
		}
	}
	
	protected function getListQuery() {
		
		$db = JFactory::getDbo();
		
		$orderCol	= $this->state->get('list.ordering', 'a.created');
		$orderDirn	= $this->state->get('list.direction', 'desc');
		
		$query = $this->_buildQuery();
		$this->_buildWhere($query);
		
		if(!empty($orderCol)){
		
			$query->order($db->escape($orderCol.' '.$orderDirn));
		}
		
		return $query;
	}
	
	public function save_activity($activity){
	
		$db = JFactory::getDbo();
		$users = array();
	
		try {

			switch ($activity->type){
				
				case 1: // new answer, notification should be added to question poster only
					
					$query = $db->getQuery(true);
					$query
						->select('created_by')
						->from('#__answers_questions')
						->where('id = '.$activity->question_id);
					
					$db->setQuery($query);
					$users = $db->loadColumn();
					$users = !empty($users) ? $users : array();
					
					break;
					
				case 2: // new comment, notification should be added to answerer and all commenters of the answer and question submitter
					
					$query = $db->getQuery(true);
					$query
						->select('created_by')
						->from('#__answers_responses')
						->where('id = '.$activity->answer_id.' or parent_id = '.$activity->answer_id);
					
					$db->setQuery($query);
					$users = $db->loadColumn();
					
					$users = !empty($users) ? $users : array();
					$users[] = $activity->asker_id;
					
					break;
					
				case 3: // accepted answer, notification should be sent to the answerer
					
					$users[] = $activity->answerer;
					break;
					
				case 4: // voted up, notification should be sent to answerer
					
					$users[] = $activity->answerer;
					break;
					
				case 5: //voted down, notification should be sent to answerer
					
					$users[] = $activity->answerer;
					break;
					
				case 6: // new badge, notification should be added only to badge owner
					
					$users[] = $activity->owner;
					
					break;
			}
				
			$query = $db->getQuery(true);
			$query->insert('#__answers_activities')->columns('title, description, activity_type, created_by, item_id, created, url, published');
			
			$users = array_unique($users);

			foreach ($users as $userid){

				$query->values(
						$db->q($activity->title).','.
						$db->q($activity->description).','.
						$db->q($activity->type).','.
						$db->q($userid).','.
						$db->q($activity->item_id).','.
						$db->q(JFactory::getDate()->toSql()).','.
						$db->q($activity->url).','.
						$db->q(1)
				);
			}
				
			$db->setQuery($query);
			$db->query();
		} catch (Exception $e){
				
			return false;
		}
	
		return true;
	}
	
	public function get_new_activity_count(){
		
		$db = JFactory::getDbo();
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		
		if($user->guest) return false;

		$last_notif_date = $app->getUserState('answers_last_viewed');
		
		if(!$last_notif_date) {

			$last_notif_date = JFactory::getDate()->toSql();
			$query = $db->getQuery(true);
			$query->update('#__answers_users')->set('last_visit = '.$db->q($last_notif_date))->where('id = '.$user->id); 
			
			try {
				
				$db->setQuery($query);
				$db->query();
			} catch (Exception $e) {
				
				return false;
			}
			
			$app->setUserState('answers_last_viewed', $last_notif_date);
		}
		
		$query = $db->getQuery(true);
		$query
			->select('count(*)')
			->from('#__answers_activities')
			->where('created_by = '.$user->id.' and created > '.$db->q($last_notif_date));
		
		try {
			
			$db->setQuery($query);
			$count = (int)$db->loadResult();
			
			return $count;
		} catch(Exception $e) {
			
			return false;
		}
		
		return false;
	}
}
?>