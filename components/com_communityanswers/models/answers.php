<?php
/**
 * @version		$Id: answers.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

jimport('joomla.application.component.modelitem');

class CommunityAnswersModelAnswers extends JModelLegacy {

	function __construct() {

		parent::__construct ();
	}

	function get_category_flat_list($parent = 0) {
		 
		$tree = new CjNestedTree($this->_db, '#__answers_categories');
		 
		return $tree->get_children($parent, true);
	}
	
	function get_categories() {
		 
		$tree = new CjNestedTree($this->_db, '#__answers_categories');
		 
		return $tree->get_tree();
	}
	
	function get_category_tree(){
		 
		$tree = new CjNestedTree($this->_db, '#__answers_categories');
		return $tree->get_selectables();
	}
	
	function get_category($id){
		 
		$tree = new CjNestedTree($this->_db, '#__answers_categories');
		 
		return $tree->get_node($id);
	}
	
	function get_breadcrumbs($id){
		 
		$tree = new CjNestedTree($this->_db, '#__answers_categories');
		 
		return $tree->get_path($id);
	}
	
	public function get_questions($action = 1, $options = array(), $params = null){
		
		$user = JFactory::getUser();
		
		$return = array();
		$wheres = array();
		if(!empty($options['userid'])){
			
			if(is_array($options['userid'])){
			
				$wheres[] = 'a.created_by in ('.implode(',', $options['userid']).')';
			} else if($options['userid'] > 0){
					
				$wheres[] = 'a.created_by = '.$options['userid'];
			}
		}
		
		
		if(!empty($options['catid'])){
			
			if(is_array($options['catid'])){
				
				$wheres[] = 'a.catid in ('.implode(',', $options['catid']).')';
			} else if($options['catid'] > 0){
				
// 				$query = 'select nleft, nright from #__answers_categories where id = '.((int)$options['catid']);
// 				$this->_db->setQuery($query);
// 				$lftrgt = $this->_db->loadObject();
				
// 				if(!empty($lftrgt)){
				
// 					$wheres[] = 'c.nleft >= '.$lftrgt->nleft.' and c.nright <= '.$lftrgt->nright;
// 				}
				
				$wheres[] = 'a.catid = '.$options['catid'];
			}
		}
		
		$wheres[] = 'a.published = 1';
		
		switch ($action){
			
			case 1: // latest
				
				break;
				
			case 2: // open
				
				$wheres[] = 'a.accepted_answer = 0';
				
				break;
				
			case 3: // solved
				
				$wheres[] = 'a.accepted_answer > 0';
				
				break;
				
			case 4: // popular
				
				break;
				
			case 5: //most answered
				
				break;
				
			case 6: // tags
				
				$wheres[] = 'a.id in (select item_id from #__answers_tagmap where tag_id = '.$options['tag_id'].')';
				
				break;
				
			case 7: // search
				
				$search_params = $options['search_params'];
				
				if($search_params['type'] == 1){ // search unsolved questions
					
					$wheres[] = 'a.accepted_answer = 0';
				} else if($search_params['type'] == 2) { // search solved questions
					
					$wheres[] = 'a.accepted_answer > 0';
				} // else search all questions
				
				if(!empty($search_params['q'])){
					
					$keywords = explode(' ', $search_params['q']);
					$stopwords = ",a's,accordingly,again,allows,also,amongst,anybody,anyways,appropriate,aside,available,because,before,below,between,by,can't,certain,com,consider,corresponding,definitely,different,don't,each,else,et,everybody,exactly,fifth,follows,four,gets,goes,greetings,has,he,her,herein,him,how,i'm,immediate,indicate,instead,it,itself,know,later,lest,likely,ltd,me,more,must,nd,needs,next,none,nothing,of,okay,ones,others,ourselves,own,placed,probably,rather,regarding,right,saying,seeing,seen,serious,she,so,something,soon,still,t's,th,that,theirs,there,therein,they'd,third,though,thus,toward,try,under,unto,used,value,vs,way,we've,weren't,whence,whereas,whether,who's,why,within,wouldn't,you'll,yourself ,able,across,against,almost,although,an,anyhow,anywhere,are,ask,away,become,beforehand,beside,beyond,c'mon,cannot,certainly,come,considering,could,described,do,done,edu,elsewhere,etc,everyone,example,first,for,from,getting,going,had,hasn't,he's,here,hereupon,himself,howbeit,i've,in,indicated,into,it'd,just,known,latter,let,little,mainly,mean,moreover,my,near,neither,nine,noone,novel,off,old,only,otherwise,out,particular,please,provides,rd,regardless,said,says,seem,self,seriously,should,some,sometime,sorry,sub,take,than,that's,them,there's,theres,they'll,this,three,to,towards,trying,unfortunately,up,useful,various,want,we,welcome,what,whenever,whereby,which,whoever,will,without,yes,you're,yourselves,about,actually,ain't,alone,always,and,anyone,apart,aren't,asking,awfully,becomes,behind,besides,both,c's,cant,changes,comes,contain,couldn't,despite,does,down,eg,enough,even,everything,except,five,former,further,given,gone,hadn't,have,hello,here's,hers,his,however,ie,inasmuch,indicates,inward,it'll,keep,knows,latterly,let's,look,many,meanwhile,most,myself,nearly,never,no,nor,now,often,on,onto,ought,outside,particularly,plus,que,re,regards,same,second,seemed,selves,seven,shouldn't,somebody,sometimes,specified,such,taken,thank,thats,themselves,thereafter,thereupon,they're,thorough,through,together,tried,twice,unless,upon,uses,very,wants,we'd,well,what's,where,wherein,while,whole,willing,won't,yet,you've,zero,above,after,all,along,am,another,anything,appear,around,associated,be,becoming,being,best,brief,came,cause,clearly,concerning,containing,course,did,doesn't,downwards,eight,entirely,ever,everywhere,far,followed,formerly,furthermore,gives,got,happens,haven't,help,hereafter,herself,hither,i'd,if,inc,inner,is,it's,keeps,last,least,like,looking,may,merely,mostly,name,necessary,nevertheless,nobody,normally,nowhere,oh,once,or,our,over,per,possible,quite,really,relatively,saw,secondly,seeming,sensible,several,since,somehow,somewhat,specify,sup,tell,thanks,the,then,thereby,these,they've,thoroughly,throughout,too,tries,two,unlikely,us,using,via,was,we'll,went,whatever,where's,whereupon,whither,whom,wish,wonder,you,your,according,afterwards,allow,already,among,any,anyway,appreciate,as,at,became,been,believe,better,but,can,causes,co,consequently,contains,currently,didn't,doing,during,either,especially,every,ex,few,following,forth,get,go,gotten,hardly,having,hence,hereby,hi,hopefully,i'll,ignored,indeed,insofar,isn't,its,kept,lately,less,liked,looks,maybe,might,much,namely,need,new,non,not,obviously,ok,one,other,ours,overall,perhaps,presumably,qv,reasonably,respectively,say,see,seems,sent,shall,six,someone,somewhere,specifying,sure,tends,thanx,their,thence,therefore,they,think,those,thru,took,truly,un,until,use,usually,viz,wasn't,we're,were,when,whereafter,wherever,who,whose,with,would,you'd,yours,";
					$wheres2 = array();
					
					foreach ($keywords as $keyword){
						
						$keyword = preg_replace('/[^\p{L}\p{N}\s]/u', '', $keyword);
						
						if(strlen($keyword) > 2 && strpos($stopwords, ','.$keyword.',') === false){
							
							if($search_params['qt'] == 1){ // search titles and description
								
								$wheres2[] = '
										a.title like \'%'.$this->_db->escape($keyword).'%\' or 
										a.description like \'%'.$this->_db->escape($keyword).'%\'';
							} else { // titles only
								
								$wheres2[] = 'a.title like \'%'.$this->_db->escape($keyword).'%\'';
							}
						}
					}
					
					if(!empty($wheres2)){
						
						$wheres[] = $search_params['all'] == 1 ? '('.implode(') and (', $wheres2).')' : '('.implode(') or (', $wheres2).')';
					}
				}
				
				if(!empty($search_params['u'])){
					
					if($search_params['m'] == 0){
						
						$wheres[] = 'u1.username like \'%'.$this->_db->escape($search_params['u']).'%\'';
					} else {
						
						$wheres[] = 'u1.username = '.$this->_db->quote($search_params['u']);
					}
				}
				
				break;
				
			case 8: // user questions
				
				$wheres[] = 'a.created_by = '.$options['userid'];
				
				break;
		}
		
		if(!empty($options['exclude_ids'])) {
			
			$wheres[] = 'a.id not in ('.implode(',', $options['exclude_ids']).')';
		}
		
		$where_clause = $this->_get_permissions_where_clause(JAccess::getGroupsByUser($user->id), 'c.permission_view');
		if($where_clause) $wheres[] = $where_clause;
		
		$where = '('.implode(' ) and (', $wheres).')';
		
		$order = !empty($options['order']) ? $options['order'] : 'a.created';
		$order_dir = !empty($options['order_dir']) ? $options['order_dir'] : 'desc';
		$limitstart = !empty($options['limitstart']) ? $options['limitstart'] : 0;
		$limit = !empty($options['limit']) ? $options['limit'] : 20;
		
		$query = '
				select
					a.id, a.title, a.alias, a.description, a.created, a.accepted_answer, a.featured, a.answers, a.hits, a.published, a.modified, a.ip_address,
					b.bounty_points as bounty, b.expiry_date as expiry, b.state as bounty_state,
					c.id as catid, c.title as category_title, c.alias as category_alias,
					u1.id as created_by, u1.username as created_by_alias,
					u2.id as modified_by, u2.name as modified_by_name, u2.username as modified_by_username,
					case when a.created_by > 0 then u1.email else a.email end as email,
					case when a.created_by > 0 then u1.'.$params->get('user_display_name', 'name').' else a.user_name end as username
				from
					#__answers_questions as a
				left join
					#__answers_categories as c on a.catid = c.id
				left join
					#__answers_bounties as b on b.question_id = a.id
				left join
					#__users as u1 on a.created_by = u1.id
				left join
					#__users as u2 on a.modified_by = u2.id
				where
					'.$where.'
				order by
					'.$order.' '.$order_dir;

		$this->_db->setQuery($query, $limitstart, $limit);
		$questions = $this->_db->loadObjectList('id');

		/************ pagination *****************/
		$query = '
				select 
					count(*) 
				from 
					#__answers_questions as a 
				left join
					#__answers_categories c on a.catid = c.id
				left join 
					#__users as u1 on a.created_by = u1.id
				where 
					'.$where;
		
		jimport('joomla.html.pagination');
		$this->_db->setQuery($query);
		$total = $this->_db->loadResult();
		
		$return['pagination'] = new JPagination( $total, $limitstart, $limit );
		/************ pagination *****************/
		
		if(!empty($questions)){
			
			$ids = array();
			
			foreach($questions as &$question){
				
				$ids[] = $question->id;
				$question->tags = array();
			}
			
			$tags = $this->get_tags_by_itemids($ids);
			
			if(!empty($tags)){
				
				foreach($tags as $tag){
					
					if(array_key_exists($tag->item_id, $questions)){
						
						$questions[$tag->item_id]->tags[] = $tag;
					}
				}
			}
		}
		
		$return['questions'] = $questions;
		$return['state'] = array('order'=>$order, 'order_dir'=>$order_dir);
		
		return $return;
	}
	
	public function get_question_details($id, $options = array(), $params = array()){
		
		$user = JFactory::getUser();
		$wheres = array();
		$wheres[] = 'a.id = '.$id.' and a.published = 1';
		
		$where_clause = $this->_get_permissions_where_clause(JAccess::getGroupsByUser($user->id), 'c.permission_view');
		if($where_clause) $wheres[] = $where_clause;
		
		$where = '('.implode(' ) and (', $wheres).')';
		
		$query = '
				select
					a.id, a.title, a.alias, a.description, a.answers, a.hits, a.published, a.created, a.accepted_answer, a.featured, a.ip_address,
					c.id as catid, c.title as category_title, c.alias as category_alias, c.nleft, c.nright,
					case when a.created_by > 0 then u.id else 0 end as created_by, 
					case when a.created_by > 0 then u.'.$params->get('user_display_name', 'name').' else a.user_name end as username,
					case when a.created_by > 0 then u.email else a.email end as email
				from
					#__answers_questions a
				left join
					#__answers_categories c on a.catid = c.id
				left join
					#__users u on a.created_by = u.id
				where
					'.$where;
		
		$this->_db->setQuery($query);
		$question = $this->_db->loadObject();
		
		if(empty($question)) return false;
		
		$question->replies = array();
		if($options['get_tags']) $question->tags = $this->get_tags_by_itemids(array($question->id));
		if($options['get_answers']) $question->responses = $this->get_responses($question, $params);
		if($options['trigger_hit']) $this->hit($question->id);
		if(isset($options['get_attachments']) && $options['get_attachments']) $this->populate_attachments($question);
		if(isset($options['get_bounty']) && isset($options['get_bounty'])) $question->bounty = $this->get_bounty_of_question($question->id);

		$question->authorize_answer = true;
		
		if(!$user->guest && !empty($question->responses) && !$user->authorise('answers.multianswer', A_APP_NAME)){
			
			foreach($question->responses as $response){
				
				if($response->created_by == $user->id){
					
					$question->authorize_answer = false;
				}
			}
		}
		
		return $question;
	}
	
	public function get_bounty_of_question($qid){
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$params = JComponentHelper::getParams(A_APP_NAME);
		$username = $params->get('user_display_name', 'name');
		$now = JFactory::getDate()->toSql();
		
		$query
			->select('a.id, a.question_id, a.bounty_points, a.created_by, a.created, a.expiry_date, a.state')
			->select('u.'.$username.' as username')
			->from('#__answers_bounties as a')
			->join('left', '#__users as u on a.created_by = u.id')
			->where('a.question_id = '.$qid);
		
		$db->setQuery($query);
		$bounty = $db->loadObject();
		
		return $bounty;
	}
	
	public function save_bounty(&$bounty){
		
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
		$app = JFactory::getApplication();
		
		// check if the bounty system is enabled or not.
		$params = JComponentHelper::getParams(A_APP_NAME);
		if($params->get('enable_bounties', 1) != '1' || $bounty->bounty_points == 0) return -1;
		
		// calculate the expiry date
		if(!empty($bounty->expiry_date) && $bounty->expiry_date != '0000-00-00 00:00:00'){
		
			$expiry = JFactory::getDate($bounty->expiry_date, $app->getCfg('offset'));
			$expiry->setTimezone(new DateTimeZone('UTC'));
			$bounty->expiry_date = $expiry->toSql();
		}else {
		
			$bounty->expiry_date = $db->getNullDate();
		}
		
		// now check if the points are greater than already available.
		$existing = $this->get_bounty_of_question($bounty->question_id);

		if(empty($existing) || $existing->bounty_points == 0){

			$bounty->created = JFactory::getDate()->toSql();
			$bounty->created_by = $user->id;
			$bounty->state = 1;

			// First check if user has enough points.
			$user_points = CJFunctions::get_user_points($params->get('points_system', 'none'), $bounty->created_by);
			if($user_points < $bounty->bounty_points) return 1;

			try{
				if($db->insertObject('#__answers_bounties', $bounty, 'id')){
					
					$bounty->id = $db->insertid();
					$user_points = -$bounty->bounty_points;
					AnswersHelper::awardPoints($params, $bounty->created_by, 7, $bounty->question_id, JText::sprintf('MSG_BOUNTY_POINTS_DEDUCTION', $bounty->question_id), $user_points);
	
					return 0;
				}
			} catch (Exception $e){
				
				return 5;
			}
		} else {

			// First check if user has enough points.
			$user_points = CJFunctions::get_user_points($params->get('points_system', 'none'), $existing->created_by);
			$difference_points = $bounty->bounty_points - $existing->bounty_points;

			if($difference_points < 0) return 2; // points cannot be decreased
			if($user_points < $difference_points) return 1; // not enough points to increase bounty

			$bounty->id = $existing->id;
			
			try{
				
				$bounty->state = 1;
				
				if($db->updateObject('#__answers_bounties', $bounty, 'id')){
					
					if($difference_points > 0){

						$user_points = -$difference_points;
						AnswersHelper::awardPoints($params, $existing->created_by, 7, null, JText::sprintf('MSG_BOUNTY_POINTS_DEDUCTION', $bounty->question_id), $user_points);
					}
					
					return 0;
				}
			} catch (Exception $e){
				
				return 5;
			}
		}
		
		return 4;
	}
	
	public function set_bounty_state($bounty_id, $status)
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$query->update('#__answers_bounties')->set('state = '.$status)->where('id = '.$bounty_id);
		$db->setQuery($query);
			
		try 
		{
			$db->query();
			return true;
		} 
		catch (Exception $e){}
		
		return false;
	}
	
	/**
	 * Checks if the bounty expired and take necessary action
	 * 
	 * State 1: Bounty running
	 * State 2: Bounty successfully awarded
	 * State 3: Bounty returned, through the "Refund Points" link by the bounty creator
	 * State 4: Answers received. Bounty is waiting to get awarded. It will be awarded when an answer accepted or an answer gets 2+ rating.
	 * State 5: No answers received before deadline. Bounty creator can either ask for refund or extend deadline.
	 * 
	 * @return boolean
	 */	
	public function check_expired_bounties(){
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$now = JFactory::getDate()->toSql();
		$null_date = $db->getNullDate();
		$params = JComponentHelper::getParams(A_APP_NAME);
		
		$query
			->select('a.id, a.bounty_points, a.question_id, a.created_by')
			->select('q.answers, q.title')
			->from('#__answers_bounties as a')
			->join('inner', '#__answers_questions as q on a.question_id = q.id')
			->where(array('a.state = 1', 'a.expiry_date != '.$db->q($null_date), 'a.expiry_date < '.$db->q($now)));
		
		$db->setQuery($query);
		$bounties = $db->loadObjectList();

		if(empty($bounties)) return  false;
			
		foreach ($bounties as $item){
			
			if($item->answers == 0) {
				
				// no answers found, reverse the bounty
				$query = $db->getQuery(true)->update('#__answers_bounties')->set('state = 5')->where('id = '.$item->id);
				
				$db->setQuery($query);
				
				try {
					$db->query();
				} catch (Exception $e){}
			} else {
				
				// there are few answers but are they having a rating?
				$query = $db->getQuery(true)
					->select('max(a.thumbup - a.thumbdown) as rating, a.created_by')
					->from('#__answers_responses a')
					->where(array('a.question_id = '.$item->question_id))
					->order('a.created');
				
				$db->setQuery($query);
				
				try{
					$answer = $db->loadObject();
					
					if(!empty($answer) && $answer->rating > 2){
						
						$query = $db->getQuery(true)
							->update('#__answers_bounties')
							->set('state = 2')
							->where('id = '.$item->id);
						
						$db->setQuery($query);
						
						try {
							if($db->query()){
								
								// award the points 
								AnswersHelper::awardPoints($params, $answer->created_by, 8, null, JText::sprintf('MSG_BOUNTY_WINNER', $item->bounty_points), $item->bounty_points);
							}
						} catch (Exception $e){}
					} else {
						
						// no answers with positive rating found, let admin decide the fate of this, i.e. when answer is accepted automatically awarded.
						$query = $db->getQuery(true);
						$query->update('#__answers_bounties')->set('state = 4')->where('id = '.$item->id);
						$db->setQuery($query);
						
						try{
							$db->query();
						}catch(Exception $e){}
					}
				} catch (Exception $e){}
			}
		}
	}
	
	public function refund_bounty_points($id){
		
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$params = JComponentHelper::getParams(A_APP_NAME);
		
		$query
			->select('a.id, a.bounty_points, a.question_id, a.created_by, a.state')
			->select('q.answers, q.title')
			->from('#__answers_bounties as a')
			->join('inner', '#__answers_questions as q on a.question_id = q.id')
			->where('a.id = '.$id);
		
		$db->setQuery($query);
		
		try{
			$bounty = $db->loadObject();
			
			if($bounty){
				
				if($bounty->answers != 0) return 3;
				if($bounty->state == 3) return 4;
				if($bounty->state != 5) return 2;

				AnswersHelper::awardPoints($params, $bounty->created_by, 7, null, JText::sprintf('MSG_BOUNTY_POINTS_REVERSED', $bounty->title), $bounty->bounty_points);
				
				$query = $db->getQuery(true);
				$query->update('#__answers_bounties')->set('state = 3')->where('id = '.$id);
				$db->setQuery($query);
				$db->query();
				
				return 1;
			}
		} catch (Exception $e){}
		
		return false;
	}
	
	public function can_answer($user_id, $question_id){
		
		$query = 'select count(*) from #__answers_responses where question_id = '.$question_id.' and created_by = '.$user_id.' and parent_id = 0';
		$this->_db->setQuery($query);
		$count = (int)$this->_db->loadResult();
		
		return $count == 0;
	}
	
	function populate_attachments(&$question){

		$ids = array();
		$question->attachments = array();
			
		if(!empty($question->responses)){
		
			foreach($question->responses as $response){
					
				$ids[] = $response->id;
			}
		}
			
		$wheres = array();
		$wheres[] = '(item_id = '.$question->id.' and item_type = 1)';
			
		if(!empty($ids)){
		
			$wheres[] = '(item_id in ('.implode(',', $ids).') and item_type = 2)';
		}
			
		$where = implode(' or ', $wheres);
			
		$query = '
					select
						id, item_id, item_type, attachment, downloads
					from
						#__answers_attachments
					where
						'.$where;
			
		$this->_db->setQuery($query);
		$attachments = $this->_db->loadObjectList();
			
		if(!empty($attachments)){
		
			foreach($attachments as $attachment){
					
				if($attachment->item_type == 1){
		
					$question->attachments[] = $attachment;
				} else {

					if(!empty($question->responses)){
						
						foreach ($question->responses as &$response){
								
							if($attachment->item_id == $response->id){
			
								$response->attachments[] = $attachment;
								break;
							}
						}
					}
				}
			}
		}
	}
	
	private function get_responses(&$question, $params = array()){
		
		$query = '
				select 
					a.id, a.description, a.created, a.thumbup, a.thumbdown, a.source, a.published, a.reply_type,
					a.ip_address, a.parent_id, (cast(a.thumbup as signed) - cast(a.thumbdown as signed)) as rating, 
					case when a.created_by > 0 then u.id else 0 end as created_by,
					case when a.created_by > 0 then u.'.$params->get('user_display_name', 'name').' else a.user_name end as username,
					case when a.created_by > 0 then u.email else a.email end as email
				from
					#__answers_responses a
				left join
					#__users u on a.created_by = u.id
				where
					question_id = '.$question->id.' and published = 1
				order by
					rating desc';
		
		$this->_db->setQuery($query);
		$results = $this->_db->loadObjectList('id');

		$answers = array();
		
		if(!empty($results)){
			
			foreach($results as $result){
				
				if($result->parent_id == 0){
					
					$answers[$result->id] = $result;
					$answers[$result->id]->replies = array();
				}
			}
			
			foreach ($results as $result){
				
				if($result->parent_id > 0 && $result->reply_type == 0){
					
					if(!empty($answers[$result->parent_id])){
					
						$answers[$result->parent_id]->replies[] = $result;
					}
				} else if($result->reply_type == 1){
					
					$question->replies[] = $result;
				}
			}
		}
				
		return $answers;
	}
	
	public function get_answer_details($question_id, $answer_id){
		
		$params = JComponentHelper::getParams(A_APP_NAME);
		
		$query = '
				select 
					a.id, a.question_id, a.created, a.parent_id, a.description, a.published, a.source,
					q.title, q.alias, q.catid, q.created_by as asker_id,
					case when a.created_by > 0 then u.id else 0 end as created_by,
					case when a.created_by > 0 then u.'.$params->get('user_display_name', 'name').' else a.user_name end as username,
					case when a.created_by > 0 then u.email else a.email end as email
				from
					#__answers_responses a
				left join
					#__answers_questions q on a.question_id = q.id
				left join
					#__users u on a.created_by = u.id
				where 
					a.id = '.$answer_id.' and a.question_id = '.$question_id.' and a.published = 1';
		
		$this->_db->setQuery($query);
		
		$answer = $this->_db->loadObject();
		
		return $answer;
	}
	
	public function search_tags($search){
		
		$query = "select id, tag_text, description from #__answers_tags where tag_text like '%".$this->_db->escape($search)."%'";
		$this->_db->setQuery($query);
		$tags = $this->_db->loadObjectList();
		
		return $tags;
	}
	
	public function get_tags($limitstart, $limit, $params, $keywords){
		
		$return = array();
		$wheres = array();
		
		if(!empty($keywords)){
			
			$wheres[] = 'a.tag_text like \'%'.$this->_db->escape($keywords).'%\'';
		}
		
		$where = !empty($wheres) ? ' where ('.implode(' ) and ( ', $wheres).')' : '';
		
		$query = '
				select 
					a.id, a.tag_text, a.alias, a.description,
					s.num_items
				from
					#__answers_tags as a
				left join
					#__answers_tags_stats as s on a.id = s.tag_id 
				'.$where;
		
		$this->_db->setQuery($query, $limitstart, $limit);
		$return['tags'] = $this->_db->loadObjectList();
		
		/************ pagination *****************/
		$query = 'select count(*) from #__answers_tags';
		
		jimport('joomla.html.pagination');
		$this->_db->setQuery($query);
		$total = $this->_db->loadResult();
		
		$return['pagination'] = new JPagination( $total, $limitstart, $limit );
		/************ pagination *****************/
		
		return $return;
	}
	
	public function get_tags_by_itemids($ids){

		$query = '
			select
				map.item_id,
				tag.id as tag_id, tag.tag_text, tag.alias, tag.description
			from
				#__answers_tagmap map
			left join
				#__answers_tags tag on tag.id = map.tag_id
			where
				map.item_id in ('.implode(',', $ids).')';
			
		$this->_db->setQuery($query);
		$tags = $this->_db->loadObjectList();
		
		return !empty($tags) ? $tags : array();
	}
	
	public function get_tag_details($tag_id){
		
		$query = 'select id, tag_text, alias, description from #__answers_tags where id = '.$tag_id;
		$this->_db->setQuery($query);
		
		return $this->_db->loadObject();
	}
	
	public function save_tag_details($tag){
		
		if (JFactory::getConfig()->get('unicodeslugs') == 1) {
			
			$tag->alias = JFilterOutput::stringURLUnicodeSlug($tag->title);
		} else {
			
			$tag->alias = JFilterOutput::stringURLSafe($tag->title);
		}
		
		$query = '
				update 
					#__answers_tags 
				set 
					tag_text = '.$this->_db->quote($tag->title).', 
					alias = '.$this->_db->quote($tag->alias).', 
					description = '.$this->_db->quote($tag->description).'
				where 
					id = '.$tag->id;
		
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			return true;
		}
		
		return false;
	}
	
	public function delete_tag($tagid){
		
		$query = 'delete from #__answers_tags where id = '.$tagid;
		$this->_db->setQuery($query);
		
		if($this->_db->query() && $this->_db->getAffectedRows() > 0){
			
			$query = 'delete from #__answers_tagmap where tag_id = '.$tagid;
			$this->_db->setQuery($query);
			$this->_db->query();
			
			$query = 'delete from #__answers_tags_stats where tag_id = '.$tagid;
			$this->_db->setQuery($query);
			$this->_db->query();
			
			return true;
		}
		
		return false;
	}
	
	public function hit($id){
		
		$query = 'update #__answers_questions set hits = hits + 1 where id = '.$id;
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			return true;
		}
		
		return false;
	}
	
	private function _get_permissions_where_clause($groups, $column){
		
		if(count($groups) > 0){
				
			$permissions = array();
				
			foreach ($groups as $group){
		
				$permissions[] = $column.' is null or find_in_set('.$this->_db->quote($group).', '.$column.') > 0';
			}
				
			return '('.implode(' ) or ( ', $permissions).')';
		}
		
		return false;
	}
	
	public function save_question(&$question){
		
		$user = JFactory::getUser();
		$date = JFactory::getDate()->toSql();
		$ip_address = CJFunctions::get_user_ip_address();
		$raw_alias = !empty($question->alias) ? $question->alias : $question->title;
		
		if (JFactory::getConfig()->get('unicodeslugs') == 1) {
		
			$question->alias = JFilterOutput::stringURLUnicodeSlug($raw_alias);
		} else {
		
			$question->alias = JFilterOutput::stringURLSafe($raw_alias);
		}
		
		$query = '
				insert into
					#__answers_questions (title, alias, description, created_by, created, catid, published, ip_address, user_name, email)
				values (
					'.$this->_db->quote($question->title).',
					'.$this->_db->quote($question->alias).',
					'.$this->_db->quote($question->description).',
					'.$user->id.',
					'.$this->_db->quote($date).',
					'.$question->catid.',
					'.$question->published.',
					'.$this->_db->quote($ip_address).',
					'.$this->_db->quote($question->username).',
					'.$this->_db->quote($question->email).'
				)';
		
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			$question->id = $this->_db->insertid();
			
			// save attachments
			$this->attach_files($question->id, 1, $question->attachments);
			
			// now save tags
			if(!empty($question->tags)) $this->insert_tags($question->id, $question->tags);
			
			if($question->published == 1){
			
				$query = 'select id, nleft, nright from #__answers_categories where id = '.$question->catid;
				$this->_db->setQuery($query);
				$category = $this->_db->loadObject();

				if(!empty($category)){
						
					$query = 'update #__answers_categories set questions = questions + 1 where nleft <= '.$category->nleft.' and nright >= '.$category->nright;
					$this->_db->setQuery($query);
					$this->_db->query();
				}
			
				if(!$user->guest){

					$subid = CJFunctions::generate_random_key(12);
					
					$query = '
							insert into 
								#__answers_users (id, questions, karma, subid)
							values
								('.$user->id.', 1, 1, '.$this->_db->quote($subid).')
							on duplicate key update
								questions = questions + 1,
								karma = questions*1 + answers*2 + best_answers*5';

					$this->_db->setQuery($query);
					$this->_db->query();
				}
			
				return true;
			} else {
			
				$question->secret = CJFunctions::generate_random_key(64);
				$query = 'insert into #__answers_approval(item_id, item_type, secret, status) values ('.$question->id.', 1,'. $this->_db->quote($question->secret).', 2)';
				$this->_db->setQuery($query);
			
				if($this->_db->query()) {
						
					return true;
				}
			}
		}
		
		return false;
	}
	
	public function update_question(&$question){
		
		$user = JFactory::getUser();
		
		if(!$user->authorise('answers.manage', A_APP_NAME)){
			
			$query = 'select created_by from #__answers_questions where id = '.$question->id;
			$this->_db->setQuery($query);
			$userid = $this->_db->loadResult();
			
			if($user->id != $userid){
				
				return false;
			}
		}
		
		$raw_alias = !empty($question->alias) ? $question->alias : $question->title;
		
		if (JFactory::getConfig()->get('unicodeslugs') == 1) {
		
			$question->alias = JFilterOutput::stringURLUnicodeSlug($raw_alias);
		} else {
		
			$question->alias = JFilterOutput::stringURLSafe($raw_alias);
		}
		
		$query = '
				update
					#__answers_questions
				set
					title = '.$this->_db->quote($question->title).',
					description = '.$this->_db->quote($question->description).',
					catid = '.$question->catid.
					($user->authorise('answwers.manage', A_APP_NAME) ? ',alias = '.$this->_db->quote($question->alias) : '').'
				where
					id = '.$question->id;
		
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			if(!empty($question->attachments)) $this->attach_files($question->id, 1, $question->attachments);
			
			if(!empty($question->tags)) $this->insert_tags($question->id, $question->tags);
			
			return true;
		}
		
		return false;
	}
	
	private function insert_tags($question_id, $strtags){

		$tags = explode(',', $strtags);
		
		// first filter out the tags
		foreach($tags as $i=>$tag){
				
			$tag = preg_replace('/[^-\pL.\x20]/u', '', $tag);
			if(empty($tag)) unset($tags[$i]);
		}
		
		// now if there are any new tags, insert them.
		if(!empty($tags)){
				
			$inserts = array();
			$sqltags = array();
			
			foreach($tags as $tag){
		
				$alias = JFilterOutput::stringURLUnicodeSlug($tag);
				$inserts[] = '('.$this->_db->quote($tag).','.$this->_db->quote($alias).')';
				$sqltags[] = $this->_db->quote($tag);
			}
			
			$query = 'insert ignore into #__answers_tags (tag_text, alias) values '.implode(',', $inserts);
			$this->_db->setQuery($query);
			
			if(!$this->_db->query()){
				
				return false;
			}
					
			// we need to get all tag ids matching the input tags
			$query = 'select id from #__answers_tags where tag_text in ('.implode(',', $sqltags).')';
			$this->_db->setQuery($query);
			$insertids = $this->_db->loadColumn();

			if(!empty($insertids)){
		
				$mapinserts = array();
				$statinserts = array();
		
				foreach($insertids as $insertid){
						
					$mapinserts[] = '('.$insertid.','.$question_id.')';
					$statinserts[] = '('.$insertid.','.'1)';
				}
		
				$query = 'insert ignore into #__answers_tagmap(tag_id, item_id) values '.implode(',', $mapinserts);
				$this->_db->setQuery($query);

				if(!$this->_db->query()){
					
					return false;
				}
				
				$query = 'insert ignore into #__answers_tags_stats(tag_id, num_items) values '.implode(',', $statinserts);
				$this->_db->setQuery($query);
				
				if(!$this->_db->query()){
					
					return false;
				}
			}
			
			// now remove all non-matching tags ids from the map
			$where = '';
			
			if(!empty($insertids)){
				
				$where = ' and tag_id not in ('.implode(',', $insertids).')';
			}

			$query = 'select tag_id from #__answers_tagmap where item_id = '.$question_id.$where;
			$this->_db->setQuery($query);
			$removals = $this->_db->loadColumn();

			$where = '';
			
			if(!empty($removals)){
				
				$query = 'delete from #__answers_tagmap where tag_id in ('.implode(',', $removals).') and item_id = '.$question_id;
				$this->_db->setQuery($query);
				$this->_db->query();
				
				$where = ' or s.tag_id in ('.implode(',', $removals).')';
			}

			// now update the stats
			$query = '
				update
					#__answers_tags_stats s
				set
					s.num_items = (select count(*) from #__answers_tagmap m where m.tag_id = s.tag_id)
				where
					s.tag_id in (select tag_id from #__answers_tagmap m1 where m1.item_id = '.$question_id.')'.$where;
			
			$this->_db->setQuery($query);
			$this->_db->query();
		} else {
			
			$query = 'delete from #__answers_tagmap where item_id = '.$question_id;
			$this->_db->setQuery($query);
			$this->_db->query();
		}
	}
	
	public function save_answer(&$answer, $params){
		
		$user = JFactory::getUser();
		$date = JFactory::getDate()->toSql();
		$ip_address = CJFunctions::get_user_ip_address();
		
		if($params->get('answering_closed_questions') != 1){
			
			$query = 'select accepted_answer from #__answers_questions where id = '.$answer->question_id;
			$this->_db->setQuery($query);
			$accepted_answer = (int)$this->_db->loadResult();
			
			if($accepted_answer > 0) return false;
		}
					
		$query = '
				insert into
					#__answers_responses(question_id, created_by, published, parent_id, created, ip_address, description, user_name, email, source)
				values ('.
					$answer->question_id.','.$user->id.','.$answer->published.',0,'.
					$this->_db->quote($date).','.
					$this->_db->quote($ip_address).','.
					$this->_db->quote($answer->description).','.
					$this->_db->quote($answer->username).','.
					$this->_db->quote($answer->email).','.
					$this->_db->quote($answer->references).')';
		
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			$answer->id = $this->_db->insertid();
			
			$this->attach_files($answer->id, 2, $answer->attachments);
			
			if($answer->published == 1){
				
				$query = '
						update 
							#__answers_questions 
						set 
							answers = answers + 1, 
							modified = '.$this->_db->quote($date).',
							modified_by = '.$user->id.'
						where 
							id = '.$answer->question_id;
				
				$this->_db->setQuery($query);
				$this->_db->query();
				
				$query = 'select id, nleft, nright from #__answers_categories where id = (select catid from #__answers_questions where id = '.$answer->question_id.')';
				$this->_db->setQuery($query);
				$category = $this->_db->loadObject();
				
				if(!empty($category)){
					
					$query = 'update #__answers_categories set answers = answers + 1 where nleft <= '.$category->nleft.' and nright >= '.$category->nright;
					$this->_db->setQuery($query);
					$this->_db->query();
				}
				
				if(!$user->guest){
					
					$subid = CJFunctions::generate_random_key(12);

					$query = '
							insert into 
								#__answers_users (id, answers, karma, subid)
							values
								('.$user->id.', 1, 2, '.$this->_db->quote($subid).')
							on duplicate key update
								answers = answers + 1,
								karma = questions*1 + answers*2 + best_answers*5';
					
					$this->_db->setQuery($query);
					$this->_db->query();
				}
				
				return true;
			} else {
				
				$answer->secret = CJFunctions::generate_random_key(64);
				$query = '
						insert into 
							#__answers_approval(item_id, item_type, secret, '.$this->_db->quoteName('status').') 
						values 
							('.$answer->id.', 2,'. $this->_db->quote($answer->secret).', 2)';
				
				$this->_db->setQuery($query);
				
				if($this->_db->query()) {
					
					return true;
				}
			}
		}
		
		return false;
	}
	
	public function save_reply($question_id, $answer_id, $qn_id, $reply){
		
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
		$date = JFactory::getDate()->toSql();
		
		$ip = CJFunctions::get_user_ip_address();
		$reply = CJFunctions::clean_value($reply, false);
		$reply_type = 0;
		
		if(!$answer_id && $qn_id > 0) {
			
			$answer_id = $qn_id;
			$reply_type = 1;
		}
		
		$query = $db->getQuery(true)
			->insert('#__answers_responses')
			->columns('question_id, description, created_by, created, published, ip_address, parent_id, reply_type')
			->values($question_id.','.$db->q($reply).','.$user->id.','.$db->q($date).', 1,'.$db->q($ip).','.$answer_id.','.$reply_type);
		
		$db->setQuery($query);
		
		try{
		
			if($this->_db->query()){
				return true;
			}
		} catch (Exception $e){}
		
		return false;
	}
	
	function accept_answer($question_id, $answer_id){	
		
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
		
		if(!$user->authorise('answers.manage', A_APP_NAME)){
		
			$query = 'select created_by from #__answers_questions where id = '.$question_id;
			$this->_db->setQuery($query);
			$created_by = (int)$this->_db->loadResult();
			
			if($created_by != $user->id) return false;
		}
		
		$query = 'update #__answers_questions set accepted_answer = '.$answer_id.' where id = '.$question_id;
		$this->_db->setQuery($query);
		
		if($this->_db->query() && $this->_db->getAffectedRows() > 0){
			
			$query = '
					update 
						#__answers_users 
					set 
						best_answers = best_answers + 1,
						karma = questions*1 + answers*2 + best_answers*5
					where 
						id = (select created_by from #__answers_responses where id = '.$answer_id.')';
			
			$this->_db->setQuery($query);
			$this->_db->query();
			
			$query = '
					update 
						#__answers_users 
					set 
						accepted = accepted + 1 
					where 
						id = (select created_by from #__answers_questions where id = '.$question_id.')';
			
			$this->_db->setQuery($query);
			$this->_db->query();
			
			return true;
		}
		
		return false;
	}

	function unaccept_answer($question_id, $answer_id){
	
		$db = JFactory::getDbo();
		$query = 'update #__answers_questions set accepted_answer = 0 where id = '.$question_id;
		$db->setQuery($query);
	
		if($db->query() && $db->getAffectedRows() > 0){
				
			$query = '
					update 
						#__answers_users 
					set 
						best_answers = best_answers - 1 
					where 
						id = (select created_by from #__answers_responses where id = '.$answer_id.') and best_answers > 0';
			
			$db->setQuery($query);
			$db->query();
				
			$query = '
					update 
						#__answers_users 
					set 
						accepted = accepted - 1 
					where 
						id = (select created_by from #__answers_questions where id = '.$question_id.') and accepted > 0';
			
			$db->setQuery($query);
			$db->query();

			$query = 'delete from #__answers_activities where item_id = '.$question_id.' and activity_type = 3';
			$db->setQuery($query);
			$db->query();
			
			// deduct bounty points if one awarded
			$bounty = $this->get_bounty_of_question($question_id);
				
			if(!empty($bounty) && $bounty->state == 2){
			
				$query = $db->getQuery(true);
				$query->update('#__answers_bounties')->set('state = 1')->where('id = '.$bounty->id);
				$db->setQuery($query);
			
				try {
					if($db->query()) {
						
						$params = JComponentHelper::getParams(A_APP_NAME);
						$points = -(abs($bounty->bounty_points));
						AnswersHelper::awardPoints($params, $bounty->created_by, 8, $bounty->question_id, JText::sprintf('MSG_BOUNTY_WINNER', $points), $points);
					}
				} catch (Exception $e){}
			}

			return true;
		}
	
		return false;
	}
	
	public function unpublish_question($id){
		
		$params = JComponentHelper::getParams(A_APP_NAME);
		$question = $this->get_question_details($id, array('get_tags'=>false, 'get_answers'=>false, 'trigger_hit'=>false, 'get_attachments'=>false), $params);
		
		if(!empty($question) && $question->id > 0){
			
			$query = 'update #__answers_questions set published = 0  where id = '.$question->id;
			$this->_db->setQuery($query);
			
			if($this->_db->query()){
				
				$query = 'update #__answers_categories set questions = questions - 1 where nleft <= '.$question->nleft.' and nright >= '.$question->nright.' and questions > 0';
				$this->_db->setQuery($query);
				$this->_db->query();
				
				$query = 'update #__answers_users set questions = questions - 1 where id = '.$question->created_by.' and questions > 0';
				$this->_db->setQuery($query);
				$this->_db->query();

				$query = 'update #__answers_activities set published = 0 where item_id = '.$question->id.' and activity_type in (1, 3)';
				$this->_db->setQuery($query);
				$this->_db->query();
				
				$query = 'update #__answers_activities set published = 0 wjhere item_id in (select id from #__answers_responses where question_id = '.$question->id.') and activity_type in (2,4,5)';
				$this->_db->setQuery($query);
				$this->_db->query();
			}
			
			return true;
		}
		
		return false;
	}
	
	public function delete_question($id){
		
		$params = JComponentHelper::getParams(A_APP_NAME);
		$question = $this->get_question_details($id, array('get_tags'=>false, 'get_answers'=>false, 'trigger_hit'=>false, 'get_attachments'=>false), $params);
		
		if(!empty($question) && $question->id > 0){
			
			$query = 'delete from #__answers_questions where id = '.$question->id;
			$this->_db->setQuery($query);
			
			if($this->_db->query()){
				
				$query = '
						select 
							attachment 
						from 
							#__answers_attachments 
						where 
							(item_id in (select id from #__answers_responses where question_id = '.$question->id.') and item_type = 2) or
							(item_id = '.$question->id.' and item_type = 1)';
				
				$this->_db->setQuery($query);
				$attachments = $this->_db->loadColumn();

				if(!empty($attachments)){
						
					foreach ($attachments as $attachment){
				
						if(JFile::exists(CA_ATTACHMENTS_PATH.$attachment)) Jfile::delete(CA_ATTACHMENTS_PATH.$attachment);
					}
				}

				$query = 'delete from #__answers_activities where item_id = '.$question->id.' and activity_type in (1, 3)';
				$this->_db->setQuery($query);
				$this->_db->query();
				
				$query = 'delete from #__answers_activities where item_id in (select id from #__answers_responses where question_id = '.$question->id.') and activity_type in (2,4,5)';
				$this->_db->setQuery($query);
				$this->_db->query();
				
				$query = 'delete from #__answers_subscribes where subscription_type = 1 and subscription_id = '.$question->id;
				$this->_db->setQuery($query);
				$this->_db->query();
				
				$query = 'delete from #__answers_responses where question_id = '.$question->id;
				$this->_db->setQuery($query);
				
				if($this->_db->query()){
				
					$count = $this->_db->getAffectedRows();
					
					if($count > 0){
	
						$query = 'update #__answers_categories set answers = answers - '.$count.' where nleft <= '.$question->nleft.' and nright >= '.$question->nright.' and answers > 0';
						$this->_db->setQuery($query);
						$this->_db->query();
					}
				}
				
				$query = 'update #__answers_categories set questions = questions - 1 where nleft <= '.$question->nleft.' and nright >= '.$question->nright.' and questions > 0';
				$this->_db->setQuery($query);
				$this->_db->query();
				
				$query = 'update #__answers_users set questions = questions - 1 where id = '.$question->created_by.' and questions > 0';
				$this->_db->setQuery($query);
				$this->_db->query();
				
				$query = 'delete from #__answers_tagmap where item_id = '.$question->id;
				$this->_db->setQuery($query);
				$this->_db->query();
				
				$query = 'update #__answers_tags_stats s set num_items = (select count(*) from #__answers_tagmap m where s.tag_id = m.tag_id)';
				$this->_db->setQuery($query);
				$this->_db->query();
				
				return true;
			}
		}
		
		return false;
	}
	
	public function unpublish_answer($question_id, $answer_id){
		
		$query = 'update #__answers_responses set published = 0 where id = '.$answer_id.' and question_id = '.$question_id;
		$this->_db->setQuery($query);
		
		if($this->_db->query() && $this->_db->getAffectedRows() > 0){
				
			$query = 'update #__answers_questions set answers = answers - 1 where id = '.$question_id.' and answers > 0';
			$this->_db->setQuery($query);
			$this->_db->query();
			
			$query = 'select nleft, nright from #__answers_categories where id = (select catid from #__answers_questions where id = '.$question_id.')';
			$this->_db->setQuery($query);
			$category = $this->_db->loadObject();
			
			$query = 'update #__answers_categories set answers = answers - 1 where nleft <= '.$category->nleft.' and nright >= '.$category->nright.' and answers > 0';
			$this->_db->setQuery($query);
			$this->_db->query();
			
			$query = 'update #__answers_users set answers = answers - 1 where id = (select created_by from #__answers_responses where id = '.$answer_id.') and answers > 0';
			$this->_db->setQuery($query);
			$this->_db->query();

			$query = 'select accepted_answer from #__answers_questions where id = '.$question_id;
			$this->_db->setQuery($query);
			$accepted_answer = $this->_db->loadResult();
			
			if($accepted_answer == $answer_id){
				
				$query = 'update #__answers_questions set accepted_answer = 0 where id = '.$question_id;
				$this->_db->setQuery($query);
				$this->_db->query();
				
				$query = 'update #__answers_users set best_answers = best_answers - 1 where id = (select created_by from #__answers_responses where id = '.$answer_id.') and best_answers > 0';
				$this->_db->setQuery($query);
				$this->_db->query();
			}

			$query = 'update #__answers_activities set published = 0 where item_id in = '.$answer_id.' and activity_type in (2,4,5)';
			$this->_db->setQuery($query);
			$this->_db->query();

			return true;
		}
		
		return false;
	}

	public function delete_answer($question_id, $answer_id){
		
		$query = 'select id, question_id, created_by from #__answers_responses where id = '.$answer_id.' and question_id = '.$question_id;
		$this->_db->setQuery($query);
		$answer = $this->_db->loadObject();
	
		if(!empty($answer)){
			
			$query = 'delete from #__answers_responses where id = '.$answer_id.' and question_id = '.$question_id;
			$this->_db->setQuery($query);
		
			if($this->_db->query() && $this->_db->getAffectedRows() > 0){
				
				$query = 'delete from #__answers_responses where parent_id = '.$answer->id;
				$this->_db->setQuery($query);
				$this->_db->query();
				
				$query = 'select attachment from #__answers_attachments where item_id = '.$answer->id.' and item_type = 2';
				$this->_db->setQuery($query);
				$attachments = $this->_db->loadColumn();
				
				if(!empty($attachments)){
					
					foreach($attachments as $attachment){
						
						if(JFile::exists(CA_ATTACHMENTS_PATH.$attachment)){
							
							JFile::delete(CA_ATTACHMENTS_PATH.$attachment);
						}
					}
				
					$query = 'delete from #__answers_attachments where item_id = '.$answer->id.' and item_type = 2';
					$this->_db->setQuery($query);
					$this->_db->query();
				}

				$query = 'delete from #__answers_activities where item_id = '.$answer->id.' and activity_type in (2,4,5)';
				$this->_db->setQuery($query);
				$this->_db->query();
				
				$query = 'update #__answers_questions set answers = answers - 1 where id = '.$question_id.' and answers > 0';
				$this->_db->setQuery($query);
				$this->_db->query();
					
				$query = 'update #__answers_categories set answers = answers - 1 where id = (select catid from #__answers_questions where id = '.$question_id.') and answers > 0';
				$this->_db->setQuery($query);
				$this->_db->query();
					
				$query = 'update #__answers_users set answers = answers - 1 where id = '.$answer->created_by.' and answers > 0';
				$this->_db->setQuery($query);
				$this->_db->query();
		
				$query = 'select accepted_answer from #__answers_questions where id = '.$question_id;
				$this->_db->setQuery($query);
				$accepted_answer = $this->_db->loadResult();
					
				if($accepted_answer == $answer_id){
		
					$query = 'update #__answers_questions set accepted_answer = 0 where id = '.$question_id;
					$this->_db->setQuery($query);
					$this->_db->query();
		
					$query = 'update #__answers_users set best_answers = best_answers - 1 where id = '.$answer->created_by.' and best_answers > 0';
					$this->_db->setQuery($query);
					$this->_db->query();
				}
					
				return true;
			}
		}
			
		return false;
	}
	
	public function unpublish_reply($answer_id){
		
		$query = 'update #__answers_responses set published = 0 where id = '.$answer_id.' and parent_id > 0';
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			return true;
		}
		
		return false;
	}

	
	public function delete_reply($answer_id){
		
		$query = 'delete from #__answers_responses where id = '.$answer_id.' and parent_id > 0';
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			return true;
		}
		
		return false;
	}
	
	public function update_answer($question_id, $answer_id, $description, $references){
	
		$user = JFactory::getUser();
		
		if(!$user->authorise('answers.manage', A_APP_NAME)){
			
			$query = 'select created_by from #__answers_responses where id = '.$answer_id.' and question_id = '.$question_id;
			$this->_db->setQuery($query);
			$created_by = (int)$this->_db->loadResult();
		
			if($created_by != $user->id){
				
				return false;
			}
		}

		$query = '
			update
				#__answers_responses
			set
				description = '.$this->_db->quote($description).',
				source = '.$this->_db->quote($references).'
			where
				id = '.$answer_id;
		
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
				
			return true;
		}
		
		return false;
	}
	
	public function like_or_dislike_answer($state, $question_id, $answer_id){
		
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		
		$cookies = $app->input->cookie->get('likes', '', 'string');
		
		if(!empty($cookies)){
			
			$cookies = explode(',', $cookies);
			
			if(in_array($answer_id, $cookies)){
				
				return -1;
			}
		}
		
		if(!$user->guest){
			
			$query = 'select created_by from #__answers_responses where id = '.$answer_id;
			$this->_db->setQuery($query);
			$userid = $this->_db->loadResult();
			
			if($userid == $user->id){
				
				return -2;
			}
			
			$query = 'select count(*) from #__answers_ratings where user_id = '.$user->id.' and answer_id = '.$answer_id;
			$this->_db->setQuery($query);
			$count = $this->_db->loadResult();
			
			if($count > 0){
				
				return -1;
			}
		}
		
		$created = JFactory::getDate()->toSql();
		$query = 'insert into #__answers_ratings (user_id, answer_id, rating, created) values ('.$user->id.','.$answer_id.','.$state.','.$this->_db->quote($created).')';
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			$column = $state == 1 ? 'thumbup' : 'thumbdown';
			$cookies = !empty($cookies) ? $cookies : array();
			$cookies[] = $answer_id;
			$cookies = implode(',', $cookies);
			
			$app->input->cookie->set('likes', $cookies, time()+60*60*24*365, JUri::current());
			
			$query = 'update #__answers_responses set '.$column.' = '.$column.' + 1 where id = '.$answer_id;
			$this->_db->setQuery($query);
			
			if($this->_db->query()){
				
				$query = 'select a.thumbup, a.thumbdown from #__answers_responses a where a.id = '.$answer_id;
				$this->_db->setQuery($query);
				$rating = $this->_db->loadObject();
				
				return $rating;
			}
		}
		
		return false;
	}
	
	private function attach_files($item_id, $item_type, $strattchments){

		if(!empty($strattchments)){
		
			$attachments = explode(',', $strattchments);
			$inserts = array();

			$query = 'select attachment from #__answers_attachments where item_type = '.$item_type.' and item_id = '.$item_id;
			$this->_db->setQuery($query);
			$existing = $this->_db->loadColumn();
			
			if(!empty($existing)){
			
				$attachments = array_diff($attachments, $existing);
			}
			
			foreach($attachments as $attachment){
					
				if(!empty($attachment) && JFile::exists(JPATH_ROOT.DS.'tmp'.DS.'answers'.DS.$attachment)){
		
					if(JFile::move(JPATH_ROOT.DS.'tmp'.DS.'answers'.DS.$attachment, CA_ATTACHMENTS_PATH.$attachment)){
							
						$inserts[] = '('.$item_type.','.$item_id.','.$this->_db->quote($attachment).')';
					}
				}
			}
		
			if(!empty($inserts)){
		
				$query = 'insert into #__answers_attachments (item_type, item_id, attachment) values '.implode(',', $inserts);
				$this->_db->setQuery($query);
				$this->_db->query();
			}
		}
	}
	
	public function get_attachment($id, $hit = true){
		
		$query = 'select id, item_id, item_type, attachment, downloads from #__answers_attachments where id = '.$id;
		$this->_db->setQuery($query);
		$attachment = $this->_db->loadObject();
		
		if($hit && !empty($attachment)){
			
			$query = 'update #__answers_attachments set downloads = downloads + 1 where id = '.$id;
			$this->_db->setQuery($query);
			$this->_db->query();
		}
		
		return $attachment;
	}
	
	function delete_attachment($id){
		
		$user = JFactory::getUser();
		$attachment = $this->get_attachment($id, false);
		$query = '';
		
		if($attachment->item_type == 1){
		
			$query = 'select created_by from #__answers_questions where id = '.$attachment->item_id;
		} else {
			
			$query = 'select created_by from #__answers_responses where id = '.$attachment->item_id;
		}
		
		$this->_db->setQuery($query);
		$userid = $this->_db->loadResult();
		
		if($user->id == $userid){
			
			$query = 'delete from #__answers_attachments where id = '.$attachment->id;
			$this->_db->setQuery($query);
			
			if($this->_db->query() && JFile::exists(CA_ATTACHMENTS_PATH.$attachment->attachment)){
				
				JFile::delete(CA_ATTACHMENTS_PATH.$attachment->attachment);
				
				return true;
			}
		}
		
		return false;
	}
	
	/** deprecated do not use**/
	public function get_admin_emails($groupId){
		
		if(!$groupId) return false;
		
		$userids = JAccess::getUsersByGroup($groupId);
		
		if(empty($userids)) return false;
		
		$query = 'select email from #__users where id in ('.implode(',', $userids).')';
		$this->_db->setQuery($query);
		$users = $this->_db->loadColumn();
		
		return $users;
	}

	public function get_admin_users($groupId){
	
		if(!$groupId) return false;
	
		$userids = JAccess::getUsersByGroup($groupId);
	
		if(empty($userids)) return false;
	
		$query = 'select name, email, 0 as subid from #__users where id in ('.implode(',', $userids).') and block = 0';
		$this->_db->setQuery($query);
		$users = $this->_db->loadObjectList();
	
		return $users;
	}
	
	public function approve_items($status, $type, $secret){
		
		$query = '
				select 
					item_id 
				from 
					#__answers_approval 
				where 
					secret = '.$this->_db->quote($secret).' and item_type = '.$type.' and '.$this->_db->quoteName('status').' = 2';
		
		$this->_db->setQuery($query);
		$item_id = $this->_db->loadResult();
		
		if($item_id){

			if($type == 2){
			
				$query = 'update #__answers_responses set published = '.$status.' where id = '.$item_id;
				$this->_db->setQuery($query);
					
				if(!$this->_db->query()){
			
					return false;
				}
			} else {
					
				$query = 'update #__answers_questions set published = '.$status.' where id = '.$item_id;
				$this->_db->setQuery($query);
			
				if(!$this->_db->query()){
						
					return false;
				}
			}
			
			$return = null;

			if($status == 1){
				
				$params = JComponentHelper::getParams(A_APP_NAME);
				
				if($type == 1){
					
					$return = $this->get_question_details(
							$item_id, 
							array('get_tags'=>false, 'get_answers'=>false, 'trigger_hit'=>false, 'get_attachments'=>false), 
							$params);

					if(!empty($return)){
						
						$query = 'select id, nleft, nright from #__answers_categories where id = '.$return->catid;
						$this->_db->setQuery($query);
						$category = $this->_db->loadObject();
							
						if(!empty($category)){
						
							$query = 'update #__answers_categories set questions = questions + 1 where nleft <= '.$category->nleft.' and nright >= '.$category->nright;
							$this->_db->setQuery($query);
							$this->_db->query();
						}
							
						$query = 'update #__answers_users set questions = questions + 1 where id = '.$return->created_by;
						$this->_db->setQuery($query);
						$this->_db->query();

						$query = 'update #__answers_activities set published = 1 where item_id = '.$item_id.' and activity_type in (1, 3)';
						$this->_db->setQuery($query);
						$this->_db->query();
					} else {
						
						return false;
					}
				} else {
					
					$query = '
							select 
								r.id, r.created_by, r.description,
								q.id as question_id, q.title, q.alias, q.catid, 
								case when r.created_by > 0 then u1.id else 0 end as created_by, 
								case when r.created_by > 0 then u1.'.$params->get('user_display_name', 'name').' else r.user_name end as username,
								case when r.created_by > 0 then u1.email else r.email end as email,
								case when q.created_by > 0 then u2.id else 0 end as asker, 
								case when q.created_by > 0 then u2.'.$params->get('user_display_name', 'name').' else q.user_name end as asker_name,
								case when q.created_by > 0 then u2.email else q.email end as asker_email
							from 
								#__answers_responses r
							left join
								#__answers_questions q on r.question_id = q.id
							left join
								#__users u1 on r.created_by = u1.id
							left join
								#__users u2 on q.created_by = u2.id
							where 
								r.id = '.$item_id;
					
					$this->_db->setQuery($query);
					$return = $this->_db->loadObject();
					
					if(!empty($return)){
	
						$query = 'update #__answers_questions set answers = answers + 1 where id = '.$return->question_id;
						$this->_db->setQuery($query);
						$this->_db->query();
						
						$query = 'select id, nleft, nright from #__answers_categories where id = (select catid from #__answers_questions where id = '.$return->question_id.')';
						$this->_db->setQuery($query);
						$category = $this->_db->loadObject();
						
						if(!empty($category)){
								
							$query = 'update #__answers_categories set answers = answers + 1 where nleft <= '.$category->nleft.' and nright >= '.$category->nright;
							$this->_db->setQuery($query);
							$this->_db->query();
						}
						
						$query = 'update #__answers_users set answers = answers + 1 where id = '.$return->created_by;
						$this->_db->setQuery($query);
						$this->_db->query();
						
						$query = 'update #__answers_activities set published = 1 where item_id '.$item_id.' and activity_type in (2,4,5)';
						$this->_db->setQuery($query);
						$this->_db->query();
					} else {
						
						return false;
					}
				}
			}
			
			$query = 'update #__answers_approval set '.$this->_db->quoteName('status').' = 1 where secret = '.$this->_db->quote($secret).' and item_type = '.$type;
			$this->_db->setQuery($query);
			$this->_db->query();
			
			return $return;
		}
		
		return false;
	}
	
	public function subscribe($type, $userid, $itemid){
		
		$query = 'select subid from #__answers_users where id = '.$userid;
		$this->_db->setQuery($query);
		$subid = $this->_db->loadResult();
		
		if(empty($subid)){
			
			$subid = CJFunctions::generate_random_key(12);
			
			$query = '
					insert into 
						#__answers_users (id, subid) 
					values 
						('.$userid.','.$this->_db->quote($subid).') 
					on duplicate key 
						update subid = values(subid)';
			
			$this->_db->setQuery($query);
			
			if(!$this->_db->query()){
				
				return false;
			}
		}
		
		$query = 'insert ignore into #__answers_subscribes (subscriber_id, subscription_type, subscription_id) values ('.$userid.','.$type.','.$itemid.')';
		$this->_db->setQuery($query);
		
		if($this->_db->query()){
			
			return true;
		}
		
		return false;
	}

	public function unsubscribe($type, $userid, $itemid){
	
		$query = 'delete from #__answers_subscribes where subscriber_id = '.$userid.' and subscription_id = '.$itemid.' and subscription_type = '.$type;
		$this->_db->setQuery($query);
	
		if($this->_db->query()){
				
			return true;
		}
		
		return false;
	}
	
	public function check_subscription($type, $userid, $catid){
		
		$query = '
				select 
					count(*) 
				from 
					#__answers_subscribes 
				where 
					subscriber_id='.$userid.' and 
					subscription_id='.$catid.' and 
					subscription_type in ('.implode(',', $type).')';
		
		$this->_db->setQuery($query);
		$count = (int)$this->_db->loadResult();
		
		return $count > 0;
	}
}