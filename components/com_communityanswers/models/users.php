<?php
/**
 * @version		$Id: users.php 01 2011-08-13 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die();

jimport('joomla.application.component.modelitem');

class CommunityAnswersModelUsers extends JModelLegacy {

	function __construct() {

		parent::__construct ();
	}
	
	public function get_users($start, $limit, $params){

		$db = JFactory::getDbo();
		$return = array();
		
		$karma_questions = $params->get('karma_questions', 1);
		$karma_answers = $params->get('karma_answers', 2);
		$karma_best_answers = $params->get('karma_best_answers', 5);
		
		$query = $db->getQuery(true);
		
		$query
			->select('u.id, u.'.$params->get('user_display_name', 'name').' as username, u.email')
			->select('au.questions, au.answers, au.best_answers, au.accepted')
			->select('(au.questions * '.$karma_questions.' + au.answers * '.$karma_answers.' + au.best_answers * '.$karma_best_answers.') as karma')
			->from('#__answers_users as au')
			->join('left', '#__users as u on au.id = u.id')
			->order('karma desc');
		
		$db->setQuery($query, $start, $limit);
		$return['users'] = $db->loadObjectList();
		
		/************ pagination *****************/
		$query = $db->getQuery(true)->select('count(*)')->from('#__answers_users');
		
		jimport('joomla.html.pagination');
		$this->_db->setQuery($query);
		$total = $db->loadResult();
		
		$return['pagination'] = new JPagination( $total, $start, $limit );
		/************ pagination *****************/
		
		return $return;
	}
	
	public function load_users_from_items($items, $component){
		
		if(empty($items) || $component == 'none') return;
		
		$ids = array();
		
		foreach($items as $item){
			
			$ids[] = $item->created_by;
		}
		
		if(!empty($ids)){
			
			CJFunctions::load_users($component, $ids);
		}
	}
	
	public function get_user_answers($id, $start, $limit, $params){
		
		$return = array();
		
		$query = '
				select 
					r.description, r.created,
					q.title, q.alias, r.question_id, q.hits,
					c.title as category,
					case when q.created_by > 0 then u.'.$params->get('user_display_name', 'name').' else q.user_name end as username
				from 
					#__answers_responses r
				left join
					#__answers_questions q on q.id = r.question_id
				left join
					#__answers_categories c on q.catid = c.id
				left join
					#__users u 	on q.created_by = u.id 
				where 
					r.created_by = '.$id;
		
		$this->_db->setQuery($query, $start, $limit);
		$return['answers'] = $this->_db->loadObjectList();
		
		/************ pagination *****************/
		$query = 'select count(*) from #__answers_responses r where r.created_by = '.$id;
		
		jimport('joomla.html.pagination');
		$this->_db->setQuery($query);
		$total = $this->_db->loadResult();
		
		$return['pagination'] = new JPagination( $total, $start, $limit );
		/************ pagination *****************/
		
		return $return;
	}
	
	public function get_user_details($id){
		
		$query = '
				select 
					au.id, au.questions, au.answers, au.best_answers, au.accepted,
					u.name, u.username, u.email
				from
					#__answers_users au
				left join
					#__users u on au.id = u.id
				where
					au.id = '.$id;
		
		$this->_db->setQuery($query);
		$user = $this->_db->loadObject();
		
		return $user;
	}
	
	public function get_user_subscriptions($user_id){
		
		$query = '
				select
					s.subscription_type, s.subscription_id,
					q.id as question_id, q.title as question, q.alias as qn_alias, 
					c.id as catid, c.title as category, c.alias as cat_alias
				from
					#__answers_subscribes s
				left join
					#__answers_categories c on s.subscription_id = c.id and (s.subscription_type = 2 or s.subscription_type = 3)
				left join
					#__answers_questions q on s.subscription_id = q.id and s.subscription_type = 1
				where
					s.subscriber_id = '.$user_id.'
				order by
					s.subscription_type desc';
		
		$this->_db->setQuery($query);
		$subscribes = $this->_db->loadObjectList();
		
		return $subscribes;
	}
}