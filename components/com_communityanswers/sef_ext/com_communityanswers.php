<?php
/**
 * @version		$Id: com_communityanswers.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.answers
 * @subpackage	Components.site
 * @copyright	Copyright (C) 2009 - 2013 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// ------------------ standard plugin initialize function - don't change ---------------------------
global $sh_LANG, $sefConfig; 
$shLangName = '';;
$shLangIso = '';
$title = array();
$shItemidString = '';
$dosef = shInitializePlugin( $lang, $shLangName, $shLangIso, $option);
// ------------------ standard plugin initialize function - don't change ---------------------------

$database = JFactory::getDBO();
shRemoveFromGETVarsList('option');
shRemoveFromGETVarsList('lang');

if (!empty($Itemid)) shRemoveFromGETVarsList('Itemid');
if (!empty($limit))  shRemoveFromGETVarsList('limit');
if (isset($limitstart)) shRemoveFromGETVarsList('limitstart');

$shName = shGetComponentPrefix($option);
$shName = empty($shName) ? getMenuTitle($option, (isset($controller)?@$controller:(isset($view) ? @$view : null)), $Itemid ) : $shName;
if (!empty($shName) && $shName != '/') $title[] = $shName;  // V x

if (isset($task)) {
	
	$title[] = $task;
}

if(isset($task) && $task == 'view'){
	
	$query = 'select title, alias from `#__answers_questions` where `id` = '.((int)$id);
	$database->setQuery($query);
	$row = $database->loadObject();

	if ($database->getErrorNum()) {
		
		die( $database->stderr());
	} elseif (!empty($row->alias)) {

		$name = $id . '-' . $row->alias;
		$title[] = $name;
	}

	shRemoveFromGETVarsList('id');
	shMustCreatePageId( 'set', true);
} else if(isset($task) && $task == 'tag'){

	$query = 'select alias from `#__answers_tags` where `id` = '.((int)$id);
	$database->setQuery($query);
	$row = $database->loadObject();
	
	if ($database->getErrorNum()) {
			
		die( $database->stderr());
	} elseif (!empty($row->alias)) {
			
		$name = $id . '-' . $row->alias;
		$title[] = $name;
	}
	shRemoveFromGETVarsList('id');
} else if(isset($id) && $id > 0){

	$query = 'select title, alias from `#__answers_categories` where `id` = '.((int)$id);
	$database->setQuery($query);
	$row = $database->loadObject();
	
	if ($database->getErrorNum()) {
			
		die( $database->stderr());
	} elseif (!empty($row->alias)) {
			
		$name = $id . '-' . $row->alias;
		$title[] = $name;
	}
	
	if(isset($task)){
		
		switch($task){
			
			case 'trending':
			case 'popular':
			case 'resolved':
			case 'open':
			case 'mostanswered':
			case 'tag':
				
				shRemoveFromGETVarsList('id');
				break;
				
			default:
				
				break;
		}
	} else {
		
		shRemoveFromGETVarsList('id');
	}
}

shRemoveFromGETVarsList('task');
shRemoveFromGETVarsList('view');


// ------------------ standard plugin finalize function - don't change ---------------------------
if ($dosef){
	$string = shFinalizePlugin( $string, $title, $shAppendString, $shItemidString,
			(isset($limit) ? @$limit : null), (isset($limitstart) ? @$limitstart : null),
			(isset($shLangName) ? @$shLangName : null));
}
// ------------------ standard plugin finalize function - don't change ---------------------------