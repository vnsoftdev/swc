<?php
/**
 * @package        MijoVideos
 * @copyright      2009-2014 Miwisoft LLC, miwisoft.com
 * @license        GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */

defined('_JEXEC') or die;

class MijovideosViewFavored extends MijovideosView {

	public function display($tpl = null) {
		$item = $this->get('Item');

		if (is_object($item) and !$this->acl->canAccess($item->access)) {
			$this->_mainframe->redirect(JRoute::_('index.php?option=com_mijovideos&view=category'), JText::_('JERROR_ALERTNOAUTHOR'), 'error');
		}

		$Itemid = MijoVideos::get('router')->getItemid(array('view' => 'favored'), null, true);

		$search          = $this->_mainframe->getUserStateFromRequest('com_mijovideos.history.mijovideos_search', 'mijovideos_search', '', 'string');
		$display         = $this->_mainframe->getUserStateFromRequest('com_mijovideos.history.display', 'display', ''.$this->config->get('listing_style').'', 'string');
		$search          = MijoVideos::get('utility')->cleanUrl(JString::strtolower($search));
		$lists['search'] = $search;

		$this->lists      = $lists;
		$this->item       = $item;
		$this->params     = $this->_mainframe->getParams();
		$this->Itemid     = $Itemid;
		$this->display    = $display;
		$this->items      = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		parent::display($tpl);

	}
}