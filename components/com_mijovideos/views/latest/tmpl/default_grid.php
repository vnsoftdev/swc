<?php
/**
 * @package        MijoVideos
 * @copyright      2009-2014 Miwisoft LLC, miwisoft.com
 * @license        GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined('_JEXEC') or die;

if (count($this->items)) {
	$utility = MijoVideos::get('utility');
	foreach ($this->items as $item) {
		$Itemid      = MijoVideos::get('router')->getItemid(array('view' => 'video', 'video_id' => $item->id), null, true);
		$url         = JRoute::_('index.php?option=com_mijovideos&view=video&video_id='.$item->id.$Itemid);
		$Itemid      = MijoVideos::get('router')->getItemid(array('view' => 'channel', 'channel_id' => $item->channel_id), null, true);
		$channel_url = JRoute::_('index.php?option=com_mijovideos&view=channel&channel_id='.$item->channel_id.$Itemid); ?>
		<div class="mijovideos_column<?php echo $this->config->get('items_per_column'); ?>">
			<div class="videos-grid-item">
				<div class="videos-aspect<?php echo $this->config->get('thumb_aspect'); ?>"></div>
				<a href="<?php echo $url; ?>">
					<img class="videos-items-grid-thumb" src="<?php echo $utility->getThumbPath($item->id, 'videos', $item->thumb); ?>" title="<?php echo $item->title; ?>" alt="<?php echo $item->title; ?>"/>
				</a>
			</div>
			<div class="videos-items-grid-box-content">
				<h3 class="mijovideos_box_h3">
					<a href="<?php echo $url; ?>" title="<?php echo $item->title; ?>">
						<?php echo $this->escape(JHtmlString::truncate($item->title, $this->config->get('title_truncation'), false, false)); ?>
					</a>
				</h3>

				<div class="videos-meta">
					<div class="mijovideos-meta-info">
						<div class="videos-view">
							<span class="value"><?php echo number_format($item->hits); ?></span>
							<span class="key"><?php echo JText::_('COM_MIJOVIDEOS_VIEWS'); ?></span>
						</div>
						<div class="date-created">
							<span class="key"><?php echo JText::_('COM_MIJOVIDEOS_DATE_CREATED'); ?></span>
							<span class="value"><?php echo JHtml::_('date', $item->created, JText::_('DATE_FORMAT_LC4')); ?></span>
						</div>
						<div class="created_by">
							<span class="key"><?php echo JText::_('COM_MIJOVIDEOS_CREATED_BY'); ?></span>
                            <span class="value">
                                <a href="<?php echo $channel_url; ?>" title="<?php echo $item->channel_title; ?>">
	                                <?php echo $this->escape(JHtmlString::truncate($item->channel_title, $this->config->get('title_truncation'), false, false)); ?>
                                </a>
                            </span>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
<?php } ?>