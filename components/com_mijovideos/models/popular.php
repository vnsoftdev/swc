<?php

class MijovideosModelPopular extends MijovideosModel {

	public function __construct() {
		parent::__construct('popular', 'videos');

		$this->_getUserStates();
		$this->_buildViewQuery();
	}

	public function _getUserStates() {
		$this->search = parent::_getSecureUserState($this->_option.'.'.$this->_context.'.mijovideos_search', 'mijovideos_search', '', 'string');
		$this->search = JString::strtolower($this->search);
	}

	public function _buildViewQuery() {
		$where = $this->_buildViewWhere();

		$this->_query = 'SELECT * FROM #__mijovideos_videos'
		                .$where
		                .' ORDER BY hits DESC';
	}

	public function _buildViewWhere() {
		$where = array();
		$user  = JFactory::getUser();

		$where[] = 'published=1';
		$where[] = 'access IN ('.implode(',', $user->getAuthorisedViewLevels()).')';

		if ($this->_mainframe->getLanguageFilter()) {
			$where[] = 'language IN ('.$this->_db->Quote(JFactory::getLanguage()->getTag()).','.$this->_db->Quote('*').')';
		}

		if (!empty($this->search)) {
			$src     = parent::secureQuery($this->search, true);
			$where[] = "(LOWER(title) LIKE {$src} OR LOWER(introtext) LIKE {$src})";
		}

		$where[] = 'DATE(created) <= CURDATE()';

		$where = (count($where) ? ' WHERE '.implode(' AND ', $where) : '');

		return $where;
	}

	public function getItems() {
		$items = parent::getItems();

		foreach ($items as $item) {
			$item->channel_title = MijoDB::loadResult('SELECT title FROM #__mijovideos_channels WHERE id='.$item->channel_id);
		}

		return $items;
	}
}
