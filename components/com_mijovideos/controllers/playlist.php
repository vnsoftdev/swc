<?php
/**
 * @package		MijoVideos
 * @copyright	2009-2014 Miwisoft LLC, miwisoft.com
 * @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined( '_JEXEC' ) or die ;

class MijovideosControllerPlaylist extends MijoVideosController {
	
	public function __construct($config = array()) {
		parent::__construct('playlist');
	}

    public function display($cachable = false, $urlparams = false) {
    $layout = JRequest::getCmd('layout');

    $function = 'display'.ucfirst($layout);

    $view = $this->getView(ucfirst($this->_context), 'html');
	$playlist_model = $this->getModel('playlist');
    $view->setModel($playlist_model, true);
    $video_model = $this->getModel('video');
    $view->setModel($video_model);

    if (!empty($layout)) {
        $view->setLayout($layout);
    }

    $view->$function();
}
}