<?php
/**
 * @package		MijoVideos
 * @copyright	2009-2014 Miwisoft LLC, miwisoft.com
 * @license		GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined( '_JEXEC' ) or die ;

class MijovideosControllerChannel extends MijoVideosController {
	
	public function __construct($config = array()) {
		parent::__construct('channel');
	}	

    public function display($cachable = false, $urlparams = false) {
        $layout = JRequest::getCmd('layout');

        $function = 'display'.ucfirst($layout);

        $view = $this->getView(ucfirst($this->_context), 'html');
	    $channel_model = $this->getModel('channel');
	    $view->setModel($channel_model, true);
	    $playlists_model = $this->getModel('playlists');
	    $view->setModel($playlists_model);

        if (!empty($layout)) {
            $view->setLayout($layout);
        }

        $view->$function();
    }
}