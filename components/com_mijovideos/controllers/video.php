<?php
/**
 * @package        MijoVideos
 * @copyright      2009-2014 Miwisoft LLC, miwisoft.com
 * @license        GNU/GPL http://www.gnu.org/copyleft/gpl.html
 */
# No Permission
defined('_JEXEC') or die;

class MijovideosControllerVideo extends MijoVideosController {

	public function __construct($config = array()) {
		parent::__construct('video');

		$id       = MijoVideos::getInput()->getInt('id');
		$video_id = MijoVideos::getInput()->getInt('video_id');

		if (empty($video_id) and !empty($id)) {
			$this->_mainframe->redirect(JRoute::_('index.php?option=com_mijovideos&view=video&video_id='.$id));
		}
	}

	public function display($cachable = false, $urlparams = false) {
		$layout = JRequest::getCmd('layout');

		$function = 'display'.ucfirst($layout);

		$view        = $this->getView(ucfirst($this->_context), 'html');
		$video_model = $this->getModel('video');
		$view->setModel($video_model, true);
		$playlists_model = $this->getModel('playlists');
		$view->setModel($playlists_model);

		if (!empty($layout)) {
			$view->setLayout($layout);
		}

		$view->$function();
	}

	public function submitReport() {
		$post = JRequest::get('post', JREQUEST_ALLOWRAW);
		$user = JFactory::getUser();
		$json = array();
		if ($user->id != 0) {
			if ($this->_model->submitReport($post)) {
				$html[]          = '<div class="mijovideos_report_success">'.JText::_('COM_MIJOVIDEOS_SUCCESS_REPORT').'</div>';
				$html[]          = '<div class="mijovideos_report_text">';
				$html[]          = '     <div style="font-weight: bold">'.JText::_('COM_MIJOVIDEOS_ISSUE_REPORTED').'</div>';
				$html[]          = '     <p id="mijovideos_reasons"></p>';
				$html[]          = '     <div style="font-weight: bold">'.JText::_('COM_MIJOVIDEOS_ADDITIONAL_DETAILS').'</div>';
				$html[]          = '     <p>'.$post['mijovideos_report'].'</p>';
				$html[]          = '</div>';
				$json['success'] = implode("\n", $html);
			}
			else {
				$json['error'] = true;
			}
		}
		else {
			$json['redirect'] = MijoVideos::get('utility')->redirectWithReturn();
		}
		echo json_encode($json);
		exit();

	}
}