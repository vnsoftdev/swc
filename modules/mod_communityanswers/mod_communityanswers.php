<?php
/**
 * @version		$Id: communityanswers.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components.site
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
//don't allow other scripts to grab and execute our file
defined('_JEXEC') or die();

defined('A_APP_NAME') or define('A_APP_NAME', 'com_communityanswers');

// CJLib includes
$cjlib = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_cjlib'.DIRECTORY_SEPARATOR.'framework.php';
if(file_exists($cjlib)){

	require_once $cjlib;
}else{

	die('CJLib (CoreJoomla API Library) component not found. Please download and install it to continue.');
}

CJLib::import('corejoomla.framework.core');

// include the helper file
require_once(dirname(__FILE__).DS.'helper.php');
require_once JPATH_SITE.DS.'components'.DS.'com_communityanswers'.DS.'router.php';

// Get the properties
$showOpen = intval($params->get('open', '1'));
$showResolved = intval($params->get('resolved', '1'));
$showMostAnswered = intval($params->get('most_answered', '1'));
$showMostPopular = intval($params->get('most_popular', '1'));
$showLatestAnswers = intval($params->get('latest_answers', '1'));
$show_tabs = intval($params->get('show_tabs', '1'));
$tab_order = trim($params->get('tab_order', 'O,R,A,P'));
$show_description = intval($params->get('show_description', '1'));
$show_avatar = intval($params->get('show_avatar', '1'));
$avatar_size = intval($params->get('avatar_size', '32'));
$showCustomTitle = intval($params->get('custom_title', '1'));

// Get the item id for community answers
$itemid = CJFunctions::get_active_menu_id(true, 'index.php?option='.A_APP_NAME.'&view=answers');
$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base(true).'/modules/mod_communityanswers/assets/communityanswers.css');

// get the items to display from the helper
$order = explode(',', $tab_order);

echo '<div id="cj-wrapper">';

if($show_tabs){	
	
	CJLib::import('corejoomla.ui.bootstrap', true);
	
	echo  '<ul class="nav nav-tabs" data-tabs="tabs">';
	
    foreach ($order as $i=>$tab){
    	
    	if((strcmp($tab,"O") == 0) && $showOpen){
    		
    		echo '<li'.($i == 0 ? ' class="active"' : '').'><a href="#catab-1" data-toggle="tab">'.JText::_('LBL_OPEN_QUESTIONS').'</a></li>';
    	}
    	
    	if((strcmp($tab,"R") == 0) && $showResolved){
    		
    		echo '<li'.($i == 0 ? ' class="active"' : '').'><a href="#catab-2" data-toggle="tab">'.JText::_('LBL_RESOLVED_QUESTIONS').'</a></li>';
    	}
    	
    	if((strcmp($tab,"A") == 0) && $showMostAnswered){
    		
    		echo '<li'.($i == 0 ? ' class="active"' : '').'><a href="#catab-3" data-toggle="tab">'.JText::_('LBL_MOST_ANSWERED').'</a></li>';
    	}
    	
    	if((strcmp($tab,"P") == 0) && $showMostPopular){
    		
    		echo '<li'.($i == 0 ? ' class="active"' : '').'><a href="#catab-4" data-toggle="tab">'.JText::_('LBL_MOST_POPULAR').'</a></li>';
    	}
        	
    	if((strcmp($tab,"L") == 0) && $showMostPopular){
    		
    		echo '<li'.($i == 0 ? ' class="active"' : '').'><a href="#catab-5" data-toggle="tab">'.JText::_('LBL_LATEST_ANSWERS').'</a></li>';
    	}
    }
    
    echo '</ul>';
    
    echo '<div class="tab-content">';
    
	foreach ($order as $i=>$tab){
		
		if((strcmp($tab,"O") == 0) && $showOpen){
			
			$openQuestions = modCommunityAnswersHelper::getOpenQuestions($params);
			
			echo '<div id="catab-1" class="tab-pane fade'.($i == 0 ? ' in active' : '').'">';
			require(JModuleHelper::getLayoutPath("mod_communityanswers","open_questions"));
			echo '</div>';
		}
		
		if((strcmp($tab,"R") == 0) && $showResolved){
			
			$resolvedQuestions = modCommunityAnswersHelper::getResolvedQuestions($params);
			
			echo '<div id="catab-2" class="tab-pane fade'.($i == 0 ? ' in active' : '').'">';
			require(JModuleHelper::getLayoutPath("mod_communityanswers","resolved_questions"));
			echo '</div>';
		}
		
		if((strcmp($tab,"A") == 0) && $showMostAnswered){
			
			$mostAnsweredQuestions = modCommunityAnswersHelper::getMostAnsweredQuestions($params);
			
			echo '<div id="catab-3" class="tab-pane fade'.($i == 0 ? ' in active' : '').'">';
			require(JModuleHelper::getLayoutPath("mod_communityanswers","most_answered"));
			echo '</div>';
		}
		
		if((strcmp($tab,"P") == 0) && $showMostPopular){
			
			$mostPopularQuestions = modCommunityAnswersHelper::getMostPopularQuestions($params);
			
			echo '<div id="catab-4" class="tab-pane fade'.($i == 0 ? ' in active' : '').'">';
			require(JModuleHelper::getLayoutPath("mod_communityanswers","most_popular"));
			echo '</div>';
		}
			
		if((strcmp($tab,"L") == 0) && $showLatestAnswers){
			
			$latestAnswers = modCommunityAnswersHelper::get_latest_answers($params);
			
			echo '<div id="catab-5" class="tab-pane fade'.($i == 0 ? ' in active' : '').'">';
			require(JModuleHelper::getLayoutPath("mod_communityanswers","latest_answers_bs"));
			echo '</div>';
		}
	}
	echo '</div>';
}else{
	foreach ($order as $tab){
		if((strcmp($tab,"O") == 0) && $showOpen){
			
			$openQuestions = modCommunityAnswersHelper::getOpenQuestions($params);
			
			require(JModuleHelper::getLayoutPath("mod_communityanswers","open_questions"));
		}
		
		if((strcmp($tab,"R") == 0) && $showResolved){
			
			$resolvedQuestions = modCommunityAnswersHelper::getResolvedQuestions($params);
			
			require(JModuleHelper::getLayoutPath("mod_communityanswers","resolved_questions"));
		}
		
		if((strcmp($tab,"A") == 0) && $showMostAnswered){
			
			$mostAnsweredQuestions = modCommunityAnswersHelper::getMostAnsweredQuestions($params);
			
			require(JModuleHelper::getLayoutPath("mod_communityanswers","most_answered"));
		}
		
		if((strcmp($tab,"P") == 0) && $showMostPopular){
			
			$mostPopularQuestions = modCommunityAnswersHelper::getMostPopularQuestions($params);
			
			require(JModuleHelper::getLayoutPath("mod_communityanswers","most_popular"));
		}
		
		if((strcmp($tab,"L") == 0) && $showLatestAnswers){
			
			$latestAnswers = modCommunityAnswersHelper::get_latest_answers($params);
			require(JModuleHelper::getLayoutPath("mod_communityanswers","latest_answers"));
		}
	}
}

echo '</div>';
?>
