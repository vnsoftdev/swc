<?php
/**
 * @version		$Id: helper.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components.site
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
defined('_JEXEC') or die('Direct Access to this location is not allowed.');
class modCommunityAnswersHelper{
	
    public static function getOpenQuestions($params) {
    	
        return modCommunityAnswersHelper::getQuestions(1, $params);
    }

    public static function getResolvedQuestions($params) {
    	
        return modCommunityAnswersHelper::getQuestions(2, $params);
    }

    public static function getMostPopularQuestions($params) {
    	
        return modCommunityAnswersHelper::getQuestions(3, $params);
    }

    public static function getMostAnsweredQuestions($params) {
    	
        return modCommunityAnswersHelper::getQuestions(4, $params);
    }

    private static function getQuestions($action=0, $params) {
    	
        $db = JFactory::getDBO();
        $app = JFactory::getApplication();
        
		$limit = intval($params->get('count', 5));
		$username = trim($params->get('username','username'));
        $categories = $params->get('categories',array());
        
		$where = array();
        $where[] = 'a.published=1';
        $order = '';
        
        if(!empty($categories)){
        	
        	if(is_array($categories)){
        		
        		JArrayHelper::toInteger($categories);
        		$categories = implode(',', $categories);
        	}else{
        		
        		$categories = (int)$categories;
        	}
        	
        	$where[] = 'a.catid in ('.$categories.')';
        }
        
        switch ($action) {
        	
            case 1: // Open
            	
                $where[] = 'a.accepted_answer=0';
                $order = ' order by a.id desc';
                
                break;
                
            case 2: // Resolved
            	
                $where[] = 'a.accepted_answer>0';
                $order = ' order by a.id desc';
                
                break;
                
            case 3: // Most popular
            	
                $order = ' order by a.hits desc';
                
                break;
                
            case 4: // Most popular
            	
                $order = ' order by a.answers desc';
                
                break;
        }
        
        $where = ' where ' . implode(' and ', $where);
        
        $query = '
        		select 
        			a.id, a.title, a.alias, a.description, a.created_by, a.created, a.catid, a.hits,
        			a.featured, a.answers, c.title as category, c.alias as calias, u.'.$username.' as username, 
        			CASE WHEN CHAR_LENGTH(a.alias) THEN CONCAT_WS(\':\', a.id, a.alias) ELSE a.id END as slug 
        		from 
        			#__answers_questions a
        		left join 
        			#__answers_categories c ON a.catid=c.id 
        		left join 
        			#__users u ON a.created_by=u.id ' .
                $where . 
        		$order;
        
        $db->setQuery($query, 0, $limit);
        $rows = $db->loadObjectList();
        
        return $rows;
    }
    
    public static function get_latest_answers($params){

    	$db = JFactory::getDBO();
    	
    	$limit = intval($params->get('count', 5));
    	$username = trim($params->get('username','username'));
    	$categories = $params->get('categories',array());
    	
    	$where = '';
    	$order = 'r.created desc';
    	 
    	$query = '
        		select
    				r.id as answer_id, r.created_by, r.description, r.created,
        			a.id, a.title, a.alias, a.catid, a.hits, a.featured, a.answers, 
    				c.title as category, c.alias as calias, 
    				u.'.$username.' as username, u.email
        		from
        			#__answers_responses r
        		left join
        			#__answers_questions a on r.question_id = a.id
        		left join
        			#__answers_categories c ON a.catid=c.id
        		left join
        			#__users u ON a.created_by=u.id 
        		where
        			r.published = 1 and a.published = 1 and r.parent_id = 0
        		order by
        			r.created desc';
    	
    	$db->setQuery($query, 0, $limit);
    	$rows = $db->loadObjectList();
    	
    	return $rows;
    }
} //end
