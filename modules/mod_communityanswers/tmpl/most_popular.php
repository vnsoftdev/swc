<?php
/**
 * @version		$Id: nost_popular.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components.site
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
if ( $mostPopularQuestions ) {
?>
<?php if($showCustomTitle): ?>
<h3 class="page-header"><?php echo JText::_("TITLE_MOST_POPULAR_QUESTIONS");?></h3>
<?php endif; ?>
<table border="0" cellpadding="5" cellspacing="0" width="100%" class="sectiontable<?php echo $params->get( 'pageclass_sfx' ); ?>">
	<?php if ( $params->get('showheader') ) { ?>
	<thead>
		<tr class="header">
			<?php if ( $params->get('showleaderboard') ) { ?>
			<th class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>" width="25px">
				<div align="center">#</div>
			</th>
			<?php } ?>
	
			<th class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>">
				<div align="left"><?php echo JText::_('LBL_TITLE'); ?></div>
			</th>
	
			<?php if ( $params->get('showcategory') ) { ?>
			<th class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>" width="20%">
				<div align="left"><?php echo JText::_('LBL_CATEGORY'); ?></div>
			</th>
			<?php } ?>
	
			<?php if ( $params->get('showauthor') ) { ?>
			<th class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>" width="20%">
				<div align="left"><?php echo JText::_('LBL_AUTHOR'); ?></div>
			</th>
			<?php } ?>
	
			<?php if ( $params->get('showanswers') ) { ?>
			<th class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>" width="50px">
				<div align="left"><?php echo JText::_('LBL_ANSWERS'); ?></div>
			</th>
			<?php } ?>
			<th class="sectiontableheader<?php echo $params->get( 'pageclass_sfx' ); ?>" width="50px">
				<div align="center"><?php echo JText::_('LBL_HITS'); ?></div>
			</th>
		</tr>
	</thead>
	<?php } ?>
	<tbody>
		<?php
		$i = 1;
		$k = 0;
		foreach ($mostPopularQuestions as $item) {
		$href = JRoute::_( 'index.php?option=com_communityanswers&view=answers&task=view&id='. $item->slug . $itemid );
		?>
		<tr class="<?php echo $k?'noalt':'alt'; ?>">
			<?php if ( $params->get('showleaderboard') ) { ?>
			<td class="sectiontableentry<?php echo $k; ?>">
				<div align="center"><?php echo $i; ?></div>
			</td>
			<?php } ?>
			
			<td align="left"><a href="<?php echo $href; ?>"> <?php echo CJFunctions::escape($item->title); ?></a></td>
			
			<?php if ( $params->get('showcategory') ) { ?>
			<td align="left"><?php echo CJFunctions::escape($item->category); ?></td>
			<?php } ?>
			
			<?php if ( $params->get('showauthor') ) { ?>
			<td align="left"><?php echo ($item->username) ? CJFunctions::escape($item->username) : JText::_('GUEST'); ?></td>
			<?php } ?>
			
			<?php if ( $params->get('showanswers') ) { ?>
			<td align="center"><?php echo $item->answers; ?></td>
			<?php } ?>
			
			<td><?php echo $item->hits;?></td>
		</tr>
		<?php
		$i++;
		$k = 1 - $k;
		}
		?>
	</tbody>
</table>
<?php
}
?>