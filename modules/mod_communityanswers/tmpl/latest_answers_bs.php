<?php
/**
 * @version		$Id: most_answered.php 01 2011-01-11 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Components.site
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
// no direct access
defined('_JEXEC') or die('Restricted access');
if ( $latestAnswers ) {

$config = JComponentHelper::getParams(A_APP_NAME);
$user_avatar = $config->get('user_avatar', 'none');
?>

<?php if($showCustomTitle): ?>
<h3 class="page-header"><?php echo JText::_("TITLE_LATEST_ANSWERS");?></h3>
<?php endif; ?>

<?php foreach ($latestAnswers as $item):?>
<div class="media">
	<?php if($show_avatar == 1 && $user_avatar != 'none'):?>
	<?php echo CJFunctions::get_user_avatar($user_avatar, $item->created_by, $config->get('user_display_name', 'name'), $avatar_size, $item->email, array('class'=>'thumbnail pull-left', 'data-toggle'=>'tooltip'), array('class'=>'media-object'));?>
	<?php endif;?>
	
	<div class="media-body">
		<h4 class="media-heading">
			<a href="<?php echo JRoute::_( 'index.php?option=com_communityanswers&view=answers&task=view&id='. $item->id.':'.$item->alias . $itemid );?>">
				<?php echo CJFunctions::escape($item->title)?>
			</a>
		</h4>
		<?php if($show_description == 1):?>
		<?php echo CJFunctions::substrws(CJFunctions::process_html($item->description, ($config->get('default_editor') == 'bbcode'), ($config->get('process_content_plugins', false) == '1')), 256);?>
		<?php endif;?>
	</div>
</div>
<?php endforeach;?>

<?php
}
?>