<?php
/**
 * Joomla! 1.5 lacategories
 *
 * @version $Id: la_categories.php 2009-06-29 01:47:01 svn $
 * @author Maverick
 * @package modules
 * @subpackage mod_latest_articles.elements
 * @license GNU/GPL
 *
 * This module displays the latest articles published on the site.
 */
defined('_JEXEC') or die( 'Restricted access' );

jimport('joomla.html.html');
jimport('joomla.form.formfield');

class JFormFieldCacategories extends JFormField{

	protected $type = 'cacategories';

	protected function getInput(){

		// CJLib includes
		$cjlib = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_cjlib'.DIRECTORY_SEPARATOR.'framework.php';
		if(file_exists($cjlib)){
		
			require_once $cjlib;
		}else{
		
			die('CJLib (CoreJoomla API Library) component not found. Please download and install it to continue.');
		}
		CJLib::import('corejoomla.framework.core');
		CJLib::import('corejoomla.nestedtree.core');
		
		$target = array();
		$db = JFactory::getDBO();
		$tree = new CjNestedTree($db, '#__answers_categories');
		$target = $tree->get_selectables();

		// Construct an array of the HTML OPTION statements.
		$options = array ();

		foreach ($target as $id=>$option){
			
			$options[] = JHTML::_('select.option', $id, JText::_($option));
		}
		// Construct the various argument calls that are supported.
		$attribs       = ' ';
		if ($v = $this->element['size']) {
			$attribs .= ' size="'.$v.'"';
		}

		if ($v = $this->element['class']) {
			$attribs .= ' class="'.$v.'"';
		} else {
			$attribs .= ' class="inputbox"';
		}

		if ($m = $this->element['multiple']){
			$attribs .= ' multiple="multiple"';
		}

		// Render the HTML SELECT list.
		return JHTML::_('select.genericlist', $options, $this->name, $attribs, 'value', 'text', $this->value );
	}
}
?>