<?php
/**
 * @version		$Id: mod_castats.php 01 2011-11-11 11:37:09Z maverick $
 * @package		CoreJoomla.answers
 * @subpackage	Modules.castats
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
//don't allow other scripts to grab and execute our file
defined('_JEXEC') or die();

$db = JFactory::getDbo();

$num_questions = intval($params->get('num_questions', 0));
$num_answers = intval($params->get('num_answers', 0));
$num_solved = intval($params->get('num_solved', 0));
$num_categories = intval($params->get('num_categories', 0));

if($num_questions || $num_answers || $num_solved){
	
	$query = 'select sum(questions) as num_questions, sum(answers) as num_answers, sum(accepted) as num_solved from #__answers_users';
	$db->setQuery($query);
	$result = $db->loadObject();
	
	if($result){
		
		echo '<div class="num_questions">'.JText::sprintf('MOD_CASTATS_NUM_QUESTIONS', $result->num_questions).'</div>';
		echo '<div class="num_answers">'.JText::sprintf('MOD_CASTATS_NUM_ANSWERS', $result->num_answers).'</div>';
		echo '<div class="num_solved">'.JText::sprintf('MOD_CASTATS_NUM_SOLVED', $result->num_solved).'</div>';
	}
}

if($num_categories){
	
	$query = 'select count(*) from #__answers_categories where parent_id > 0';
	$db->setQuery($query);
	$result = $db->loadResult();
	
	echo '<div class="num_categories">'.JText::sprintf('MOD_CASTATS_NUM_CATEGORIES', $result).'</div>';
}