<?php
/**
 * @version		$Id: default.php 01 2012-04-21 11:37:09Z maverick $
 * @package		CoreJoomla.Polls
 * @subpackage	Modules.site
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

//don't allow other scripts to grab and execute our file
defined('_JEXEC') or die('Direct Access to this location is not allowed.');
?>
<div class="ca_top_users" id="cj-wrapper">
	<?php 
	if(count($users) > 0){
		
		foreach ($users as $user){
			
			echo '<div class="ca-user">';
			
			if($avatar == 1){
				
				echo '<div class="ca-user-avatar">'.CJFunctions::get_user_avatar($params->get('user_avatar', 'none'), $user->id, $username, 36).'</div>';
			}
			
			echo '<div class="ca-user-name">'.(($user->id > 0) ? CJFunctions::escape($user->$username) : JText::_('LBL_GUEST')).'</div>';
			echo '<div class="ca-user-stats">'.JText::sprintf($text, $user->$field).'</div>';
			
			if($avatar == 1){
				
				echo '<div class="clear"></div>';
			}
			
			echo '</div>';
		}
	}
	?>
</div>