<?php
/**
 * @version		$Id: mod_gpstracks.php 01 2011-11-11 11:37:09Z maverick $
 * @package		CoreJoomla.gpstools
 * @subpackage	Modules.gpxtracks
 * @copyright	Copyright (C) 2009 - 2011 corejoomla.com. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */
//don't allow other scripts to grab and execute our file
defined('_JEXEC') or die();

defined('A_APP_NAME') or define('A_APP_NAME', 'com_communityanswers');

// CJLib includes
$cjlib = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_cjlib'.DIRECTORY_SEPARATOR.'framework.php';
if(file_exists($cjlib)){

	require_once $cjlib;
}else{

	die('CJLib (CoreJoomla API Library) component not found. Please download and install it to continue.');
}
CJLib::import('corejoomla.framework.core');

require_once(JPATH_SITE.DS.'components'.DS.A_APP_NAME.DS.'router.php');

$count = intval($params->get('count', 5));
$avatar = intval($params->get('avatar', 0));
$listtype = intval($params->get('listtype', 1));
$username = trim($params->get('username', 'username'));

$order = '';
$field = '';
$text = '';

switch ($listtype){
	
	case 1: // top askers
		
		$field = 'questions';
		$text = 'LBL_QUESTIONS';
		$order = ' order by a.'.$field.' desc';
		
		break;
		
	case 2: // top answerers
		
		$field = 'answers';
		$text = 'LBL_ANSWERS';
		$order = ' order by a.'.$field.' desc';
		
		break;
		
	case 3: // best answerers
		
		$field = 'best_answers';
		$text = 'LBL_BEST_ANSWERS';
		$order = ' order by a.'.$field.' desc';
		
		break;
}

$db = JFactory::getDbo();

$query = 'select a.id, a.'.$field.', u.name, u.username from #__answers_users a left join #__users u on a.id = u.id'.$order;
$db->setQuery($query, 0, $count);
$users = $db->loadObjectList();

$params = JComponentHelper::getParams(A_APP_NAME);

require JModuleHelper::getLayoutPath( 'mod_catopusers', $params->get('layout', 'default') );
?>
