<?php
/**
 * @version		$Id: mod_cacategories.php 01 2012-05-19 11:37:09Z maverick $
 * @package		CoreJoomla.Answers
 * @subpackage	Modules.site
 * @copyright	Copyright (C) 2009 - 2012 corejoomla.com, Inc. All rights reserved.
 * @author		Maverick
 * @link		http://www.corejoomla.com/
 * @license		License GNU General Public License version 2 or later
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
defined('A_APP_NAME') or define('A_APP_NAME', 'com_communityanswers');

// CJLib includes
$cjlib = JPATH_ROOT.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.'com_cjlib'.DIRECTORY_SEPARATOR.'framework.php';
if(file_exists($cjlib)){

	require_once $cjlib;
}else{

	die('CJLib (CoreJoomla API Library) component not found. Please download and install it to continue.');
}
CJLib::import('corejoomla.framework.core');
CJLib::import('corejoomla.nestedtree.core');

$db = JFactory::getDbo();
$app = JFactory::getApplication();
$document = JFactory::getDocument();

CJFunctions::load_jquery(array('libs'=>array('treeview')));

$tree = new CjNestedTree($db, '#__answers_categories');
$tree->reload_tree();
$nodes = $tree->get_tree();

$catid = $app->input->getInt('cacatid');

$appname = $app->input->getCmd('option', '');
$itemid = CJFunctions::get_active_menu_id(true, 'index.php?option='.A_APP_NAME.'&view=answers');
$display_counts = $params->get('category_counts', 1);
$fields = array('url'=>'index.php?option='.A_APP_NAME.'&view=answers&task=open', 'itemid'=>$itemid);

if($display_counts){
	
	$fields['title'] = 'MSG_CATEGORY_TOOLTIP';
	$fields['stat_field'] = 'questions';
	$fields['stat_field2'] = 'answers';
}

$script = '';

if($catid > 0 && ($appname == A_APP_NAME)){
	
	$script = '
		$(".ca_categories").find("li[rel=\''.$catid.'\']").find(".expandable-hitarea:first").click();
		$(".ca_categories").find("li[rel=\''.$catid.'\']").parents("li.expandable").find(".expandable-hitarea:first").click();
		$(".ca_categories").find("li[rel=\''.$catid.'\']").find("a:first").css("font-weight", "bold");';
}

$document->addScriptDeclaration('
		jQuery(document).ready(function($){
			$(".ca_categories").find(".cat-list:first").treeview({collapsed: true});'.$script.' 
		});');

$document->addStyleDeclaration('#cj-wrapper .cat-list {margin: 0;}');

echo '<div id="cj-wrapper" class="ca_categories">'.$tree->get_tree_list($nodes, $fields).'</div>';
?>